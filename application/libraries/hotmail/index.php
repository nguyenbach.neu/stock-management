<?php
class Person {
		  public $first_name, $last_name, $email_address;
		
		  public function __construct($first, $last, $em) {
			$this->first_name = $first;
			$this->last_name = $last;
			$this->email_address = $em;
		  }
		
		  public function __toString() {
			return "$this->first_name 		$this->$last_name   	:	$this->$email_address <BR>";
		  }
	}
/** 
 * This is the main page of the first Delegated Authentication sample.  
 * It displays the link for granting consent as well as a user 
 * greeting.
 */

// Load common settings. For more information, see Settings.php.
include 'settings.php';

include 'windowslivelogin.php';

// Initialize the WindowsLiveLogin module.
$wll = WindowsLiveLogin::initFromXml($KEYFILE);
$wll->setDebug($DEBUG);
//Get the consent URL for the specified offers.
$consenturl = $wll->getConsentUrl($OFFERS);

// If the raw consent token has been cached in a site cookie, attempt to
// process it and extract the consent token.
$message_html = "<p>Please <a href=\"$consenturl\">click here</a> to grant consent 
                 for this application to access your Windows Live data.</p>";

$token = null;
$cookie = @$_COOKIE[$COOKIE];
if ($cookie) {
    $token = $wll->processConsentToken($cookie);
}

if ($token && !$token->isValid()) {
    $token = null;
}

if ($token) {
   // Convert Unix epoch time stamp to user-friendly format.
				$expiry = $token->getExpiry();
				$expiry = date(DATE_RFC2822, $expiry);
		
		
		//*******************CONVERT HEX TO DOUBLE LONG INT ***************************************
				$hexIn = $token->getLocationID();
				include "hex.php";		
				$longint=$output;		//here's the magic long integer to be sent to the Windows Live service
				
		//*******************CURL THE REQUEST ***************************************
				$uri = "https://livecontacts.services.live.com/users/@C@".$output."/LiveContacts";
				$session = curl_init($uri);
				
				curl_setopt($session, CURLOPT_HEADER, true);
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				curl_setopt ($session, CURLOPT_USERAGENT, "Windows Live Data Interactive SDK");
				curl_setopt($session, CURLOPT_HTTPHEADER, array("Authorization: DelegatedToken dt=\"".$token->getDelegationToken()."\""));
				curl_setopt($session, CURLOPT_VERBOSE, 1);
				curl_setopt ($session, CURLOPT_HTTPPROXYTUNNEL, TRUE);
				curl_setopt ($session, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
				curl_setopt ($session, CURLOPT_PROXY,$PROXY_SVR);
				curl_setopt ($session, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt ($session, CURLOPT_TIMEOUT, 120);
				$response_h = curl_exec($session);
				curl_close($session);	
				
		//*******************PARSING THE RESPONSE ****************************************************
				$response=strstr($response_h,"<?xml version");
		
				try {
				$xml = new SimpleXMLElement($response);
				}
				catch (Exception $e) {
					echo $response_h."<br>".$uri;
					die;
				}		
				$lengthArray=sizeof($xml->Contacts->Contact);
				for ($i=0;$i<$lengthArray;$i++)
				{
					//There can be more fields, depending on how you configure.  Here's
					//where you should access the fields and send them to the constructor
					
					$fn = $xml->Contacts->Contact[$i]->Profiles->Personal->FirstName;			
					$ln = $xml->Contacts->Contact[$i]->Profiles->Personal->LastName;				
					$em = $xml->Contacts->Contact[$i]->Emails->Email->Address;
					
					//instantiate an object and add it to the array
					$person_array[]=new Person($fn,$ln,$em);
				}
				
				print_r($person_array);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <title>Windows Live ID&trade; Web Delegated Authentication Sample</title>
    <?php echo $BODYSTYLE; ?>
  </head>
  <body><table width="320"><tr><td>
    <h1>Welcome to the PHP Sample for the Windows Live&trade; ID Delegated
    Authentication SDK</h1>

    <?php echo $message_html; ?>
  </td></tr></table></body>
</html>
