<?php
/**
 * The AuthorizeNet PHP SDK. Include this file in your project.
 *
 * @package AuthorizeNet
 */
require dirname(__FILE__) . '/authorizenet/shared/AuthorizeNetRequest.php';
require dirname(__FILE__) . '/authorizenet/shared/AuthorizeNetTypes.php';
require dirname(__FILE__) . '/authorizenet/shared/AuthorizeNetXMLResponse.php';
require dirname(__FILE__) . '/authorizenet/shared/AuthorizeNetResponse.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetAIM.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetARB.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetCIM.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetSIM.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetDPM.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetTD.php';
require dirname(__FILE__) . '/authorizenet/AuthorizeNetCP.php';

if (class_exists("SoapClient")) {
    require dirname(__FILE__) . '/authorizenet/AuthorizeNetSOAP.php';
}
/**
 * Exception class for AuthorizeNet PHP SDK.
 *
 * @package AuthorizeNet
 */
class AuthorizeNetException extends Exception
{
}