<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Parameter {
	
	var $CI;
	
	/**
	 * Template Constructor
	 *
	 * @access	public
	 */
	function Parameter()
	{
        // Get CodeIgniter instance
		$this->CI =& get_instance();
		
		$this->UriToParams();
    }
	
	// ------------------------------------------------------------------------
	
	/**
	 * Set Default Variables
	 *
	 * @access	public
	 */
	function UriToParams()
	{	
		$segments = $this->CI->uri->segment_array();
		foreach ($segments as $segment) {
			$param = explode(':', $segment);
			if (count($param) == 2) {
				$this->CI->params[$param['0']] = $param['1']; 
			}
		}
	}
	function getParam($name) {
		if (isset($this->CI->params[$name])) {
			return $this->CI->params[$name];
		} else {
			return false;
		}
	}

}

/* End of file Template.php */
/* Location: ./application/libraries/Template.php */