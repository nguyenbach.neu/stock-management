<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

//product_markup_type
define("FIXED",1);
define("PERCENTAGE",2);
//img upload
define('IMG_UPLOAD','media/upload/');

define('UPLOAD_IMAGE', 0);
define('UPLOAD_DOC', 1);

define('MEDIA','media');
define('MEDIA_IMAGE','media/images/');
define('MEDIA_UPLOAD','media/uploads/');
define('MEDIA_PHOTO','media/uploads/photos/');
define('MEDIA_TEMP', 'media/temps/');
define('SCRIPT', 'scripts/');

/**follow system
*with cash: IN for supplier, OUT for saler
*with stockL IN for receipt, OUT for issue
**/
define("IN",1);
define("OUT",-1);
//default tax
define("TAX_PERCENTAGE","10");
//user role 
define("ROLE_ADMIN",1);
define("ROLE_ACCOUNTING",2);
define("ROLE_SUPERVISOR",3);
define("ROLE_SALEPERSON",4);
define("ROLE_STOCKKEEPER",5);

//user disable
define("USER_ENABLE",0);
define("USER_DISABLE",1);
//stock_change_reasons
DEFINE("INITIAL_STOCK",0);     
DEFINE("STOCK_CHANGE_REASON_ISSUE", 1);     
DEFINE("STOCK_CHANGE_REASON_RECEIPT", 2);     
DEFINE("STOCK_CHANGE_REASON_ISSUE_CHANGE", 3);
DEFINE("STOCK_CHANGE_REASON_RECEIPT_CHANGE", 4);
DEFINE("STOCK_CHANGE_REASON_ADJUSTMENT", 5);
//stock_issue_status
define("STOCK_CHANGE_STATUS_DRAFT",0);
define("STOCK_CHANGE_STATUS_PROCESSED",1);
//stock_issue_history_message
define("STOCK_ISSUE_HISTORY_TYPE_CHANGE_STOCK",1);
define("STOCK_ISSUE_HISTORY_TYPE_CHANGE_PRODUCT",2);
define("STOCK_ISSUE_HISTORY_TYPE_ADD_PRODUCT",3);
define("STOCK_ISSUE_HISTORY_TYPE_DELETE_PRODUCT",4);

//stock_receipt_type
define("STOCK_RECEIPT_TYPE_PURCHASE",2);
define("STOCK_RECEIPT_TYPE_RETURN",1); 
//number decimal
define("NUMBER_DECIMAL",2);
define("NUMBER_INTEGER",0);



/* End of file constants.php */
/* Location: ./application/config/constants.php */