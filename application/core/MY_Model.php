<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class MY_Model extends CI_Model {
	var $CI = false;

	function setUserData($newdata = array(), $newval = '')
	{
		if (!$this->CI) {
			$this->CI =& get_instance();
		}
		if (is_string($newdata))
		{
			$newdata = array($newdata => $newval);
		}

		if (count($newdata) > 0)
		{
			foreach ($newdata as $key => $val)
			{
				$this->CI->data[$key] = $val;
			}
		}

	}
	function getUserData($item)
	{
		if (!$this->CI) {
			$this->CI =& get_instance();
		}	
		return ( ! isset($this->CI->data[$item])) ? FALSE : $this->CI->data[$item];
	}		
}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */