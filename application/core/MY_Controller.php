<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    var $data = array();

    function try_archive($stock_issue_id) {
        $delivery_expenses = $this->cashs_model->sumDeliveryExpensesByStockIssue($stock_issue_id);
        $receipt = $this->cashs_model->sumReceiptsByStockIssue($stock_issue_id);
        $discount = $this->cashs_model->sumDiscountsByStockIssue($stock_issue_id);
        $return = $this->stock_bill_model->sumStockReturnReceiptsByStockIssue($stock_issue_id);
		$total = $this->stock_bill_model->getTotal($stock_issue_id);
		if ($total['total'] == ($delivery_expenses['sum'] + $receipt['sum'] + $discount['sum'] + $return['sum']) ) {
			$arr['stock_bill_archive'] = 1;
		} else {
			$arr['stock_bill_archive'] = 0;
		}
		
		$this->db->where('stock_bill_id', $stock_issue_id);
        $this->db->update('stock_bills', $arr);
    }

	function setUserData($newdata = array(), $newval = '')
	{
		if (is_string($newdata))
		{
			$newdata = array($newdata => $newval);
		}

		if (count($newdata) > 0)
		{
			foreach ($newdata as $key => $val)
			{
				$this->data[$key] = $val;
			}
		}
	}
	function getUserData($item)
	{
		return ( ! isset($this->data[$item])) ? FALSE : $this->data[$item];
	}	
	
    function __construct()
    {
        parent::__construct();
		$this->output->enable_profiler(TRUE);
		$lastActivity = $this->session->userdata('last_activity'); 
		$timeOut = time() - 300; // now minus the the session 
										   // timeout plus 5 minutes
		// echo $timeOut;
		// echo "<br />";
		// echo $lastActivity;
		if ($lastActivity <= $timeOut)
		{
			$data = $this->session->userdata;
			$this->session->unset_userdata($data);
		}
		if($this->router->fetch_method() != "login"){
			if($this->session->userdata('user_id') == null){
				redirect("admin/users/login");
			}
		}
		
    }

	function _getlayoutbackend($container, $data = "", $template = 'backend'){
		$this->template->set_partial('header', 'blocks/backend/header');
		$this->template->set_partial('footer', 'blocks/backend/footer');
		$this->template->set_partial('profile', 'blocks/backend/profile');
		$this->template->set_partial('change_password', 'blocks/backend/change_pass');
		$this->template->set_layout($template)->build($container, $data);
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
