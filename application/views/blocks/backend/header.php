,
<div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">HM Accounting</a>
        </div>
		<div  id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<?php if(has_permission($this->session->userdata('user_role'),"dashboard","index")):?>
				<li<?php echo (($this->router->fetch_class() == 'dashboard' && $this->router->fetch_method() == 'index') ? ' class="active"' : '');?>>
					<a href="<?php echo site_url('admin/dashboard/')?>"><?php echo __('Dashboard'); ?></a>
				</li>
				<?php endif; ?>
				<?php if(has_permission($this->session->userdata('user_role'),"orders","index")):?>
					<li class="dropdown<?php echo (($this->router->fetch_class() == 'orders' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add')) ? ' active' : '');?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Orders'); ?><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo site_url('admin/orders/')?>"><?php echo __('Orders'); ?></a>
							</li>

							<li>
								<a href="<?php echo site_url('admin/orders/add')?>"><?php echo __('Add new'); ?></a>
							</li>
						</ul>
					</li>
				<?php endif; ?>

				<?php if(has_permission($this->session->userdata('user_role'),"stockissues","index")):?>
				<li class="dropdown<?php echo (($this->router->fetch_class() == 'stockissues' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add')) ? ' active' : '');?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Stock issue'); ?><b class="caret"></b></a>
					<ul class="dropdown-menu">

						<li>
							<a href="<?php echo site_url('admin/stockissues/')?>"><?php echo __('Stock issue'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/stockissues/add')?>"><?php echo __('Add new'); ?></a>
						</li>
					</ul>
				</li>
				<?php endif; ?>
				<?php if(has_permission($this->session->userdata('user_role'),"stockreceipts","index")):?>
				<li class="dropdown<?php echo (($this->router->fetch_class() == 'stockreceipts' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add')) ? ' active' : '');?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Stock receipt'); ?><b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo site_url('admin/stockreceipts/')?>"><?php echo __('Stock receipt'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/stockreceipts/add/salers')?>"><?php echo __('Add new for sale personals'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/stockreceipts/add/suppliers')?>"><?php echo __('Add new for suppliers'); ?></a>
						</li>
					</ul>
				</li>
				<?php endif; ?>
				<?php if( has_permission($this->session->userdata('user_role'),"cashs","receipts") || has_permission($this->session->userdata('user_role'),"cashs","paids") ):?>
				<li class="dropdown<?php echo (($this->router->fetch_class() == 'cashs') ? ' active' : '');?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Cash'); ?><b class="caret"></b></a>
					<ul class="dropdown-menu">
                        <li>
                            <b><?php echo __('Phiếu thu tiền giao hàng'); ?></b>
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/cashs/receipts')?>"><?php echo __('Phieu thu tien giao hang'); ?></a>
                        </li>
                        <li>
							<b><?php echo __('Công nợ'); ?></b>
						</li>
						<li>
							<a href="<?php echo site_url('admin/cashs/debit_advance')?>"><?php echo __('Cong no khach hang'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/cashs/debit_receipt')?>"><?php echo __('Phieu thu cong no ban hang'); ?></a>
						</li>
                        <li>
                            <b><?php echo __('Đao giá'); ?></b>
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/cashs/discount_advance')?>"><?php echo __('Tam ung giam gia ban hang'); ?></a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/cashs/discount_receipt')?>"><?php echo __('Payment receipt for discount'); ?></a>
                        </li>
						<li>
							<b><?php echo __('Suppliers'); ?></b>
						</li>						
                        <li>
                            <a href="<?php echo site_url('admin/cashs/commission_receipt')?>"><?php echo __('Nhan tien chiet khau dai ly'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/cashs/commission_discount')?>"><?php echo __('Nhan tien khuyen mai'); ?></a>
                        </li>
						<li>
							<b><?php echo __('Accounts'); ?></b>
						</li>							
						<li>
							<a href="<?php echo site_url('admin/cashs/transfer')?>"><?php echo __('Chuyen tien'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/cashs/withdraw')?>"><?php echo __('Withdraw'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/cashs/load')?>"><?php echo __('Load into bank account'); ?></a>
						</li>
						<li>
							<b><?php echo __('Expenses'); ?></b>
						</li>
                        <li>
                            <a href="<?php echo site_url('admin/cashs/delivery_expenses')?>"><?php echo __('Delivery expenses'); ?></a>
                        </li>
						<li>
							<a href="<?php echo site_url('admin/cashs/salary')?>"><?php echo __('Salaries'); ?></a>
						</li>
						<li>
							<a href="<?php echo site_url('admin/cashs/expense')?>"><?php echo __('Expenses'); ?></a>
						</li>
						
					</ul>
				</li>
		
				<?php endif; ?>
				<li class="dropdown<?php echo (( ($this->router->fetch_class() == 'stockissues' && $this->router->fetch_method() == 'reports') || ($this->router->fetch_class() == 'stockreceipts' && $this->router->fetch_method() == 'reports') || ($this->router->fetch_class() == 'users' && $this->router->fetch_method() == 'debts') || ($this->router->fetch_class() == 'dashboard' && $this->router->fetch_method() == 'stockreport') || ($this->router->fetch_class() == 'suppliers' && $this->router->fetch_method() == 'salePerStaff') || ($this->router->fetch_class() == 'suppliers' && $this->router->fetch_method() == 'salePerMonth')  ) ? ' active' : '');?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Report'); ?><b class="caret"></b></a>
					<ul class="dropdown-menu">
						<?php if(has_permission($this->session->userdata('user_role'),"stockissues","reports")):?>
                        <li>
                            <a href="<?php echo site_url('admin/reports/accounts')?>"><?php echo __('Qui tien mat'); ?></a>
                        </li>
						<li>
							<a href="<?php echo site_url('admin/reports/cash_overview')?>"><?php echo __('Tai san'); ?></a>
						</li>

                        <?php if(has_permission($this->session->userdata('user_role'),"cashs","debts")):?>
                        <li>
                            <a href="<?php echo site_url('admin/reports/debts')?>"><?php echo __('Report debt'); ?></a>
                        </li>
                        <?php endif; ?>

						<?php if(has_permission($this->session->userdata('user_role'),"cashs","discounts")):?>
							<li>
								<a href="<?php echo site_url('admin/reports/discounts')?>"><?php echo __('Report discount'); ?></a>
							</li>
						<?php endif; ?>


						<li>
							<a href="<?php echo site_url('admin/reports/stock_issues')?>"><?php echo __('Stock issues report'); ?></a>
						</li>


						<?php endif; ?>
						<?php if(has_permission($this->session->userdata('user_role'),"stockreceipts","reports")):?>
						<li>
							<a href="<?php echo site_url('admin/reports/stock_receipts')?>"><?php echo __('Stock receipt report'); ?></a>
						</li>
						<?php endif; ?>
                        <li>
                            <a href="<?php echo site_url('admin/reports/commission_discount')?>"><?php echo __('Commission discount report'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/reports/salePerStaff')?>"><?php echo __('Sales per staff'); ?></a>
                        </li>
                        <!--<li>
							<a href="<?php echo site_url('admin/suppliers/salePerMonth')?>"><?php echo __('Sales per month'); ?></a>
						</li>-->
						<li>
							<a href="<?php echo site_url('admin/reports/inventory')?>"><?php echo __('Stock report'); ?></a>
						</li>

						<li>
							<a href="<?php echo site_url('admin/reports/profit')?>"><?php echo __('Loi nhuan tam tinh'); ?></a>
						</li>
                        <li>
                            <a href="<?php echo site_url('admin/reports/audit')?>"><?php echo __('Kiem soat so lieu'); ?></a>
                        </li>
					</ul>
				</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Khoi tao'); ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php if(has_permission($this->session->userdata('user_role'),"products","index")):?>
                            <li <?php echo (($this->router->fetch_class() == 'products') ? ' class="active"' : '');?>>
                                <a href="<?php echo site_url('admin/products/')?>"><?php echo __('Products'); ?></a>
                            </li>
                            <li <?= ($this->router->fetch_class() == 'products_groups') ? 'class="active"' : '' ?>>
                                <a href="<?= site_url('admin/product_groups/') ?>"> <?= __('Product Groups') ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if(has_permission($this->session->userdata('user_role'),"suppliers","index")):?>
                            <li <?php echo (($this->router->fetch_class() == 'suppliers' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add' || $this->router->fetch_method() == 'edit')) ? ' class="active"' : '');?>>
                                <a href="<?php echo site_url('admin/suppliers/')?>"><?php echo __('Suppliers'); ?></a>
                            </li>
                            <li <?php echo (($this->router->fetch_class() == 'accounts' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add' || $this->router->fetch_method() == 'edit')) ? ' class="active"' : '');?>>
                                <a href="<?php echo site_url('admin/accounts/')?>"><?php echo __('Bank accounts'); ?></a>
                            </li>
                        <?php endif; ?>
                    <?php if(has_permission($this->session->userdata('user_role'),"users","index")):?>
                        <li <?php echo (($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'add' || $this->router->fetch_method() == 'edit')) ? ' class="active"' : '');?>>
                            <a href="<?php echo site_url('admin/users/')?>"><?php echo __('Users'); ?></a>
                        </li>
                    <?php endif; ?>
                    </ul>
                </li>
                <?php if(has_permission($this->session->userdata('user_role'),"dailyreport","index")):?>
                    <li>
                        <a href="<?php echo site_url('admin/dailyreport/')?>"><?php echo __('Daily Report'); ?></a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ucfirst($this->session->userdata('user_name')) ;?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo site_url('admin/users/logout')?>"><?php echo __('Logout'); ?></a>
                            <a href="<?php echo site_url('admin/users/password')?>"> <?php echo __('Edit password');?></a>
                        </li>
                    </ul>
                </li>

			</ul>
		</div>
</div>