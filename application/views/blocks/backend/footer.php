  <!-- Footer -->
  <script type="text/javascript">
      $(function () {
          $('.single_date').datetimepicker({format:'YYYY-MM-DD'});
          $('.start_date').datetimepicker({format:'YYYY-MM-DD'});
          $('.end_date').datetimepicker({format:'YYYY-MM-DD'});
          $(".start_date").on("dp.change", function (e) {
              $('.end_date').data("DateTimePicker").minDate(e.date);
          });
          $(".end_date").on("dp.change", function (e) {
              $('.start_date').data("DateTimePicker").maxDate(e.date);
          });
      });
      $("#btn_cancel").click(function(){
          window.history.back();
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function(){
          jQuery('.formatnumber').on('keyup', function(){
              price = jQuery(this).val();
              this.value = numberFormat(this.value);
          });
          $(".form-validation").validate({
              errorElement: "div"
          });
		  
		  $('form').submit(function(){ //prevent multiple submit
            $(':submit', this).click(function() {
                //console.log("submit prevented"); // Debug purpose.
                return false;
            });
		});

      });
	  
  </script>
	<div style="background:#000;height:50px;line-height:50px;color:#FFF;">
			Stock Management 1.0
	</div>