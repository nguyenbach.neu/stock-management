<div id="change-password-container" class="modal hide fade form-container  " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form id="popup-change-password-form" class="popup-form"  method="post" action="<?php echo site_url('users/register'); ?>">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
			<h3 id="myModalLabel">Change Password</h3>
		</div>
		<div class="modal-body">
			<div>
				<label for="password" class="label">Password</label>
				<input name="password" type="password" id="password" value="" />
			</div>
			<div>
				<label for="confirm_password" class="label">Confirm Password</label>
				<input name="confirm_password" type="password" id="confirm_password" value="" />
			</div>	
		</div>
		<div class="modal-footer">
			<button class="btn custom-close" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<input type="submit" class="btn btn-primary" value="Update" />
		</div>
	</form>
</div>
<script type="text/javascript">
	jQuery(function($){
		
		$("#popup-change-password-form").validate({
			rules: {
				password: {
					required: true,
					minlength: 5,
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#password",
				}
			},
			messages: {
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above",
				}
			}
		});
		
		$('#popup-change-password-form .btn-primary').click(function(e){
			e.preventDefault();
			if($('#popup-change-password-form').valid()){
				var current = $(this);
				var dataString =$("#popup-change-password-form").serialize();
				  
				$.ajax({
					type: 'POST',
					url:"<?php echo site_url('admin/users/ajaxChangePassword'); ?>",  
					data:dataString,
					success:function(result){
						if(result == 1){
							window.location.reload();
						}
						else{
							$('.error-ajax').text(result);
							 
						}
						
					},
					error:function (xhr, ajaxOptions, thrownError){
						alert(xhr.status);
						alert(thrownError);
					} 

				});
			}
		});
	})
</script>