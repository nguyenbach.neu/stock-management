<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $template['title']; ?></title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

</style>
</head>
<body>

<h1><?php echo $template['title']; ?></h1>

<p>This page is generated from default controller/action</p>
<code>/welcome/index</code>

<p>A. View Example</p>
<code>application/views/welcome_message.php</code>

<p>B. Controller Example</p>
<code>application/controllers/welcome.php</code>

<p>C. Model Example</p>
<code>application/models/users_model.php</code>

<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
<p>1. <?php echo __('How to make a translatable text') ?></p>
<code>echo __('I love CodeIgniter')</code>
<p>2. Set page title from controller</p>
<code>$this->template->title('CodeIgniter Guide');</code>
<p>and use from anywhere in view</p>
<code>echo $template['title'];</code>
<p>3. Set layout</p>
<code>$this->template->set_layout('default');</code>
<p>4. Add block(a part of page)</p>
<code>$this->template->set_partial('header', 'blocks/header');</code>
<p>and use from anywhere in view</p>
<code>echo echo $template['partials']['header'];</code>
<p>5. Display content (render view on layout and output to browser)</p>
<code>$this->template->build('welcome_message');</code>
<p>6. Send email</p>
<pre>
<code>$vars = array('name'=>'Tung');
$this->email->from('your@example.com', 'Your Name');
$this->email->to('someone@example.com');
$this->email->cc('another@another-example.com');
$this->email->bcc('them@their-example.com');
$this->email->subject('Email Test');
$this->email->message($this->load->view('emails/welcome_new_user'), $vars);
$this->email->send();</code>
</pre>
<p>7. How to validate and use built-in validation</p>
<pre>
<code>$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean');
$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|md5');
$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');</code></pre>
<p>8. How to add custom validation like existEmail()</p>
<p>9. How to get data from get, post</p>
<p>10. How to check permission anywhere</p>
<code>if (hasPermission('Hotels', 'view')) {}
if (hasPermission('Hotels', 'delete')) {}
if (hasPermission('Admin')) {}</code>
<p>11. How to create URL</p>
<code>echo site_url("news/local/123");</code>

<p>12. More ability - hooks, upload, pagination, session, ..... and more common functions,......please ask before you do anything</p>



<p><br />Page rendered in {elapsed_time} seconds</p>
<p><?php echo $template['partials']['copyright']; ?></p>
<?php $passed = array('page'=>3,
					'cat'=>5);
echo site_url('news/local/123', $passed,'this-is-seo-friendly-title'); ?>
</body>
</html>