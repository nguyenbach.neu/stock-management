<?php

	// $this->load->view('blocks/backend/top_menu');
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("product_code") . "</th>";
	$html .= "<th>" . __("product_price") . "</th>";
	$html .= "<th>" . __("product_quantity") . "</th>";
	$html .= "</tr>";
	if(count($histories) > 0 && $histories){
		foreach($histories as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".$item["stock_bill_product_history_name"]."</td>";
			$html .= "<td>".$item["stock_bill_product_history_code"]."</td>";
			$html .= "<td>".number_format($item["stock_bill_product_history_price"],NUMBER_DECIMAL)."</td>";
			$html .= "<td>".(float)($item["stock_bill_product_history_quantity"])."</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
?>