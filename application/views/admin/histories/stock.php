<?php 

?>
<?php

	echo "<h2>" . __("quantity_history") . "</h2>";

	$html = "";
	$html .= "<table class=\"table table-hover\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("Tong so hang xuat cu") . "</th>";
	$html .= "<th>" . __("Tien tien cu") . "</th>";
	$html .= "<th>" . __("Nguoi sua") . "</th>";
    $html .= "<th>" . __("Date time") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	if(count($histories) > 0 && $histories){
		foreach($histories as $k => $item){
			$html .= "<tr>";
			$html .= "<td>". number_format($item["numbers_item"],0) ."</td>";
			$html .= "<td>". number_format($item["totals"],NUMBER_DECIMAL) ."</td>";
			$html .= "<td>". $item["user_name"] ."</td>";
            $html .= "<td>".formatLocalDatetime($item["stock_bill_history_created_date"])."</td>";
			$html .= "<td><a class=\"btn btn-info\" href=\"". site_url("admin/histories/stock_detail/".$item["stock_bill_history_id"]) ."\">". __("detail") ."</a></td>";
			$html .= "</tr>";
		}
	}
	else{
		$html .= "<tr><td colspan=\"6\">". __("no_history") . "</td></tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
