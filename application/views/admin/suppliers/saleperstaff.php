<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Year");?></label>
                <select name="year" class="form-control">
                    <?php
                    for($i = date('Y'); $i > (date('Y') - 20); $i--) {
                        $selected = '';
                        if(isset($_GET['year'])) {
                            if($_GET['year'] == $i)
                                $selected = ' selected';
                        }
                        else {
                            if(date('Y') == $i)
                                $selected = ' selected';
                        }
                        echo '<option value="'. $i .'"'. $selected .'>'. $i .'</option>';
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label><?php echo __("Supplier");?></label>
                <?php echo supplier_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<?php
$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("Saler") . "</th>";
for($i=1; $i <= 12; $i++) {
	$html .= "<th>" . date("M", mktime(0, 0, 0, $i, 10)) ."</th>";
}
$html .= "</tr>";
if(count($reports) > 0) {
	foreach($reports as $k => $item) {
		$html .= '<tr><td>'. $item['user_name'] .'</td>';
		foreach($item as $l => $value) {
			if($l != 'user_name') {
				if(isset($value['totals_price'])) {
					$html .= '<td>'. number_format($value['totals_price'],NUMBER_DECIMAL) .'</td>';
				}
				else {
					$html .= '<td>0</td>';
				}
			}
		}
		$html .= '</tr>';
	}
}
$html .= "</table>";
echo $html;
?>
