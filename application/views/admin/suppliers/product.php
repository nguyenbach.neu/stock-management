<?php

	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("supplier_name") . "</th>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "</tr>";
	if(count($products) > 0){
		foreach($products as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".$item["supplier_name"]."</td>";
			$html .= "<td>".$item["product_name"]."</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
?>