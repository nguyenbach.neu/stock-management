<h2><?php echo __("stock_receipt_history");?></h2>
<?php 
	$html = "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("supplier") . "</th>";
	$html .= "<th>" . __("numbers of items") . "</th>";
	$html .= "<th>" . __("total amount") . "</th>";
	$html .= "<th>" . __("note") . "</th>";
	$html .= "<th>" . __("user_create") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if($receipts && count($receipts) >0){
		foreach($receipts as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".formatLocalDatetime($item["stock_bill_created_date"])."</td>";
			$html .= "<td>". getName($item["stock_bill_object_id"],"supplier") ."</td>";
			$html .= "<td>". (float)($item["numbers_item"]) ."</td>";
			$html .= "<td>". number_format($item["totals"],NUMBER_DECIMAL) ."</td>";
			$html .= "<td>".$item["stock_bill_note"]."</td>";
			$html .= "<td>".getName($item["stock_bill_create_user_id"])."</td>";
			$html .= '<td><a href="' . site_url("admin/stockreceipts/view/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("view") . '</a></td>';
			$html .= "</tr>";
		}
		
	}
	else{
		$html .= "<tr>";
		$html .= "<td colspan=\"7\">". __("no history") ."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	echo $html;
?>
<h2><?php echo __("cash_paid_history");?></h2>
<?php
	$html = "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("supplier") . "</th>";
	$html .= "<th>" . __("amount") . "</th>";
	$html .= "<th>" . __("note") . "</th>";
	$html .= "<th>" . __("user_create") . "</th>";
	$html .= "</tr>";
	if(count($paids) > 0){
		foreach($paids as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".formatLocalDatetime($item["cash_create_date"])."</td>";
			$html .= "<td>". getName($item["supplier_id"],"supplier") ."</td>";
			$html .= "<td>". number_format($item["cash_amount"],NUMBER_DECIMAL)."</td>";
			$html .= "<td>".$item["cash_note"]."</td>";
			$html .= "<td>".getName($item["cash_create_user_id"])."</td>";
			$html .= "</tr>";
		}
	}
	else{
		$html .= "<tr>";
		$html .= "<td colspan=\"7\">". __("no history") ."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	echo $html;
?>