<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/suppliers/add")?>"><?php echo __('Add new'); ?></a>
    </div>
</div>

<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("supplier_name") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if(count($suppliers) > 0 && $suppliers){
		foreach($suppliers as $k => $item){
			$html .= "<tr>";
			$html .= "<td><a href=\"" . site_url("admin/suppliers/edit/".$item["supplier_id"]) . "\">".$item["supplier_name"]."</a></td>";
			$html .= "<td>
						<a href=\"" . site_url("admin/suppliers/view/".$item["supplier_id"]) . "\" class=\"btn btn-info\">". __("product") ."</a>
						<a href=\"javascript:void(0)\" data-toggle=\"confirmation\" class=\"btn btn-danger\">". __("delete") ."</a>
						<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/suppliers/delete/".$item["supplier_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>
						
						</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
?>