<?php

if($supplier){
?>
<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("edit_supplier");?></legend>
			<input type="hidden" name="supplier_id" value="<?php echo $supplier["supplier_id"];?>" />
			<div class="control-group">
				<label class="control-label"><?php echo __("supplier_name");?></label>
				<div class="controls">
					<input type="text" class="form-control" id="supplier_name" name="supplier_name" value="<?php echo $supplier["supplier_name"];?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("supplier_discount");?></label>
				<div class="controls">
					<input type="text" class="form-control" id="supplier_discount" name="supplier_discount" value="<?php echo $supplier["supplier_discount"];?>" disabled readonly />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("change");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>

<?php } ?>