<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Year");?></label>
                <select name="year" class="form-control">
                    <?php
                    for($i = date('Y'); $i > (date('Y') - 20); $i--) {
                        $selected = '';
                        if(isset($_GET['year'])) {
                            if($_GET['year'] == $i)
                                $selected = ' selected';
                        }
                        else {
                            if(date('Y') == $i)
                                $selected = ' selected';
                        }
                        echo '<option value="'. $i .'"'. $selected .'>'. $i .'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Month");?></label>
                <select name="month" class="form-control">
                    <?php
                    for($i = 1; $i <= 12; $i++) {
                        $selected = '';
                        if(isset($_GET['month'])) {
                            if($_GET['month'] == $i)
                                $selected = ' selected';
                        }
                        else {
                            if(date('m') == $i)
                                $selected = ' selected';
                        }
                        echo '<option value="'. $i .'"'. $selected .'>'. sprintf("%02d",$i) .'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label><?php echo __("Supplier");?></label>
                <?php echo supplier_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<?php	
$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("product_code") . "</th>";
$html .= "<th>" . __("product_name") . "</th>";
$html .= "<th>" . __("product_quantity") . "</th>";
$html .= "<th>" . __("total amount") . "</th>";
$html .= "</tr>";
$total = array('quantity' => 0, 'price' => 0);
if(count($reports) >0 ){
	foreach($reports as $k => $item){
		$html .= "<tr>";
		$html .= "<td>".$item["stock_bill_product_code"]."</td>";
		$html .= "<td>".$item["stock_bill_product_name"]."</td>";
		$html .= "<td>".$item["totals_quantity"]."</td>";
		$html .= "<td class='text-right'>".number_format(($item["totals_price"]),NUMBER_DECIMAL)."</td>";
		$html .= "</tr>";
		$total['quantity'] = ($total['quantity'] + $item["totals_quantity"]);
		$total['price'] = ($total['price'] + $item["totals_price"]);
	}
	$html .= '<tr><td>'. __('Total') .'</td>';
	$html .= '<td>'. count($reports) . ((count($reports) > 1) ? " products" : " product") . '</td>';
	$html .= '<td>'. $total['quantity'] .'</td>';
	$html .= '<td class="text-right">'. number_format(($total['price']),NUMBER_DECIMAL) .'</td>';
	$html .= '</tr>';
}
$html .= "</table>";
echo $html;
echo $paginator;
?>
