<style type="text/css">
	.frm-ajax{margin:0;padding:0;}
	.span6.wp50{width:50%;margin-left:0;}
	.span6 td{font-size:23px;}
	.span6 td.td-text{padding: 20px 0;vertical-align: bottom;}
	.span6 td.td-text span{white-space: nowrap;}
	.span6 td.td-img{width:34%;}
	.span6 td.td-sell{width:32%;text-align:left;padding-left:15px;}
</style>
<div class="frm-ajax">
	
	<div style="margin-left:-20px;margin-right:-20px;background-color:#000;color:#FFF;width:1920px;padding-top: 20px;">


	<div class="lp33">
		<h1 class="text-center head-feed">GOLD</h1>
		<span class="wp30 text-center pull-left">
			<strong>
				BID
			</strong><br />
			<span class="price">
				<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801gold.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp30 text-center pull-left">
			<strong>
				ASK
			</strong></br>
			<span class="price">
				<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldask.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp40 pull-left text-right">
			<strong>
				
			</strong><br />
			<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldchangedollar.php');
				?>
			<span class="price price_<?php echo (($goldfeed > 0) ? "up" : "down"); ?>">
				<?php echo ($goldfeed); ?>
			</span>
		</span>
		<div style="clear:both;"></div>
	</div>
	<div class="lp33">
		<h1 class="text-center head-feed">SILVER</h1>
		<span class="wp30 text-center pull-left">
			<strong>
				BID
			</strong><br />
			<span class="price">	
				<?php
				$goldfeed =	file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silver.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp30 text-center pull-left">
			<strong>
				ASK
			</strong><br />
			<span class="price">
				<?php
				$goldfeed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverask.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp40 pull-left text-right">
			<strong>
				
			</strong><br />
			<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverchangedollar.php');
				?>
			<span class="price price_<?php echo (($goldfeed > 0) ? "up" : "down"); ?>">
				<?php echo ($goldfeed); ?>
			</span>
		</span>
	</div>
	<div class="lp30">
		<h1 class="text-center head-feed">PLATINUM</h1>
		<span class="wp30 text-center pull-left">
			<strong>
				BID
			</strong><br />
			<span class="price">
				<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinum.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp30 text-center pull-left">
			<strong>
				ASK
			</strong><br />
			<span class="price">
				<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumask.php');
				echo '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
				?>
			</span>
		</span>
		<span class="wp40 pull-left text-right">
			<strong>
				
			</strong><br />
			<?php
				$goldfeed =
				file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumchangedollar.php');
				
				?>
			<span class="price price_<?php echo (($goldfeed > 0) ? "up" : "down"); ?>">
				<?php echo ($goldfeed); ?>
			</span>				
		</span>
	</div>
	<div style="clear:both;"></div>

	</div>
	<div style="margin-left:-20px;margin-right:-20px;background-color:#FFF;min-height:600px;width:1920px;">
		<div class="span6 wp50">
			<?php 
			if($productsGold) {
				echo "<table class='table-striped table-hover' width='100%'>";
				$gold_bid = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801gold.php');
				$gold_ask = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldask.php');
				foreach($productsGold as $k => $item) {
					$html  = "";
					$html .= "<tr>";
					$html .= "<td class='text-center td-img'>";
					$src = '../'.IMG_UPLOAD.$item['product_thumbnail'];
					$img = base_url() . SCRIPT . '/' . 'timthumb.php?src=' . $src . '&amp;h=auto&amp;w=100&amp;zc=1';  
					$html .= "<img src='" . $img . "' />";
						
					
					$html .= "</td>";
					$html .= "<td class='td-text'>";
					$html .= "<span>" . $item["product_name"] . "</span><br />";
					$gold_buy = "";
					$gold_sell = "";
					if($item["product_markup_type"] == FIXED){
						$gold_buy = $gold_bid + $item["product_markup_buy"];
						$gold_sell = $gold_ask + $item["product_markup_sell"];
					}
					else if($item["product_markup_type"] == PERCENTAGE) {
						$gold_buy = $gold_bid * (100 + $item["product_markup_buy"]) / 100;
						$gold_sell = $gold_ask *(100 + $item["product_markup_sell"]) /100;
					}
					$html .= "<p><strong>Buy $ " . number_format($gold_buy, 2, '.', ',') . "</strong></p>";
					$html .= "</td>";
					$html .= "<td class='td-sell td-text'>";
					$html .= "<br />";
					$html .= "<p><strong>Sell $ " . number_format($gold_sell, 2, '.', ',') . "</strong></p>";
					$html .= "</td>";
					
					$html .= "</tr>";
					echo $html;
				}
				echo "</table>";
			}
			?>
		</div>
		<div class="span6 wp50">
			<?php 
			if($productsSilver) {
				echo "<table class='table-striped table-hover' width='100%'>";
				$silver_ask = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverask.php');
				$silver_bid = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silver.php');
				foreach($productsSilver as $k => $item) {
					$html  = "";
					$html .= "<tr>";
					$html .= "<td class='text-center td-img'>";
					$src = '../'.IMG_UPLOAD.$item['product_thumbnail'];
					$img = base_url() . SCRIPT . '/' . 'timthumb.php?src=' . $src . '&amp;h=auto&amp;w=100&amp;zc=1';  
					$html .= "<img src='" . $img . "' />";
						
					
					$html .= "</td>";
					$html .= "<td class='td-text'>";
					$html .= "<span>" . $item["product_name"] . "</span><br />";
					$silver_buy = "";
					$silver_sell = "";
					if($item["product_markup_type"] == FIXED){
						$silver_buy = $silver_bid + $item["product_markup_buy"];
						$silver_sell = $silver_ask + $item["product_markup_sell"];
					}
					else if($item["product_markup_type"] == PERCENTAGE) {
						$silver_buy = $silver_bid * (100 + $item["product_markup_buy"]) / 100;
						$silver_sell = $silver_ask *(100 + $item["product_markup_sell"]) /100;
					}
					$html .= "<p><strong>Buy $ " . number_format($silver_buy, 2, '.', ',') . "</strong></p>";
					$html .= "</td>";
					$html .= "<td class='td-sell td-text'>";
					$html .= "<br />";
					$html .= "<p><strong>Sell $ " . number_format($silver_sell, 2, '.', ',') . "</strong></p>";
					$html .= "</td>";
					
					$html .= "</tr>";
					echo $html;
				}
				echo "</table>";
			}
			?>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="footer-front" style="width:1920px;">
	<div>
	
	</div>
</div>
<script type="text/javascript">
	$("body").css("background","0 none");
	var auto_refresh = setInterval(
	function ()
	{
	jQuery.post("<?php echo site_url('products/ajaxLoadFeed');?>",function(data){
		if(data != ""){
			jQuery('.frm-ajax').html(data);
		}
		else return;
	});
	return false;
	}, 60000); // refresh every 60000 milliseconds
</script>