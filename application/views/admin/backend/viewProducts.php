<div class="hero-unit">
	<?php if($category){
		echo ("<h1>" . $category["cate_name"] . "</h1><p>" . $category["cate_description"] . "</p><p class=\"text-right\"><a ". ((count($products) < 6 ) ? 'href="'. site_url("admin/products/addNew/".$category["cate_id"]) . '"' : "href='javascript:void(0)' onclick='alertProduct()'" ) . "><button type=\"button\" class=\"btn btn-info\">Add product &raquo;</button></a></p>");
	} ?>
</div>
<?php if($this->session->flashdata('success_message')) {
	echo '<div class="msg">';
		echo '<div>'.$this->session->flashdata('success_message').'</div>';
	echo '</div>';
}?>          
<?php if($this->session->flashdata('error_message')) {
	echo '<div class="err">';
	echo '<div>'.$this->session->flashdata('error_message').'</div>';
	echo '</div>';
}?>	
<div class="row-fluid" style="min-height:400px;">
	<?php if($products){
		echo "<ul>";
		foreach($products as $k => $item){
			$src = '../'.IMG_UPLOAD.$item['product_thumbnail'];
			$img = base_url() . SCRIPT . '/' . 'timthumb.php?src=' . $src . '&amp;h=auto&amp;w=125&amp;zc=1';  
			?>
		<li class="itemli itemli<?php echo $k;?>">
			
			<div class="span3 product-thum">
				
				<p class="text-center product-thum"><img src="<?php echo $img; ?>" /></p>
				<p class="text-center">
					<span  class="btn btn-info" id="upload<?php echo $k;?>"><?php echo __('Edit'); ?></span>
					<a href="<?php echo site_url('admin/products/deletePicture/' . $item["product_id"] ); ?>" class="btn btn-danger"><?php echo __('Delete'); ?></a><br />
					<span id="status<?php echo $k;?>" ></span>
				</p>
				<script type="text/javascript" >
					$(function(){
						var btnUpload=$('#upload<?php echo $k;?>');
						var status=$('#status<?php echo $k;?>');
						new AjaxUpload(btnUpload, {
							action: '<?php echo site_url("admin/products/ajaxUpload/" . $item["product_id"])?>',
							name: 'uploadfile',
							onSubmit: function(file, ext){
								 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
									// extension is not allowed 
									status.text('Only JPG, PNG or GIF files are allowed');
									return false;
								}
								status.text('Uploading...');
							},
							onComplete: function(file, response){
								//On completion clear the status
								status.text('');
								//Add uploaded file to list
								if(response!=="error"){
									btnUpload.parent().parent().children(".product-thum").html('<img src="/' + response + '" alt="" />');
								} else{
									//$('<li></li>').appendTo('#files<?php echo $k;?>').text(file).addClass('error');
								}
							}
						});
						
					});
				</script>
			</div>
			<div class="span9">
				<form method="post" action="<?php echo (site_url("admin/products/editProduct"))?>" id="frmProduct<?php echo $k;?>">
					<input type="hidden" name="product_id" value="<?php echo $item["product_id"]; ?>" />
					<div class="controls">
						<p>Product name</p>
						 <input class="span8" name="product_name" type="text" readonly="readonly" value="<?php echo $item["product_name"]; ?>" />
					</div>
					<div class="controls controls-row">
						<div class="span3">
							<p>Markup Buy</p>
							<input style="width:85%;" readonly="readonly" type="text" name="product_markup_buy" value="<?php echo $item["product_markup_buy"]; ?>" />
						</div>
						<div class="span3">
							<p>Markup Sell</p>
							<input style="width:85%;" readonly="readonly" type="text" name="product_markup_sell" value="<?php echo $item["product_markup_sell"]; ?>" >
						</div>
						<div class="span5">
							<p>Select Fixed or Percentage</p>
							<?php echo selectProductType("product_markup_type", $item["product_markup_type"],"disabled"); ?>
						</div>
						
					</div>
					<div class="controls">
						<div class="span8">
							<button type="button" class="btn edit-product"><i class="icon-edit"></i> Edit</button>
							<button type="button" class="btn btn-info save-product">Save</button>
							<a href="<?php echo site_url('admin/products/deleteProduct/'. $item['product_id']); ?>" class="btn btn-danger delete-product"><i class="icon-trash icon-white"></i> Delete</a>
						</div>
						<div style="clear:both;"></div>
					</div>
				</form>
			</div>
			<div style="clear:both;"></div>
			<script type="text/javascript">
				$(".delete-product").confirmation({
					"href":"<?php echo site_url('admin/products/deleteProduct/'. $item['product_id']); ?>",
					"popout":true
				});
			</script>
		</li>
		<?php
		}
		echo "</ul>";
	} ?>
</div>
<script type="text/javascript">
	$(".edit-product").live("click",function(){
		$(this).parent().parent().parent().find("input").removeAttr("readonly");
		$(this).parent().parent().parent().find("select").removeAttr("disabled");
	});
	$(".save-product").live("click",function(){
		var formParrent = $(this).parent().parent().parent();
		if(formParrent.find("select").attr("disabled") !="disabled") {
			formParrent.validate({
				rules:{
					"product_name" : "required",
					"product_markup_buy" : {number: true,required:true},
					"product_markup_sell" : {number: true,required:true}
				},
				
				errorElement: "div", //Thành phần HTML hiện thông báo lỗi
			});
			formParrent.submit();
		}
	});
	function alertProduct() {
		alert("Category have 6 products");
	}
</script>