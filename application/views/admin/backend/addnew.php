<div class="container registration-container">
	<div class="span6">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend>Add new product</legend>
			<input type="hidden" name="product_category_id" value="<?php echo $cate_id;?>" />
			<div class="control-group">
				<label class="control-label">Product name</label>
				<div class="controls">
					<input type="text" class="form-control" id="product_name" name="product_name" rel="popover" data-content="Enter product name" data-original-title="Product Name">
				</div>
			</div>


			<div class="control-group">
				<label class="control-label">Product image</label>
				<div class="controls">
					<input type="file" class="form-control" id="product_image" name="product_image" rel="popover">
				</div>
			</div> 
			<div class="control-group">
				<label class="control-label">Product Markup Buy</label>
				<div class="controls">
					<input type="text" class="form-control" id="product_markup_buy" name="product_markup_buy" rel="popover" data-content="Enter product markup buy" data-original-title="Product Markup Buy">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Product Markup Sell</label>
				<div class="controls">
					<input type="text" class="form-control" id="product_markup_sell" name="product_markup_sell" rel="popover" data-content="Enter product markup sell" data-original-title="Product Markup Sell">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Select Fixed or Percentage</label>
				<div class="controls">
					<?php echo selectProductType("product_markup_type"); ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-success" >Create Product</button>
					<button type="button" class="btn btn-danger" >Cancel</button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>