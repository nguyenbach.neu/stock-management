<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="date1" value="<?php echo $date1; ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="date2" value="<?php echo $date2; ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<h3>Bao cao ket qua kinh doanh (Tinh theo tai san)</h3>
So lieu dau ki
<table class="table-responsive table">
    <tr>
        <td><?php echo __('Tong tai san qui doi (1)'); ?></td><td><a href="<?php echo site_url("admin/reports/cash_overview").'?date='.$date1; ?>"><?php echo number_format($total_equipment1, NUMBER_DECIMAL); ?></a></td>
    </tr>
</table>
So lieu cuoi ki
<table class="table-responsive table">
    <tr>
        <td><?php echo __('Tong tai san qui doi (1)'); ?></td><td><a href="<?php echo site_url("admin/reports/cash_overview").'?date='.$date2; ?>"><?php echo number_format($total_equipment2, NUMBER_DECIMAL); ?></a></td>
    </tr>

    <tr>
        <td><?php echo __('Gia tang von chu so huu (3)'); ?></td><td><a href="<?php echo site_url("admin/reports/cash_overview").'?date='.$date2; ?>"><?php echo number_format($total_capital2, NUMBER_DECIMAL); ?></a></td>
    </tr>
    <tr>
        <td><?php echo __('Tong gia tri tang (1  - 3)'); ?></td><td><?php echo number_format($total_equipment2 -$total_capital2, NUMBER_DECIMAL); ?></td>
    </tr>
</table>
Thong ke
<table class="table-responsive table">
    <tr>
        <td><?php echo __('Doanh thu'); ?></td><td><?php echo number_format($total_revenue, NUMBER_DECIMAL); ?></td>
    </tr>
    <tr>
        <td><?php echo __('Chiet khau truc tiep'); ?></td><td><?php echo number_format($total_discount, NUMBER_DECIMAL); ?> (<?php echo number_format(($total_discount/$total_revenue)*100, NUMBER_DECIMAL); ?>%) </td>
    </tr>
    <tr>
        <td><?php echo __('Chi phi hoat dong'); ?></td><td><?php echo number_format($total_expense, NUMBER_DECIMAL); ?> (<?php echo number_format(($total_expense/$total_revenue)*100, NUMBER_DECIMAL); ?>%) </td>
    </tr>
    <tr>
        <td><?php echo __('Loi nhuan tam tinh sau chi phi'); ?></td><td><?php echo number_format(($total_equipment2 -$total_capital2 - $total_equipment1), NUMBER_DECIMAL); ?> (<?php echo number_format(((($total_equipment2 -$total_capital2 -$total_equipment1)/$total_revenue)*100), NUMBER_DECIMAL); ?>%)</td>
    </tr>
</table>

<h3>Bao cao ket qua kinh doanh (Tinh theo tung phieu xuat)</h3>
<table class="table-responsive table">
    <tr>
        <td><?php echo __('Lợi nhuận sau trừ giá nhập (Lợi nhuận gộp)'); ?></td><td><?php echo number_format($gross_profit, NUMBER_DECIMAL); ?></td>
    </tr>

    <tr>
        <td><?php echo __('Chi phí'); ?></td><td><?php echo number_format($expenses, NUMBER_DECIMAL); ?></td>
    </tr>

    <tr>
        <td><?php echo __('Chiết khấu đại lý') ?></td><td><?php echo number_format($commission, NUMBER_DECIMAL)?></td>
    </tr>

    <tr>
        <td><?php echo __('Lợi nhuận')?></td><td><?php echo number_format($net_profit, NUMBER_DECIMAL)?></td>
    </tr>
</table>
Thong ke
<table class="table-responsive table">
    <tr>
        <td><?php echo __('Chi phi hoat dong'); ?></td><td><?php echo number_format(($expenses/$revenue)*100, NUMBER_DECIMAL); ?>% </td>
    </tr>
    <tr>
        <td><?php echo __('Loi nhuan tam tinh sau chi phi'); ?></td><td><?php echo number_format((($net_profit/$revenue)*100), NUMBER_DECIMAL); ?>%</td>
    </tr>
</table>