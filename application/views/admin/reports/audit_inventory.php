<div>
    <table class="table-responsive table">
        <tr>
            <td><?php echo __('Ten'); ?></td>
            <td><?php echo __('Ma'); ?></td>
            <td><?php echo __('So luong tren so kho'); ?></td>
            <td><?php echo __('So luong theo phieu xuat nhap'); ?></td>
            <td><?php echo __('So luong theo lich su thay doi'); ?></td>
        </tr>
<?php foreach ($products as $id => $product) { ?>
        <tr class="<?php echo ((float)$product['product_stock_quantity'] != (float)$products_history[$id]['quantity'] || (float)$product['product_stock_quantity'] != (float)$products_bill[$id]['quantity'])?'danger':''; ?>">
            <td><?php echo $product['product_name']; ?></td>
            <td><?php echo $product['product_code']; ?></td>
            <td><?php echo number_format($product['product_stock_quantity'], NUMBER_DECIMAL); ?></td>
            <td><?php echo number_format($products_bill[$id]['quantity'], NUMBER_DECIMAL); ?></td>
            <td><?php echo number_format($products_history[$id]['quantity'], NUMBER_DECIMAL); ?></td>
        </tr>
<?php } ?>
    </table>
</div>