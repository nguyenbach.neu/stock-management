<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<div>
    <table class="table-responsive table">
        <tr class="<?php echo ((float)$cost['sum'] != (float)$purchase['sum'])?'danger':''; ?>">
            <td><?php echo __('Tong tien nhap hang tren tai khoan'); ?></td>
            <td><?php echo number_format($cost['sum'], NUMBER_DECIMAL); ?></td>
            <td><?php echo number_format($purchase['sum'], NUMBER_DECIMAL); ?></td>
            <td><?php echo __('Tong tien hang nhap tren phieu nhap'); ?></td>
        </tr>
        <tr class="<?php echo ((float)$transferIn['sum'] != (float)$transferOut['sum'])?'danger':''; ?>">
            <td><?php echo __('Tong tien chuyen khoi tai khoan'); ?></td>
            <td><?php echo number_format($transferIn['sum'], NUMBER_DECIMAL); ?></td>
            <td><?php echo number_format($transferOut['sum'], NUMBER_DECIMAL); ?></td>
            <td><?php echo __('Tong tien chuyen vao vao tai khoan'); ?></td>
        </tr>
    </table>
</div>