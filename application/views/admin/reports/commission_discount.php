<table class="table-responsive table">
    <tr>
        <td style="width:30%">
            <table class="table-responsive table">
                <tr><td>#</td></tr>
                <tr><td>Tiền khuyến mãi đã trả cho khách hàng (1)</td></tr>
                <tr><td>Tiền khuyễn mãi nhận được của công ty (2)</td></tr>
                <tr><td>Còn lại (1-2)</td></tr>
            </table>
        </td>
        <?php foreach ($commission_discounts as $commission_discount) {?>
            <td style="width:35%">
                <table class="table-responsive table">
                    <tr><td><?php echo $commission_discount['supplier_name'] ?></td></tr>
                    <tr><td><?php echo number_format($commission_discount['sum'],NUMBER_DECIMAL) ?></td></tr>
                    <tr><td><?php echo number_format(($commission_discount['cash_receipt']+$commission_discount['stock_receipt']),NUMBER_DECIMAL) ?></td></tr>
                    <tr><td><?php echo number_format(($commission_discount['sum'] - $commission_discount['cash_receipt'] - $commission_discount['stock_receipt']),NUMBER_DECIMAL) ?></td></tr>
                </table>
            </td>
        <?php } ?>
    </tr>

</table>