<?php 
if($id == 0){
	echo __("order deleted");
}
else {

?>
<form class="form-horizontal" method="post" action="">
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("Order ID");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;">PX-<?php echo $order["order_id"];?></div>
	</div>
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("date time");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo formatLocalDatetime($order["order_date"]);?></div>
	</div>
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("user_sale");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($order["saler_user_name"]);?></div>
	</div>
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("user_create");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($order["creater_user_name"]);?></div>
	</div>
<?php	
	$html = "";
	$totals = 0;
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("product_code") . "</th>";
	$html .= "<th>" . __("package_quantity") . "</th>";
	$html .= "<th>" . __("unit_quantity") . "</th>";
	$html .= "<th>" . __("product_price") . "</th>";
	$html .= "<th style=\"text-align:right\">" . __("total") . "</th>";
	$html .= "</tr>";
	if(count($products) > 0){
		foreach($products as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".$item["order_product_name"]."</td>";
			$html .= "<td>".$item["order_product_code"]."</td>";
			$html .= "<td>".$item["package_quantity"]."</td>";
			$html .= "<td>".$item["unit_quantity"]."</td>";
			$html .= "<td>".number_format($item["order_product_price"],NUMBER_DECIMAL)."</td>";
			$html .= "<td style=\"text-align:right\">".number_format(($item["order_product_price"]*$item["order_product_quantity"]),NUMBER_DECIMAL)."</td>";
			$html .= "</tr>";
			$totals = $totals + ($item["order_product_price"]*$item["order_product_quantity"]);
		}
		$html .= "<tr>";
		$html .= "<td colspan=\"4\" style=\"text-align:right\">". __("totals") ."</td>";
		$html .= "<td style=\"text-align:right\">". number_format($totals,NUMBER_DECIMAL) ."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	echo $html;
?>
	<div class="control-group">
		<label class="control-label"></label>
		<input type="hidden" name="order_id" value="<?php echo $order["order_id"];?>" />
		<div class="controls">
			<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
		</div>
	</div>
</form>

<?php } ?>