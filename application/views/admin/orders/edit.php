
		<form class="form-horizontal form-validation" id="order_form" method='post'>
		<fieldset>
			<legend><?php echo __("edit_orders");?></legend>

			<input type="hidden" value="0" name="order_status" id="order_status" />
			<input type="hidden" value="<?php echo $order["order_id"];?>" name="order_id" id="order_id" />
			<input type="hidden" value="<?php echo $order["order_status"];?>" name="curr_order_status" id="curr_order_status" />
			<?php echo product_dropdown(false,"","clone_product",false,'display:none','non-select2')?>
			<div class="col-xs-6">
				<label class="control-label"><?php echo __("order_sale");?></label>
				<div class="controls">
					<?php echo sale_user_dropdown($order["order_object_id"]);?>
				</div>

                <div style="padding-top: 10px">
                    <label for="inputEmail"><?php echo __("Ngày");?></label>
                    <div class='input-group date start_date' id='datetimepicker1'>
                        <input type='text' class="form-control" name="created_date" value="<?php echo $order['order_date']; ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                         </span>
                    </div>
                </div>
			</div>
			<div class="col-xs-6">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea rows="3" name="order_note" class="form-control"><?php echo $order["order_note"]?></textarea>
				</div>
			</div>
            <div style="clear: both; padding-bottom: 10px"></div>
			<div class="control-group">
				<label class="control-label"><?php echo __("products");?></label>
			</div>
			<div class="control-group">
				<input type="hidden" value="<?php echo count($order["products"]);?>" name="total_item" id="total_item" />
				<table id="tbl_order_products" class="table table-hover">
					<thead>
						<tr>
							<th><?php echo __("order");?></th>
							<th><?php echo __("product_name");?></th>

							<th><?php echo __("package_quantity");?></th>
                            <th><?php echo __("unit_quantity");?></th>
							<th><?php echo __("action");?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($order["products"]) > 0 ) {
							foreach($order["products"] as $k => $item){
						?>
						<tr id="tr_row_<?php echo $k;?>">
							<td class="text-center"><?php echo ($k+1)?></td>
							<td><?php echo product_dropdown($item["product_id"],"product_id_x_".$k,"product_id_x_".$k,true)?>
								<input type="hidden" class="hdstockproduct_id" id="order_product_id_<?php echo $k; ?>" name="order_product_id_<?php echo $k; ?>" value="<?php echo $item["order_product_id"];?>" />
								<input type="hidden" class="hdproduct_id" id="product_id_<?php echo $k; ?>" name="product_id_<?php echo $k?>" value="<?php echo $item["product_id"] ?>" />
								<input type="hidden" class="hdproduct_code form-control" id="product_code_<?php echo $k; ?>" name="product_code_<?php echo $k; ?>" value="<?php echo $item["order_product_code"];?>" />
								<input type="hidden" class="hdproduct_name form-control" id="product_name_<?php echo $k; ?>" name="product_name_<?php echo $k; ?>" value="<?php echo $item["order_product_name"];?>" /> </td>
							<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_<?php echo $k; ?>" name="package_quantity_<?php echo $k?>" rel="popover" data-content="Product quantity" data-original-title="Product quantity" value="<?php echo (float)($item["package_quantity"]);?>" /></td>
                            <td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_<?php echo $k?>" name="unit_quantity_<?php echo $k?>" value="<?php echo $item["unit_quantity"]?>"/></td>
							<td><a class="btn btn-danger" onclick="removeRowTable('tr_row_<?php echo $k;?>')"><?php echo __("delete") ;?></a></td>
						</tr>
					<?php }} ?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5">
								<button type="button" class="btn btn-default pull-right" id="btn_add_row"><span class="glyphicon glyphicon-plus"></span> <?php echo __("add_row");?></button>
							</td>
					</tfoot>
				</table>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
					<button type="button" class="btn btn-primary" id="btn_process"><?php echo __("process");?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".sl_order_product").select2({
			width: 200
		});
		
		$("#btn_process").click(function(){
			$("#order_status").attr("value",0);
			$('.select2-container').attr('style', 'display: block !important');

			$("#order_form").submit();
			
		});


		$("#btn_add_row").click(function(){
			var name = $('#tbl_order_products tbody tr').length;
            var newRow='<tr id="tr_row_'+name+'">';
                newRow+='<td class="text-center">'+ parseInt(name + 1) +'</td>';
                newRow+='<td><select class="sl_order_product" name="product_id_'+name+'" id="product_id_'+name+'" title="Please choose product"></select>';
                newRow+='<input type="hidden"  class="hdproduct_name" name="product_name_'+name+'" id="product_name_'+name+'" /><input type="hidden"  class="hdproduct_code" name="product_code_'+name+'" id="product_code_'+name+'" /></td>';
                newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_'+name+'" name="package_quantity_'+name+'" rel="popover" data-content="Package quantity" data-original-title="Package quantity" /></td>';
                newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_'+name+'" name="unit_quantity_'+name+'" rel="popover" data-content="Unit quantity" data-original-title="Unit quantity" /></td>';
                newRow+='<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_' + name + '")><?php echo __("delete")?></a></td>';      
                newRow += '</tr>';
            if($('#tbl_order_products tbody tr').length == 0){
				$('#tbl_order_products tbody').append(newRow);
				
			}
			else{
				$('#tbl_order_products tbody tr:last').after(newRow);
			}
			$('#clone_product option').clone().appendTo("#product_id_"+name);
			$("#product_id_"+name).val(0).attr("selected",true);
			$('#total_item').attr("value", (name+1));
			$("#product_id_" + name).select2({width: 200});
			$("#product_id_"+name).rules('add', { required: true,min:1 });
			$("#package_quantity_"+name).rules('add', { required: true, number:true });
            $("#unit_quantity_"+name).rules('add', { required: true, number:true });
			jQuery('.formatnumber').on('keyup', function(){
				price = jQuery(this).val();
				this.value = numberFormat(this.value);
			});
			$(".sl_order_product").change(function(){
				var curr = $(this);
				if($(this).val() != ""){
					$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
						var objJSON = eval("(function(){return " + data + ";})()");
						curr.parent().find(".hdproduct_code").val(objJSON.product_code);
						curr.parent().find(".hdproduct_name").val(objJSON.product_name);
					});
				}
			});
		});
		$(".sl_order_product").change(function(){
			var curr = $(this);
			if($(this).val() != ""){
				$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
					var objJSON = eval("(function(){return " + data + ";})()");
					curr.parent().next("td").find(".hdproduct_code").val(objJSON.product_code);
					curr.parent().next("td").find(".hdproduct_name").val(objJSON.product_name);
					curr.parent().next("td").find("input[type='text']").val(numberFormat(objJSON.product_price));
				});
			}
		});
	});
	function removeRowTable(t) {
        $("#" + t).remove();
            SetBangTaiLieu();
    }
    function SetBangTaiLieu() {
        var rowCount = $('#tbl_order_products tbody tr').length;
        if(rowCount == 0){
			$("#total_item").val(0);
		}
        for (var i = 0; i < rowCount; i++) {
            var indexID = $('#tbl_order_products tbody').children().eq(i).children().eq(0).html();            
            $('#tbl_order_products tbody').children().eq(i).children().eq(0).html(i+1);
            indexID = eval(indexID) - 1;			
            nsID = eval(i);
            $("#sorder_product_id_" + indexID).attr("name", "stock_issue_product_id_" + nsID);
            $("#order_product_id_" + indexID).attr("id", "stock_issue_product_id_" + nsID);

            $("#product_id_" + indexID).attr("name", "product_id_" + nsID);
            $("#product_id_" + indexID).attr("id", "product_id_" + nsID);

            $("#product_name_" + indexID).attr("name", "product_name_" + nsID);
            $("#product_name_" + indexID).attr("id", "product_name_" + nsID);

            $("#product_code_" + indexID).attr("name", "product_code_" + nsID);
            $("#product_code_" + indexID).attr("id", "product_code_" + nsID);

            $("#product_price_" + indexID).attr("name", "product_price_" + nsID);
            $("#product_price_" + indexID).attr("id", "product_price_" + nsID);

            $("#package_quantity_" + indexID).attr("name", "package_quantity_" + nsID);
            $("#package_quantity_" + indexID).attr("id", "package_quantity_" + nsID);

            $("#unit_quantity_" + indexID).attr("name", "unit_quantity_" + nsID);
            $("#unit_quantity_" + indexID).attr("id", "unit_quantity_" + nsID);
            $("#total_item").val(nsID+1);
        }
    }   
</script>