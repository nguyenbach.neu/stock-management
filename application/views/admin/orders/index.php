
<div class="panel panel-default">
    <div class="panel-heading text-right"><a class="btn btn-success" href="<?php echo site_url("admin/orders/add")?>"><?php echo __('Add new'); ?></a></div>
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label><?php echo __("Saler");?></label>
                <?php echo sale_user_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<form class="form-horizontal form-validation" id="order_form" method='post' action="/admin/orders/actions">
	<fieldset>
        <div>
            <div class="action form-group" >
                <label>Thao tác</label>
                <select id="action" name="action" class="form-control">
                    <option value="stock_issue">In phiếu xuất kho</option>
                    <option value="print_order">In hóa đơn</option>
                    <option value="general_issue">In phiếu tổng</option>
                </select>
            </div>
        </div>
		<?php
        $html .= "<table class=\"table table-hover table-striped\">";
		$html .= "</tr>";

		$html .= "<tr>";
		$html .= "<th></th>";
		$html .= "<th>" . __("Ma") . "</th>";
		$html .= "<th>" . __("date time") . "</th>";
		$html .= "<th>" . __("user_sale") . "</th>";

		$html .= "<th>" . __("status") . "</th>";
		$html .= "<th>" . __("user_create") . "</th>";
		$html .= "<th>" . __("action") . "</th>";
		$html .= "</tr>";
		if(count($orders) >0 ){
			foreach($orders as $k => $item){
				$html .= "<tr>";
                $html .= "<td> <input type='checkbox' name='order_selected_".$k."' value='".$item["order_id"]."'/> </td>";
				$html .= "<td>DH-".$item["order_id"]."</td>";
				$html .= "<td>".formatLocalDatetime($item["order_date"])."</td>";
				$html .= "<td>".$item["saler_user_name"]."</td>";

				$order_status = "";
				$proccess = "";
				$lock = false;
				$view = '<a href="' . site_url("admin/orders/view/".$item["order_id"]) . '" class="btn btn-info">' . __("view") . '</a>';

				switch($item["order_status"]){
					case STOCK_CHANGE_STATUS_DRAFT: $order_status = __("draft");
						$proccess = '<a href="' . site_url("admin/orders/process/".$item["order_id"]) . '" class="btn btn-info">' . __("process") . '</a>';
						$lock = false;
						break;
					case STOCK_CHANGE_STATUS_PROCESSED: $order_status = __("processed"); $lock = true; break;
				}
				$html .= "<td>".$order_status."</td>";
				$html .= "<td>".$item["creater_user_name"]."</td>";
				$html .= "<td>" . $view ;
				if(has_permission($this->session->userdata('user_role'),"orders","edit",$lock)){
					$html .= '<a href="' . site_url("admin/orders/edit/".$item["order_id"]) . '" class="btn btn-info">' . __("edit") . '</a>';
				}
				else {
					$html .= '<a href="javascript:void(0)" class="btn btn-info disabled">' . __("edit") . '</a>';
				}
				if(has_permission($this->session->userdata('user_role'),"orders","delete",$lock)){
					$html .= '<a href="javascript:void(0)" data-toggle="confirmation" class="btn btn-danger">' . __("delete") . '</a>';
					$html .= "<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/orders/delete/".$item["order_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>";
				}
				else{
					$html .= '<a href="javascript:void(0)" class="btn btn-danger disabled">' . __("delete") . '</a>';
				}
				$html .= "</td>";

				$html .= "</tr>";
			}
		}

		$html .= "</table>";
		echo $html;
		echo $paginator;
		?>
        <?php if ($this->session->userdata('user_role') != 4){ ?>
		<button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Submit");?></button>
	    <?php } ?>
    </fieldset>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
	function deleteUser(el){
			
	}
</script>