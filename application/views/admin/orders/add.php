<?php
$item = array();
$item["sale_user_id"] = "";
$item["order_note"] = "";
$item["total_item"] = "";
if (isset($posts)) {
    // print_r($posts);
    $item["sale_user_id"] = $posts["sale_user_id"];
    $item["order_note"] = $posts["order_note"];
    $item["total_item"] = $posts["total_item"];
}
?>


<form class="form-horizontal form-validation" id="stock_bill_form" method='post'>
    <fieldset>
        <legend><?php echo __("add_order"); ?></legend>
        <div class="panel panel-default">
            <div class="panel-body">
                <input type="hidden" value="-1" name="order_status" id="order_status"/>
                <?php echo product_dropdown(false, "", "clone_product", false, 'display:none', 'non-select2') ?>
                <div class="row">

                    <div class="col-xs-6">
                        <div>
                            <label><?php echo __("order_sale"); ?></label>
                            <?php echo sale_user_dropdown($item["sale_user_id"]); ?>
                        </div>

                        <div style="padding-top: 10px">
                            <label for="inputEmail"><?php echo __("Chon ngay bat dau"); ?></label>
                            <div class='input-group date start_date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="created_date" value=""/>
                                <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                         </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <label><?php echo __("note"); ?></label>
                        <textarea class="form-control" rows="3"
                                  name="order_note"><?php echo $item["order_note"]; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="control-group">
            <input type="hidden" value="<?php echo(($item["total_item"] != "") ? $item["total_item"] : '1') ?>"
                   name="total_item" id="total_item"/>
            <table id="tbl_order_products" class="table table-hover">
                <thead>
                <tr>
                    <th class="text-center"><?php echo __("order"); ?></th>
                    <th><?php echo __("product_name"); ?></th>

                    <th><?php echo __("package_quantity"); ?></th>
                    <th><?php echo __("unit_quantity"); ?> </th>
                    ;
                    <th><?php echo __("action"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($posts)): ?>
                    <?php for ($k = 0; $k < $posts["total_item"]; $k++) : ?>
                        <tr id="tr_row_<?php echo $k; ?>">
                            <td class="text-center"><?php echo($k + 1) ?></td>
                            <td><?php echo product_dropdown($posts["product_id_" . $k], "product_id_" . $k, "product_id_" . $k) ?></td>
                            <td><input type="text" class="input-medium formatnumber form-control"
                                       id="package_quantity_<?php echo $k ?>" name="package_quantity_<?php echo $k ?>"
                                       value="<?php echo($posts["package_quantity_" . $k]); ?>"/></td>
                            <td><input type="text" class="input-medium formatnumber form-control"
                                       id="unit_quantity_<?php echo $k ?>" name="unit_quantity_<?php echo $k ?>"
                                       value="<?php echo($posts["unit_quantity_" . $k]) ?>"/></td>
                            <td><?php if ($k > 0) {
                                    echo '<a class="btn btn-danger" onclick=removeRowTable("tr_row_' . $k . '")>' . __("delete") . '</a>';
                                } ?></td>
                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <tr id="tr_row_0">
                        <td class="text-center">1</td>
                        <td><?php echo product_dropdown(false, "product_id_0", "product_id_0") ?></td>
                        <td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_0"
                                   name="package_quantity_0" rel="popover" data-content="Package quantity"
                                   data-original-title="Package quantity"/></td>
                        <td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_0"
                                   name="unit_quantity_0" rel="popover" data-content="Unit quantity"
                                   data-original-title="Unit quantity"/></td>
                        <td><a class="btn btn-danger"
                               onclick='removeRowTable("tr_row_0")'><?php echo __("delete") ?></a></td>
                    </tr>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var selected = '#product_id_0';
                            $('#clone_product option').clone().appendTo(selected);
                            $(selected).select2({width: 200});
                        });
                    </script>
                <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <button type="button" class="btn btn-default pull-right" id="btn_add_row"><span
                                        class="glyphicon glyphicon-plus"></span> <?php echo __("add_row"); ?></button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="control-group text-center">
            <button type="button" class="btn btn-primary" id="btn_process"><?php echo __("process"); ?></button>
        </div>
    </fieldset>
</form>


<script type="text/javascript">
    $(document).ready(function () {

        $(".sl_order_product").select2({
            width: 200
        });


        $("#btn_process").click(function () {
            $("#order_status").attr("value", 0);
            $('.select2-container').attr('style', 'display: block !important');

            $("#stock_bill_form").submit();

        });

        $("#btn_add_row").click(function () {
            var name = $('#tbl_order_products tbody tr').length;
            var newRow = '<tr id="tr_row_' + name + '">';
            newRow += '<td class="text-center">' + parseInt(name + 1) + '</td>';
            newRow += '<td><select class="sl_stock_bill_product" name="product_id_' + name + '" id="product_id_' + name + '" title="Please choose product"></select></td>';

            newRow += '<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_' + name + '" name="package_quantity_' + name + '" rel="popover" data-content="Package quantity" data-original-title="Package quantity" /></td>';
            newRow += '<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_' + name + '" name="unit_quantity_' + name + '" rel="popover" data-content="Unit quantity" data-original-title="Unit quantity" /></td>';
            newRow += '<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_' + name + '")><?php echo __("delete")?></a></td>';
            newRow += '</tr>';
            if ($('#tbl_order_products tbody tr').length == 0) {
                $('#tbl_order_products tbody').append(newRow);

            } else {
                $('#tbl_order_products tbody tr:last').after(newRow);
            }
            $('#clone_product option').clone().appendTo("#product_id_" + name);
            $('#total_item').attr("value", (name + 1));
            $("#product_id_" + name).val("0").attr("selected", true);
            $("#product_id_" + name).select2({width: 200});
            $("#product_id_" + name).rules('add', {required: true, min: 1});
            $("#package_quantity_" + name).rules('add', {required: true, number: true});
            $("#unit_quantity_" + name).rules('add', {required: true, number: true});

            jQuery('.formatnumber').on('keyup', function () {
                price = jQuery(this).val();
                this.value = numberFormat(this.value);
            });
            $(".sl_order_product").change(function () {
                var curr = $(this);
                if ($(this).val() != "") {
                    $.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>', {"product_id": $(this).val()}, function (data) {
                        var objJSON = eval("(function(){return " + data + ";})()");
                        curr.parent().next("td").find(".hdproduct_code").val(objJSON.product_code);
                        curr.parent().next("td").find(".hdproduct_name").val(objJSON.product_name);
                        curr.parent().next("td").find("input[type='text']").val(numberFormat(objJSON.product_price));
                    });
                }
            });
        });
        $(".sl_order_product").change(function () {
            var curr = $(this);
            if ($(this).val() != "") {
                $.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>', {"product_id": $(this).val()}, function (data) {
                    var objJSON = eval("(function(){return " + data + ";})()");
                    curr.parent().next("td").find(".hdproduct_code").val(objJSON.product_code);
                    curr.parent().next("td").find(".hdproduct_name").val(objJSON.product_name);
                    curr.parent().next("td").find("input[type='text']").val(numberFormat(objJSON.product_price));
                });
            }
        });
    });

    function removeRowTable(t) {
        $("#" + t).remove();
        SetBangTaiLieu();
    }

    function SetBangTaiLieu() {
        var rowCount = $('#tbl_order_products tbody tr').length;
        if (rowCount == 0) {
            $("#total_item").val(0);
        }
        for (var i = 0; i < rowCount; i++) {
            var indexID = $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html();
            $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html(i + 1);
            indexID = eval(indexID) - 1;
            nsID = eval(i);
            $("#product_id_" + indexID).attr("name", "product_id_" + nsID);
            $("#product_id_" + indexID).attr("id", "product_id_" + nsID);

            $("#package_quantity_" + indexID).attr("name", "package_quantity_" + nsID);
            $("#package_quantity_" + indexID).attr("id", "package_quantity_" + nsID);
            $("#unit_quantity_" + indexID).attr("name", "unit_quantity_" + nsID);
            $("#unit_quantity_" + indexID).attr("id", "unit_quantity_" + nsID);
            $("#total_item").val(nsID + 1);
        }
    }
</script>