<div class="container">
	<h1>Sign in </h1>
	<p>Please sign in using your registered account details</p>
					<?php if($this->session->flashdata('success_message')) {
						echo '<div class="msg">';
							echo '<div>'.$this->session->flashdata('success_message').'</div>';
						echo '</div>';
					}?>          
					<?php if($this->session->flashdata('error_message')) {
						echo '<div class="err">';
						echo '<div>'.$this->session->flashdata('error_message').'</div>';
						echo '</div>';
					}?>	
					<?php echo validation_errors('<div class="err"><div>', '</div></div>');?>
    <form method="POST" action="" accept-charset="UTF-8" class="text-center" autocomplete="off">
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="username" class="form-control" name="username" placeholder="Email address" required autofocus />
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="password" class="form-control" name="password" placeholder="Password" required="" />

        <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Sign in</button>
        <p class="pull-left">
            <a href="#">Lost your password?</a>
        </p>
    </form>
</div>