<div class="registration-container">
	<div class="span12">
		<h4><?php echo __('Latest created stock issues'); ?></h4>
		<ul>
			<?php 
			if($newIssues){
				foreach ($newIssues as $newIssue) {
					echo "<li><a href=\"". site_url("admin/stockissues/view/".$newIssue["stock_bill_id"]) ."\">" . sprintf(__("Add new stock issue to %s at %s"),$newIssue["saler_user_name"],formatLocalDatetime($newIssue["stock_bill_created_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest created stock receipts'); ?></h4>
		<ul>
			<?php 
			if($newReceipts){
				foreach ($newReceipts as $newReceipt) {
					$type = "";
					if($newReceipt["stock_bill_type"] == STOCK_RECEIPT_TYPE_RETURN){
						$type = "sale";
					}
					else{
						$type = "supplier";
					}
					echo "<li><a href=\"". site_url("admin/stockreceipts/view/".$newReceipt["stock_bill_id"]) ."\">" . sprintf(__("Add new stock receipt to %s at %s"),$newReceipt["object_name"],formatLocalDatetime($newReceipt["stock_bill_created_date"])) . "</a></li>";
				}
			}
			?>
		<h4><?php echo __('Latest cashes receipts'); ?></h4>	
		<ul>
			<?php
			if($lastCashReceipts){
				foreach ($lastCashReceipts as $cashReceipt) {
					echo "<li><a href=\"". site_url("admin/cashs/receipts") ."\">" . sprintf(__("Receive %s at %s"),$cashReceipt['cash_amount'],formatLocalDatetime($cashReceipt["cash_create_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest modified stock issues'); ?></h4>	
		<ul>
			<?php
			if($lastIssues){
				foreach ($lastIssues as $lastIssue) {
					echo "<li><a href=\"". site_url("admin/histories/stock/".$lastIssue["stock_bill_id"]) ."\">" . sprintf(__("Change stock issue to %s at %s"),$lastIssue["saler_user_name"],formatLocalDatetime($lastIssue["stock_bill_modified_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest modified stock receipts'); ?></h4>
		<ul>
			<?php 
			if($lastReceipts){
				foreach ($lastReceipts as $lastReceipt) {
					$type = "";
					if($lastReceipt["stock_bill_type"] == STOCK_RECEIPT_TYPE_RETURN){
						$type = "sale";
					}
					else{
						$type = "supplier";
					}
					echo "<li><a href=\"". site_url("admin/histories/stock/".$lastReceipt["stock_bill_id"]) ."\">" . sprintf(__("Change stock receipt to %s at %s"),$lastReceipt["object_name"],formatLocalDatetime($lastReceipt["stock_bill_modified_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest modified cashes receipts'); ?></h4>	
		<ul>
			<?php
			if($lastCashModified){
				foreach ($lastCashModified as $cashModified) {
					echo "<li><a href=\"". site_url("admin/cashs/receipts") ."\">" . sprintf(__("Change to %s at %s"),$cashModified['cash_amount'],formatLocalDatetime($cashModified["cash_modify_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest stock adjustment'); ?></h4>	
		<ul>
			<?php
			if($lastAdjustedStocks){
				foreach ($lastAdjustedStocks as $adjustedStock) {
					echo "<li><a href=\"". site_url("admin/products/history/".$adjustedStock["product_id"]) ."\">" . sprintf(__("Stock adjustment at %s"),formatLocalDatetime($adjustedStock["product_stock_history_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
		<h4><?php echo __('Latest price adjustment'); ?></h4>	
		<ul>
			<?php
			if($lastAdjustedPrices){
				foreach ($lastAdjustedPrices as $adjustedPrice) {
					echo "<li><a href=\"". site_url("admin/products/history/".$adjustedPrice["product_id"]) ."\">" . sprintf(__("Price adjustment at %s"),formatLocalDatetime($adjustedPrice["product_price_history_date"])) . "</a></li>";
				}
			}
			?>
		</ul>
	</div>
</div>