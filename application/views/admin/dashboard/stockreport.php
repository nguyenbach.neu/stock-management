<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Ngay (so lieu ton kho tai thoi diem ket thuc ngay duoc chon)");?></label>
                <div class='input-group date single_date' id='datetimepicker'>
                    <input type='text' class="form-control" name="date" value="<?php echo $this->input->get('date'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label><?php echo __("Supplier");?></label>
                <?php echo supplier_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<?php
$total = 0;
$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("Product name") . "</th>";
$html .= "<th>" . __("Product code") . "</th>";
$html .= "<th class='text-right'>" . __("package_quantity") . "</th>";
$html .= "<th class='text-right'>" . __("unit_quantity") . "</th>";
$html .= "<th style='padding-left: 50px'>" . __("Last Date Changed") . "</th>";
$html .= "<th class='text-right'>" . __("Amount") . "</th>";
$html .= "<th>" . __("Action") . "</th>";
$html .= "</tr>";
	if(!empty($reports)){
		foreach($reports as $k => $item) {

			$html .= '<tr>';
			$html .= '<td>'. $item['product_name'] .'</td>';
			$html .= '<td>'. $item['product_code'] .'</td>';
			$html .= '<td class="text-right">'. number_format($item['package_quantity'],NUMBER_INTEGER).'</td>';
            $html .= '<td class="text-right">'. $item['unit_quantity'] .'</td>';
			$html .= '<td style="padding-left: 50px">'. date('d/m/Y',strtotime($item['product_stock_history_date'])) .'</td>';
			$html .= '<td class="text-right">'. number_format($item['product_cost'] * $item['new_product_stock_quantity'],NUMBER_DECIMAL) .'</td>';
            $html .= '<td>'.'<a href="' . site_url("admin/products/history/".$item["product_id"]) .'" class="btn btn-info">'.__("view_history")."</a>".'</td>';
			$html .= '</tr>';
			
			$total += $item['product_cost'] * $item['new_product_stock_quantity'];
		}
	}

$html .= "<tr>";
$html .= "<th colspan=\"4\">" . __("Total") . "</th>";
$html .= "<th colspan=\"2\">" . number_format($total,NUMBER_DECIMAL) . "</th>";
$html .= "</tr>";
$html .= "</table>";
echo $html;
?>

<script type="text/javascript">
$(function() {
	var dates = $('#date').datepicker({
		defaultDate: "-2m",
		changeMonth: true,
		numberOfMonths: 3,
		onSelect: function(selectedDate) {
			var option = this.id == "from" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
});
</script>