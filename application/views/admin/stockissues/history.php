<?php 

?>
<?php

	echo "<h2>" . __("quantity_history") . "</h2>";
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("object") . "</th>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("product_code") . "</th>";
	$html .= "<th>" . __("product_price") . "</th>";
	$html .= "<th>" . __("product_quantity") . "</th>";
	$html .= "<th>" . __("reason") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	if(count($histories) > 0 && $histories){
		foreach($histories as $k => $item){
			$html .= "<tr>";
			$html .= "<td rowspan=\"" . ((count($item["childs"]) > 0) ? (count($item["childs"])+1) : 1) . "\">" . formatLocalDatetime($item["stock_bill_history_created_date"]) . "</td>";
			$html .= "</tr>";
			if(count($item["childs"]) > 0){
				foreach($item["childs"] as $l => $value){
					$html .= "<tr>";
					$html .= "<td>". getName($item["stock_bill_history_object_id"]) ."</td>";
					$html .= "<td>". $value["stock_bill_product_history_name"] ."</td>";
					$html .= "<td>". $value["stock_bill_product_history_code"] ."</td>";
					$html .= "<td class='text-right'>". $value["stock_bill_product_history_price"] ."</td>";
					$html .= "<td class='text-right'>". $value["stock_bill_product_history_quantity"] ."</td>";
					$html .= "<td></td>";
					$html .= "</tr>";
				}
			}
			else{
				$html .= "<tr><td colspan=\"5\"></tr>";
			}
		}
	}
	else{
		$html .= "<tr><td colspan=\"6\">". __("no_history") . "</td></tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
