<?php 
if($id == 0){
	echo __("stock issue deleted");
}
else {

?>
<form class="form-horizontal" method="post" action="">
    <div id="stock-issue-info" class="info-container">
        <div class="control-group text-left">
            <label class="control-label" style="text-align:left;"><?php echo __("Stock bill ID");?></label>
            <div class="controls" style="padding-top:5px;margin-bottom:5px;">PX-<?php echo $stock_issue["stock_bill_id"];?></div>
        </div>
        <div class="control-group no-print">
            <label class="control-label" style="text-align:left;"><?php echo __("date time");?></label>
            <div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo formatLocalDatetime($stock_issue["stock_bill_created_date"]);?></div>
        </div>
        <div class="control-group text-print-right">
            <label class="control-label" style="text-align:left;"><?php echo __("user_sale");?></label>
            <div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($stock_issue["saler_user_name"]);?></div>
        </div>
        <div class="control-group no-print">
            <label class="control-label" style="text-align:left;"><?php echo __("user_create");?></label>
            <div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($stock_issue["creater_user_name"]);?></div>
        </div>
    </div>
<?php
	$html = "";
	$totals = 0;
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("product_code") . "</th>";
	$html .= "<th>" . __("package_quantity") . "</th>";
	$html .= "<th>" . __("unit_quantity") . "</th>";
	$html .= "<th class='no-print'>" . __("product_price") . "</th>";
	$html .= "<th class='no-print' style=\"text-align:right\">" . __("total") . "</th>";
	$html .= "</tr>";
	if(count($products) > 0){
		foreach($products as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".$item["stock_bill_product_name"]."</td>";
			$html .= "<td>".$item["stock_bill_product_code"]."</td>";
			$html .= "<td>".number_format($item["package_quantity"],NUMBER_INTEGER)."</td>";
			$html .= "<td>".number_format($item["unit_quantity"],NUMBER_INTEGER)."</td>";
			$html .= "<td class='no-print'>".number_format($item["stock_bill_product_price"],NUMBER_DECIMAL)."</td>";
			$html .= "<td class='no-print' style=\"text-align:right\">".number_format(($item["stock_bill_product_price"]*$item["stock_bill_product_quantity"]),NUMBER_DECIMAL)."</td>";
			$html .= "</tr>";
			$totals = $totals + ($item["stock_bill_product_price"]*$item["stock_bill_product_quantity"]);
		}
		$html .= "<tr class='no-print'>";
		$html .= "<td colspan=\"4\" style=\"text-align:right\">". __("totals") ."</td>";
		$html .= "<td style=\"text-align:right\">". number_format($totals,NUMBER_DECIMAL) ."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	echo $html;

	if (count($stock_return_receipts)) {
?>

	<b><?php echo __("Stock Return Receipt");?></b><br />
	<table class="table table-hover table-striped">
<?php
		foreach ($stock_return_receipts as $stock_return_receipt) {
			echo "<tr class='no-print'>";
			echo "<td>PN-".$stock_return_receipt["stock_bill_id"]."</td>";
			echo "<td>".number_format($stock_return_receipt["totals"], NUMBER_DECIMAL)."</td>";
			echo "</tr>";
		}
?>
	</table>
<?php
	}

	if (count($cash_receipts)) {
?>

	<b><?php echo __("Cash Receipt");?></b><br />
	<table class="table table-hover table-striped">
<?php
		foreach ($cash_receipts as $cash_receipt) {
			echo "<tr class='no-print'>";
			echo "<td>PT-".$cash_receipt["cash_id"]."</td>";
			echo "<td>".number_format($cash_receipt["cash_amount"], NUMBER_DECIMAL)."</td>";
			echo "</tr>";
		}
?>
	</table>
<?php
	}

    if (count($discounts)) {
?>

    <b><?php echo __("Cash Discount");?></b><br />
    <table class="table table-hover table-striped no-print">
        <?php
        foreach ($discounts as $discount) {
            echo "<tr>";
            echo "<td>PT-".$discount["cash_id"]."</td>";
            echo "<td>".number_format($discount["cash_amount"], NUMBER_DECIMAL)."</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <?php
    }
    if (count($debits)) {
?>
        <b><?php echo __("Debit");?></b><br />
        <table class="table table-hover table-striped no-print">
            <?php
            foreach ($debits as $debit) {
                echo "<tr>";
                echo "<td>PT-".$debit["cash_id"]."</td>";
                echo "<td>".number_format($debit["cash_amount"], NUMBER_DECIMAL)."</td>";
                echo "</tr>";
            }
            ?>
        </table>
        <?php
    }
?>

    <div class="control-group">
		<label class="control-label"></label>
		<input type="hidden" name="stock_bill_id" value="<?php echo $stock_issue["stock_bill_id"];?>" />
		<div class="controls">
			<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
		</div>
	</div>
</form>


<?php } ?>