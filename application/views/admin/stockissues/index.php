<div class="panel panel-default">
    <div class="panel-heading text-right">
		<a class="btn btn-success" href="<?php echo site_url("admin/stockissues/add")?>"><?php echo __('Add new'); ?></a>
		<a class="btn btn-success" href="<?php echo site_url("admin/stockissues/archive_all")?>"><?php echo __('Archive all'); ?></a>
	</div>
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <div class="form-group">
                <label><?php echo __("Saler");?></label>
                <?php echo sale_user_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php
    echo '<div class="well">'.__('Tong') . ' : ' . number_format($sum['sum'],NUMBER_DECIMAL).'</div>';
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
    $html .= "<th>" . __("Ma") . "</th>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("user_sale") . "</th>";
	$html .= "<th class='text-right'>" . __("numbers of items") . "</th>";
	$html .= "<th class='text-right'>" . __("total amount") . "</th>";
	$html .= "<th>" . __("status") . "</th>";
	$html .= "<th>" . __("user_create") . "</th>";
	$html .= "<th class='text-right'>" . __("Balance") . "</th>";
	$html .= "<th class='text-center'>" . __("action") . "</th>";
	$html .= "</tr>";
	$total_balance = 0;
	if(count($stock_issues) >0 ){
		foreach($stock_issues as $k => $item){
			$html .= "<tr>";
            $html .= "<td>PX-".$item["stock_bill_id"]."</td>";
			$html .= "<td>".formatLocalDatetime($item["stock_bill_date"])."</td>";
			$html .= "<td>".$item["saler_user_name"]."</td>";
			$html .= "<td class='text-right'>". number_format($item["numbers_item"],NUMBER_INTEGER) ."</td>";
			$html .= "<td class='text-right'>". number_format($item["totals"],NUMBER_DECIMAL) ."</td>";
			$stock_issue_status = "";
			$proccess = "";
			$lock = false;
			$view = '<a href="' . site_url("admin/stockissues/view/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("view") . '</a>';
			$history = '<a href="' . site_url("admin/histories/stock/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("history") . '</a>';
			switch($item["stock_bill_status"]){
				case STOCK_CHANGE_STATUS_DRAFT: $stock_issue_status = __("draft");
					$proccess = '<a href="' . site_url("admin/stockissues/process/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("process") . '</a>';
					$lock = false;
					break;
				case STOCK_CHANGE_STATUS_PROCESSED: $stock_issue_status = __("processed"); $lock = true; break;
			}
			$html .= "<td>".$stock_issue_status."</td>";
			$html .= "<td>".$item["creater_user_name"]."</td>";

			$balance = $item["totals"]-$item["total_return"];
			$total_balance += $balance;
			$html .= "<td style='text-align: right'>".number_format($balance,NUMBER_DECIMAL)."</td>";
			$html .= "<td>" . $view .$history ;
			if(has_permission($this->session->userdata('user_role'),"stockissues","edit",$lock)){
				$html .= '<a href="' . site_url("admin/stockissues/edit/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("edit") . '</a>';
			}
			else {
				$html .= '<a href="javascript:void(0)" class="btn btn-info disabled">' . __("edit") . '</a>';
			}
			if ($item['stock_bill_archive']) {
				$html .= '<a href="' . site_url("admin/stockissues/unarchive/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("Un-archive") . '</a>';
			} else {
				$html .= '<a href="' . site_url("admin/stockissues/archive/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("Archive") . '</a>';
			}
			
			if(has_permission($this->session->userdata('user_role'),"stockissues","delete",$lock)){
			$html .= '<a href="javascript:void(0)" data-toggle="confirmation" class="btn btn-danger">' . __("delete") . '</a>';
			$html .= "<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/stockissues/delete/".$item["stock_bill_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>";
			}
			else{
				$html .= '<a href="javascript:void(0)" class="btn btn-danger disabled">' . __("delete") . '</a>';
			}
			$html .= "</td>";
			$html .= "</tr>";
		}
	}
	$html .= "<tr>";
	$html .= "<td>" . __("total") . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td>" . "</td>";
	$html .= "<td class='text-right'>" .number_format($total_balance,NUMBER_DECIMAL). "</td>";
	$html .= "<td>" . "</td>";
	$html .= "</tr>";
	$html .= "</table>";
	echo $html;
	echo $paginator;
?>

<script type="text/javascript">
	$(document).ready(function(){
		
	});
	function deleteUser(el){
			
	}
</script>