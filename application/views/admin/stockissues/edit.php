
		<form class="form-horizontal form-validation" id="stock_bill_form" method='post'>
		<fieldset>
			<legend><?php echo __("edit_stock_issues");?></legend>

			<input type="hidden" value="0" name="stock_issue_status" id="stock_issue_status" />
			<input type="hidden" value="<?php echo $stock_issue["stock_bill_id"];?>" name="stock_issue_id" id="stock_issue_id" />
			<input type="hidden" value="<?php echo $stock_issue["stock_bill_status"];?>" name="curr_stock_issue_status" id="curr_stock_issue_status" />
			<?php echo product_dropdown(false,"","clone_product",false,'display:none','non-select2')?>
			<div class="col-xs-6">
                <div>
                    <label class="control-label"><?php echo __("stock_issue_sale");?></label>
                    <div class="controls">
                        <?php echo sale_user_dropdown($stock_issue["stock_bill_object_id"]);?>
                    </div>
                </div>

                <div style="padding-top: 10px">
                    <label for="inputEmail"><?php echo __("Ngày");?></label>
                    <div class='input-group date start_date' id='datetimepicker1'>
                        <input type='text' class="form-control" name="created_date"
                               value="<?php echo $stock_issue['stock_bill_date']; ?>" required
                        />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                         </span>
                    </div>
                </div>
			</div>
			<div class="col-xs-6">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea rows="3" name="stock_issue_note" class="form-control"><?php echo $stock_issue["stock_bill_note"]?></textarea>
				</div>
			</div>
            <div style="clear: both; padding-bottom: 10px"></div>
			<div class="control-group">
				<label class="control-label"><?php echo __("products");?></label>
			</div>
			<div class="control-group">
				<input type="hidden" value="<?php echo count($stock_issue["products"]); ?>" name="total_item" id="total_item" />
				<table id="tbl_stock_bill_products" class="table table-hover">
					<thead>
						<tr>
							<th><?php echo __("order");?></th>
							<th><?php echo __("product_name");?></th>
							<th><?php echo __("product_price");?></th>
							<th><?php echo __("package_quantity");?></th>
							<th><?php echo __("unit_quantity");?> </th>;
							<th><?php echo __("action");?></th>
						</tr>
					</thead>
					<tbody>
						<?php if(count($stock_issue["products"]) > 0 ) { 
							foreach($stock_issue["products"] as $k => $item){
						?>
						<tr id="tr_row_<?php echo $k;?>">
							<td class="text-center"><?php echo ($k+1)?></td>
							<td><?php echo product_dropdown($item["product_id"],"product_id_".$k,"product_id_".$k,true)?></td>
							<td>
								<input type="hidden" class="hdstockproduct_id" id="stock_issue_product_id_<?php echo $k; ?>" name="stock_issue_product_id_<?php echo $k; ?>" value="<?php echo $item["stock_bill_product_id"];?>" />
                                <input type="hidden" class="hdproduct_code form-control" id="product_id_<?php echo $k; ?>" name="product_id_<?php echo $k; ?>" value="<?php echo $item["product_id"];?>" />
                                <input type="hidden" class="hdproduct_code form-control" id="product_code_<?php echo $k; ?>" name="product_code_<?php echo $k; ?>" value="<?php echo $item["stock_bill_product_code"];?>" />
								<input type="hidden" class="hdproduct_name form-control" id="product_name_<?php echo $k; ?>" name="product_name_<?php echo $k; ?>" value="<?php echo $item["stock_bill_product_name"];?>" />
                                <input type="hidden" class="hdproduct_cost form-control" id="product_cost_<?php echo $k; ?>" name="product_cost_<?php echo $k; ?>" value="<?php echo $item["stock_bill_product_cost"];?>" />
                                <input type="hidden" class="hdproduct_discount form-control" id="product_discount_<?php echo $k; ?>" name="product_discount_<?php echo $k; ?>" value="<?php echo $item["stock_bill_product_discount"];?>" />
								<input type="text" class="input-medium formatnumber form-control" id="product_price_<?php echo $k; ?>" name="product_price_<?php echo $k?>" rel="popover" data-content="Product price" data-original-title="Product price" readonly="readonly" value="<?php echo number_format($item["stock_bill_product_price"],NUMBER_DECIMAL);?>" /></td>
							<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_<?php echo $k?>" name="package_quantity_<?php echo $k?>" value="<?php echo $item["package_quantity"];?>" /></td>
							<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_<?php echo $k?>" name="unit_quantity_<?php echo $k?>" value="<?php echo $item["unit_quantity"]?>"/></td>
							<td><a class="btn btn-danger" onclick="removeRowTable('tr_row_<?php echo $k;?>')"><?php echo __("delete") ;?></a></td>
						</tr>
					<?php }} ?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5">
								<button type="button" class="btn btn-default pull-right" id="btn_add_row"><span class="glyphicon glyphicon-plus"></span> <?php echo __("add_row");?></button>
							</td>
					</tfoot>
				</table>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
				<?php if($stock_issue["stock_bill_status"] == 0) {?>
					<button type="submit" class="btn btn-default" id="btn_save_draft"><span class="glyphicon glyphicon-floppy-save"></span> <?php echo __("save_as_draft");?></button>
				<?php } ?>
					<button type="button" class="btn btn-primary" id="btn_process"><?php echo __("process");?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".sl_stock_issue_product").select2({
			width: 200
		});

		$("#btn_save_draft").click(function(){
			$("#stock_issue_status").attr("value",0);
			$('.select2-container').attr('style', 'display: block !important');
			$("#stock_bill_form").submit();
		});
		
		$("#btn_process").click(function(){
			$("#stock_issue_status").attr("value",1);
			$('.select2-container').attr('style', 'display: block !important');

			$("#stock_bill_form").submit();
			
		});


		$("#btn_add_row").click(function(){
			var name = $('#tbl_stock_bill_products tbody tr').length;
            var newRow='<tr id="tr_row_'+name+'">';
                newRow+='<td class="text-center">'+ parseInt(name + 1) +'</td>';
                newRow+='<td><select class="sl_stock_issue_product" name="product_id_'+name+'" id="product_id_'+name+'" title="Please choose product"></select></td>';
                newRow+='<td><input type="hidden"  class="hdproduct_name" name="product_name_'+name+'" id="product_name_'+name+'" /><input type="hidden"  class="hdproduct_code" name="product_code_'+name+'" id="product_code_'+name+'" /><input type="hidden"  class="hdproduct_cost" name="product_cost_'+name+'" id="product_cost_'+name+'" /><input type="hidden"  class="hdproduct_discount" name="product_discount_'+name+'" id="product_discount_'+name+'" />';
				newRow+='<input type="text" class="input-medium form-control" id="product_price_'+name+'" name="product_price_'+name+'" rel="popover" data-content="Product price" data-original-title="Product price" readonly="readonly" /></td>';
				newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_'+name+'" name="package_quantity_'+name+'" rel="popover" data-content="Package quantity" data-original-title="Package quantity" /></td>';
				newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_'+name+'" name="unit_quantity_'+name+'" rel="popover" data-content="Unit quantity" data-original-title="Unit quantity" /></td>';
                newRow+='<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_' + name + '")><?php echo __("delete")?></a></td>';      
                newRow += '</tr>';
            if($('#tbl_stock_bill_products tbody tr').length == 0){
				$('#tbl_stock_bill_products tbody').append(newRow);
				
			}
			else{
				$('#tbl_stock_bill_products tbody tr:last').after(newRow);
			}
			$('#clone_product option').clone().appendTo("#product_id_"+name);
			$("#product_id_"+name).val(0).attr("selected",true);
			$('#total_item').attr("value", (name+1));
			$("#product_id_" + name).select2({width: 200});
			$("#product_id_"+name).rules('add', { required: true,min:1 });
			$("#package_quantity_"+name).rules('add', { required: true,number:true });
			$("#unit_quantity_"+name).rules('add', { required: true,number:true });
			jQuery('.formatnumber').on('keyup', function(){
				price = jQuery(this).val();
				this.value = numberFormat(this.value);
			});
			$(".sl_stock_issue_product").change(function(){
				var curr = $(this);
				if($(this).val() != ""){
					$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
						var objJSON = eval("(function(){return " + data + ";})()");
						curr.parent().next("td").find(".hdproduct_code").val(objJSON.product_code);
						curr.parent().next("td").find(".hdproduct_name").val(objJSON.product_name);
                        curr.parent().next("td").find(".hdproduct_cost").val(objJSON.product_cost);
                        curr.parent().next("td").find(".hdproduct_discount").val(objJSON.product_discount);
						curr.parent().next("td").find("input[type='text']").val(numberFormat(objJSON.product_price));
					});
				}
			});
		});
		$(".sl_stock_issue_product").change(function(){
			var curr = $(this);
			if($(this).val() != ""){
				$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
					var objJSON = eval("(function(){return " + data + ";})()");
					curr.parent().next("td").find(".hdproduct_code").val(objJSON.product_code);
					curr.parent().next("td").find(".hdproduct_name").val(objJSON.product_name);
                    curr.parent().next("td").find(".hdproduct_cost").val(objJSON.product_cost);
                    curr.parent().next("td").find(".hdproduct_discount").val(objJSON.product_discount);
					curr.parent().next("td").find("input[type='text']").val(numberFormat(objJSON.product_price));
				});
			}
		});
	});
	function removeRowTable(t) {
        $("#" + t).remove();
            SetBangTaiLieu();
    }
    function SetBangTaiLieu() {
        var rowCount = $('#tbl_stock_bill_products tbody tr').length;
        if(rowCount == 0){
			$("#total_item").val(0);
		}
        for (var i = 0; i < rowCount; i++) {
            var indexID = $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html();            
            $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html(i+1);
            indexID = eval(indexID) - 1;			
            nsID = eval(i);
            $("#stock_issue_product_id_" + indexID).attr("name", "stock_issue_product_id_" + nsID);
            $("#stock_issue_product_id_" + indexID).attr("id", "stock_issue_product_id_" + nsID);

            $("#product_id_" + indexID).attr("name", "product_id_" + nsID);
            $("#product_id_" + indexID).attr("id", "product_id_" + nsID);

            $("#product_name_" + indexID).attr("name", "product_name_" + nsID);
            $("#product_name_" + indexID).attr("id", "product_name_" + nsID);

            $("#product_code_" + indexID).attr("name", "product_code_" + nsID);
            $("#product_code_" + indexID).attr("id", "product_code_" + nsID);

            $("#product_cost_" + indexID).attr("name", "product_cost_" + nsID);
            $("#product_cost_" + indexID).attr("id", "product_cost_" + nsID);

            $("#product_discount_" + indexID).attr("name", "product_discount_" + nsID);
            $("#product_discount_" + indexID).attr("id", "product_discount_" + nsID);

            $("#product_price_" + indexID).attr("name", "product_price_" + nsID);
            $("#product_price_" + indexID).attr("id", "product_price_" + nsID);

            $("#package_quantity_" + indexID).attr("name", "package_quantity_" + nsID);
			$("#package_quantity_" + indexID).attr("id", "package_quantity_" + nsID);
			
			$("#unit_quantity_" + indexID).attr("name", "unit_quantity_" + nsID);
			$("#unit_quantity_" + indexID).attr("id", "unit_quantity_" + nsID);
            $("#total_item").val(nsID+1);
        }
    }   
</script>