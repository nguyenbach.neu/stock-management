<?php
	$arr = array(
		'group_name' => '',
		'sort_order' => ''
	);
	if(isset($posts)){
		$arr['group_name'] = $posts['group_name'];
		$arr['sort_order'] = $posts['sort_order'];
	}
?>
<div class="span12">
	<form class="form-horizontal form-validation"  method='post'>
	<fieldset>
		<legend><?= __("Add new product group"); ?></legend>
		<div class="control-group">
			<label class="control-label"><?php echo __("Product Group Name"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="group_name" name="group_name" rel="popover"
                       data-content="Enter product group name" data-original-title="Product Group Name" value="<?php echo $arr['group_name'];?>" />
			</div>
		</div>


		<div class="control-group">
			<label class="control-label"><?= __("Order"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="sort_order" name="sort_order" rel="popover"
                       data-content="Enter Group Order" data-original-title="Order" value="<?php echo $arr['sort_order'];?>" />
			</div>
		</div> 

		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<button type="submit" class="btn btn-primary" ><?= __("Create Product Group");?></button>
				<button type="button" class="btn btn-default" id="btn_cancel">
                    <span class="glyphicon glyphicon-arrow-left"></span> <?= __('Cancel'); ?>
                </button>
			</div>
			<div class="control-group success">
				
			</div>
			<div class="control-group error">
				<!-- TODO -->
			</div>
		</div>
	</fieldset>
	</form>
</div>
