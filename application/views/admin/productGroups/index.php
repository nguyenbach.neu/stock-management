<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/product_groups/add")?>"><?php echo __('Add new'); ?></a>
    </div>
</div>
<div class="well">
    <span><?= __('Total:') ?> <?= number_format($sum,NUMBER_INTEGER) ?></span>
</div>
<table class="table table-hover table-striped">
    <tr>
        <th><?= __("Product Group") ?></th>
        <th><?= __("Order") ?></th>
        <th><?= __("Action") ?></th>
    </tr>
    <?php if(count($product_groups)): ?>
        <?php foreach ($product_groups as $group): ?>
            <tr>
                <td><?= $group['group_name'] ?></td>
                <td><?= $group['sort_order'] ?></td>
                <td>
                    <a href="<?= site_url("admin/product_groups/edit/".$group["id"]) ?>" class="btn btn-info"><?=__("Edit") ?></a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>
<?= $paginator ?>