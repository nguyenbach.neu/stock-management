<?php if($group): ?>
    <div class="span12">
        <form class="form-horizontal form-validation"  method='post'>
            <fieldset>
                <legend><?php echo __("Edit Product Group"); ?></legend>
                <input type="hidden" name="product_id" value="<?php echo $group["id"]; ?>" />
                <div class="control-group">
                    <label class="control-label"><?php echo __("Product Group Name"); ?></label>
                    <div class="controls">
                        <input type="text" class="form-control" id="group_name" name="group_name" rel="popover"
                               data-content="Enter product group name" data-original-title="Product Group Name" value="<?php echo $group['group_name'];?>" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?= __("Order"); ?></label>
                    <div class="controls">
                        <input type="text" class="form-control" id="sort_order" name="sort_order" rel="popover"
                               data-content="Enter Group Order" data-original-title="Order" value="<?php echo $group['sort_order'];?>" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <button type="submit" class="btn btn-primary" ><?= __("Edit Product Group");?></button>
                        <button type="button" class="btn btn-default" id="btn_cancel">
                            <span class="glyphicon glyphicon-arrow-left"></span> <?= __('Cancel'); ?>
                        </button>
                    </div>
                    <div class="control-group success">

                    </div>
                    <div class="control-group error">
                        <!-- TODO -->
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
<?php endif; ?>