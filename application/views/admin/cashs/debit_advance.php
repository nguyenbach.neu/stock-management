<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/cashs/add/debit_advance")?>"><?php echo __('Add Cash Debit Advance'); ?></a>
    </div>
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <div class="form-group">
                <label><?php echo __("Saler");?></label>
                <?php echo sale_user_dropdown(); ?>
            </div>
            <div class="form-group">
                <label><?php echo __("Phieu Xuat");?></label>
                <?php echo open_stock_bill_issues_dropdown(); ?>
            </div>				
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php
    echo '<div class="well">'.__('Tong') . ' : ' . number_format($sum['sum'],NUMBER_DECIMAL).'</div>';

	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("Stock issue") . "</th>";
	$html .= "<th class='text-right'>" . __("amount") . "</th>";
	$html .= "<th class='text-center'>" . __("note") . "</th>";
	$html .= "<th>" . __("user_create") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if(count($advance) > 0){
		foreach($advance as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".formatLocalDatetime($item["cash_date"])."</td>";
            $html .= "<td><a href=\"" . site_url("admin/cashs/edit/debit_advance/".$item["cash_id"]) . "\">PX - ".$item["stock_bill_id"]."( ".$item["saler_user_name"]." - ".formatLocalDatetime($item['stock_bill_created_date'])." )</a></td>";
			$html .= "<td class='text-right'>".number_format($item["cash_amount"],NUMBER_DECIMAL)."</td>";
			$html .= "<td>".$item["cash_note"]."</td>";
			$html .= "<td>".$item["creater_user_name"]."</td>";
			$html .= "<td>";
			if(has_permission($this->session->userdata('user_role'),"cashs","delete")){
			$html .= "<a href=\"javascript:void(0)\" data-toggle=\"confirmation\" class=\"btn btn-danger\">". __("delete") ."</a>
						<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/cashs/delete/debit_advance/".$item["cash_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>";
			}
			else {
				$html .= "<a href=\"javascript:void(0)\" class=\"btn btn-danger disabled\">". __("delete") ."</a>";
			}
			$html .= "</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
	echo $paginator;
?>