<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<div>
    <table class="table-responsive table">
        <tr>
            <td><?php echo __('Tong tien khuyen mai da nhan'); ?></td><td><?php echo number_format($discount_receipt['sum'], NUMBER_DECIMAL); ?></td><td><a href="<?php echo site_url("admin/cashs/discount_receipt?from=".$this->input->get('from').'&to='.$this->input->get('to')); ?>" class="btn btn-info"><?php echo __("Xem chi tiet"); ?></a></td>
        </tr>
        <tr>
            <td><?php echo __('Tong tien khuyen mai da tam ung'); ?></td><td><?php echo number_format($discount_advance['sum'], NUMBER_DECIMAL); ?></td><td><a href="<?php echo site_url("admin/cashs/discount_advance?from=".$this->input->get('from').'&to='.$this->input->get('to')); ?>" class="btn btn-info"><?php echo __("Xem chi tiet"); ?></a></td>
        </tr>
        <tr>
            <td><?php echo __('Da nhan - Da tam ung'); ?></td><td><?php echo number_format(($discount_receipt['sum'] - $discount_advance['sum']), NUMBER_DECIMAL); ?></td><td></td>
        </tr>
    </table>
</div>