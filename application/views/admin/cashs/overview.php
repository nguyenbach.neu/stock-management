<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Ngay (tai san thong ke tai thoi diem ket thuc ngay duoc chon)");?></label>
                <div class='input-group date single_date' id='datetimepicker'>
                    <input type='text' class="form-control" name="date" value="<?php echo $this->input->get('date'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<div>
    <table class="table-responsive table">
        <tr>
            <td><?php echo __('Tong so tien chuyen vao kinh doanh (1)'); ?></td><td><a href="<?php echo site_url("admin/cashs/load"); ?>"><?php echo number_format($total_load, NUMBER_DECIMAL); ?></a></td><td><a href="<?php echo site_url("admin/cashs/load"); ?>" class="btn btn-info"><?php echo __("Xem chi tiet"); ?></a></td>
        </tr>
        <tr>
            <td><?php echo __('Tong so tien rut ra khoi tai khoan (2)'); ?></td><td><a href="<?php echo site_url("admin/cashs/withdraw"); ?>"><?php echo number_format($total_withdraw, NUMBER_DECIMAL); ?></a></td><td><a href="<?php echo site_url("admin/cashs/withdraw"); ?>" class="btn btn-info"><?php echo __("Xem chi tiet"); ?></a></td>
        </tr>
        <tr>
            <td><?php echo __('Tong so tien luu thong (1-2)'); ?></td><td><?php echo number_format(($total_load - $total_withdraw), NUMBER_DECIMAL); ?></td><td></td>
        </tr>
    </table>
    <table class="table-responsive table">
        <tr>
            <td><?php echo __('Tien hang ton kho (1)'); ?></td><td><a href="<?php echo site_url("admin/reports/inventory"); ?>"><?php echo number_format($total_stock, NUMBER_DECIMAL); ?></a></td><td><a href="<?php echo site_url("admin/reports/inventory"); ?>" class="btn btn-info"><?php echo __("Xem chi tiet"); ?></a></td>
        </tr>
        <tr>
            <td><?php echo __('So du trong tai khoan tien mat (2)'); ?></td><td><a href="<?php echo site_url("admin/reports/accounts"); ?>"><?php echo number_format($total_account, NUMBER_DECIMAL); ?></a></td></td><td></td>
        </tr>

        <tr>
            <td><?php echo __('Cong no khach hang (3)'); ?></td><td><a href="<?php echo site_url("admin/reports/debts"); ?>"><?php echo number_format($customer_debit, NUMBER_DECIMAL); ?></a></td><td></td>
        </tr>

        <tr>
            <td><?php echo __('Tien giam gia ban hang (3)'); ?></td><td><a href="<?php echo site_url("admin/reports/discounts"); ?>"><?php echo number_format($discount, NUMBER_DECIMAL); ?></a></td><td></td>
        </tr>

        <tr>
            <td><?php echo __('Cong no nhan vien(3)'); ?></td><td><a href="<?php echo site_url("admin/stockissues"); ?>"><?php echo number_format($staff_debt, NUMBER_DECIMAL); ?></a></td><td></td>
        </tr>

        <tr>
            <td><?php echo __('So du trong tai khoan tra truoc (4)'); ?></td><td><a href="<?php echo site_url("admin/reports/accounts"); ?>"><?php echo number_format($total_credit, NUMBER_DECIMAL); ?></a></td><td></td>
        </tr>
        <tr>
            <td><?php echo __('So no trong tai khoan tra sau (5)'); ?></td><td><a href="<?php echo site_url("admin/reports/accounts"); ?>"><?php echo number_format($total_debit, NUMBER_DECIMAL); ?></a></td><td></td>
        </tr>
        <tr>
            <td><?php echo __('Tong tai san qui doi (1+2+3+4+5)'); ?></td><td><?php echo number_format(($total_stock +  $total_account + $staff_debt + $discount + $customer_debit + $total_credit + $total_debit), NUMBER_DECIMAL); ?></td><td></td>
        </tr>
    </table>
</div>