<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/cashs/add/debit_receipt")?>"><?php echo __('Add Cash Debit receipt'); ?></a>
    </div>
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <div class="form-group">
                <label><?php echo __("Bank account");?></label>
                <?php echo bank_account_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<?php
echo '<div class="well">'.__('Tong') . ' : ' . number_format($sum['sum'],NUMBER_DECIMAL).'</div>';

$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("date time") . "</th>";
$html .= "<th>" . __("bank_account") . "</th>";
$html .= "<th>" . __("Người bán") ."</th>";
$html .= "<th class='text-right'>" . __("amount") . "</th>";
$html .= "<th class='text-center'>" . __("note") . "</th>";
$html .= "<th>" . __("user_create") . "</th>";
$html .= "<th>" . __("action") . "</th>";
$html .= "</tr>";
if(count($receipt) > 0){
    foreach($receipt as $k => $item){
        $html .= "<tr>";
        $html .= "<td>".formatLocalDatetime($item["cash_date"])."</td>";
        $html .= "<td><a href=\"" . site_url("admin/cashs/edit/debit_receipt/".$item["cash_id"]) . "\">".$item["account_name"]."</a></td>";
        $html .= "<td>".$item["saler_user_name"]."</td>";
        $html .= "<td class='text-right'>".number_format($item["cash_amount"],NUMBER_DECIMAL)."</td>";
        $html .= "<td class='text-center'>".$item["cash_note"]."</td>";
        $html .= "<td>".$item["creater_user_name"]."</td>";
        $html .= "<td>";
        if(has_permission($this->session->userdata('user_role'),"cashs","delete")){
            $html .= "<a href=\"javascript:void(0)\" data-toggle=\"confirmation\" class=\"btn btn-danger\">". __("delete") ."</a>
						<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/cashs/delete/debit_receipt/".$item["cash_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>";
        }
        else {
            $html .= "<a href=\"javascript:void(0)\" class=\"btn btn-danger disabled\">". __("delete") ."</a>";
        }
        $html .= "</td>";
        $html .= "</tr>";
    }
}
$html .= "</table>";
echo $html;
echo $paginator;
?>