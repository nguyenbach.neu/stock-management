<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("add_new_cash_expenses");?></legend>
            <div class="control-group">
                <label for="created_date"><?php echo __("Ngày");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' id="created_date" class="form-control" name="created_date" value="" required/>
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
			<div class="control-group">
				<label class="control-label"><?php echo __("Bank account");?></label>
				<div class="controls">
					<?php echo bank_account_dropdown();?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("amount");?></label>
				<div class="controls">
					<input type="text" class="input-medium formatnumber form-control" required id="cash_amount" name="cash_amount" />
					(cac chi phi hoat dong ngoai luong nhu xang xe, sua chua,.....)
				</div>
			</div> 
			
			<div class="control-group">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea name="cash_note" rows="3" class="form-control"></textarea>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("create");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>
