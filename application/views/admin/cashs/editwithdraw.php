<?php 

if($withdraw){
?>
<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("edit_cash_withdraw");?></legend>
			<input type="hidden" name="cash_id" value="<?php echo $withdraw["cash_id"];?>" />
            <div class="control-group">
                <label for="created_date"><?php echo __("Ngày");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' id="created_date" class="form-control" name="created_date"
                           value="<?php echo $withdraw['cash_date'] ?>"
                        <?php if($this->session->userdata('user_role') != 1): echo 'readonly'; endif; ?>
                           required/>
                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                </div>
            </div>
			<div class="control-group">
				<label class="control-label"><?php echo __("Account");?></label>
				<div class="controls">
					<?php echo bank_account_dropdown($withdraw["account_id"]);?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("amount");?></label>
				<div class="controls">
					<input type="text" class="input-medium formatnumber form-control" required id="cash_amount" name="cash_amount" value="<?php echo number_format($withdraw["cash_amount"],NUMBER_DECIMAL);?>" />
				</div>
			</div> 
			<div class="control-group">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea name="cash_note" rows="3" class="form-control"><?php echo $withdraw["cash_note"];?></textarea>
				</div>
			</div>
			<input type="hidden" id="type" name="type" value="widthdraw" />
			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("change");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>

<?php } ?>