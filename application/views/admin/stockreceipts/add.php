		<form class="form-horizontal form-validation" id="stock_bill_form" method='post'>
		<fieldset>
			<legend><?php echo __("add_stock_receipt"); ?></legend>

			<input type="hidden" value="-1" name="stock_receipt_status" id="stock_receipt_status" />
			<input type="hidden" value="<?php echo (($type == "salers") ? STOCK_RECEIPT_TYPE_RETURN : STOCK_RECEIPT_TYPE_PURCHASE);?>" name="stock_receipt_type" id="stock_receipt_type" />
			<?php echo product_dropdown(false,"","clone_product",false,'display:none','non-select2')?>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <?php if($type == "salers") { ?>
                            <div class="col-xs-6">
                                <div>
                                    <label><?php echo __("Stock issue");?></label>
                                    <?php echo open_stock_bill_issues_dropdown();?>
                                </div>

                                <div style="padding-top: 10px">
                                    <label for="inputEmail"><?php echo __("Ngày");?></label>
                                    <div class='input-group date start_date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name="created_date" value="" />
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label><?php echo __("note");?></label>
                                <textarea class="form-control" rows="3" name="stock_receipt_note"></textarea>
                            </div>
                        <?php } else { ?>
                            <div class="col-xs-4">
                                <div>
                                    <label><?php echo __("supplier")?></label>
                                    <?php echo supplier_dropdown()?>
                                </div>

                                <div style="padding-top: 10px">
                                    <label for="inputEmail"><?php echo __("Ngày");?></label>
                                    <div class='input-group date start_date' id='datetimepicker1'>
                                        <input type='text' class="form-control" name="created_date" value="" required/>
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <label><?php echo __("Account");?></label>
                                <?php echo bank_account_dropdown();?>
                            </div>
                            <div class="col-xs-4">
                                <label><?php echo __("note");?></label>
                                <textarea class="form-control" rows="3" name="stock_receipt_note"></textarea>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

			<div class="control-group">
				<input type="hidden" value="1" name="total_item" id="total_item" />
				<table id="tbl_stock_bill_products" class="table table-hover">
					<thead>
						<tr>
							<th style="width:10%" class="text-center"><?php echo __("order");?></th>
							<th style="width:21%" class="text-center"><?php echo __("product_name");?></th>
                            <th style="width:10%" class="text-center discount-active"><?php echo __("Hàng khuyễn mãi");?></th>
							<th style="width:15%" class="text-center"><?php echo __("product_cost");?></th>
							<th style="width:12%" class="text-center"><?php echo __("package_quantity");?></th>
							<th style="width:12%" class="text-center"><?php echo __("unit_quantity");?></th>
							<th style="width:10%" class="text-center"><?php echo __("action");?></th>
						</tr>
					</thead>
					<tbody>
						<tr id="tr_row_0">
							<td class="text-center">1</td>
							<td><?php echo product_dropdown(false,"product_id_0","product_id_0")?></td>
                            <td style="text-align: center" class="discount-active">
                                <input class="discount" type="checkbox" id="discount_0" name="discount_0" value="1" />
                            </td>
							<td>
								<input type="hidden" class="hdproduct_code" id="product_code_0" name="product_code_0" />
								<input type="hidden" class="hdproduct_name" id="product_name_0" name="product_name_0" />
								<input type="text" title="Please re-choose product" class="input-medium product_cost form-control" id="product_cost_0" name="product_cost_0" rel="popover" data-content="Product cost" data-original-title="Product cost" <?php echo (($type == "salers") ? 'readonly="readonly"' :''); ?>  /></td>
							<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_0" name="package_quantity_0" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>
							<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_0" name="unit_quantity_0" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>
							<td><a class="btn btn-danger" onclick='removeRowTable("tr_row_0")'><?php echo __("delete")?></a></td>
						</tr>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                //var selected = '#product_id_0';
                                //$('#clone_product option').clone().appendTo(selected);
                                //$(selected).select2({width: 200});
                            });
                        </script>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<button type="button" class="btn btn-default pull-right" id="btn_add_row"><span class="glyphicon glyphicon-plus"></span> <?php echo __("add_row");?></button>
							</td>
					</tfoot>
				</table>
			</div>
			<div class="control-group text-center">
					<button type="submit" class="btn btn-default" id="btn_save_draft"><span class="glyphicon glyphicon-floppy-save"></span> <?php echo __("save_as_draft");?></button>
					<button type="button" class="btn btn-primary" id="btn_process"><?php echo __("process");?></button>
			</div>
		</fieldset>
		</form>

<script type="text/javascript">
	$(document).ready(function(){

		$(".sl_stock_issue_product").select2({
			width: 200
		});

		$("#btn_save_draft").click(function(){
			$("#stock_receipt_status").attr("value",0);
			$('.select2-container').attr('style', 'display: block !important');
			$("#stock_bill_form").submit();
		});
		$("#btn_process").click(function(){
			$("#stock_receipt_status").attr("value",1);
			$('.select2-container').attr('style', 'display: block !important');
			$("#stock_bill_form").submit();
		});
        $(".discount").change(function () {
            var curr = $(this);
            if ($(this).prop("checked"))
            {
                curr.parent().next("td").find("input[type='text']").val(0);
                curr.parent().next("td").find("input[type='text']").attr('readonly', true);
            } else {
                curr.parent().next("td").find("input[type='text']").attr('readonly', false);
            }
        });
        <?php if ($type == 'salers'){?>
            $('.discount-active').hide();
        <?php } ?>
		$("#btn_add_row").click(function(){
			var name = $('#tbl_stock_bill_products tbody tr').length;
            var newRow='<tr id="tr_row_'+name+'">';
                newRow+='<td class="text-center">'+ parseInt(name + 1) +'</td>';
                newRow+='<td><select class="sl_stock_receipt_product" name="product_id_'+name+'" id="product_id_'+name+'" title="Please choose product"></select></td>';
                <?php if ($type != 'salers'):?>
                    newRow+='<td style="text-align: center" class="discount-active"><input type="checkbox" class="discount" id="discount_'+name+'" name="discount_'+name+'" value="1"></td>';
                <?php endif; ?>
                newRow+='<td><input type="hidden"  class="hdproduct_name" name="product_name_'+name+'" id="product_name_'+name+'" /><input type="hidden"  class="hdproduct_code" name="product_code_'+name+'" id="product_code_'+name+'"  />';
				newRow+='<input type="text" class="input-medium product_cost form-control" id="product_cost_'+name+'" name="product_cost_'+name+'" rel="popover" data-content="Product price" data-original-title="Product price" <?php echo (($type == "salers") ? 'readonly="readonly"' :''); ?> /></td>';
                newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_'+name+'" name="package_quantity_'+name+'" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>';
				newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_'+name+'" name="unit_quantity_'+name+'" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>';
                newRow+='<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_' + name + '")><?php echo __("delete")?></a></td>';      
                newRow += '</tr>';
			if($('#tbl_stock_bill_products tbody tr').length == 0){
				$('#tbl_stock_bill_products tbody').append(newRow);
			}
			else{
				$('#tbl_stock_bill_products tbody tr:last').after(newRow);
			}
			$('#clone_product option').clone().appendTo("#product_id_"+name);
			$('#total_item').attr("value", (name+1));
			$("#product_id_" + name).val("0").attr("selected",true);
			$("#product_id_" + name).select2({width: 200});
			$("#product_id_"+name).rules('add', { required: true,min:1 });
			$("#package_quantity_"+name).rules('add', { required: true, number:true });
			$("#unit_quantity_"+name).rules('add', { required: true, number:true });
			jQuery('.formatnumber').on('keyup', function(){
				price = jQuery(this).val();
				this.value = numberFormat(this.value);
			});
            $(".discount").change(function () {
                var curr = $(this);
                if ($(this).prop("checked"))
                {
                    curr.parent().next("td").find("input[type='text']").val(0);
                    curr.parent().next("td").find("input[type='text']").attr('readonly', true);
                } else {
                    curr.parent().next("td").find("input[type='text']").attr('readonly', false);
                }
            });
			$(".sl_stock_receipt_product").change(function(){
				var curr = $(this);
				if($(this).val() != ""){
					$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
						var objJSON = eval("(function(){return " + data + ";})()");
						curr.parent().nextAll().slice(0,2).find(".hdproduct_code").val(objJSON.product_code);
						curr.parent().nextAll().slice(0,2).find(".hdproduct_name").val(objJSON.product_name);
						if($("#stock_receipt_type").val() == 1){
							curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_price));
						}
						else {
							curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_cost));
						}
					});
				}
			});
		});
		$(".sl_stock_issue_product").change(function(){
			var curr = $(this);
			if($(this).val() != ""){
				$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
					var objJSON = eval("(function(){return " + data + ";})()");
					curr.parent().nextAll().slice(0,2).find(".hdproduct_code").val(objJSON.product_code);
					curr.parent().nextAll().slice(0,2).find(".hdproduct_name").val(objJSON.product_name);
					if($("#stock_receipt_type").val() == 1){
						curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_price));
					}
					else {
						curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_cost));
					}
				});
			}
		});
	});
	function removeRowTable(t) {
        $("#" + t).remove();
            SetBangTaiLieu();
    }
    function SetBangTaiLieu() {
        var rowCount = $('#tbl_stock_bill_products tbody tr').length;
        if(rowCount == 0){
			$("#total_item").val(0);
		}
        for (var i = 0; i < rowCount; i++) {
            var indexID = $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html();            
            $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html(i+1);
            indexID = eval(indexID) - 1;			
            nsID = eval(i);
            $("#product_id_" + indexID).attr("name", "product_id_" + nsID);
            $("#product_id_" + indexID).attr("id", "product_id_" + nsID);

            $("#product_name_" + indexID).attr("name", "product_name_" + nsID);
            $("#product_name_" + indexID).attr("id", "product_name_" + nsID);

            $("#discount_" + indexID).attr("name", "discount_" + nsID);
            $("#discount_" + indexID).attr("id", "discount_" + nsID);

            $("#product_code_" + indexID).attr("name", "product_code_" + nsID);
            $("#product_code_" + indexID).attr("id", "product_code_" + nsID);

            $("#product_cost_" + indexID).attr("name", "product_cost_" + nsID);
            $("#product_cost_" + indexID).attr("id", "product_cost_" + nsID);

            $("#package_quantity_" + indexID).attr("name", "package_quantity_" + nsID);
            $("#package_quantity_" + indexID).attr("id", "package_quantity_" + nsID);
            
			$("#unit_quantity_" + indexID).attr("name", "unit_quantity_" + nsID);
			$("#unit_quantity_" + indexID).attr("id", "unit_quantity_" + nsID);
            $("#total_item").val(nsID+1);
        }
    }   
</script>