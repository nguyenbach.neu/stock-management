<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>

            <div class="form-group">
                <label><?php echo __("Supplier");?></label>
                <?php echo supplier_dropdown(); ?>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php
$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("product_code") . "</th>";
$html .= "<th style='width: 15%'>" . __("product_name") . "</th>";
$html .= "<th class='text-right' style='width: 30%'>" . __("product_quantity") . "</th>";
$html .= "<th class='text-right'>" . __("total amount") . "</th>";
$html .= "</tr>";
$total = array('quantity' => 0, 'price' => 0);
if(count($reports) >0 ){
	foreach($reports as $k => $item){
		$html .= "<tr>";
		$html .= "<td>".$item["stock_bill_product_code"]."</td>";
		$html .= "<td>".$item["stock_bill_product_name"]."</td>";
		$html .= "<td class='text-right'>".$item['product_quantity']."</td>";
		$html .= "<td class='text-right'>".number_format(($item["totals_price"]),NUMBER_DECIMAL)."</td>";
		$html .= "</tr>";
		$total['quantity'] = ($total['quantity'] + $item["totals_quantity"]);
		$total['price'] = ($total['price'] + $item["totals_price"]);
	}
	$html .= '<tr><td>'. __('Total') .'</td>';
	$html .= '<td>'. count($reports) . ((count($reports) > 1) ? " products" : " product") . '</td>';
	$html .= '<td></td>';
	$html .= '<td class="text-right">'. number_format(($total['price']),NUMBER_DECIMAL) .'</td>';
	$html .= '</tr>';
}
$html .= "</table>";
echo $html;
echo $paginator;
?>