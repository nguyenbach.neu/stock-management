<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation" id="stock_bill_form" method='post'>
		<fieldset>
			<legend><?php echo __("edit_receipt_issues");?></legend>
			<input type="hidden" value="-1" name="stock_receipt_status" id="stock_receipt_status" />
			<input type="hidden" value="<?php echo $stock_receipt["stock_bill_id"];?>" name="stock_receipt_id" id="stock_receipt_id" />
			<input type="hidden" value="<?php echo $stock_receipt["stock_bill_status"];?>" name="curr_stock_receipt_status" id="curr_stock_receipt_status" />
			<?php echo product_dropdown(false,"","clone_product",false,'display:none','non-select2')?>
			<div class="control-group">
				<label class="control-label"><?php echo __("type");?></label>
				<div class="controls">
					<?php echo stock_receipt_type_dropdown($stock_receipt["stock_bill_type"],true);?>
				</div>
			</div>

            <?php if($stock_receipt["stock_bill_type"] == 2): ?>
                <div class="col-xs-6">
                    <div>
                        <label class="control-label"><?php echo __("supplier") ?></label>
                        <div class="controls"><?php echo supplier_dropdown($stock_receipt["stock_bill_object_id"]) ?></div>
                    </div>

                    <div>
                        <label class="control-label"><?php echo __("Account Payment") ?></label>
                        <div class="controls">
                            <?php echo bank_account_dropdown($stock_receipt["stock_bill_account_id"]) ?>
                        </div>
                    </div>

                    <div style="padding-top: 10px">
                        <label for="inputEmail"><?php echo __("Ngày");?></label>
                        <div class='input-group date start_date' id='datetimepicker1'>
                            <input type='text' class="form-control" name="created_date" value="<?php echo $stock_receipt['stock_bill_created_date']; ?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            <?php elseif ($stock_receipt["stock_bill_type"] == 1): ?>
                <div class="col-xs-6">
                    <div>
                        <label class="control-label"><?php echo __("Stock issue"); ?> </label>
                        <div class="controls"><?php echo open_stock_bill_issues_dropdown($stock_receipt["stock_bill_object_id"])?> </div>
                    </div>
                    <div style="padding-top: 10px">
                        <label for="inputEmail"><?php echo __("Ngày");?></label>
                        <div class='input-group date     start_date' id='datetimepicker1'>
                            <input type='text' class="form-control" name="created_date"
                                   value="<?php echo $stock_receipt['stock_bill_date']; ?>"
                                <?php if($this->session->userdata('user_role') != 1): echo 'readonly'; endif; ?>
                                   required
                            />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
			
			<div class="col-xs-6">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea rows="3" name="stock_receipt_note" class="form-control"><?php echo $stock_receipt["stock_bill_note"];?></textarea>
				</div>
			</div>

            <div style="clear: both; padding-bottom: 10px"></div>

			<div class="control-group">
				<label class="control-label"><?php echo __("products");?></label>
			</div>
			<div class="control-group">
				<input type="hidden" value="<?php echo (count($stock_receipt["products"]));?>" name="total_item" id="total_item" />
				<table id="tbl_stock_bill_products" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center"><?php echo __("order");?></th>
							<th class="text-center"><?php echo __("product_name");?></th>
                            <th style="width:10%" class="text-center discount-active"><?php echo __("Hàng khuyễn mãi");?></th>
							<th class="text-center"><?php echo __("product_cost");?></th>
							<th class="text-center"><?php echo __("package_quantity");?></th>
							<th class="text-center"><?php echo __("unit_quantity");?></th>
							<th class="text-center"><?php echo __("action");?></th>
						</tr>
					</thead>
					<tbody>
						<?php
							if(count($stock_receipt["products"]) > 0){
								$html = "";
								foreach($stock_receipt["products"] as $k => $item){
									$html .= '<tr id="tr_row_'.$k.'">
										<td class="text-center">'.($k+1).'</td>
										<td>'. product_dropdown($item["product_id"],"product_id_x_$k","product_id_x_$k",true) . '</td>
										<td style="text-align: center" class="discount-active"><input class="discount" type="checkbox" id="discount_0" name="discount_0" value="1" /></td>
										<td>
											<input type="hidden" class="hdstockproduct_id" id="stock_bill_product_id_'.$k.'" name="stock_bill_product_id_'.$k.'" value="' . $item["stock_bill_product_id"] . '" />
											<input type="hidden" class="hdproduct_id" id = "product_id_'.$k.'" name="product_id_'.$k.'" value="' . $item["product_id"] . '" />
											<input type="hidden" class="hdproduct_code" id = "product_code_'.$k.'" name="product_code_'.$k.'" value="'. $item["stock_bill_product_code"] .'" />
											<input type="hidden" class="hdproduct_name" id = "product_name_'.$k.'" name="product_name_'.$k.'" value="'. $item["stock_bill_product_name"] .'" />
											<input type="text" class="input-medium product_cost form-control" id="product_cost_'.$k.'" name="product_cost_'.$k.'"  value="'. number_format($item["stock_bill_product_price"],NUMBER_DECIMAL) .'"'. (($stock_receipt["stock_bill_type"] == STOCK_RECEIPT_TYPE_RETURN)?' readonly="readonly"':'').' />
										</td>
										<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_'.$k.'" name="package_quantity_'.$k.'" value="'. $item["package_quantity"] .'" /></td>
										<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_'.$k.'" name="unit_quantity_'.$k.'" value="'. $item["unit_quantity"] .'" /></td>
										<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_'.$k.'")>'. __("delete") .'</a></td>
									</tr>';
								}
								echo $html;
							}
						?>
						
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<button type="button" class="btn btn-default pull-right" id="btn_add_row"><span class="glyphicon glyphicon-plus"></span> <?php echo __("add_row");?></button>
							</td>
					</tfoot>
				</table>
			</div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
				<?php if($stock_receipt["stock_bill_status"] == 0) {?>
					<button type="submit" class="btn btn-default" id="btn_save_draft"><span class="glyphicon glyphicon-floppy-save"></span> <?php echo __("save_as_draft");?></button>
				<?php } ?>
					<button type="button" class="btn btn-primary" id="btn_process"><?php echo __("process");?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$(".sl_stock_issue_product").select2({
			width: 200
		});

		$(".stock_receipt_type").change(function(){
			$(".product_cost").val("0");
			$(".sl_stock_issue_product").val("").attr("selected",true);
			if(parseInt($(this).val()) === 0){
				$("#dvChooseType label").text('<?php echo __("supplier")?>');
				$("#dvChooseType .controls").html('<?php echo supplier_dropdown()?>');
			}
			else if(parseInt($(this).val()) === 1){
				$("#dvChooseType label").text('<?php echo __("saler")?>');
				$("#dvChooseType .controls").html('<?php echo sale_user_dropdown()?>');
			}
		});
		$("#btn_save_draft").click(function(){
			$("#stock_receipt_status").attr("value",0);
			$('.select2-container').attr('style', 'display: block !important');
			$("#stock_bill_form").submit();
		});
		$("#btn_process").click(function(){
			$("#stock_receipt_status").attr("value",1);
			$('.select2-container').attr('style', 'display: block !important');
			$("#stock_bill_form").submit();
		});
        $(".discount").change(function () {
            var curr = $(this);
            if ($(this).prop("checked"))
            {
                curr.parent().next("td").find("input[type='text']").val(0);
                curr.parent().next("td").find("input[type='text']").attr('readonly', true);
            } else {
                curr.parent().next("td").find("input[type='text']").attr('readonly', false);
            }
        });
        $("#btn_add_row").click(function(){
            var name = $('#tbl_stock_bill_products tbody tr').length;
            var newRow='<tr id="tr_row_'+name+'">';
            newRow+='<td class="text-center">'+ parseInt(name + 1) +'</td>';
            newRow+='<td><select class="sl_stock_receipt_product" name="product_id_'+name+'" id="product_id_'+name+'" title="Please choose product"></select></td>';
            newRow+='<td style="text-align: center" class="discount-active"><input type="checkbox" class="discount" id="discount_'+name+'" name="discount_'+name+'" value="1"></td>';
            newRow+='<td><input type="hidden"  class="hdproduct_name" name="product_name_'+name+'" id = "product_name_'+name+'" /><input type="hidden"  class="hdproduct_code" name="product_code_'+name+'" id = "product_code_'+name+'" />';
            newRow+='<input type="text" title="Please re-choose product" class="input-medium product_cost form-control" id="product_cost_'+name+'" name="product_cost_'+name+'" rel="popover" data-content="Product price" data-original-title="Product price" /></td>';
            newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="package_quantity_'+name+'" name="package_quantity_'+name+'" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>';
            newRow+='<td><input type="text" class="input-medium formatnumber form-control" id="unit_quantity_'+name+'" name="unit_quantity_'+name+'" rel="popover" data-content="Product quantity" data-original-title="Product quantity" /></td>';
            newRow+='<td><a class="btn btn-danger" onclick=removeRowTable("tr_row_' + name + '")><?php echo __("delete")?></a></td>';
            newRow += '</tr>';
            if($('#tbl_stock_bill_products tbody tr').length == 0){
                $('#tbl_stock_bill_products tbody').append(newRow);

            }
            else{
                $('#tbl_stock_bill_products tbody tr:last').after(newRow);
            }
            $('#clone_product option').clone().appendTo("#product_id_"+name);
            $('#total_item').attr("value", (name+1));
            $("#product_id_" + name).val("0").attr("selected",true);
            $("#product_id_" + name).select2({width: 200});
            $("#product_id_"+name).rules('add', { required: true,min:1 });
            // $("#product_cost_"+name).rules('add', { min:1 });
            $("#package_quantity_"+name).rules('add', { required: true,number:true });
            $("#unit_quantity_"+name).rules('add', { required: true,number:true });
            jQuery('.formatnumber').on('keyup', function(){
                price = jQuery(this).val();
                this.value = numberFormat(this.value);
            });
            $(".discount").change(function () {
                var curr = $(this);
                if ($(this).prop("checked"))
                {
                    curr.parent().next("td").find("input[type='text']").val(0);
                    curr.parent().next("td").find("input[type='text']").attr('readonly', true);
                } else {
                    curr.parent().next("td").find("input[type='text']").attr('readonly', false);
                }
            });
            $(".sl_stock_receipt_product").change(function(){
                var curr = $(this);
                if($(this).val() != ""){
                    $.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
                        var objJSON = eval("(function(){return " + data + ";})()");
                        curr.parent().nextAll().slice(0,2).find(".hdproduct_code").val(objJSON.product_code);
                        curr.parent().nextAll().slice(0,2).find(".hdproduct_name").val(objJSON.product_name);
                        if($("#stock_receipt_type").val() == 1){
                            curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_price));
                        }
                        else {
                            curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_cost));
                        }
                    });
                }
            });
        });
		$(".sl_stock_issue_product").change(function(){
			var curr = $(this);
			if($(this).val() != ""){
				$.post('<?php echo site_url("admin/products/ajaxInfoProduct")?>',{"product_id":$(this).val()},function(data){
					var objJSON = eval("(function(){return " + data + ";})()");
                    curr.parent().nextAll().slice(0,2).find(".hdproduct_code").val(objJSON.product_code);
                    curr.parent().nextAll().slice(0,2).find(".hdproduct_name").val(objJSON.product_name);
                    if($("#stock_receipt_type").val() == 1){
                        curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_price));
                    }
                    else {
                        curr.parent().nextAll().slice(0,2).find("input[type='text']").val(numberFormat(objJSON.product_cost));
                    }
				});
			}
		});
	});
    <?php if ($stock_receipt["stock_bill_type"] == 1){ ?>
        $('.discount-active').hide();
    <?php } ?>
	function removeRowTable(t) {
        $("#" + t).remove();
            SetBangTaiLieu();
    }
    function SetBangTaiLieu() {
        var rowCount = $('#tbl_stock_bill_products tbody tr').length;
        if(rowCount == 0){
			$("#total_item").val(0);
		}
        for (var i = 0; i < rowCount; i++) {
            var indexID = $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html();           
            $('#tbl_stock_bill_products tbody').children().eq(i).children().eq(0).html(i+1);
            indexID = eval(indexID) - 1;			
            nsID = eval(i);
            $("#stock_bill_product_id_" + indexID).attr("name", "stock_bill_product_id_" + nsID);
            $("#stock_bill_product_id_" + indexID).attr("id", "stock_bill_product_id_" + nsID);

            $("#product_id_" + indexID).attr("name", "product_id_" + nsID);
            $("#product_id_" + indexID).attr("id", "product_id_" + nsID);

            $("#product_name_" + indexID).attr("name", "product_name_" + nsID);
            $("#product_name_" + indexID).attr("id", "product_name_" + nsID);

            $("#product_code_" + indexID).attr("name", "product_code_" + nsID);
            $("#product_code_" + indexID).attr("id", "product_code_" + nsID);

            $("#product_cost_" + indexID).attr("name", "product_cost_" + nsID);
            $("#product_cost_" + indexID).attr("id", "product_cost_" + nsID);

            $("#package_quantity_" + indexID).attr("name", "package_quantity_" + nsID);
			$("#package_quantity_" + indexID).attr("id", "package_quantity_" + nsID);
			
			$("#unit_quantity_" + indexID).attr("name", "unit_quantity_" + nsID);
			$("#unit_quantity_" + indexID).attr("id", "unit_quantity_" + nsID);
            $("#total_item").val(nsID+1);
        }
    }   
</script>