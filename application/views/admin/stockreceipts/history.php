<?php 

?>
<?php
if(count($histories) > 0 && $histories){
	echo "<h2>" . __("quantity_history") . "</h2>";
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("preview_quantity") . "</th>";
	$html .= "<th>" . __("new_quantity") . "</th>";
	$html .= "<th>" . __("reason") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	
	foreach($histories as $k => $item){
		$reason = "";
		$product_name = "";
		switch($item["stock_issue_message"]){
			case STOCK_ISSUE_HISTORY_TYPE_CHANGE_STOCK: $reason = __('Change stock issue detail'); break;
			case STOCK_ISSUE_HISTORY_TYPE_CHANGE_PRODUCT: 
				$reason = __('Change product record quantity');
				$product_name = $item["stock_receipt_product_name"];
			break;
			case STOCK_ISSUE_HISTORY_TYPE_ADD_PRODUCT: 
				$reason = sprintf(__('add_product_with_quantity'),$item["stock_receipt_product_name"],$item["stock_receipt_product_quantity"]);
				$product_name = $item["stock_receipt_product_name"];
			break;
			case STOCK_ISSUE_HISTORY_TYPE_DELETE_PRODUCT: 
				$reason = sprintf(__('delete_product_with_quantity'),$item["product_name"],$item["before_quantity"]);
				$product_name = $item["product_name"];
			break;
		}
		$html .= "<tr>";
		$html .= "<td>".$product_name."</td>";
		$html .= "<td class='text-right'>".number_format($item["before_quantity"],NUMBER_INTEGER)."</td>";
		$html .= "<td class='text-right'>".number_format($item["after_quantity"],NUMBER_INTEGER)."</td>";
		$html .= "<td>$reason</td>";
		$html .= "<td></td>";
		$html .= "</tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
}