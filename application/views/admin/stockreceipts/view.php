<?php 
if($id == 0){
	echo __("stock receipt deleted");
}
else {
?>
<form class="form-horizontal" method="post" action="">
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("Stock bill ID");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;">PN-<?php echo $stock["stock_bill_id"];?></div>
	</div>
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("date time");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo formatLocalDatetime($stock["stock_bill_created_date"]);?></div>
	</div>
	<div class="control-group">
		<?php if($stock["stock_bill_type"] == STOCK_RECEIPT_TYPE_PURCHASE):?>
		<label class="control-label" style="text-align:left;"><?php echo __("supplier");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($stock["supplier_name"]);?></div>
		<?php else : ?>
		<label class="control-label" style="text-align:left;"><?php echo __("user_sale");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($stock["saler_user_name"]);?></div>
		<?php endif; ?>
	</div>
	<div class="control-group">
		<label class="control-label" style="text-align:left;"><?php echo __("user_create");?></label>
		<div class="controls" style="padding-top:5px;margin-bottom:5px;"><?php echo ($stock["creater_user_name"]);?></div>
	</div>

<?php
	$html = "";
	$totals = 0;
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("product_code") . "</th>";
	$html .= "<th>" . __("package_quantity") . "</th>";
	$html .= "<th>" . __("unit_quantity") . "</th>";
	$html .= "<th>" . __("product_cost") . "</th>";
	$html .= "<th>" . __("total") . "</th>";
	$html .= "</tr>";
	if(count($products) > 0){
		foreach($products as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".$item["stock_bill_product_name"]."</td>";
			$html .= "<td>".$item["stock_bill_product_code"]."</td>";
			$html .= "<td>".number_format($item["package_quantity"],NUMBER_INTEGER)."</td>";
			$html .= "<td>".number_format($item["unit_quantity"],NUMBER_INTEGER)."</td>";
			$html .= "<td>".number_format($item["stock_bill_product_price"],NUMBER_DECIMAL)."</td>";
			$html .= "<td>".number_format(($item["stock_bill_product_price"]*$item["stock_bill_product_quantity"]),NUMBER_DECIMAL)."</td>";
			$html .= "</tr>";
			$totals = $totals + ($item["stock_bill_product_price"]*$item["stock_bill_product_quantity"]);
		}
		$html .= "<tr>";
		$html .= "<td colspan=\"4\" style=\"text-align:right\">". __("totals") ."</td>";
		$html .= "<td>". number_format($totals,NUMBER_DECIMAL) ."</td>";
		$html .= "</tr>";
	}
	$html .= "</table>";
	echo $html;
?>
	<div class="control-group">
		<label class="control-label"></label>
		<input type="hidden" name="stock_bill_id" value="<?php echo $stock["stock_bill_id"];?>" />
		<div class="controls">
		<?php if($stock["stock_bill_status"] == STOCK_CHANGE_STATUS_DRAFT) :?>
			<button type="submit" class="btn btn-primary" id="btn_process"><?php echo __("process");?></button>
		<?php endif; ?>
			<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
		</div>
	</div>
</form>

<?php } ?>