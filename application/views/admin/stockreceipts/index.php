<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/stockreceipts/add/salers")?>"><?php echo __('Add new for sale personals'); ?></a>
        <a class="btn btn-success" href="<?php echo site_url("admin/stockreceipts/add/suppliers")?>"><?php echo __('Add new for suppliers'); ?></a>
    </div>
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Kieu phieu nhap");?></label>
                <div class='input-group'>
                    <select required="" class="form-control" name="type"><option value="">--</option><option value="<?php echo STOCK_RECEIPT_TYPE_RETURN;?>"><?php echo __("Nhap lai kho");?></option><option value="<?php echo STOCK_RECEIPT_TYPE_PURCHASE; ?>"><?php echo __("Tu nha san xuat");?></option></select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Nha cung cap");?></label>
                <div class='input-group'>
                    <?php echo supplier_dropdown() ?>
                </div>
            </div>

            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>

<?php
    echo '<div class="well">'.__('Tong') . ' : ' . number_format($sum['sum'],NUMBER_DECIMAL).'</div>';
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
    $html .= "<th>" . __("Ma") . "</th>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("Ten") . "</th>";
	$html .= "<th class='text-right'>" . __("package_quantity") . "</th>";
	$html .= "<th class='text-right'>" . __("total amount") . "</th>";
	$html .= "<th class='text-center'>" . __("note") . "</th>";
	$html .= "<th>" . __("status") . "</th>";
	$html .= "<th>" . __("user_create") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if(count($stock_receipts) >0 && $stock_receipts){
		foreach($stock_receipts as $k => $item){
			$stock_receipt_status = "";
			$stock_receipt_type = "";
			$person = "";
			$lock = false;
			switch($item["stock_bill_status"]){
				case STOCK_CHANGE_STATUS_DRAFT: $stock_receipt_status = __("draft");break;
				case STOCK_CHANGE_STATUS_PROCESSED: $stock_receipt_status = __("processed"); $lock = true; break;
			}

			$html .= "<tr>";
            $html .= "<td>PN-".$item["stock_bill_id"]."</td>";
			$html .= "<td>".formatLocalDatetime($item["stock_bill_date"])."</td>";
			$html .= "<td>".$item["object_name"]."</td>";
			$html .= "<td class='text-right'>". number_format($item["package_quantity"],NUMBER_DECIMAL) ."</td>";
			$html .= "<td class='text-right'>". number_format($item["totals"],NUMBER_DECIMAL) ."</td>";
			$html .= "<td>".$item["stock_bill_note"]."</td>";
			
			$edit = '<a href="' . site_url("admin/stockreceipts/edit/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("edit") . '</a>';
			$view = '<a href="' . site_url("admin/stockreceipts/view/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("view") . '</a>';
			$history = '<a href="' . site_url("admin/histories/stock/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("history") . '</a>';
			$delete = '<a href="javascript:void(0)" data-toggle="confirmation" class="btn btn-danger">' . __("delete") . '</a>';
			$html .= "<td>".$stock_receipt_status."</td>";
			$html .= "<td>".$item["creater_user_name"]."</td>";
			$html .= "<td>" . $view . $history;
			if(has_permission($this->session->userdata('user_role'),"stockreceipts","edit",$lock)){
				$html .= '<a href="' . site_url("admin/stockreceipts/edit/".$item["stock_bill_id"]) . '" class="btn btn-info">' . __("edit") . '</a>';
			}
			else {
				$html .= '<a href="javascript:void(0)" class="btn btn-info disabled">' . __("edit") . '</a>';
			}
			if(has_permission($this->session->userdata('user_role'),"stockreceipts","delete",$lock)){
			$html .= '<a href="javascript:void(0)" data-toggle="confirmation" class="btn btn-danger">' . __("delete") . '</a>';
			$html .= "<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/stockreceipts/delete/".$item["stock_bill_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>";
			}
			else{
				$html .= '<a href="javascript:void(0)" class="btn btn-danger disabled">' . __("delete") . '</a>';
			}
			
			$html .= "</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
	echo $paginator;
?>

<script type="text/javascript">
	$(document).ready(function(){
		
	});
	function deleteUser(el){
			
	}
</script>