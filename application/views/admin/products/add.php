<?php
	$arr = array(
		'product_name' => '',
		'product_group' => '',
		'product_code' => '',
		'product_cost' => '',
		'product_price' => '',
		'product_discount' => '',
		'product_quantity' => '',
        'package_unit' => '',
		'product_supplier' => ''
	);
	if(isset($posts)){
		$arr['product_name'] = $posts['product_name'];
		$arr['product_code'] = $posts['product_code'];
		$arr['product_cost'] = $posts['product_cost'];
		$arr['product_price'] = $posts['product_price'];
        $arr['product_discount'] = $posts['product_discount'];
		$arr['product_quantity'] = $posts['product_quantity'];
		$arr['product_supplier'] = $posts['product_supplier_id'];
		$arr['package_unit'] = $posts['package_unit'];
	}
?>
<div class="span12">
	<form class="form-horizontal form-validation"  method='post'>
	<fieldset>
		<legend><?php echo __("add_new_product"); ?></legend>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_name"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="product_name" name="product_name" rel="popover" data-content="Enter product name" data-original-title="Product Name" value="<?php echo $arr['product_name'];?>" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label"><?php echo __("product_code"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="product_code" name="product_code" rel="popover" data-content="Enter product code" data-original-title="Product Code" value="<?php echo $arr['product_code'];?>" />
			</div>
		</div>

        <div class="control-group">
            <label class="control-label"><?= __("Product Group") ?></label>
            <div class="controls">
                <?= product_group_dropdown($arr['product_group']);?>
            </div>
        </div>

		<div class="control-group">
			<label class="control-label"><?php echo __("product_cost"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="product_cost" name="product_cost" rel="popover" data-content="Enter product cost" data-original-title="Product Cost" value="<?php echo $arr['product_cost'];?>" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_price"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="product_price" name="product_price" rel="popover" data-content="Enter product price" data-original-title="Product Price" value="<?php echo $arr['product_price'];?>" />
			</div>
		</div>
        <div class="control-group">
            <label class="control-label"><?php echo __("product_discount"); ?></label>
            <div class="controls">
                <input type="text" class="input-medium formatnumber form-control" id="product_discount" name="product_discount" rel="popover" data-content="Enter product discount" data-original-title="Product Discount" value="<?php echo $arr['product_discount'];?>" />
            </div>
        </div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_quantity"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="product_quantity" name="product_quantity" rel="popover" data-content="Enter product quantity" data-original-title="Product Quantity" value="0" readonly="readonly" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("package_unit"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="package_unit" name="package_unit" rel="popover" data-content="Enter package unit" data-original-title="Package unit" value="<?php echo $arr['package_unit'];?>" required />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_supplier"); ?></label>
			<div class="controls">
				<?php echo supplier_dropdown($arr['product_supplier']);?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<button type="submit" class="btn btn-primary" ><?php echo __("create_product");?></button>
				<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
			</div>
			<div class="control-group success">
				
			</div>
			<div class="control-group error">
				<!-- TODO -->
			</div>
		</div>
	</fieldset>
	</form>
</div>
