<?php 

	if($product){
?>
<div class="span12">
	<form class="form-horizontal form-validation"  method='post'>
	<fieldset>
		<legend><?php echo __("change_price"); ?></legend>
		<input type="hidden" name="product_id" value="<?php echo $product["product_id"]; ?>" />
		<div class="control-group">
			<label class="control-label"><?php echo __("product_name"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="product_name" name="product_name" rel="popover" value="<?php echo $product["product_name"]?>" readonly="readonly" />
			</div>
		</div>


		<div class="control-group">
			<label class="control-label"><?php echo __("product_code"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="product_code" name="product_code" rel="popover" value="<?php echo $product["product_code"]?>" readonly="readonly" />
			</div>
		</div> 
		<div class="control-group">
			<label class="control-label"><?php echo __("product_cost"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="product_cost" name="product_cost" value="<?php echo number_format($product["product_cost"],NUMBER_DECIMAL)?>" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_price"); ?></label>
			<div class="controls">
				<input type="text" class="input-medium formatnumber form-control" id="product_price" name="product_price" value="<?php echo number_format($product["product_price"],NUMBER_DECIMAL)?>" />
			</div>
		</div>
        <div class="control-group">
            <label class="control-label"><?php echo __("product_discount"); ?></label>
            <div class="controls">
                <input type="text" class="input-medium formatnumber form-control" id="product_discount" name="product_discount" value="<?php echo number_format($product["product_discount"],NUMBER_DECIMAL)?>" />
            </div>
        </div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_quantity"); ?></label>
			<div class="controls">
				<input type="text" class="form-control" id="product_quantity" name="product_quantity" rel="popover" data-content="Enter product quantity" data-original-title="Product Quantity" value="<?php echo (float)($product["product_stock_quantity"]);?>" readonly="readonly" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_supplier"); ?></label>
			<div class="controls">
				<?php echo supplier_dropdown($product["product_supplier_id"],true);?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<button type="submit" class="btn btn-primary" ><?php echo __("change_price");?></button>
				<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
			</div>
			<div class="control-group success">
				
			</div>
			<div class="control-group error">
				<!-- TODO -->
			</div>
		</div>
	</fieldset>
	</form>
</div>

<?php } ?>