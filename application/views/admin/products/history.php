<?php
if(count($his_quantity) > 0 && $his_quantity){
	echo "<h2>" . __("quantity_history") . "</h2>";
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th class='text-right'>" . __("Số thùng trước") . "</th>";
	$html .= "<th class='text-right'>" . __("Số thùng sau") . "</th>";
    $html .= "<th class='text-right'>" . __("Số hộp trước") . "</th>";
    $html .= "<th class='text-right'>" . __("Số hợp sau") . "</th>";
	$html .= "<th>" . __("reason") . "</th>";
	$html .= "<th>" . __("user_change") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	
	$previous_item = false;
	foreach($his_quantity as $k => $item){
		$reason = "";
		$link = "";
		switch($item["reason"]){
			case INITIAL_STOCK: $reason = __("initial_stock"); break;
				case STOCK_CHANGE_REASON_ISSUE: 
					$reason = __('According stock issue');
					$link = site_url("admin/stockissues/view/".$item["linked_object_id"]);
				break;
				case STOCK_CHANGE_REASON_RECEIPT: 
					$reason = __('According stock receipt');
					$link = site_url("admin/stockreceipts/view/".$item["linked_object_id"]);
				break;
				case STOCK_CHANGE_REASON_ISSUE_CHANGE: 
					$reason = __('Adjustment according stock issue change');
					$link = site_url("admin/histories/stock/".$item["linked_object_id"]);
				break;
				case STOCK_CHANGE_REASON_RECEIPT_CHANGE: 
					$reason = __('Adjustment according stock receipt change');
					$link = site_url("admin/histories/stock/".$item["linked_object_id"]);
				break;
				case STOCK_CHANGE_REASON_ADJUSTMENT: $reason = __('Stock adjustment'); break;
		}
		$alert_class = '';
		if ($previous_item && $previous_item['previous_product_stock_quantity'] != $item["new_product_stock_quantity"]) {
			$alert_class = ' class="alert alert-danger" ';;
		}
		
		$html .= "<tr>";
		$html .= "<td>".formatLocalDatetime($item["product_stock_history_date"])."</td>";
		$html .= "<td $alert_class style='text-align: right'>".$item["previous_product_quantity"]."</td>";
		$html .= "<td $alert_class style='text-align: right'>".$item["new_product_quantity"]."</td>";
        $html .= "<td $alert_class style='text-align: right'>".$item["previous_product_stock_quantity"]."</td>";
        $html .= "<td $alert_class style='text-align: right'>".$item["new_product_stock_quantity"]."</td>";
		$html .= "<td><a href=\"" .(($link != "") ? $link : "javascript:void(0)"). "\">".$reason."</a></td>";
		$html .= "<th>" . $item["user_name"] . "</th>";
		$html .= "</tr>";
		
		$previous_item = $item;
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
	echo $paginator;
}
if(count($his_prices) > 0 && $his_prices){	
	echo "<h2>" . __('Price History') . "</h2>";
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("date time") . "</th>";
	$html .= "<th>" . __("Previous Cost") . "</th>";
	$html .= "<th>" . __("new_cost") . "</th>";	
	$html .= "<th>" . __("Previus Price") . "</th>";
	$html .= "<th>" . __("new_price") . "</th>";
	$html .= "<th>" . __("user_change") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	foreach($his_prices as $k => $item){
		$html .= "<tr>";
		$html .= "<td>".formatLocalDatetime($item["product_price_history_date"])."</td>";
		$html .= "<th class='text-right'>" . number_format($item["previous_product_cost"],NUMBER_DECIMAL) . "</th>";
		$html .= "<th class='text-right'>" . number_format($item["new_product_cost"],NUMBER_DECIMAL) . "</th>";
		$html .= "<th class='text-right'>" . number_format($item["previous_product_price"],NUMBER_DECIMAL) . "</th>";
		$html .= "<th class='text-right'>" . number_format($item["new_product_price"],NUMBER_DECIMAL) . "</th>";
		$html .= "<th>" . $item["user_name"] . "</th>";
		$html .= "</tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
	
}
?>