<?php 

	if($product){
?>
<div class="span12">
	<form class="form-horizontal form-validation"  method='post'>
	<fieldset>
		<legend><?php echo __("edit_product"); ?></legend>
		<input type="hidden" name="product_id" value="<?php echo $product["product_id"]; ?>" />
		<div class="control-group">
			<label class="control-label"><?php echo __("product_name"); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge form-control" id="product_name" name="product_name" rel="popover" data-content="Enter product name" data-original-title="Product Name" value="<?php echo $product["product_name"]?>" />
			</div>
		</div>


		<div class="control-group">
			<label class="control-label"><?php echo __("product_code"); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge form-control" id="product_code" name="product_code" rel="popover" data-content="Enter product code" data-original-title="Product Code" value="<?php echo $product["product_code"]?>" readonly="readonly" />
			</div>
		</div> 
		<div class="control-group">
			<label class="control-label"><?php echo __("product_cost"); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge form-control" id="product_cost" name="product_cost" rel="popover" data-content="Enter product cost" data-original-title="Product Cost" value="<?php echo number_format($product["product_cost"],NUMBER_DECIMAL)?>" readonly="readonly" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_price"); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge form-control" id="product_price" name="product_price" rel="popover" data-content="Enter product price" data-original-title="Product Price" value="<?php echo number_format($product["product_price"],NUMBER_DECIMAL)?>" readonly="readonly" />
			</div>
		</div>
        <div class="control-group">
            <label class="control-label"><?php echo __("product_discount"); ?></label>
            <div class="controls">
                <input type="text" class="input-xlarge form-control" id="product_price" name="product_discount" rel="popover" data-content="Enter product discount" data-original-title="Product Discount" value="<?php echo number_format($product["product_discount"],NUMBER_DECIMAL)?>" readonly="readonly" />
            </div>
        </div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_quantity"); ?></label>
			<div class="controls">
				<input type="text" class="input-xlarge form-control" id="product_quantity" name="product_quantity" rel="popover" data-content="Enter product quantity" data-original-title="Product Quantity" value="<?php echo (float)($product["product_stock_quantity"])?>" readonly="readonly" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?php echo __("product_supplier"); ?></label>
			<div class="controls">
				<?php echo supplier_dropdown($product["product_supplier_id"]);?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<button type="submit" class="btn btn-primary" ><?php echo __("edit_product");?></button>
				<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
			</div>
			<div class="control-group success">
				
			</div>
			<div class="control-group error">
				<!-- TODO -->
			</div>
		</div>
	</fieldset>
	</form>
</div>

<?php } ?>