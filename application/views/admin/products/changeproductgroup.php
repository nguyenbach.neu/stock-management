<?php if($product): ?>
    <?php $productGroup = empty($product['product_group']) ? null : $product['product_group'] ?>
    <div class="span12">
        <form class="form-horizontal form-validation"  method='post'>
        <fieldset>
            <legend><?= __("change_price"); ?></legend>
            <input type="hidden" name="product_id" value="<?= $product["product_id"]; ?>" />
            <div class="control-group">
                <label class="control-label"><?= __("product_name"); ?></label>
                <div class="controls">
                    <input type="text" class="form-control" id="product_name" name="product_name" rel="popover" value="<?= $product["product_name"]?>" readonly="readonly" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><?= __("product_code"); ?></label>
                <div class="controls">
                    <input type="text" class="form-control" id="product_code" name="product_code" rel="popover" value="<?= $product["product_code"]?>" readonly="readonly" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><?= __("Product Group") ?></label>
                <div class="controls">
                    <?= product_group_dropdown($product['product_group']) ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <button type="submit" class="btn btn-primary" ><?= __("Change Group");?></button>
                    <button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?= __('Cancel'); ?></button>
                </div>
            </div>
        </fieldset>
        </form>
    </div>

<?php endif; ?>