<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/products/add")?>"><?php echo __('Add new'); ?></a>
    </div>
</div>
<div class="well"><?= __('Sum:')?> <?= number_format($sum['sum'], NUMBER_DECIMAL)?></div>
<table class="table table-hover table-striped">
    <tr>
        <th><?= __("product_name") ?></th>
        <th><?= __("product_code") ?></th>
        <th class="text-right"><?= __("product_cost") ?></th>
        <th class="text-right"><?= __("product_price") ?></th>
        <th class="text-right"><?= __("package_quantity") ?></th>
        <th class="text-right"><?= __("unit_quantity") ?></th>
        <th class="text-right"><?= __("product_quantity") ?></th>
        <th class="text-right"><?= __("package_unit") ?></th>
        <th class="text-right"><?= __("product_supplier") ?></th>
        <th class="text-center"><?= __("action") ?></th>
    </tr>
    <?php if (count($products) > 0):?>
        <?php foreach ($products as $k => $item):?>
            <tr>
                <td><a href="<?= site_url("admin/products/edit/".$item["product_id"]) ?>"><?= $item["product_name"] ?></a></td>
                <td><?= $item["product_code"] ?></td>
                <td class='text-right'><?= number_format($item["product_cost"],NUMBER_DECIMAL) ?></td>
                <td class='text-right'><?= number_format($item["product_price"],NUMBER_DECIMAL) ?></td>
                <td class='text-right'><?= number_format($item["package_quantity"],NUMBER_INTEGER) ?></td>
                <td class='text-right'><?= number_format($item["unit_quantity"],NUMBER_INTEGER) ?></td>
                <td class='text-right'><?= number_format($item["product_stock_quantity"],NUMBER_INTEGER) ?></td>
                <td class='text-right'><?= $item["package_unit"] ?></td>
                <td class='text-right'><?= $item["supplier_name"] ?></td>
                <td>
                    <?php if(has_permission($this->session->userdata('user_role'),"products","changeprice")): ?>
                        <a href="<?= site_url("admin/products/changeprice/".$item["product_id"]) ?>" class="btn btn-info"><?= __("change_price") ?></a>
                    <?php endif; ?>

                    <?php if(has_permission($this->session->userdata('user_role'),"products","changequantity")): ?>
                        <a href="<?= site_url("admin/products/changequantity/".$item["product_id"]) ?>" class="btn btn-info"><?= __("change_quantity") ?></a>
                    <?php endif; ?>

                    <a href="<?= site_url("admin/products/changegroup/".$item["product_id"]) ?>" class="btn btn-info"><?= __("Change Group") ?></a>

                    <a href="<?= site_url("admin/products/history/".$item["product_id"]) ?>" class="btn btn-info"><?= __("view_history") ?></a>

                    <?php if(has_permission($this->session->userdata('user_role'),"products","delete", (int)$item["product_stock_quantity"])): ?>
                        <a href="javascript:void(0)" class="btn btn-danger" data-toggle="confirmation"><?= __("delete") ?></a>
                        <script type="text/javascript">
							$('[data-toggle="confirmation"]').confirmation({
								"href": '<?= site_url("admin/products/delete/".$item["product_id"]) ?>',
								"popout":true,
								"singleton": true
							});
						</script>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>
<?= $paginator ?>