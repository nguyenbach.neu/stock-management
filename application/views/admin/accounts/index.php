<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/accounts/add")?>"><?php echo __('Add new'); ?></a>
    </div>
</div>
<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("Ten tai khoan") . "</th>";
	$html .= "<th>" . __("Account Note") . "</th>";
	$html .= "<th>" . __("Loai tai khoan") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if(count($accounts) > 0 && $accounts){
		foreach($accounts as $k => $item){
			$html .= "<tr>";
			$html .= "<td><a href=\"" . site_url("admin/accounts/edit/".$item["account_id"]) . "\">".$item["account_name"]."</a></td>";
			$html .= "<td>". $item["account_note"] ."</td>";
            $html .= "<td>". ($item["account_cash"]?__('Thanh toan'):__('Ghi no hoac tra truoc')) ."</td>";
			$html .= "<td>
							<a href=\"javascript:void(0)\" data-toggle=\"confirmation\" class=\"btn btn-danger\">". __("delete") ."</a>
						<script type=\"text/javascript\">
							$('[data-toggle=\"confirmation\"]').confirmation({
								\"href\": '". site_url("admin/accounts/delete/".$item["account_id"]) ."',
								\"popout\":true,
								\"singleton\": true
							});
						</script>
						
						</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
?>