<?php 

?>
<h3> <?php echo __("History Transation"); ?></h3>
<p>&nbsp;</p>
<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("cash_create_date") . "</th>";
	$html .= "<th>" . __("cash_amount") . "</th>";
	$html .= "<th>" .__("transfer_account")."</th>";
	$html .= "<th>" . __("cash_note") . "</th>";

	$html .= "<th>" . __("type") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	$totals = 0;
	$money_status = '';
	if(count($histories) > 0){
		foreach($histories as $k => $item){
			if($item["cash_follow"]== 1){ 
			$follow =  'color:blue;';
			$money_status = '&nbsp;';
			} 
			elseif($item["cash_follow"]== -1){
				$follow = 'color:red;';
				$money_status = '-';
			}
			
			$html .= "<tr >";
			$html .= "<td>".formatLocalDatetime($item["cash_create_date"])."</td>";
			$html .= "<td style = '".$follow."'>".$money_status.' '.number_format($item["cash_amount"],NUMBER_DECIMAL)."</td>";
			if(empty($item["transfer_account"]))
            {
                $html .= "<td></td>";
            } else {
                $html .= "<td>".$item["transfer_account"]." </td>";
            }

			$html .= "<td>".$item["cash_note"]."</td>";
			$html .= "<td>".$item["type"]."</td>";
			$html .= "</tr>";
			if($item["cash_follow"]==1){
				$totals = ($totals + $item["cash_amount"]);
			}elseif($item["cash_follow"]==-1){
				$totals = ($totals - $item["cash_amount"]);
			}
			
		}
	}
	$html .= '<tr><td>'. __('Total') .'</td>';
	$html .= '<td>'. number_format($totals,NUMBER_DECIMAL) .'</td>';
	$html .= '<td></td>';
	$html .= '<td class="text-right"></td>';
	$html .= '</tr>';
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
?>