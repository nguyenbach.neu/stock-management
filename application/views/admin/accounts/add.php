		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("Them tai khoan");?></legend>

			<div class="control-group">
				<label class="control-label"><?php echo __("acount_name");?></label>
				<div class="controls">
					<input type="text" class="input-medium form-control" id="account_name" name="account_name" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea name="account_note" rows="3" class="form-control"></textarea>
				</div>
			</div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Tai khoan thanh toan (tien mat hoac ngan hang)");?></label>
                <div class="controls">
                    <input name="account_cash" type="checkbox" value="1" class="checkbox" />
                </div>
            </div>
			<div class="control-group">
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("create");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
			</div>
		</fieldset>
		</form>

