<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Ngay (so lieu tien mat thoi diem ket thuc ngay duoc chon)");?></label>
                <div class='input-group date single_date' id='datetimepicker'>
                    <input type='text' class="form-control" name="date" value="<?php echo $this->input->get('date'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php

$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<tr>";
$html .= "<th>" . __("product_code") . "</th>";
$html .= "<th>" . __("account_name") . "</th>";
$html .= "<th>" . __("total amount") . "</th>";
$html .= "<th>" . __("action") . "</th>";
$html .= "</tr>";
$total = array('quantity' => 0, 'price' => 0, 'quantity_receipt' => 0);
if(is_array($accounts) && count($accounts) >0 ){
	foreach($accounts as $k => $item){
		$html .= "<tr>";
		$html .= "<td>".$item["account_id"]."</td>";
		$html .= "<td>".$item["account_name"]."</td>";
		$html .= "<td>".number_format($item["sum"],NUMBER_DECIMAL)."</td>";
		$html .= "<td><a class=\"btn btn-info\" href='".site_url('admin/accounts/history/'.$item["account_id"])."'>". __('detail') ."</a></td>";
		$html .= "</tr>";
	}
}

$html .= "</table>";
echo $html;

?>
<script type="text/javascript">
$(function() {
	var dates = $('#from, #to').datepicker({
		defaultDate: "-2m",
		changeMonth: true,
		numberOfMonths: 3,
		onSelect: function(selectedDate) {
			var option = this.id == "from" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
});
</script>