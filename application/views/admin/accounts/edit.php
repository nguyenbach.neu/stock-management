<?php

if($account){
?>
<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("edit_account");?></legend>
			<input type="hidden" name="account_id" value="<?php echo $account["account_id"];?>" />
			<div class="control-group">
				<label class="control-label"><?php echo __("account_name");?></label>
				<div class="controls">
					<input type="text" class="input-medium form-control" id="account_name" name="account_name" value="<?php echo $account["account_name"];?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("note");?></label>
				<div class="controls">
					<textarea name="account_note" rows="3" class="form-control"><?php echo $account["account_note"];?></textarea>
				</div>
			</div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Tai khoan thanh toan (tien mat hoac ngan hang)");?></label>
                <div class="controls">
                    <input name="account_cash" type="checkbox" class="checkbox" value="1" <?php if ($account["account_cash"]) { echo 'checked'; } ?> />
                </div>
            </div>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("change");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>
<?php } ?>