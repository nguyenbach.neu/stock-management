<?php

?>
<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php
$html = "";
$html .= "<table class=\"table table-hover table-striped\">";
$html .= "<thead>";
$html .= "<tr>";
$html .= "<th>" . __("Nhan vien giao hang") . "</th>";
$html .= "<th>" . __("Beginning debit") . "</th>";
$html .= "<th>" . __("Stock issue") . "</th>";
$html .= "<th>" . __("Stock receive") . "</th>";
$html .= "<th>" . __("Cash receive") . "</th>";
$html .= "<th>" . __("Total debit") . "</th>";
$html .= "</tr>";
$html .= "</thead>";
$html .= "<tbody>";
if(count($debits) > 0){
    foreach($debits as $k => $item){
        $alert = '';

        $html .= "<tr>";
        $html .= "<td>".$item["user_name"]."</td>";
        $html .= '<td><a href="'.site_url("admin/reports/delivery_staff_debt?to=". date( 'Y-m-d', strtotime( $this->input->get('from') . ' -1 day' ) )).'">'.number_format($item["initial_debit"],NUMBER_DECIMAL)."</a></td>";
        $html .= '<td><a href="'.site_url("admin/stockissues/?sale_user_id =".$item["user_id"]).'&from='.$this->input->get('from').'&to='.$this->input->get('to').'">'.number_format($item["stock_issue"],NUMBER_DECIMAL)."</a></td>";
        $html .= '<td><a href="'.site_url("admin/stockreceipts/?type=1&sale_user_id=".$item["user_id"]).'&from='.$this->input->get('from').'&to='.$this->input->get('to').'">'.number_format($item["stock_receipt"],NUMBER_DECIMAL)."</a></td>";
        $html .= '<td><a href="'.site_url("admin/cashs/receipts?sale_user_id=".$item["user_id"]).'&from='.$this->input->get('from').'&to='.$this->input->get('to').'">'.number_format($item["cash_receipt"],NUMBER_DECIMAL)."</a></td>";
        $html .= "<td>".number_format($item["discount"],NUMBER_DECIMAL). $alert."</td>";
        $html .= "</tr>";
    }
}
$html .= "</tbody>";
$html .= "<tfoot>";
$html .= "<tr>";
$html .= "<td>" . __("total") . "</td>";
$html .= "<td>" . number_format($total_initial_discount, NUMBER_DECIMAL) . "</td>";
$html .= "<td>" . number_format($total_discount_advance, NUMBER_DECIMAL) . "</td>";
$html .= "<td>" . number_format($total_discount_receive, NUMBER_DECIMAL) . "</td>";

$html .= "<td>" . number_format($total_discount,NUMBER_DECIMAL) . "</td>";
$html .= "</tr>";
$html .= "</tfoot>";
$html .= "</table>";
echo $html;

?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btnSubmit").click(function(){
            if($("#selectyear").val() != ""){
                $("#formFilter").submit();
            }
        });
    })
</script>