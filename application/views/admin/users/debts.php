<?php 

?>
<div class="panel panel-default">
    <div class="panel-body">
	<form class="form-inline" action="" method="get" id="formFilter">
		<div class="form-group">
			<label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
            <div class='input-group date start_date' id='datetimepicker1'>
                <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
		</div>
		<div class="form-group">
			<label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
            <div class='input-group date end_date' id='datetimepicker2'>
                <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>

		</div>
				<button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
	</form>
    </div>
</div>
<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("Nhan vien ban hang") . "</th>";
	$html .= "<th class='text-right'>" . __("Beginning debt") . "</th>";
	$html .= "<th class='text-right'>" . __("Cong no khach hang") . "</th>";
	$html .= "<th class='text-right'>" . __("Thu no") . "</th>";
	$html .= "<th class='text-right'>" . __("Tong no") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	if(count($debts) > 0){
		foreach($debts as $k => $item){
            $alert = '';

			$html .= "<tr>";
			$html .= "<td>".$item["user_name"]."</td>";
			$html .= '<td class=\'text-right\'><a href="'.site_url("admin/reports/debts?to=". date( 'Y-m-d', strtotime( $this->input->get('from') . ' -1 day' ) )).'">'.number_format($item["initial_debt"],NUMBER_DECIMAL)."</a></td>";
			$html .= '<td class=\'text-right\'><a href="'.site_url("admin/cashs/debit_advance?sale_user_id=".$item["user_id"]).'&from='.$this->input->get('from').'&to='.$this->input->get('to').'">'.number_format($item["debit_advance"],NUMBER_DECIMAL)."</a></td>";
			$html .= '<td class=\'text-right\'><a href="'.site_url("admin/cashs/debit_receipt?sale_user_id=".$item["user_id"]).'&from='.$this->input->get('from').'&to='.$this->input->get('to').'">'.number_format($item["debit_receive"],NUMBER_DECIMAL)."</a></td>";
			$html .= "<td class='text-right'>".number_format($item["debt"],NUMBER_DECIMAL). $alert."</td>";
			$html .= "</tr>";
		}
	}
	$html .= "</tbody>";
	$html .= "<tfoot>";
	$html .= "<tr>";
	$html .= "<td>" . __("total") . "</td>";
	$html .= "<td class='text-right'>" . number_format($total_initial_debt, NUMBER_DECIMAL) . "</td>";
	$html .= "<td class='text-right'>" . number_format($total_debit_advance, NUMBER_DECIMAL) . "</td>";
	$html .= "<td class='text-right'>" . number_format($total_debit_receive, NUMBER_DECIMAL) . "</td>";

	$html .= "<td class='text-right'>" . number_format($total_debt,NUMBER_DECIMAL) . "</td>";
	$html .= "</tr>";
	$html .= "</tfoot>";
	$html .= "</table>";
	echo $html;

?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#btnSubmit").click(function(){
			if($("#selectyear").val() != ""){
				$("#formFilter").submit();
			}
		});
	})
</script>