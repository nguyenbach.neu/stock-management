<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-inline" action="" method="get" id="formFilter">
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay bat dau");?></label>
                <div class='input-group date start_date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="from" value="<?php echo $this->input->get('from'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail"><?php echo __("Chon ngay ket thuc");?></label>
                <div class='input-group date end_date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="to" value="<?php echo $this->input->get('to'); ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>
            <button type="submit" class="btn btn-info" id="btnSubmit"><?php echo __("Filter");?></button>
        </form>
    </div>
</div>
<?php
	if(count($histories) > 0){
		$totals = 0;
		$html = "<h2>" . __("stock_issues_history") . "</h2>";
		$html .= "<table class=\"table table-hover table-striped\">";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>" . __("type") . "</th>";
		$html .= "<th>" . __("note") . "</th>";
		$html .= "<th>" . __("date time") . "</th>";
		$html .= "<th>" . __("action") . "</th>";
		$html .= "<th style=\"text-align:right;width:150px;\">" . __("total") . "</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";
		foreach($histories as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".(($item["stock_bill_flow"] == "1") ? "Receipt" : "Issue" )."</td>";
			$html .= "<td>".$item["stock_bill_note"]."</td>";
			$html .= "<td>".formatLocalDatetime($item["stock_bill_created_date"])."</td>";
			
			if($item["stock_bill_flow"] == "1"){
				$html .= "<td><a id=\"\" class=\"detail btn btn-info\" rel=\"stock_issue\" href=\"". site_url('admin/stockreceipts/view/'.$item["stock_bill_id"]) ."\" >". __("detail") ."</a></td>";
			}
			else {
				$html .= "<td><a id=\"\" class=\"detail btn btn-info\" rel=\"stock_issue\" href=\"". site_url('admin/stockissues/view/'.$item["stock_bill_id"]) ."\" >". __("detail") ."</a></td>";
			}
			$html .= "<td style=\"text-align:right\">". number_format($item["stock_bill_flow"]*$item["totals"],NUMBER_DECIMAL) ."</td>";
			$html .= "</tr>";
			$totals = $totals + $item["stock_bill_flow"] * $item["totals"];
			
		}
		$html .= "<tr>";
		$html .= "<td colspan=\"4\" style=\"text-align:right\">". __("totals") ."</td>";
		$html .= "<td style=\"text-align:right\">". number_format($totals,NUMBER_DECIMAL) ."</td>";
		$html .="</tr>";
		$html .= "</tbody>";
		$html .= "</table>";
		echo $html;
	}
	if(count($cash_receipts) > 0){
		$totals = 0;
		$html = "<h2>" . __("cash_receipt_history") . "</h2>";
		$html .= "<table class=\"table table-hover table-striped\">";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>" . __("date time") . "</th>";
		$html .= "<th>" . __("note") . "</th>";
		$html .= "<th style=\"text-align:right;width:150px;\">" . __("total") . "</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";
		foreach($cash_receipts as $k => $item){
			$html .= "<tr>";
			$html .= "<td>".formatLocalDatetime($item["cash_create_date"])."</td>";
			$html .= "<td>".($item["cash_note"])."</td>";
			$html .= "<td style=\"text-align:right\">". number_format($item["cash_amount"],NUMBER_DECIMAL) ."</td>";
			$html .= "</tr>";
			$totals = $totals + $item["cash_amount"];
		}
		$html .= "<tr>";
		$html .= "<td colspan=\"2\" style=\"text-align:right\">". __("totals") ."</td>";
		$html .= "<td style=\"text-align:right\">". number_format($totals,NUMBER_DECIMAL) ."</td>";
		$html .="</tr>";
		$html .= "</tbody>";
		$html .= "</table>";
		echo $html;
	}
?>



