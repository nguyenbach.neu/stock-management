<div class="panel panel-default">
    <div class="panel-heading text-right">
        <a class="btn btn-success" href="<?php echo site_url("admin/users/add")?>"><?php echo __('Add new'); ?></a>
    </div>
</div>

<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<tr>";
	$html .= "<th>" . __("user_id") . "</th>";
	$html .= "<th>" . __("user_name") . "</th>";
	$html .= "<th>" . __("user_email") . "</th>";
	$html .= "<th>" . __("user_role") . "</th>";
	$html .= "<th>" . __("user_type") . "</th>";
	$html .= "<th>" . __("action") . "</th>";
	$html .= "</tr>";
	if(count($users) > 0){
		foreach($users as $k => $item){
			$user_role = "";
			switch($item["user_role"]){
				case ROLE_ADMIN: $user_role = __("Quan tri");break;
				case ROLE_STOCKKEEPER: $user_role = __("Thu kho");break;
				case ROLE_SUPERVISOR: $user_role = __("Giam sat");break;
				case ROLE_SALEPERSON: $user_role = __("Nhan vien ban hang");break;
                case ROLE_ACCOUNTING: $user_role = __("Ke toan");break;
			}
			$html .= "<tr>";
			$html .= "<td>".$item["user_id"]."</td>";
			$html .= "<td>".$item["user_name"]."</td>";
			$html .= "<td><a href=\"" . site_url("admin/users/edit/".$item["user_id"]) . "\">".$item["user_email"]."</a></td>";
			$html .= "<td>".$user_role."</td>";
			$html .= "<td>".(($item["user_disabled"] == USER_ENABLE) ? __("user_type_enable") : __("user_type_disable"))."</td>";
            $html .= '<td><a href="' . site_url("admin/users/edit/".$item["user_id"]).'" class="btn btn-info">'. __("edit") . '</a>';
			$html .= "<button type=\"button\" data-toggle=\"confirmation\" class=\"btn btn-danger\"><i class=\"icon-trash icon-white\"></i>".__('Delete')."</button>
				<script type=\"text/javascript\">
				$('[data-toggle=\"confirmation\"]').confirmation({
					\"href\": '". site_url("admin/users/delete/".$item["user_id"]) ."',
					\"popout\":true,
					\"singleton\": true
				});
				</script>";
			if ($item["access"] == 0){
			    $html .= '<a href="' . site_url("admin/users/access/".$item["user_id"]).'" class="btn btn-info">'. __("access") . '</a>';
            } elseif ($item["access"] == 1){
			    $html .= '<a href="' . site_url("admin/users/lock/".$item["user_id"]).'" class="btn btn-info">'. __("lock") . '</a>';
            }
			$html .= "</tr>";
		}
	}
	$html .= "</table>";
	echo $html;
?>

<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>