<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
		<fieldset>
			<legend><?php echo __("add_new_user");?></legend>
			<div class="control-group">
				<label class="control-label"><?php echo __("user_name");?></label>
				<div class="controls">
					<input type="text" class="form-control" id="user_name" name="user_name" rel="popover" data-content="Enter name" data-original-title="Name" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("user_email");?></label>
				<div class="controls">
					<input type="text" class="form-control" id="user_email" name="user_email" rel="popover" data-content="Enter user email" data-original-title="User email" />
				</div>
			</div> 
			<div class="control-group">
				<label class="control-label"><?php echo __("user_password");?></label>
				<div class="controls">
					<input type="password" class="form-control" id="user_password" name="user_password" rel="popover" data-content="Enter user password" data-original-title="user password" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label"><?php echo __("user_re_password");?></label>
				<div class="controls">
					<input type="password" class="form-control" id="user_repassword" name="user_repassword" rel="popover" data-content="Enter user retype password" data-original-title="user retype password" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label"><?php echo __("user_type");?></label>
				<div class="controls">
					<?php echo user_role_dropdown();?>
				</div>
			</div>

            <div class="control-group">
                <label class="control-label"><?php echo __("access");?></label>
                <input type="checkbox" name="access" value="1">
            </div>

			<div class="control-group">
				<label class="control-label"></label>
				<div class="control-group text-center">
					<button type="submit" class="btn btn-primary" ><?php echo __("create_user");?></button>
					<button type="button" class="btn btn-default" id="btn_cancel"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo __('Cancel'); ?></button>
				</div>
				<div class="control-group success">
					
				</div>
				<div class="control-group error">
					<!-- TODO -->
				</div>
			</div>
		</fieldset>
		</form>
	</div>
</div>
