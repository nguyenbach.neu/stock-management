<div class="registration-container">
	<div class="span12">
		<form class="form-horizontal form-validation"  method='post'>
            <fieldset>
                <legend><?php echo __("Edit User Information");?></legend>
                <div class="control-group">
                    <label class="control-label"><?php echo __("user_password");?></label>
                    <div class="controls">
                        <input type="password" class="form-control" id="user_password" name="user_password" rel="popover" data-content="Enter user password" data-original-title="user password" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo __("user_re_password");?></label>
                    <div class="controls">
                        <input type="password" class="form-control" id="user_repassword" name="user_repassword" rel="popover" data-content="Enter user retype password" data-original-title="user retype password" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="control-group text-center">
                        <button type="submit" class="btn btn-primary" ><?php echo __('Update');?></button>
                        <a href="<?php echo base_url().'index.php/admin/dashboard';?>"><button type="button" class="btn btn-danger btn-cancel" ><?php echo __('Cancel');?></button></a>
                    </div>
                    <div class="control-group success">

                    </div>
                    <div class="control-group error">

                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>