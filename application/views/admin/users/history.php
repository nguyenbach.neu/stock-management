<?php 

?>
<form class="form-horizontal" action="" method="get" id="formFilter">
	<div class="control-group">
		<label class="control-label" for="inputEmail"><?php echo __("choose_month");?></label>
		<div class="controls">
			<select name="selectmonth" id="selectmonth">
				<option value=""><?php echo __("choose");?></option>
				<?php
					for($i = 1; $i < 13; $i++){
						echo "<option " . (($_GET["selectmonth"] == $i) ? "selected": "" ) . " value=\"$i\">$i</option>";
					}
				?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="inputEmail"><?php echo __("choose_year");?></label>
		<div class="controls">
			<select name="selectyear" id="selectyear">
				<option value=""><?php echo __("choose");?></option>
				<?php
					for($i = date("Y"); $i > (date("Y") - 20); $i--){
						echo "<option " . (($_GET["selectyear"] == $i) ? "selected": "" ) . " value=\"$i\">$i</option>";
					}
				?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="button" class="btn" id="btnSubmit"><?php echo __("filter");?></button>
		</div>
	</div>

<?php
	$html = "";
	$html .= "<table class=\"table table-hover table-striped\">";
	$html .= "<thead>";
	$html .= "<tr>";
	$html .= "<th>" . __("product_name") . "</th>";
	$html .= "<th>" . __("preview_quantity") . "</th>";
	$html .= "<th>" . __("new_quantity") . "</th>";
	$html .= "<th>" . __("reason") . "</th>";
	$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	if(count($histories) > 0){
		foreach($histories as $k => $item){
			$reason = "";
			switch($item["reason"]){
				case INITIAL_STOCK: $reason = __("initial_stock"); break;
				case STOCK_CHANGE_REASON_ISSUE: 
					$reason = __('According stock issue');
				break;
				case STOCK_CHANGE_REASON_RECEIPT: 
					$reason = __('According stock receipt');
				break;
				case STOCK_CHANGE_REASON_ISSUE_CHANGE: 
					$reason = __('Adjustment according stock issue change');
				break;
				case STOCK_CHANGE_REASON_RECEIPT_CHANGE: 
					$reason = __('Adjustment according stock receipt change');
				break;
				case STOCK_CHANGE_REASON_ADJUSTMENT: $reason = __('Stock adjustment'); break;
			}
			$html .= "<tr>";
			$html .= "<td>".$item["product_name"]."</td>";
			$html .= "<td>".$item["previous_product_stock_quantity"]."</td>";
			$html .= "<td>".$item["new_product_stock_quantity"]."</td>";
			$html .= "<td>$reason</td>";
			$html .= "<td></td>";
			$html .= "</tr>";
		}
	}
	$html .= "</tbody>";
	$html .= "</table>";
	echo $html;
?>
<p><?php echo $links; ?></p>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#btnSubmit").click(function(){
			if($("#selectyear").val() != ""){
				$("#formFilter").submit();
			}
		});
		$(".page").change(function(){
			$("#formFilter").submit();
		});
	})
</script>