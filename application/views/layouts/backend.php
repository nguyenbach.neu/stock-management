<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Apple iOS and Android stuff (do not remove) -->
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />
<link rel="stylesheet" href="<?php echo base_url()?>skin/backend/css/bootstrap.css" type="text/css"/>

<link rel="stylesheet" href="<?php echo base_url()?>skin/backend/css/icon-style.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url()?>skin/backend/css/style.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url()?>skin/backend/css/bootstrap-datetimepicker.css" type="text/css"/>
<!--login-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/core/login.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/jui/css/jquery.ui.all.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/jui/css/jquery.ui.dialog.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/select2.css" media="screen" />
<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/mws.theme.css" media="screen" />

<!-- JavaScript Plugins -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/bootstrap-confirmation.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/select2.min.js"></script>
<!-- jQuery-UI Dependent Scripts -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/jui/js/jquery-ui-1.8.20.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/validate/jquery.validate-min.js"></script>
<title>Stock Management</title>
<script type="text/javascript">
	function displayConfirm(txt) {
		return confirm(txt);
	}
	function numberFormat(nr){
		var regex = /,/g;
		nr = nr.replace(regex,'');
		nr += '';
		var x = nr.split('.');
		var p1 = x[0];
		var p2 = x.length > 1 ? '.' + x[1] : '';
		regex = /(\d+)(\d{3})/;
		while (regex.test(p1)) {
			p1 = p1.replace(regex, '$1' + ',' + '$2');
		}
		return p1 + p2;
	}
	$(document).ready(function() {
		$.extend($.expr[':'], {
		  'containsi': function(elem, i, match, array) {
			return (elem.textContent || elem.innerText || '').toLowerCase()
				.indexOf((match[3] || "").toLowerCase()) >= 0;
		  }
		});
		$('.click-change-pass').on('click',function(){
			$("#change-password-container").modal('show');
		});
		
		$('.click-profile').on('click',function(){
			$("#profile-container").modal('show');
		});
		
		var dates = $('#from, #to').datepicker({
			defaultDate: "-2m",
			changeMonth: true,
			numberOfMonths: 3,
			onSelect: function(selectedDate) {
				var option = this.id == "from" ? "minDate" : "maxDate";
				var instance = $(this).data("datepicker");
				var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				dates.not(this).datepicker("option", option, date);
			}
		});		
	})

</script>
</head>

<body>

<?php if($this->router->fetch_method() !='login'):?>
	
    <!-- Start Main Wrapper -->
    <div id="wrap">
		<div class="navbar navbar-default navbar-fixed-top navbar-inverse">
			<?php echo $template['partials']['header']; ?>
		</div>
		<div class="row-fluid">
			
			<div class="span12">
				<div class="container-fluid">
					<?php if($this->session->flashdata('success_message')) {
						echo '<div class="msg">';
							echo '<div>'.$this->session->flashdata('success_message').'</div>';
						echo '</div>';
					}?>          
					<?php if($this->session->flashdata('error_message')) {
						echo '<div class="err">';
						echo '<div>'.$this->session->flashdata('error_message').'</div>';
						echo '</div>';
					}?>	
					<?php echo validation_errors('<div class="err"><div>', '</div></div>');?>

					<?php echo $template['body']; ?>
				</div>
			</div>
		</div>
		
	</div>		
	<div id="footer">
        <footer class="footer">
		<?php echo $template['partials']['footer']; ?>
        </footer>
	</div>
<?php else:?>
	<div class="container-fluid">
        <?php echo $template['body']; ?>
    </div>
<?php endif;?>
<?php echo $template['partials']['change_password']; ?>
<?php echo $template['partials']['profile']; ?>
</body>
</html>