<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accounts_model extends MY_Model
{
    var $table = 'accounts';
    function __construct(){
        parent::__construct();
    }
	// insert
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	//update
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('account_id', $arr["account_id"]);
		$this->db->update($this->table, $arr); 
	}
	//delete 1 item
	// var $id item
	function delete($id)
	{
        $this->db->where('account_id', $id);
		$this->db->update($this->table, array('deleted' => 1));
	}
	//getinfo 1 item
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("account_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return false;
	}
	//getall item
	//var $order: field order
	function getAll($order = "account_id",$type = "DESC")
	{
		$this->db->select("accounts.*");
		$this->db->from($this->table);
		//$this->db->join("cashs","accounts.account_id = cashs.account_id and cash_follow =".IN,"LEFT");
        $this->db->where('accounts.deleted', 0);
		$this->db->group_by('accounts.account_id');
		$this->db->order_by($order,$type);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return false;
	}

	function sumAmountGroupByAccount($date = false)
	{
        $on = 'accounts.account_id = cashs.account_id';

        if (!$date) {
            $date = $this->input->get("date");
        }
        if($date){
            $on .= " AND UNIX_TIMESTAMP(cashs.cash_create_date) <= " . strtotime('+1day', strtotime($date));
        }
		$this->db->select("accounts.*,sum(cashs.cash_amount * cashs.cash_follow) as sum");
		$this->db->from($this->table);
		$this->db->join("cashs",$on, 'left');
		$this->db->group_by('accounts.account_id');
		$this->db->order_by('accounts.account_id','ASC');

		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return false;
	}
	
	
	
	function getHistory($id,$limit = 9999,$start = 0){
		$query = $this->db->query("SELECT cash_id, cash_amount, cash_note, cash_follow, account_id, type, cash_create_date, transfer_account FROM cashs WHERE account_id =".$id.' ORDER BY cash_id DESC limit '.$start.','.$limit);
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();	
	}
	
	
	function getAccount()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return false;
	}
	//getall item
	//var $order: field order
	function getAllProduct($id, $select = "*")
	{
		$this->db->select($select);
		$this->db->from($this->table);
		$this->db->join("products","products.product_account_id = accounts.account_id","INNER");
		if(!empty($id))
			$this->db->where("accounts.account_id",$id);
		$this->db->order_by("products.product_id","ASC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	//get all amount receipt
	function getAmount($id)
	{
		$this->db->select("stock_bills.*,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
		$this->db->from("stock_bills");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where("stock_bill_flow",IN);
		$this->db->where("stock_bill_type",STOCK_RECEIPT_TYPE_PURCHASE);
		$this->db->where("stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("stock_bill_object_id",$id);
		$this->db->group_by("stock_bills.stock_bill_id");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	
}