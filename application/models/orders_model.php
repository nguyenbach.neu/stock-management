<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**********
*	table cash_payout and cash_receipts
*	list function cash_payout and cash_receipts
*	tungpa
*/
class orders_model extends MY_Model
{
    var $table = 'orders';
	/*
	*	construct
	*/
    function __construct(){

        parent::__construct();

    }
	// insert cash
	// var $arr is array item

    //fsdfhtry
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	// insert cash
	// var $arr is array item
	function insert_products($arr = array())
	{
		return $this->db->insert_batch("order_products", $arr); 
	}
	function insert_product($arr = array())
	{
		$this->db->insert("order_products", $arr);
        return $this->db->insert_id();
	}
	function insert_histories($arr = array())
	{
		return $this->db->insert_batch("product_stock_history", $arr); 
	}
	function insert_history($arr = array())
	{
		$this->db->insert("order_history", $arr);
        return $this->db->insert_id();
	}
	function insert_stock_history($arr = array())
	{
		$this->db->insert("product_stock_history", $arr);
        return $this->db->insert_id();
	}
	function insert_product_history($arr = array())
	{
		$this->db->insert("order_product_history", $arr);
        return $this->db->insert_id();
	}
	
	// insert row in table product_stock_history
	function insert_st_pro_his($arr = array())
	{
		$this->db->insert("product_stock_history", $arr);
        return $this->db->insert_id();
	}
	
	//update cash_receipts
	// var $arr is array item edit
    //45454545
	function update($arr = array())
	{
		$this->db->where('order_id', $arr["order_id"]);
		return $this->db->update($this->table, $arr); 
	}
	function update_product($arr = array())
	{
		$this->db->where('order_product_id', $arr["order_product_id"]);
		$this->db->update("order_products", $arr); 
	}
	//delete 1 item in table cash_receipts
	// var $id item
	function delete($id)
	{
		$this->db->delete($this->table, array('order_id' => $id, 'order_status' => STOCK_CHANGE_STATUS_DRAFT));
	}
	//delete all item in table order_products by order_id
	// var $id item
	function delete_products($order_id)
	{
		$this->db->query('DELETE FROM order_products WHERE order_id='.$order_id);
	}
	function delete_history($order_id)
	{
		$this->db->query('DELETE FROM order_history WHERE order_id='.$order_id);
	}
	function delete_products_history($order_id)
	{
		$this->db->query('DELETE FROM order_product_history WHERE order_history_id='.$order_id);
	}
	///adfsafs
	function delete_product($id)
	{
		$this->db->delete("order_products", array('order_product_id' => $id)); 
	}
    function try_archive($order_id) {
        $delivery_expenses = $this->_getDiscountForStockIssue($order_id);
        $receipt = $this->_getReceiptForStockIssue($order_id);;
        $discount = $this->_getDeliveryExpensesForStockIssue($order_id);;
        $this->_archive($order_id,floatval($delivery_expenses['0']['total']),floatval($receipt['0']['total']),floatval($discount['0']['total']));
    }
    //Can bang phieu xuat
    function _archive($order_id, $delivery_expenses = 0, $receipt = 0, $discount= 0){
        //Tong tien phieu xuat = chi phi giao hang + tien nhan tren phieu xuat + giam gia
        $total_tmp = $delivery_expenses + $receipt + $discount;
        $query = $this->db->query("SELECT sum(sbp.order_product_price * sbp.order_product_quantity) as totals FROM orders as sb INNER JOIN order_products as sbp ON sb.order_id = sbp.order_id WHERE sb.order_id =".$order_id);
        if($query->num_rows() > 0){
            $total_db =  $query->result();
        }
        $arr = array();
        if($total_tmp == floatval($total_db[0]->totals)){
            $arr['order_archive'] = 1;
        }else{
            $arr['order_archive'] = 0;
        }

        $this->db->where('order_id', $order_id);
        $this->db->update('orders', $arr);
    }

    function _getDiscountForStockIssue($id){
        $query = $this->db->query("SELECT sum(cash_amount) as  total FROM cashs WHERE type = 'discount' AND  order_id =".$id);
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function _getReceiptForStockIssue($id){
        $query = $this->db->query("SELECT sum(cash_amount) as  total FROM cashs WHERE type = 'receipt' AND  order_id =".$id);
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function _getDeliveryExpensesForStockIssue($id){
        $query = $this->db->query("SELECT sum(cash_amount) as  total FROM cashs WHERE type = 'expense' AND  order_id =".$id);
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }
	//getinfo 1 item in table cashs
	// var $id item
    //hkhkh
	function getInfo($id)
	{
		$this->db->select("orders.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name", false);
		$this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = orders.order_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->where("orders.order_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else 
			return false;
	}
	//get all item products in stock issue
	// var $id item
    //hkhkh
	function getInfoProducts($id,$select = "*")
	{
		$this->db->select("order_products.*, products.package_unit as package_unit, product_group.sort_order");
		$this->db->from("order_products");
		$this->db->where("order_id",$id);
        $this->db->join("products", "products.product_id = order_products.product_id","LEFT");
		$this->db->join("product_group","products.product_group = product_group.id","LEFT");
        $this->db->order_by('(CASE WHEN products.product_group IS NULL THEN 1 ELSE 0 END)', FALSE);
		$this->db->order_by("product_group.sort_order","ASC");
        $this->db->order_by("order_products.order_product_id","ASC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else return array();
	}
	//get 1 item products in stock issue
	// var $id item
    //454
	function getInfoProduct($id)
	{
		$this->db->select("*");
		$this->db->from("order_products");
		$this->db->where("order_product_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return array();
	}
    function sumStockPurchases() {

    }
    function sumStockReceipts($type = false, $from = false, $to = false)
    {
        $this->db->select("sum(order_product_price*order_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id and orders.order_type=1","LEFT");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = order_issue.order_object_id and orders.order_type=1 and orders.order_account_id = 0","LEFT");//day la phieu xuat -> order_object_id is user id
        $this->db->where("orders.order_flow",IN);
        $this->db->where("orders.order_status",STOCK_CHANGE_STATUS_PROCESSED);
        $filter = "orders.order_id > 0";

        if (!$type) {
            $type = $this->input->get("type");
        }
        if($type){
            $filter .= " AND orders.order_type = " . $type;
        }
        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumStockReturnReceiptsGroupByUser()
    {
        $this->db->select("users.user_id as user_id, sum(order_product_price*order_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id","inner");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users","users.user_id = order_issue.order_object_id","inner");//day la phieu xuat -> order_object_id is user id
        $this->db->join("order_products","order_products.order_id = orders.order_id","inner");
        $this->db->where("orders.order_flow",IN);
        $this->db->where("orders.order_type",STOCK_RECEIPT_TYPE_RETURN);
        $this->db->where("orders.order_status",1);
        $filter = "orders.order_id > 0";


        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('users.user_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumStockReturnReceiptsGroupByProduct()
    {
        $this->db->select("order_products.product_id as product_id, sum(order_product_quantity) as quantity, sum(order_product_price*order_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("order_products","order_products.order_id = orders.order_id","inner");
        $this->db->join("products","order_products.product_id = products.product_id","left");
        $this->db->where("orders.order_flow",IN);
        $this->db->where("orders.order_type",STOCK_RECEIPT_TYPE_RETURN);
        $this->db->where("orders.order_status",1);
        $filter = "orders.order_id > 0";

        if($this->input->get("supplier_id") != ""){
            $filter .= " AND products.product_supplier_id = " . $this->input->get("supplier_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('order_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function getStockReceipts($start,$limit)
    {
        $this->db->select("orders.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name,sum(order_products.order_product_quantity) as numbers_item,sum(order_product_price*order_product_quantity) as totals", false);
        $this->db->from($this->table);
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id and orders.order_type=1","LEFT");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = order_issue.order_object_id and orders.order_type=1 and orders.order_account_id = 0","LEFT");//day la phieu xuat -> order_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = orders.order_object_id and orders.order_type=".STOCK_RECEIPT_TYPE_PURCHASE." and orders.order_account_id <> 0","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
        $this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
        $this->db->where("orders.order_flow",IN);
        $filter = "orders.order_id > 0";

        if($this->input->get("type")){
            $filter .= " AND orders.order_type = " . $this->input->get("type");
        }

        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);

        $this->db->group_by("orders.order_id");
        $this->db->order_by('orders.order_id','desc');
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function countStockReceipts(){
        $this->db->from($this->table);
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id and orders.order_type=1","LEFT");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = order_issue.order_object_id and orders.order_type=1 and orders.order_account_id = 0","LEFT");//day la phieu xuat -> order_object_id is user id

        $this->db->where("orders.order_flow",IN);
        $filter = "orders.order_id > 0";

        if($this->input->get("type") != ""){
            $filter .= " AND orders.order_type = " . $this->input->get("type");
        }
        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);


        return $this->db->count_all_results();
    }
	//getall item in table cash_receipts
	//var $order: field order


    function sumStockIssuesGroupByUser()
    {
        $this->db->select("orders.order_object_id as user_id, sum(order_product_price*order_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("order_products","order_products.order_id = orders.order_id","inner");
        $this->db->where("orders.order_flow",OUT);
        $this->db->where("orders.order_status",1);
        $filter = "orders.order_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('orders.order_object_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumStockIssuesGroupByProduct()
    {
        $this->db->select("order_products.product_id as product_id, sum(order_product_quantity) as quantity, sum(order_product_price*order_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("order_products","order_products.order_id = orders.order_id","inner");
        $this->db->join("products","order_products.product_id = products.product_id","left");
        $this->db->where("orders.order_flow",OUT);
        $this->db->where("orders.order_status",1);
        $filter = "orders.order_id > 0";

        if($this->input->get("supplier_id") != ""){
            $filter .= " AND products.product_supplier_id = " . $this->input->get("supplier_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('order_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }


    //jladsjfasj
	function getOrders($start,$limit)
	{
		$this->db->select("orders.*, saler.user_name as saler_user_name,creater.user_name as creater_user_name");
		$this->db->from($this->table);
		$this->db->join("users as saler","saler.user_id = orders.order_object_id","LEFT");
		$this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");

        $filter = "orders.order_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND orders.order_object_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

    	$this->db->where($filter);

		$this->db->group_by("orders.order_id");
		$this->db->order_by('orders.order_id','desc');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	//count all with variable follow
	function countOrders(){
		$this->db->select('orders.*');
        $filter = "order_id > 0";
        if($_GET){
            if($this->input->get("sale_user_id") != 0){
                $filter .= " AND order_object_id = ". $this->input->get("sale_user_id");
            }

            if($this->input->get("from") != ""){
                $filter .= " AND UNIX_TIMESTAMP(order_created_date) > " . strtotime($this->input->get("from"));
            }
            if($this->input->get("to") != ""){
                $filter .= " AND UNIX_TIMESTAMP(order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
            }
        }
        $this->db->where($filter);

		$this->db->from($this->table);

		return $this->db->count_all_results();
	}

	//get all stock_issue_history
	function getHistory($id,$limit = false,$start = false)
	{
		$this->db->select("order_history.* ,creater.*, sum(order_product_history.order_product_history_quantity) as numbers_item,sum(order_product_history_price*order_product_history_quantity) as totals");
		$this->db->from("order_history");
        $this->db->join("order_product_history","order_product_history.order_history_id = order_history.order_history_id","LEFT");
		$this->db->join("users as creater","creater.user_id = order_history.order_history_create_user_id","LEFT");
		$this->db->where("order_history.order_id",$id);
		$this->db->group_by("order_history.order_history_id");
		$this->db->order_by("order_history_id","DESC");
		if($limit != false && $start != false)
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getNumHistory($id)
	{
		$this->db->select("*");
		$this->db->from("order_history");
		$this->db->where("order_history.order_id",$id);
		$this->db->order_by("order_history_id","DESC");
		$query = $this->db->get();
		return $query->num_rows();

	}
	function getProductHistory($order_history_id)
	{
		$this->db->select("*");
		$this->db->from("order_product_history");
		//$this->db->join("order_product_history","order_product_history.order_history_id = order_history.order_history_id","LEFT");
		$this->db->where("order_product_history.order_history_id",$order_history_id);
		$this->db->order_by("order_product_history_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getBySaler($id,$datefrom = false,$dateto = false){
		$this->db->select("orders.*,users.*,sum(order_products.order_product_quantity) as numbers_item,sum(order_product_price*order_product_quantity) as totals");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = orders.order_object_id and orders.order_type=" . STOCK_RECEIPT_TYPE_RETURN,"LEFT");
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
		$this->db->where("order_object_id",$id);
		$this->db->where("order_status",STOCK_CHANGE_STATUS_PROCESSED);
        if($datefrom){
            $this->db->where("order_created_date >=  '$datefrom'");
        }
        if ($dateto) {
            $this->db->where("order_created_date <= '$dateto'");
        }
		$this->db->group_by("orders.order_id");
		$this->db->order_by("order_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getBySupplier($id)
	{
		$this->db->select("orders.*,users.*,sum(order_products.order_product_quantity) as numbers_item,sum(order_product_price*order_product_quantity) as totals");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = orders.order_object_id and orders.order_type=" . STOCK_RECEIPT_TYPE_PURCHASE,"LEFT");
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
		$this->db->where("order_object_id",$id);
		$this->db->where("order_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->group_by("orders.order_id");
		$this->db->order_by("orders.order_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getLatestNewIssues()
	{
		$this->db->select("orders.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
		$this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = orders.order_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->where("orders.order_flow",OUT);
		$this->db->where("orders.order_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->order_by("orders.order_id","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLatestNewReceipts()
	{
        $this->db->select("orders.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name", false);
        $this->db->from($this->table);
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id","LEFT");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = order_issue.order_object_id","LEFT");//day la phieu xuat -> order_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = orders.order_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->where("orders.order_flow",IN);
		$this->db->where("orders.order_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->order_by("orders.order_id","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLastestModifiedIssues()
	{
        $this->db->select("orders.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
		$this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = orders.order_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->where("orders.order_flow",OUT);
		$this->db->where("orders.order_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("orders.order_modified_date != ''");
		$this->db->order_by("orders.order_modified_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLastestModifiedReceipts()
	{
        $this->db->select("orders.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name", false);
        $this->db->from($this->table);
        $this->db->join("orders as `order_issue`","order_issue.order_id = orders.order_object_id","LEFT");//tinh huong nhap lai kho -> order_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = order_issue.order_object_id","LEFT");//day la phieu xuat -> order_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = orders.order_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = orders.order_create_user_id","LEFT");
		$this->db->where("orders.order_flow",IN);
		$this->db->where("orders.order_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("orders.order_modified_date != ''");
		$this->db->order_by("orders.order_modified_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}

	
	function countReports($where = ""){
		$this->db->select("*,sum(order_product_price * order_product_quantity) as totals_price, sum(order_product_quantity) as totals_quantity");
		$this->db->from($this->table);
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by("order_products.product_id");
		$query = $this->db->get();
		return $query->num_rows();
	}
	function getReports($where = "",$start,$limit){
		$this->db->select("*,sum(order_product_price * order_product_quantity) as totals_price, sum(order_product_quantity) as totals_quantity");
		$this->db->from($this->table);
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
		$this->db->where('order_status', STOCK_CHANGE_STATUS_PROCESSED);
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by("order_products.product_id");
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return array();
	}
	function getStaffReports($where = "") {
		$this->db->select("*,sum(order_product_price * order_product_quantity * order_flow) * -1 as totals_price, sum(order_product_quantity) as totals_quantity,MONTH(order_created_date) AS mm");
		$this->db->from($this->table);
		$this->db->join("order_products","order_products.order_id = orders.order_id","LEFT");
		$this->db->join("users", "users.user_id = orders.order_object_id", "LEFT");
		$this->db->where('order_status', STOCK_CHANGE_STATUS_PROCESSED);
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by(" mm ,orders.order_object_id");
		$this->db->order_by("mm, user_name", "ASC");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$arr = array();
            foreach($query->result_array() as $k => $item) {
				$user_name = '';
				for($i = 1; $i <= 12; $i++) {
					if($item['mm'] == $i) {
						$arr[$item['user_id']][$i] = $item;
						$user_name = $item['user_name'];
					}
					elseif(!isset($arr[$item['user_id']][$i])){
						$arr[$item['user_id']][$i] = array();
					}
				}
				$arr[$item['user_id']]['user_name'] = $user_name;
			}
			return $arr;
		}	
		else
			return array();
	}
	function getStockAtMoment($date_time, $supplier_id = false)
	{
		$this->db->select('max(product_history_id) as max_history_id');
		$this->db->from('product_stock_history');
		// $this->db->join('products', 'products.product_id = product_stock_history.product_id', 'INNER');
		$this->db->where('UNIX_TIMESTAMP(product_stock_history_date) <= ' . $date_time);
		$this->db->group_by('product_stock_history.product_id');
		$this->db->order_by('product_stock_history_date', 'desc');
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$arr_id = array();
			// print_r($query->result_array());die;
            foreach($query->result_array() as $k => $item) {
				$arr_id[] = $item['max_history_id'];
			}
			$lst_id = implode(',', $arr_id);
			$this->db->select('*');
			$this->db->from('product_stock_history');
			$this->db->join('products', 'products.product_id = product_stock_history.product_id', 'INNER');
			$this->db->where_in('product_history_id', $arr_id);
			
			if ($supplier_id) {
				$this->db->where("product_supplier_id",$supplier_id);
			}
			$this->db->where("new_product_stock_quantity <> 0");
			
			$this->db->order_by('products.product_code', 'ASC');
			$query = $this->db->get();
			return $query->result_array();
		}
		else
			return array();
	}
    function getAllStockIssues()
    {
        $this->db->select("orders.*,users.*");
        $this->db->from($this->table);
        $this->db->join("users","users.user_id = orders.order_create_user_id and orders.order_type=" . STOCK_RECEIPT_TYPE_RETURN,"LEFT");
        $this->db->where("orders.order_status",1);
        $this->db->order_by("orders.order_id","DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;
    }

    function getOpenStockIssues()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = orders.order_object_id","LEFT");//nguoi nhan hang
		$this->db->where("orders.order_status",1);
		$this->db->where("orders.order_flow",-1);
		$this->db->where("orders.order_archive",0);
		$this->db->order_by("orders.order_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
    
    //get item supplier
	function getItemSupplier($id)
	{
		$this->db->select("*");
		$this->db->from("supplies");
		$this->db->where("supplier_id",$id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
    function getStockFromStockBills() {
        $this->db->select("order_products.product_id as product_id, sum(order_product_quantity * orders.order_flow) as quantity", false);
        $this->db->from($this->table);
        $this->db->join("order_products","order_products.order_id = orders.order_id","inner");
        $this->db->where("orders.order_status",1);
        $filter = "orders.order_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(orders.order_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('order_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }


}