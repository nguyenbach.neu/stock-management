<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**********
*	table cash_payout and cash_receipts
*	list function cash_payout and cash_receipts
*	tungpa
*/
class Cashs_model extends MY_Model
{
    var $table = 'cashs';
	/*
	*	construct
	*/
    function __construct(){

        parent::__construct();

    }
	// insert cash
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	//update cash_receipts
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('cash_id', $arr["cash_id"]);
		return $this->db->update($this->table, $arr); 
	}
	//delete 1 item in table cash_receipts
	// var $id item
	function delete($id)
	{
		$this->db->delete($this->table, array('cash_id' => $id)); 
	}
	//getinfo 1 item in table cashs
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = cashs.saler_id","LEFT");
		$this->db->join("supplies","supplies.supplier_id = cashs.supplier_id","LEFT");
		$this->db->where("cash_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else 
			return false;
	}

	function countCashs()
    {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }

    function getAllCashs()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }


	function sumReceiptsByStockIssue($stock_issue_id) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'receipt');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
	}
	function sumDeliveryExpensesByStockIssue($stock_issue_id) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
	}
	function sumDiscountsByStockIssue($stock_issue_id) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
	}
    function sumDebitsByStockIssue($stock_issue_id) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
	function getReceiptsByStockIssue($stock_issue_id) {
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->where('type', 'receipt');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
		
	}
	function getCommissionDiscountGroupBySupplier()
    {
        $this->db->select("sum(cashs.cash_amount) as sum, supplies.supplier_id");
        $this->db->from($this->table);
        $this->db->join("supplies","supplies.supplier_id = cashs.supplier_id");
        $this->db->group_by("supplies.supplier_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function getReceipts($start, $limit) {
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->limit($limit,$start);
        $this->db->where('type', 'receipt');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }
        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumReceipts($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'receipt');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }
        $this->db->where($filter);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countReceipts() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'receipt');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
		
        $this->db->where($filter);
        return $this->db->count_all_results();
    }
    function sumDeliveryExpensesGroupByUser($from = false, $to = false) {
        $this->db->select("stock_bills.stock_bill_object_id as user_id,sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1 and stock_bill_flow=-1 and cashs.account_id=0","inner");
        $this->db->where("cashs.type",'expense');
        $filter = "cash_id > 0";

		if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
		} else if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
		
		if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
		} if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);

        $this->db->group_by("stock_bills.stock_bill_object_id");
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }
    function sumReceiptsGroupByUser($from = false, $to = false) {
        $this->db->select("users.user_id,sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1 and stock_bill_flow=-1","inner");
        $this->db->join("users","stock_bills.stock_bill_object_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where("cashs.type",'receipt');
        $filter = "cash_id > 0";

		if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
		} else if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
		
		if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
		} if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        
		$this->db->where($filter);

        $this->db->group_by("users.user_id");
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function sumDiscountAdvancesGroupBySaler($saler_id)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0 AND cashs.saler_id = $saler_id";
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDiscountReceiptsGroupBySaler($saler_id)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0 AND cashs.saler_id = $saler_id";
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDebitAdvancesGroupBySaler($saler_id)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0 AND cashs.saler_id = $saler_id";
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDebitReceiptsGroupBySaler($saler_id)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0 AND cashs.saler_id = $saler_id";
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    function sumDiscountAdvancesGroupByUser($from = false, $to = false, $skip_url_param = false)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum, users.deleted");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $this->db->where('users.deleted', 0);
        $filter = "cash_id > 0";
        if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        } else if($this->input->get("from") != "" && !$skip_url_param){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        } if($this->input->get("to") != "" && !$skip_url_param){
        $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
    }
        $this->db->where($filter);

        $this->db->group_by("users.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDebitAdvancesGroupByUser($from = false, $to = false, $skip_url_param = false)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum, users.deleted");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $this->db->where('users.deleted', 0);
        $filter = "cash_id > 0";

        if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        } else if ($this->input->get("from") != "" && !$skip_url_param) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }

        if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        } if($this->input->get("to") != "" && !$skip_url_param){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);

        $this->db->group_by("users.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDiscountReceivesGroupByUser($from = false, $to = false, $skip_url_param = false)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum, users.deleted");
        $this->db->from($this->table);
		$this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);
        $this->db->where('users.deleted', 0);
        $filter = "cash_id > 0";

		if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
		} else if($this->input->get("from") != "" && !$skip_url_param){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
		
		if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
		} if($this->input->get("to") != "" && !$skip_url_param){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        
		$this->db->where($filter);

		$this->db->group_by("users.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDebitReceivesGroupByUser($from = false, $to = false,$skip_url_param = false)
    {
        $this->db->select("users.user_id, sum(cash_amount) as sum, users.deleted");
        $this->db->from($this->table);
        $this->db->join("users","cashs.saler_id = users.user_id and users.user_role=".ROLE_SALEPERSON,"inner");
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $this->db->where('users.deleted', 0);
        $filter = "cash_id > 0";

        if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        } else if($this->input->get("from") != "" && !$skip_url_param){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }

        if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        } if($this->input->get("to") != "" && !$skip_url_param){
        $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
    }

        $this->db->where($filter);

        $this->db->group_by("users.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    function sumAccount($from = false, $to = false) {
        $this->db->select("sum(cashs.cash_amount * cashs.cash_follow) as sum");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id", 'left');
        $this->db->where("accounts.account_cash = 1");
        $filter = 'cashs.account_id > 0';
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
    }

    //getall item in table cash_receipts
    //var $order: field order
    function getDiscountAdvances($start,$limit)
    {
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name,creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');//saler - actually is delivery person here
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->limit($limit,$start);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");//this is actual saler id who is recorded for advacne
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
		
        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    function getDiscountAdvancesByStockIssue($stock_issue_id){
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }



    function sumDiscountAdvances($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
				

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumDebitAdvances($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }


        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countDiscountAdvances() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
				
        $this->db->where($filter);

        return $this->db->count_all_results();
    }
	
    function getDebitAdvances($start,$limit)
    {
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name,creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');//saler - actually is delivery person here
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->limit($limit,$start);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");//this is actual saler id who is recorded for advacne
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
		
        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    function getDebitAdvancesByStockIssue($stock_issue_id){
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        $filter .= " AND cashs.stock_bill_id = $stock_issue_id";

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }




    function countDebitAdvances() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("stock_bill_issue_id") != ""){
            $filter .= " AND stock_bills.stock_bill_id = ".  $this->input->get("stock_bill_issue_id");
        }		
				
        $this->db->where($filter);

        return $this->db->count_all_results();
    }
	

	//getall item in table cash_receipts
	//var $order: field order
	function getAll($follow, $start, $limit, $filter = '',$typefield='', $order = "cash_id",$type = "DESC")
	{
		$this->db->select("*");
		$this->db->from($this->table);

		if($typefield != null){
			$this->db->where("type",$typefield);	
		}
		if ($filter) {
			$this->db->where($filter);
		}
		$this->db->order_by($order, $type);
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
		return array();
	}
	//count all with variable follow
	function countAll($follow, $filter = '',$typefield=''){
		$this->db->select("count(*)");
		$this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
		if($typefield != null){
			$this->db->where("type",$typefield);	
		}
		if ($filter) {
			$this->db->where($filter);
		}
		return $this->db->count_all_results();
	}


    function sumWithdraws($from = false, $to = false)
    {
        $this->db->select("sum(cash_amount) as sum", false);
        $this->db->from($this->table);
        $this->db->where('type', 'withdraw');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }

  
    function countWithdraws() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
         $this->db->where('type', 'withdraw');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }
    //get list withdraw (rut tien)
   	function getWithdraws($start, $limit)
	{
		$this->db->select("*,creater.user_name as creater_user_name");
		$this->db->from($this->table);
		$this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->where('type', 'withdraw');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

		$this->db->limit($limit,$start);

		$query = $this->db->get();
		//echo $this->db->last_query();die;
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
		return array();
	}

    function countLoads() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'load');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }
    function sumLoads($from = false, $to = false)
    {
        $this->db->select("sum(cash_amount) as sum", false);
        $this->db->from($this->table);
        $this->db->where('type', 'load');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    //get list withdraw (rut tien)
    function getLoads($start, $limit)
    {
        $this->db->select("*,creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->where('type', 'load');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND cashs.account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);

        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }

    function getOperatingExpenses($start, $limit) {
        $this->db->select("cashs.*, accounts.*, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->limit($limit,$start);
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumOperatingExpenses($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->limit($to,$from);
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countOperatingExpenses() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }    //get list expense
   	function getAllExpense($follow, $start, $limit, $filter = '',$typefield='', $order = "cash_id",$type = "DESC")
	{
		$this->db->select("*,accounts.account_name");
		$this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
		if($typefield != null){
			$this->db->where("type",$typefield);	
		}
		if ($filter) {
			$this->db->where($filter);
		}
		$this->db->order_by($order, $type);
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
		return array();
	}

    function getDeliveryExpenses($start, $limit) {
        $this->db->select("cashs.*, stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->join("users as saler","stock_bills.stock_bill_object_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->limit($limit,$start);
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDeliveryExpenses($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countDeliveryExpenses() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->join("stock_bills","stock_bills.stock_bill_id = cashs.stock_bill_id and stock_bill_status=1", 'left');
        $this->db->where('type', 'expense');
        $this->db->where('cash_follow', 0);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }

    function getDiscountReceipts($start,$limit)
    {
        $this->db->select("cashs.*, accounts.*, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","cashs.account_id = accounts.account_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->limit($limit,$start);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND cashs.account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function getDebitReceipts($start,$limit)
    {
        $this->db->select("cashs.*, accounts.*, creater.user_name as creater_user_name, saler.user_name as saler_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","cashs.account_id = accounts.account_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');
        $this->db->join("users as saler","saler.user_id = cashs.saler_id", 'left');
        $this->db->limit($limit,$start);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("account_id") != 0){
            $filter .= " AND cashs.account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumDiscountReceiptsGroupByUser()
    {
        $this->db->select("user.user_id, sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }

    function sumDiscountReceipts($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);
		
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumDebitReceipts($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND  saler_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumAdvanceReceives($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countDiscountReceipts() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }

    function countDebitReceipts() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'advance');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }


    function getCommissionReceipts($start,$limit)
    {
        $this->db->select("cashs.*, accounts.*, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","cashs.account_id = accounts.account_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->limit($limit,$start);
        $this->db->where('type', 'commission');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND cashs.account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumCommissionReceipts($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'commission');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countCommissionReceipts() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'commission');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }

    function getCommissionDiscounts($start,$limit)
    {
        $this->db->select("cashs.*, accounts.*, creater.user_name as creater_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","cashs.account_id = accounts.account_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->limit($limit,$start);
        $this->db->where('type', 'commission_discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";

        if($this->input->get("account_id") != 0){
            $filter .= " AND cashs.account_id = ". $this->input->get("account_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumCommissionDiscounts($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'commission_discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function countCommissionDiscounts() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'commission_discount');
        $this->db->where('cash_follow', 1);
        $filter = "cash_id > 0";
        if($this->input->get("account_id") != 0){
            $filter .= " AND account_id = ". $this->input->get("account_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }
    //function getall receipt by user_id
	function getAllReceipt($id,$follow = OUT,$datefrom = false,$dateto = false)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("cash_follow",$follow);
		$this->db->where("cash_object_id",$id);
        if($datefrom){
            $this->db->where("cash_date >=  '$datefrom'");
        }
        if ($dateto) {
            $this->db->where("cash_date <= '$dateto'");
        }
        $this->db->order_by("cash_id","DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			return $query->result_array();
		else 
			return array();
	}
	function getLastestCashModified() {
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("cash_modify_date != ''");
		$this->db->order_by("cash_modify_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;	
	}
	function getLastestCashReceipts() {
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->order_by("cash_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;	
	}
	
	//get discount report
	function getDiscountReport($filter = '',$cash_follow = 0,$order = "cash_id",$type = "DESC")
	{
		$this->db->select("*");
		$this->db->from('cashs');
		$this->db->where('type = "discount" AND cash_follow = '.$cash_follow);
		if ($filter) {
			$this->db->where($filter);
		}
		$this->db->order_by($order,$type);
		//echo $this->db->last_query();
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return false;
	}
    function sumTransferOut() {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'tranfer');
        $this->db->where('cash_follow', -1);
        $filter = "account_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumTransferIn() {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'tranfer');
        $this->db->where('cash_follow', 1);
        $filter = "account_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
    function sumCost() {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'cost');
        $this->db->where('cash_follow', -1);
        $filter = "account_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }

    function sumSalaries($from = false, $to = false) {
        $this->db->select("sum(cash_amount) as sum");
        $this->db->from($this->table);
        $this->db->where('type', 'salary');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND saler_id = ". $this->input->get("sale_user_id");
        }
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($to));
        }
        $this->db->where($filter);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }

    function countSalaries() {
        $this->db->select("count(*)");
        $this->db->from($this->table);
        $this->db->where('type', 'salary');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND saler_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        return $this->db->count_all_results();
    }
    //get list withdraw (rut tien)
    function getSalaries($start, $limit)
    {
        $this->db->select("cashs.*,accounts.*,creater.user_name as creater_user_name, saler.user_name as saler_user_name");
        $this->db->from($this->table);
        $this->db->join("accounts","accounts.account_id = cashs.account_id");
        $this->db->join("users as saler","cashs.saler_id = saler.user_id", 'left');
        $this->db->join("users as creater","cashs.cash_create_user_id = creater.user_id", 'left');

        $this->db->where('type', 'salary');
        $this->db->where('cash_follow', -1);
        $filter = "cash_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND cashs.saler_id = ". $this->input->get("sale_user_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(cash_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);
        $this->db->order_by("cash_id","DESC");

        $this->db->limit($limit,$start);

        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }


    function getAllStockissues($follow,$start,$limit,$filter = '', $order = "billproducts.stock_bill_id",$type = "DESC")
    {
        $this->db->select("billproducts.*,sp.*,stock_bill.*");
        $this->db->from("stock_bill_products as billproducts");
        $this->db->join("products as p"," p.product_id = billproducts.product_id");
        $this->db->join("supplies as sp","sp.supplier_id  = p.product_supplier_id");
        $this->db->join("stock_bills as stock_bill","stock_bill.stock_bill_id  = billproducts.stock_bill_id");
        //echo $this->db->last_query();die('zzzzz');
        if ($filter) {
            $this->db->where($filter);
        }

        //$this->db->group_by("stock_bills.stock_bill_id");
        $this->db->order_by($order,$type);
        if(!empty($limit)){
            $this->db->limit($limit,$start);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
}