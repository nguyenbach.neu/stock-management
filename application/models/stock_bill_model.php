<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**********
*	table cash_payout and cash_receipts
*	list function cash_payout and cash_receipts
*	tungpa
*/
class Stock_bill_model extends MY_Model
{
    var $table = 'stock_bills';
	/*
	*	construct
	*/
    function __construct(){

        parent::__construct();

    }
	// insert cash
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	// insert cash
	// var $arr is array item
	function insert_products($arr = array())
	{
		return $this->db->insert_batch("stock_bill_products", $arr); 
	}
	function insert_product($arr = array())
	{
		$this->db->insert("stock_bill_products", $arr);
        return $this->db->insert_id();
	}
	function insert_histories($arr = array())
	{
		return $this->db->insert_batch("product_stock_history", $arr); 
	}
	function insert_history($arr = array())
	{
		$this->db->insert("stock_bill_history", $arr);
        return $this->db->insert_id();
	}
	function insert_stock_history($arr = array())
	{
		$this->db->insert("product_stock_history", $arr);
        return $this->db->insert_id();
	}
	function insert_product_history($arr = array())
	{
		$this->db->insert("stock_bill_product_history", $arr);
        return $this->db->insert_id();
	}
	
	// insert row in table product_stock_history
	function insert_st_pro_his($arr = array())
	{
		$this->db->insert("product_stock_history", $arr);
        return $this->db->insert_id();
	}
	
	//update cash_receipts
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('stock_bill_id', $arr["stock_bill_id"]);
		return $this->db->update($this->table, $arr); 
	}
	function update_product($arr = array())
	{
		$this->db->where('stock_bill_product_id', $arr["stock_bill_product_id"]);
		$this->db->update("stock_bill_products", $arr); 
	}
	//delete 1 item in table cash_receipts
	// var $id item
	function delete($id)
	{
		$this->db->delete($this->table, array('stock_bill_id' => $id, 'stock_bill_status' => STOCK_CHANGE_STATUS_DRAFT));
	}
	//delete all item in table stock_bill_products by stock_bill_id
	// var $id item
	function delete_products($stock_bill_id)
	{
		$this->db->query('DELETE FROM stock_bill_products WHERE stock_bill_id='.$stock_bill_id);
	}
	function delete_history($stock_bill_id)
	{
		$this->db->query('DELETE FROM stock_bill_history WHERE stock_bill_id='.$stock_bill_id);
	}
	function delete_products_history($stock_bill_id)
	{
		$this->db->query('DELETE FROM stock_bill_product_history WHERE stock_bill_history_id='.$stock_bill_id);
	}
	function delete_product($id)
	{
		$this->db->delete("stock_bill_products", array('stock_bill_product_id' => $id)); 
	}
	//getinfo 1 item in table cashs
	// var $id item
	function getTotal($id) {
		$query = $this->db->query("SELECT sum(sbp.stock_bill_product_price * sbp.stock_bill_product_quantity) as total FROM stock_bills as sb INNER JOIN stock_bill_products as sbp ON sb.stock_bill_id = sbp.stock_bill_id WHERE sb.stock_bill_id =".$id);
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();        	
	}
	function getInfo($id)
	{
		$this->db->select("stock_bills.*, if(saler1.user_name IS NULL, saler2.user_name, saler1.user_name) as saler_user_name,supplies.supplier_name, creater.user_name as creater_user_name", false);
		$this->db->from($this->table);
        $this->db->join("users as saler1","saler1.user_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_flow=".OUT,"LEFT");

        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=1 and stock_bills.stock_bill_flow=".IN,"LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler2","saler2.user_id = stock_bill_issue.stock_bill_object_id and stock_bills.stock_bill_type=1 and stock_bills.stock_bill_account_id = 0 and stock_bills.stock_bill_flow=".IN,"LEFT");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=".STOCK_RECEIPT_TYPE_PURCHASE." and stock_bills.stock_bill_account_id <> 0 and stock_bills.stock_bill_flow=".IN,"LEFT");

        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");

		$this->db->where("stock_bills.stock_bill_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else 
			return false;
	}
	//get all item products in stock issue
	// var $id item
	function getInfoProducts($id,$select = "*")
	{
		$this->db->select("stock_bill_products.*, products.package_unit");
		$this->db->from("stock_bill_products");
        $this->db->where("stock_bill_id",$id);
        $this->db->join("products", "products.product_id = stock_bill_products.product_id","LEFT");
        $this->db->join("product_group","products.product_group = product_group.id","LEFT");
        $this->db->order_by('(CASE WHEN products.product_group IS NULL THEN 1 ELSE 0 END)', FALSE);
        $this->db->order_by("product_group.sort_order","ASC");
        $this->db->order_by("stock_bill_product_id","ASC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else return array();
	}
	//get 1 item products in stock issue
	// var $id item
	function getInfoProduct($id)
	{
		$this->db->select("*");
		$this->db->from("stock_bill_products");
		$this->db->where("stock_bill_product_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return array();
	}
    function sumStockPurchases() {

    }
    function sumStockDiscount(){
        $this->db->select("sum(products.product_cost*stock_bill_products.stock_bill_product_quantity) as sum ");
        $this->db->from("stock_bill_products");
        $this->db->join("products","products.product_id = stock_bill_products.product_id");
        $this->db->where("stock_bill_product_price", 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        } else {
            return array();
        }
    }
    function sumStockReceipts($type = false, $from = false, $to = false)
    {
        $this->db->select("sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=1","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = stock_bill_issue.stock_bill_object_id and stock_bills.stock_bill_type=1 and stock_bills.stock_bill_account_id = 0","LEFT");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
        $filter = "stock_bills.stock_bill_id > 0";

        if (!$type) {
            $type = $this->input->get("type");
        }
        if($type){
            $filter .= " AND stock_bills.stock_bill_type = " . $type;
        }
        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }
        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
    }
	function sumStockReturnReceiptsByStockIssue($stock_issue_id) {
        $this->db->select("sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $filter = "stock_bills.stock_bill_id > 0";
        $filter .= " AND stock_bills.stock_bill_object_id = $stock_issue_id";

        $this->db->where($filter);
        
		$query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();
	}
	function getCommissionDiscountGroupBySupplier()
    {
        $this->db->select("supplies.supplier_id, supplies.supplier_name, sum(stock_bill_products.stock_bill_product_quantity*stock_bill_products.stock_bill_product_discount) as sum");
        $this->db->from('stock_bill_products');
        $this->db->join("stock_bills","stock_bills.stock_bill_id = stock_bill_products.stock_bill_id");
        $this->db->join("products","stock_bill_products.product_id = products.product_id");
        $this->db->join("supplies","supplies.supplier_id = products.product_supplier_id");
        $this->db->where("stock_bills.stock_bill_flow",'-1');
        $this->db->group_by("supplies.supplier_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function getCommissionStockGroupBySupplier()
    {
        $this->db->select("supplies.supplier_id, supplies.supplier_name, sum(stock_bill_products.stock_bill_product_quantity*stock_bill_products.stock_bill_product_cost) as sum");
        $this->db->from('stock_bill_products');
        $this->db->join("stock_bills","stock_bills.stock_bill_id = stock_bill_products.stock_bill_id");
        $this->db->join("products","stock_bill_products.product_id = products.product_id");
        $this->db->join("supplies","supplies.supplier_id = products.product_supplier_id");
        $this->db->where("stock_bills.stock_bill_flow",'1');
        $this->db->where("stock_bill_products.stock_bill_product_price",'0');
        $this->db->group_by("supplies.supplier_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
	function getStockReturnReceiptsByStockIssue($stock_issue_id) {
        $this->db->select("stock_bills.*, sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals", false);
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=1","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $filter = "stock_bills.stock_bill_id > 0";
        $filter .= " AND stock_bills.stock_bill_object_id = $stock_issue_id";

        $this->db->where($filter);

        $this->db->group_by("stock_bills.stock_bill_id");
        $this->db->order_by('stock_bills.stock_bill_id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
		
	}
    function sumStockReturnReceiptsGroupByUser($from = false, $to = false)
    {
        $this->db->select("users.user_id as user_id, sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id","inner");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users","users.user_id = stock_bill_issue.stock_bill_object_id","inner");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","inner");
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $this->db->where("stock_bills.stock_bill_type",STOCK_RECEIPT_TYPE_RETURN);
        $this->db->where("stock_bills.stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";

		if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($from);
		} else if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
		
		if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($to));
		} else if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('users.user_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumStockReturnReceiptsGroupByProduct()
    {
        $this->db->select("stock_bill_products.product_id as product_id, sum(stock_bill_product_quantity) as quantity, sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","inner");
        $this->db->join("products","stock_bill_products.product_id = products.product_id","left");
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $this->db->where("stock_bills.stock_bill_type",STOCK_RECEIPT_TYPE_RETURN);
        $this->db->where("stock_bills.stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("supplier_id") != ""){
            $filter .= " AND products.product_supplier_id = " . $this->input->get("supplier_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('stock_bill_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function getStockReceipts($start,$limit)
    {
        $this->db->select("stock_bills.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name,sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals", false);
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=1","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = stock_bill_issue.stock_bill_object_id and stock_bills.stock_bill_type=1 and stock_bills.stock_bill_account_id = 0","LEFT");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=".STOCK_RECEIPT_TYPE_PURCHASE." and stock_bills.stock_bill_account_id <> 0","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->where("stock_bills.stock_bill_flow",IN);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("type")){
            $filter .= " AND stock_bills.stock_bill_type = " . $this->input->get("type");
        }

        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        if($this->input->get("supplier_id")){
            $filter .= " AND supplies.supplier_id = ".$this->input->get("supplier_id");
        }
        $this->db->where($filter);

        $this->db->group_by("stock_bills.stock_bill_id");
        $this->db->order_by('stock_bills.stock_bill_id','desc');
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function countStockReceipts(){
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=1","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = stock_bill_issue.stock_bill_object_id and stock_bills.stock_bill_type=1 and stock_bills.stock_bill_account_id = 0","LEFT");//day la phieu xuat -> stock_bill_object_id is user id

        $this->db->where("stock_bills.stock_bill_flow",IN);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("type") != ""){
            $filter .= " AND stock_bills.stock_bill_type = " . $this->input->get("type");
        }
        if ($this->input->get('sale_user_id')) {
            $filter .= " AND saler.user_id = " . $this->input->get('sale_user_id');
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);


        return $this->db->count_all_results();
    }
	//getall item in table cash_receipts
	//var $order: field order
    function sumStockIssues($from = false, $to = false) {
        $this->db->select("sum(stock_bill_product_price*stock_bill_product_quantity) as sum");
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->where("stock_bill_flow",OUT);
        $this->db->where("stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }

        if (!$from) {
            $from = $this->input->get("from");
        }
        if($from){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($from);
        }
        if (!$to) {
            $to = $this->input->get("to");
        }
        if($to){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($to));
        }

        $this->db->where($filter);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return array();

    }
    function sumStockIssuesGroupByUser($from = false, $to = false)
    {
        $this->db->select("stock_bills.stock_bill_object_id as user_id, sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","inner");
        $this->db->where("stock_bills.stock_bill_flow",OUT);
        $this->db->where("stock_bills.stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";

		if ($from) {
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($from);
		} else if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
		
		if ($to) {
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($to));
		} else if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        
		$this->db->where($filter);
        $this->db->group_by('stock_bills.stock_bill_object_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
    function sumStockIssuesGroupByProduct()
    {
        $this->db->select("stock_bill_products.product_id as product_id, sum(stock_bill_product_quantity) as quantity, sum(stock_bill_product_price*stock_bill_product_quantity) as sum", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","inner");
        $this->db->join("products","stock_bill_products.product_id = products.product_id","left");
        $this->db->where("stock_bills.stock_bill_flow",OUT);
        $this->db->where("stock_bills.stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("supplier_id") != ""){
            $filter .= " AND products.product_supplier_id = " . $this->input->get("supplier_id");
        }
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('stock_bill_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
	function getStockIssues($start = false,$limit = false)
	{
		$this->db->select("stock_bills.*, saler.user_name as saler_user_name,creater.user_name as creater_user_name,sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
		$this->db->from($this->table);
		$this->db->join("users as saler","saler.user_id = stock_bills.stock_bill_object_id","LEFT");
		$this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where("stock_bill_flow",OUT);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("sale_user_id") != 0){
            $filter .= " AND stock_bills.stock_bill_object_id = ". $this->input->get("sale_user_id");
        }

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

    	$this->db->where($filter);

		$this->db->group_by("stock_bills.stock_bill_id");
		$this->db->order_by('stock_bills.stock_bill_id','desc');
        $this->db->limit($limit,$start);

		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	function getInfoStockIssues(){
        $this->db->select("stock_bills.*, saler.user_name as saler_user_name,creater.user_name as creater_user_name,sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
        $this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = stock_bills.stock_bill_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
        $this->db->where("stock_bill_flow",OUT);
        $this->db->where("stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }

        $this->db->where($filter);

        $this->db->group_by("stock_bills.stock_bill_id");
        $this->db->order_by('stock_bills.stock_bill_id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
	//count all with variable follow
	function countStockIssues(){
		$this->db->where("stock_bill_flow",OUT);
        $filter = "stock_bill_id > 0";
        if($_GET){
            if($this->input->get("sale_user_id") != 0){
                $filter .= " AND stock_bill_object_id = ". $this->input->get("sale_user_id");
            }

            if($this->input->get("from") != ""){
                $filter .= " AND UNIX_TIMESTAMP(stock_bill_created_date) > " . strtotime($this->input->get("from"));
            }
            if($this->input->get("to") != ""){
                $filter .= " AND UNIX_TIMESTAMP(stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
            }
        }
        $this->db->where($filter);

		$this->db->from($this->table);

		return $this->db->count_all_results();
	}

	//get all stock_issue_history
	function getHistory($id,$limit = false,$start = false)
	{
		$this->db->select("stock_bill_history.* ,creater.*, sum(stock_bill_product_history.stock_bill_product_history_quantity) as numbers_item,sum(stock_bill_product_history_price*stock_bill_product_history_quantity) as totals");
		$this->db->from("stock_bill_history");
        $this->db->join("stock_bill_product_history","stock_bill_product_history.stock_bill_history_id = stock_bill_history.stock_bill_history_id","LEFT");
		$this->db->join("users as creater","creater.user_id = stock_bill_history.stock_bill_history_create_user_id","LEFT");
		$this->db->where("stock_bill_history.stock_bill_id",$id);
		$this->db->group_by("stock_bill_history.stock_bill_history_id");
		$this->db->order_by("stock_bill_history_id","DESC");
		if($limit != false && $start != false)
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getNumHistory($id)
	{
		$this->db->select("*");
		$this->db->from("stock_bill_history");
		$this->db->where("stock_bill_history.stock_bill_id",$id);
		$this->db->order_by("stock_bill_history_id","DESC");
		$query = $this->db->get();
		return $query->num_rows();

	}
	function getProductHistory($stock_bill_history_id)
	{
		$this->db->select("*");
		$this->db->from("stock_bill_product_history");
		//$this->db->join("stock_bill_product_history","stock_bill_product_history.stock_bill_history_id = stock_bill_history.stock_bill_history_id","LEFT");
		$this->db->where("stock_bill_product_history.stock_bill_history_id",$stock_bill_history_id);
		$this->db->order_by("stock_bill_product_history_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getBySaler($id,$datefrom = false,$dateto = false){
		$this->db->select("stock_bills.*,users.*,sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=" . STOCK_RECEIPT_TYPE_RETURN,"LEFT");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where("stock_bill_object_id",$id);
		$this->db->where("stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
        if($datefrom){
            $this->db->where("stock_bill_created_date >=  '$datefrom'");
        }
        if ($dateto) {
            $this->db->where("stock_bill_created_date <= '$dateto'");
        }
		$this->db->group_by("stock_bills.stock_bill_id");
		$this->db->order_by("stock_bill_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getBySupplier($id)
	{
		$this->db->select("stock_bills.*,users.*,sum(stock_bill_products.stock_bill_product_quantity) as numbers_item,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = stock_bills.stock_bill_object_id and stock_bills.stock_bill_type=" . STOCK_RECEIPT_TYPE_PURCHASE,"LEFT");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where("stock_bill_object_id",$id);
		$this->db->where("stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->group_by("stock_bills.stock_bill_id");
		$this->db->order_by("stock_bills.stock_bill_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getLatestNewIssues()
	{
		$this->db->select("stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
		$this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = stock_bills.stock_bill_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
		$this->db->where("stock_bills.stock_bill_flow",OUT);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->order_by("stock_bills.stock_bill_id","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLatestNewReceipts()
	{
        $this->db->select("stock_bills.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name", false);
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = stock_bill_issue.stock_bill_object_id","LEFT");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = stock_bills.stock_bill_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
		$this->db->where("stock_bills.stock_bill_flow",IN);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->order_by("stock_bills.stock_bill_id","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLastestModifiedIssues()
	{
        $this->db->select("stock_bills.*, saler.user_name as saler_user_name, creater.user_name as creater_user_name");
		$this->db->from($this->table);
        $this->db->join("users as saler","saler.user_id = stock_bills.stock_bill_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
		$this->db->where("stock_bills.stock_bill_flow",OUT);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("stock_bills.stock_bill_modified_date != ''");
		$this->db->order_by("stock_bills.stock_bill_modified_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
	function getLastestModifiedReceipts()
	{
        $this->db->select("stock_bills.*, if(supplies.supplier_name IS NULL, saler.user_name, supplies.supplier_name) as object_name, creater.user_name as creater_user_name", false);
        $this->db->from($this->table);
        $this->db->join("stock_bills as `stock_bill_issue`","stock_bill_issue.stock_bill_id = stock_bills.stock_bill_object_id","LEFT");//tinh huong nhap lai kho -> stock_bill_object_id is phieu xuat
        $this->db->join("users as saler","saler.user_id = stock_bill_issue.stock_bill_object_id","LEFT");//day la phieu xuat -> stock_bill_object_id is user id
        $this->db->join("supplies","supplies.supplier_id = stock_bills.stock_bill_object_id","LEFT");
        $this->db->join("users as creater","creater.user_id = stock_bills.stock_bill_create_user_id","LEFT");
		$this->db->where("stock_bills.stock_bill_flow",IN);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("stock_bills.stock_bill_modified_date != ''");
		$this->db->order_by("stock_bills.stock_bill_modified_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}

	
	function countReports($where = ""){
		$this->db->select("*,sum(stock_bill_product_price * stock_bill_product_quantity) as totals_price, sum(stock_bill_product_quantity) as totals_quantity");
		$this->db->from($this->table);
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by("stock_bill_products.product_id");
		$query = $this->db->get();
		return $query->num_rows();
	}
	function getReports($where = "",$start,$limit){
		$this->db->select("*,sum(stock_bill_product_price * stock_bill_product_quantity) as totals_price, sum(stock_bill_product_quantity) as totals_quantity");
		$this->db->from($this->table);
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where('stock_bill_status', STOCK_CHANGE_STATUS_PROCESSED);
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by("stock_bill_products.product_id");
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return array();
	}
	function getStaffReports($where = "") {
		$this->db->select("users.user_id, users.user_name, sum(stock_bill_product_price * stock_bill_product_quantity * stock_bill_flow) * -1 as totals_price, sum(stock_bill_product_quantity) as totals_quantity,MONTH(stock_bill_created_date) AS mm");
		$this->db->from($this->table);
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->join("users", "users.user_id = stock_bills.stock_bill_object_id", "LEFT");
		$this->db->where('stock_bill_status', STOCK_CHANGE_STATUS_PROCESSED);
		if(!empty($where))
			$this->db->where($where);
		$this->db->group_by(" mm ,stock_bills.stock_bill_object_id");
		$this->db->order_by("mm, user_name", "ASC");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$arr = array();
            foreach($query->result_array() as $k => $item) {
				$user_name = '';
				for($i = 1; $i <= 12; $i++) {
					if($item['mm'] == $i) {
						$arr[$item['user_id']][$i] = $item;
						$user_name = $item['user_name'];
					}
					elseif(!isset($arr[$item['user_id']][$i])){
						$arr[$item['user_id']][$i] = array();
					}
				}
				$arr[$item['user_id']]['user_name'] = $user_name;
			}
			return $arr;
		}	
		else
			return array();
	}
	function getStockAtMoment($date_time, $supplier_id = false)
	{
		$this->db->select('max(product_history_id) as max_history_id');
		$this->db->from('product_stock_history');
		// $this->db->join('products', 'products.product_id = product_stock_history.product_id', 'INNER');
		$this->db->where('UNIX_TIMESTAMP(product_stock_history_date) <= ' . $date_time);
		$this->db->group_by('product_stock_history.product_id');
		$this->db->order_by('max(product_stock_history_date)', 'desc');
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$arr_id = array();
			// print_r($query->result_array());die;
            foreach($query->result_array() as $k => $item) {
				$arr_id[] = $item['max_history_id'];
			}
			$lst_id = implode(',', $arr_id);
			$this->db->select('*');
			$this->db->from('product_stock_history');
			$this->db->join('products', 'products.product_id = product_stock_history.product_id', 'INNER');
			$this->db->where_in('product_history_id', $arr_id);
			
			if ($supplier_id) {
				$this->db->where("product_supplier_id",$supplier_id);
			}
			$this->db->where("new_product_stock_quantity <> 0");
			
			$this->db->order_by('products.product_code', 'ASC');
			$query = $this->db->get();
			return $query->result_array();
		}
		else
			return array();
	}
    function getAllStockIssues()
    {
        $this->db->select("stock_bills.*,users.*");
        $this->db->from($this->table);
        $this->db->join("users","users.user_id = stock_bills.stock_bill_create_user_id and stock_bills.stock_bill_type=" . STOCK_RECEIPT_TYPE_RETURN,"LEFT");
        $this->db->where("stock_bills.stock_bill_status",1);
        $this->db->order_by("stock_bills.stock_bill_id","DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;
    }

    function getOpenStockIssues()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("users","users.user_id = stock_bills.stock_bill_object_id","LEFT");//nguoi nhan hang
		$this->db->where("stock_bills.stock_bill_status",1);
		$this->db->where("stock_bills.stock_bill_flow",-1);
		$this->db->where("stock_bills.stock_bill_archive",0);
		$this->db->order_by("stock_bills.stock_bill_id","DESC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
    
    //get item supplier
	function getItemSupplier($id)
	{
		$this->db->select("*");
		$this->db->from("supplies");
		$this->db->where("supplier_id",$id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
    function getStockFromStockBills() {
        $this->db->select("stock_bill_products.product_id as product_id, sum(stock_bill_product_quantity * stock_bills.stock_bill_flow) as quantity", false);
        $this->db->from($this->table);
        $this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","inner");
        $this->db->where("stock_bills.stock_bill_status",1);
        $filter = "stock_bills.stock_bill_id > 0";

        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(stock_bills.stock_bill_created_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('stock_bill_products.product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }


}