<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends MY_Model
{
    var $table = 'users';
    function __construct(){
        parent::__construct();
    }
	// insert
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	//update
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('user_id', $arr["user_id"]);
		return $this->db->update($this->table, $arr); 
	}
	//delete 1 item
	// var $id item
	function delete($id)
	{
        if ($id != 1) {
            $this->db->where('user_id', $id);
            $this->db->update($this->table, array('deleted' => 1));
        } else {
            return false;
        }
	}
	//getinfo 1 item
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("user_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return false;
	}
	//getall item
	//var $order: field order
	function getAll($order = "user_id",$type = "DESC")
	{
		$this->db->select("*");
		$this->db->from($this->table);
        $this->db->where('deleted', 0);
		$this->db->order_by($order,$type);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return false;
	}
	function getAllByRole($role, $include_deleted = true)
	{
		$this->db->select("*");
		$this->db->from($this->table);
        if (!$include_deleted) {
            $this->db->where('deleted', 0);
        }
		$this->db->where("user_role",$role);
		$this->db->order_by("user_name","ASC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
    /*
     * @param string $email user's email.
     * @param string $pass.
     * @param int $user_type.
     * @param boolean $all_data.
     * @param boolean $sha1_password.
     * @param string $user_type.
     * @return array|int|boolean The user's array or user's id or boolean if none user.
     */

    function getLogin($email,$password, $user_type, $all_data = false, $sha1_password = false)
	{
		$pass = ($sha1_password) ? $password : sha1($password);
		$this->db->where(array(
						'user_email'=>$email,
						'password'=>$pass,
						'user_type'=>$user_type,
                        'access' => 1
					 ));            
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
        {
            $user = $query->row_array();
            if($all_data)
                return $user;
			return $user['user_id'];
        }
		else 
			return false;
	}
	function getLoginAdmin($email, $password)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where(array('user_email'=>$email,'user_password'=> $password, 'access'=>1 ));
		$query = $this->db->get()->row_array();
		if(!empty($query)) {
			return $query;
        }
		else 
			return false;
	}
    
    /*
     * @param string $id user's id.
     * @param string $select .
     * 
     */
    function getByID($id, $select = '*')
    {
        $this->db->select($select);
        $query = $this->db->get_where('users', array('user_id' => $id));
        if($query->num_rows() > 0)
            return $query->row_array();
        return false;
    }
	function checkEmail($email){
		$query = $this->db->get_where($this->table, array('user_email' => $email));
		if($query->num_rows() > 0)
			return true;
		else return false;
	}
	function getDebts()
	{
		$this->db->select("users.user_id,sum(stock_bill_product_price * stock_bill_product_quantity * stock_bill_type) as sum_stock,sum(cash_amount) as value");
		$this->db->from($this->table);
		$this->db->join("stock_bills","stock_bills.stock_bill_object_id = users.user_id","left");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","left");
		$this->db->join("cashs","cashs.cash_object_id = users.user_id","left");
		$this->db->where("users.user_role",ROLE_SALEPERSON);
		$this->db->where("users.user_id",3);
		$this->db->group_by("stock_bills.stock_bill_id");
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}
	function getIssue($id,$datefrom = false,$dateto = false)
	{
		$this->db->select("users.user_id,sum(stock_bill_product_price * stock_bill_product_quantity) as sum_issue");
		$this->db->from($this->table);
		$this->db->join("stock_bills","stock_bills.stock_bill_object_id = users.user_id","left");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","left");
		$this->db->where("users.user_role",ROLE_SALEPERSON);
		$this->db->where("stock_bills.stock_bill_flow = '" .OUT."'");
		$this->db->where("users.user_id",$id);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		if($datefrom){
			$this->db->where("stock_bill_created_date >=  '$datefrom'");
        }
        if ($dateto) {
			$this->db->where("stock_bill_created_date <= '$dateto'");
		}
		$this->db->group_by("users.user_id");
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result =  $query->row_array();
            return $result["sum_issue"];
		}
		else 
			return 0;
	}
	function getReceipt($id,$datefrom = false,$dateto = false)
	{
		$this->db->select("users.user_id,sum(stock_bill_product_price * stock_bill_product_quantity) as sum_receipt");
		$this->db->from($this->table);
		$this->db->join("stock_bills","stock_bills.stock_bill_object_id = users.user_id","left");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","left");
		$this->db->where("users.user_role",ROLE_SALEPERSON);
		$this->db->where("stock_bills.stock_bill_flow",IN);
		$this->db->where("stock_bills.stock_bill_type",STOCK_RECEIPT_TYPE_RETURN);
		$this->db->where("users.user_id",$id);
		$this->db->where("stock_bills.stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
        if($datefrom){
            $this->db->where("stock_bill_created_date >=  '$datefrom'");
        }
        if ($dateto) {
            $this->db->where("stock_bill_created_date <= '$dateto'");
        }

		$this->db->group_by("users.user_id");
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result =  $query->row_array();
            return $result["sum_receipt"];
		}
		else 
			return 0;
	}
	function getCash($id,$datefrom = false,$dateto = false)
	{
		$this->db->select("users.user_id,sum(cash_amount) as sum_cash");
		$this->db->from($this->table);
		$this->db->join("cashs","cashs.cash_object_id = users.user_id and cashs.type='receipt'","inner");
		$this->db->where("users.user_role",ROLE_SALEPERSON);
		$this->db->where("users.user_id",$id);
        if($datefrom){
            $this->db->where("cash_create_date >=  '$datefrom'");
        }
        if ($dateto) {
            $this->db->where("cash_create_date <= '$dateto'");
        }

		$this->db->group_by("users.user_id");
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result =  $query->row_array();
            return $result["sum_cash"];
		}
		else 
			return 0;
	}
	function getHistory($limit,$start,$datefrom=false,$dateto=false,$type = "")
	{
		$this->db->select("product_stock_history.*,products.*,stock_issues.created_user_id as issue_user_create,stock_issues.sale_user_id as issue_sale,stock_issues.stock_issue_note,stock_receipts.created_user_id as receipt_user_create,stock_receipts.supplier_id,stock_receipts.sale_user_id as receipt_sale,stock_receipts.stock_receipt_note,stock_receipts.stock_receipt_type,stock_issue_history.*,stock_issue_history.stock_issue_message as issue_message,stock_receipt_history.*,stock_receipt_history.stock_issue_message as receipt_message");
		$this->db->from("product_stock_history");
		$this->db->join("products","products.product_id = product_stock_history.product_id","LEFT");
		$this->db->join("stock_issues","stock_issues.stock_issue_id = product_stock_history.linked_object_id and reason = " . STOCK_CHANGE_REASON_ISSUE,"LEFT");
		$this->db->join("stock_receipts","stock_receipts.stock_receipt_id = product_stock_history.linked_object_id and reason = " . STOCK_CHANGE_REASON_RECEIPT,"LEFT");
		$this->db->join("stock_issue_history","stock_issue_history.stock_issue_history_id = product_stock_history.linked_object_id and reason = " . STOCK_CHANGE_REASON_ISSUE_CHANGE,"LEFT");
		$this->db->join("stock_receipt_history","stock_receipt_history.stock_receipt_history_id = product_stock_history.linked_object_id and reason = " . STOCK_CHANGE_REASON_RECEIPT_CHANGE,"LEFT");
		if($datefrom && $dateto){
			$this->db->where("product_stock_history_date >=  '$datefrom'");
			$this->db->where("product_stock_history_date <= '$dateto'");
		}
		$this->db->order_by("product_stock_history_date","DESC");
            $this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else 
			return array();
	}

}
