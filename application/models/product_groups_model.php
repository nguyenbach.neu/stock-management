<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_groups_model extends MY_Model
{
    var $table = 'product_group';

    function __construct() {
        parent::__construct();
    }

    function insert($arr = array())
    {
        $this->db->insert($this->table, $arr);
        return $this->db->insert_id();
    }

    function update($arr = array())
    {
        $this->db->where('id', $arr["group_id"]);
        return $this->db->update($this->table, $arr);
    }

    function countAll() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getAll($start = false, $limit = false, $order = "sort_order", $type = "ASC", $include_deleted = false)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->order_by($order, $type);

        if (!$include_deleted) {
            $this->db->where("product_group.deleted",0, false);
        }

        $this->db->limit($limit,$start);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }

        return array();
    }

    function getInfo($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where("id",$id);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
            return false;
    }
}