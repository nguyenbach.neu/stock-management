<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**********
*	table products
*	list function product
*	tungpa
*/
class Products_model extends MY_Model
{
    var $table = 'products';
	/*
	*	construct
	*/
    function __construct(){

        parent::__construct();

    }
	// insert
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	// insert product_price_history
	// var $arr is array item
	function insert_history($arr = array())
	{
		$this->db->insert("product_price_history", $arr);
        return $this->db->insert_id();
	}
	//update
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('product_id', $arr["product_id"]);
		return $this->db->update($this->table, $arr); 
	}
	//delete 1 item
	// var $id item
	function delete($id)
	{
        $this->db->where('product_id', $id);
        $this->db->update($this->table, array('deleted' => 1));
	}
	//delete 1 item
	// var $id item
	function delete_history_price($product_id)
	{
		$sql = "DELETE FROM product_price_history WHERE product_id =" .$product_id;
		$this->db->query($sql);
	}
	function delete_history_quantity($product_id)
	{
		$sql = "DELETE FROM product_stock_history WHERE product_id =" .$product_id;
		$this->db->query($sql);
	}

	//getinfo 1 item
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("supplies","supplies.supplier_id = products.product_supplier_id","LEFT");
		$this->db->where("product_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else 
			return false;
	}
	//getall item
	//var $order: field order
	function getAll($start = false, $limit = false, $order = "product_code", $type = "ASC", $include_deleted = false)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("supplies","supplies.supplier_id = products.product_supplier_id","LEFT");
		$this->db->order_by($order, $type);

        $filter = 'products.product_id > 0';
        if (!$include_deleted) {
            $this->db->where("products.deleted",0, false);
        }

        $this->db->limit($limit,$start);

        if($this->input->get("supplier_id") != ""){
            $filter .= " AND products.product_supplier_id = " . $this->input->get("supplier_id");
        }
        $this->db->where($filter);

		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	//count all
	function countAll(){
		$this->db->where("products.deleted",0);
        $this->db->from($this->table);
        return $this->db->count_all_results();
	}
	function checkCode($code)
	{
		$query = $this->db->get_where($this->table, array('product_code' => $code));
		if($query->num_rows() > 0)
			return true;
		else return false;
	}
	function his_price($id)
	{
		$this->db->select("*");
		$this->db->from("product_price_history");
		$this->db->join("users","users.user_id = product_price_history.modified_user_id");
		$this->db->where("product_id",$id);
		$this->db->order_by("product_price_history_id","DESC");
		$query = $this->db->get();
		return $query->result_array();
	}
	function his_quantity($id,$start,$limit)
	{
		$this->db->select("*");
		$this->db->from("product_stock_history");
		$this->db->join("users","users.user_id = product_stock_history.modified_user_id");
		$this->db->join("stock_bills","stock_bills.stock_bill_id = product_stock_history.linked_object_id","LEFT");
		$this->db->where("product_stock_history.product_id",$id);
		$this->db->order_by("product_history_id","DESC");
		if(!empty($limit)){
			$this->db->limit($limit,$start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	//count all with variable follow
	function count_quantity($id){
		$this->db->where("product_id",$id);
		$this->db->from('product_stock_history');
		return $this->db->count_all_results();
	}
	
	function getLastestAdjustedPrices() {
		$this->db->select("*");
		$this->db->from("product_price_history");
		$this->db->order_by("product_price_history_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
    function sumAmount() {
        $this->db->select("sum(product_cost * product_stock_quantity) as sum");
        $this->db->from($this->table);
        $this->db->where("deleted", 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        return array();
    }
	function getLastestAdjustedStocks() {
		$this->db->select("*");
		$this->db->from("product_stock_history");
		$this->db->where("reason", STOCK_CHANGE_REASON_ADJUSTMENT);
		$this->db->order_by("product_stock_history_date","DESC");
		$this->db->limit(5, 0);
		$query = $this->db->get();
		if($query->num_rows() > 0)
            return $query->result_array();
		else
			return false;
	}
    function getStockFromHistory() {
        $this->db->select("product_id, (sum(new_product_stock_quantity) - sum(previous_product_stock_quantity)) as quantity", false);
        $this->db->from('product_stock_history');
        $filter = 'product_history_id > 0';
        if($this->input->get("from") != ""){
            $filter .= " AND UNIX_TIMESTAMP(product_stock_history_date) > " . strtotime($this->input->get("from"));
        }
        if($this->input->get("to") != ""){
            $filter .= " AND UNIX_TIMESTAMP(product_stock_history_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
        }
        $this->db->where($filter);
        $this->db->group_by('product_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
    }
	
}