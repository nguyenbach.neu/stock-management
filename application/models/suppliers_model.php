<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Suppliers_model extends MY_Model
{
    var $table = 'supplies';
    function __construct(){
        parent::__construct();
    }
	// insert
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	//update
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('supplier_id', $arr["supplier_id"]);
		$this->db->update($this->table, $arr); 
	}
	//delete 1 item
	// var $id item
	function delete($id)
	{
        $this->db->where('supplier_id', $id);
        $this->db->update($this->table, array('deleted' => 1));
	}
	//getinfo 1 item
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("supplier_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return false;
	}
	//getall item
	//var $order: field order
    function getAllSuppliers($order = "supplier_id",$type = "DESC")
    {
        $this->db->select("supplies.*");
        $this->db->from($this->table);
        //$this->db->join("cashs","supplies.supplier_id = cashs.supplier_id and cash_follow =".IN,"LEFT");
        $this->db->group_by('supplies.supplier_id');
        $this->db->order_by($order,$type);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return false;
    }
	function getAll($order = "supplier_id",$type = "DESC")
	{
		$this->db->select("supplies.*");
		$this->db->from($this->table);
		//$this->db->join("cashs","supplies.supplier_id = cashs.supplier_id and cash_follow =".IN,"LEFT");
        $this->db->where('deleted', 0);
		$this->db->group_by('supplies.supplier_id');
		$this->db->order_by($order,$type);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return false;
	}
	//getall item
	//var $order: field order
	function getAllProduct($id, $select = "*")
	{
		$this->db->select($select);
		$this->db->from($this->table);
		$this->db->join("products","products.product_supplier_id = supplies.supplier_id","INNER");
		if(!empty($id))
			$this->db->where("supplies.supplier_id",$id);
		$this->db->order_by("products.product_id","ASC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
	//get all amount receipt
	function getAmount($id)
	{
		$this->db->select("stock_bills.*,sum(stock_bill_product_price*stock_bill_product_quantity) as totals");
		$this->db->from("stock_bills");
		$this->db->join("stock_bill_products","stock_bill_products.stock_bill_id = stock_bills.stock_bill_id","LEFT");
		$this->db->where("stock_bill_flow",IN);
		$this->db->where("stock_bill_type",STOCK_RECEIPT_TYPE_PURCHASE);
		$this->db->where("stock_bill_status",STOCK_CHANGE_STATUS_PROCESSED);
		$this->db->where("stock_bill_object_id",$id);
		$this->db->group_by("stock_bills.stock_bill_id");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else 
			return array();
	}
}