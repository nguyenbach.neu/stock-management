<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roles_model extends MY_Model
{
    var $table = 'roles';
    function __construct(){
        parent::__construct();
    }
	// insert
	// var $arr is array item
	function insert($arr = array())
	{
		$this->db->insert($this->table, $arr);
        return $this->db->insert_id();
	}
	//update
	// var $arr is array item edit
	function update($arr = array())
	{
		$this->db->where('role_id', $arr["role_id"]);
		$this->db->update($this->table, $arr); 
	}
	//delete 1 item
	// var $id item
	function delete($id)
	{
		$this->db->delete($this->table, array('role_id' => $id)); 
	}
	//getinfo 1 item
	// var $id item
	function getInfo($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("role_id",$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else return false;
	}
	function getPermission($type,$controller,$action,$lock = false)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("role_type",$type);
		$this->db->where("role_controller",$controller);
		$this->db->where("role_action",$action);
		if($lock)
		$this->db->where("role_lock",$lock);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			return true;
		else return false;
	}
	
}