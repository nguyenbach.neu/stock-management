<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function __($line = '')
{
	$CI =& get_instance();
	if (!isset($CI->lang)) {
		$CI->load->library('lang');
	}
	$translation = $CI->lang->line($line); 
	// return (empty($translation)?'['.$line.']':$translation);	
	return (empty($translation)?''.$line.'':$translation);	
}

function isLogged() {
	$CI =& get_instance();
	if ($CI->session->userdata('user_id')) {
		 
		return true;
	} else {
		return false;
	}
}


// message box
function error_message($content)
{
	$CI =& get_instance();
	return $CI ->session->set_flashdata('error_message', ($content));
}
function success_message($content)
{
	$CI =& get_instance();
	return	$CI->session->set_flashdata('success_message', ($content));
	 
}

//has permission
function has_permission($role, $controller = "", $action = "", $locked = true){
	//echo "$controller, $action, $locked <br />";
    switch ($role) {
        case ROLE_ADMIN:
            //only lock delete on stockissues and stockreceipts because it will affect to debt and stock and mess whole system
            if ($action == 'delete' && $locked && ($controller == 'stockissues' || $controller == 'stockreceipts')) {
                return false;
            } else {
                return true;
            }
            break;
        case ROLE_ACCOUNTING:
            if (($action == 'edit' && $locked)//thu kho can't edit/udpate or delete any "locked" data
                || ($action == 'delete' && $locked)
                || ($controller == 'products' && $action == 'changequantity')
                || ($controller == 'products' && $action == 'changeprice')) {
                return false;
            }
            if (($controller == 'reports' && $action == 'cash_overview') || ($controller == 'reports' && $action == 'profit')) {//sensetive report
                return false;
            }
            if ($controller == 'users'){
                if ($action == 'password')
                {
                    return true;
                }
                return false;
            }
            return true;
            break;
        case ROLE_SALEPERSON:
            if ($controller == 'users' && $action == 'password'){
                return true;
            }
            if($controller == 'orders'){
                if($action == 'add_stock_issue'){
                    return false;
                }
                return true;
            } else {
                return false;
            }

            break;
        case ROLE_SUPERVISOR:
            if ($controller == 'users' && $action == 'password'){
                return true;
            }
            if ($controller == 'orders'){
                return true;
            }
            if ($action != 'view' && $action != 'index' && $action != 'add') {
                return false;
            } else {
                return true;
            }
            break;
        case ROLE_STOCKKEEPER:
            if ($controller == 'users' && $action == 'password'){
                return true;
            }
            if (($action == 'edit' && $locked)//thu kho can't edit/udpate or delete any "locked" data
                || ($action == 'delete' && $locked)
                || ($controller == 'products' && $action == 'changequantity')
                || ($controller == 'products' && $action == 'changeprice')) {
                return false;
            }
            if (($controller == 'reports' && $action == 'cash_overview') || ($controller == 'reports' && $action == 'profit')) {//sensetive report
                return false;
            }

            if ($controller != 'stockissues' && $controller != 'stockreceipts' && !($controller == 'reports' && $action == 'inventory')) {//chi dc phep access phieu xuat nhap kho va thong ke stock
                return false;
            }
            return true;
            break;
    }

    return false;

	$CI =& get_instance();
	if($CI->roles_model->getPermission($role,$controller,$action,$locked))
		return true;
	else
		return false;
	
}
//check permission
function check_permission($role, $controller = "", $action = "", $lock = true) {

	$CI =& get_instance();
	if(!has_permission($role,$controller,$action,$lock))
	{
			error_message(__("DENY_PERMISSION"));
			redirect("admin/dashboard/");
	}
}
//select role user
function user_role_dropdown($select = false){
	$arr = array();
	$arr[1] = "Toan quyen quan tri";
	$arr[2] = "Ke toan (kiem xuat kho)";
	$arr[3] = "Giam sat (chi xem)";
	$arr[4] = "Nhan vien ban hang";
	$arr[5] = "Thu kho (chi xuat va nhap hang)";

	$html = "<select class=\"form-control\" name=\"user_role\" id=\"user_role\" required><option value=\"\">" . __("choose") ."</option>";
	foreach($arr as $k => $item){
		$html .= "<option ".(($select == $k) ? "selected" : "")." value=\"$k\">$item</option>";
	}
	$html .= "</select>";
	return $html;
}
//select product supplier
function supplier_dropdown($select = false,$disabled = false){
	$CI =& get_instance();
	$CI->load->model('suppliers_model');
    if (!$select) {
        $select = $CI->input->get('supplier_id');
    }
	$supplies = $CI->suppliers_model->getAll("supplier_name","ASC");
    $required = ($CI->router->fetch_method()=='add' || $CI->router->fetch_method()=='edit')?'required':'';
	$html = "<select class=\"form-control\" id=\"supplier_id\" name=\"supplier_id\" ".(($disabled) ? "readonly" : "")." $required>";
	
	$html .= "<option value=\"\">" . __("choose") ."</option>";
	if(count($supplies) > 0 && $supplies){
		foreach($supplies as $k => $item){
			$html .= "<option " . (($item["supplier_id"] == $select) ? "selected" : "") . " value=\"" . $item["supplier_id"] . "\">" . $item["supplier_name"] . "</option>";
		}
	}
	$html .= "</select>";
	return $html;
}

//select user role sale
function user_dropdown($select = false)
{
    $CI =& get_instance();
    if (!$select) {
        $select = $CI->input->get('sale_user_id');
    }
    $supplies = $CI->users_model->getAll();
    $required = ($CI->router->fetch_method()=='add' || $CI->router->fetch_method()=='edit')?'required':'';
    $html = "<select id=\"sale_user_id\" name=\"sale_user_id\" class=\"form-control\" $required>";

    $html .= "<option value=\"\">" . __("choose") ."</option>";
    if(count($supplies) > 0 ){
        foreach($supplies as $k => $item){
            $html .= "<option " . (($item["user_id"] == $select) ? "selected" : "") . " value=\"" . $item["user_id"] . "\">" . $item["user_name"] . "</option>";
        }
    }
    $html .= "</select>";
    return $html;
}
//select user role sale
function sale_user_dropdown($select = false)
{
	$CI =& get_instance();
    if (!$select) {
        $select = $CI->input->get('sale_user_id');
    }
	$supplies = $CI->users_model->getAllByRole(ROLE_SALEPERSON, false);
    $required = ($CI->router->fetch_method()=='add' || $CI->router->fetch_method()=='edit')?'required':'';
	$html = "<select id=\"sale_user_id\" name=\"sale_user_id\" class=\"form-control\" $required>";
	
	$html .= "<option value=\"\">" . __("choose") ."</option>";
	if(count($supplies) > 0 ){
		foreach($supplies as $k => $item){
			$html .= "<option " . (($item["user_id"] == $select) ? "selected" : "") . " value=\"" . $item["user_id"] . "\">" . $item["user_name"] . "</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
//select product stock issues
function product_dropdown($select = false,$name = "product_id",$id = "",$disabled = false, $style = '',$class = 'sl_stock_issue_product')
{
	$CI =& get_instance();
	$products = $CI->products_model->getAll(false, false,"product_name","ASC");
	$html = "<select style='$style' name=\"$name\" id=\"$id\" class=\"$class \" ". (($disabled) ? "disabled" : "") ." title=\"Please choose product\">";
	$html .= "<option value=\"0\">" . __("choose") ."</option>";
	if(count($products) > 0 ){
		foreach($products as $k => $item){
			$html .= "<option " . (($item["product_id"] == $select) ? "selected" : "") . " value=\"" . $item["product_id"] . "\">". $item["product_code"] . " - " . $item["product_name"] ."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
//select stock_receipt_type
function stock_receipt_type_dropdown($select = -1, $disabled = false){
	$arr = array();
	$arr[STOCK_RECEIPT_TYPE_PURCHASE] = __("purchase");
	$arr[STOCK_RECEIPT_TYPE_RETURN] = __("return");
	$html = "<select class=\"form-control\" name=\"stock_receipt_type\" class=\"stock_receipt_type\" " . (($disabled) ? "readonly" : "") . " >";
	$html .= "<option value=\"\">" . __("choose") ."</option>";
	if(count($arr) > 0 ){
		foreach($arr as $k => $item){
			$html .= "<option " . (($k == $select) ? "selected" : "") . " value=\"" . $k . "\">". $item ."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}

function open_stock_bill_issues_dropdown($select = false){
	$CI =& get_instance();
	$stock_bill = $CI->stock_bill_model->getOpenStockIssues();
	
	if (!$select) {
        $select = $CI->input->get('stock_bill_issue_id');
    }
		
	$html = "<select class=\"form-control\" name=\"stock_bill_issue_id\" id=\"stock_bill_issue_id\" required><option value=\"\">" . __("choose") ."</option>";
	if(is_array($stock_bill) && count($stock_bill) > 0 ){
		foreach($stock_bill as $k => $item){
			$html .= "<option ".(($select == $item['stock_bill_id']) ? "selected" : "")." value=".$item['stock_bill_id'].">PX - ".$item['stock_bill_id']."( ".$item['user_name']." - ".formatLocalDatetime($item['stock_bill_created_date'])." )"."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}

//select bank account
function bank_account_dropdown($select = false, $name = false){
    if (!$name) {
        $name = 'account_id';
    }
	$CI =& get_instance();
    if (!$select) {
        $select = $CI->input->get($name);
    }
	$account = $CI->accounts_model->getAll("account_id","DESC");
    $required = ($CI->router->fetch_method()=='add' || $CI->router->fetch_method()=='edit')?'required':'';
	$html = "<select class=\"form-control\" name=\"$name\" id=\"$name\" $required><option value=\"\">" . __("choose") ."</option>";
	if(is_array($account) && count($account) > 0 ){
		foreach($account as $k => $item){
			$html .= "<option ".(($select == $item['account_id']) ? "selected" : "")." value=\"".$item['account_id']."\">".$item['account_name']."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}

function formatLocalDatetime($mysql_datetime) {
	$CI = & get_instance();
    if (!$mysql_datetime) {
        return '--';
    }
	return date($CI->config->item('display_datetime_format'), gmt_to_local(mysql_to_unix($mysql_datetime), $CI->config->item('display_time_zone'), false));
}

function product_group_dropdown($select = false,$name = "product_group",$id = "",$disabled = false, $style = '',$class = 'form-control') {
    $CI =& get_instance();
    $CI->load->model('product_groups_model');
    $groups = $CI->product_groups_model->getAll();
    $html = "<select style='$style' name=\"$name\" id=\"$id\" class=\"$class \" ". (($disabled) ? "disabled" : "") ." title=\"Please choose product group\">";
    $html .= "<option value=\"0\">" . __("choose") ."</option>";
    if(count($groups) > 0 ){
        foreach($groups as $k => $item){
            $html .= "<option " . (($item["id"] == $select) ? "selected" : "") . " value=\"" . $item["id"] . "\">". $item["group_name"] ."</option>";
        }
    }
    $html .= "</select>";
    return $html;
}

function convert_number_to_words( $number )
{
    $hyphen = ' ';
    $conjunction = '  ';
    $separator = ' ';
    $negative = 'âm ';
    $decimal = ' phẩy ';
    $dictionary = array(
        0 => 'Không',
        1 => 'Một',
        2 => 'Hai',
        3 => 'Ba',
        4 => 'Bốn',
        5 => 'Năm',
        6 => 'Sáu',
        7 => 'Bảy',
        8 => 'Tám',
        9 => 'Chín',
        10 => 'Mười',
        11 => 'Mười một',
        12 => 'Mười hai',
        13 => 'Mười ba',
        14 => 'Mười bốn',
        15 => 'Mười năm',
        16 => 'Mười sáu',
        17 => 'Mười bảy',
        18 => 'Mười tám',
        19 => 'Mười chín',
        20 => 'Hai mươi',
        30 => 'Ba mươi',
        40 => 'Bốn mươi',
        50 => 'Năm mươi',
        60 => 'Sáu mươi',
        70 => 'Bảy mươi',
        80 => 'Tám mươi',
        90 => 'Chín mươi',
        100 => 'trăm',
        1000 => 'ngàn',
        1000000 => 'triệu',
        1000000000 => 'tỷ',
        1000000000000 => 'nghìn tỷ',
        1000000000000000 => 'ngàn triệu triệu',
        1000000000000000000 => 'tỷ tỷ'
    );

    if( !is_numeric( $number ) )
    {
        return false;
    }

    if( ($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX )
    {
        // overflow
        trigger_error( 'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING );
        return false;
    }

    if( $number < 0 )
    {
        return $negative . convert_number_to_words( abs( $number ) );
    }

    $string = $fraction = null;

    if( strpos( $number, '.' ) !== false )
    {
        list( $number, $fraction ) = explode( '.', $number );
    }

    switch (true)
    {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int)($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if( $units )
            {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if( $remainder )
            {
                $string .= $conjunction . convert_number_to_words( $remainder );
            }
            break;
        default:
            $baseUnit = pow( 1000, floor( log( $number, 1000 ) ) );
            $numBaseUnits = (int)($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words( $numBaseUnits ) . ' ' . $dictionary[$baseUnit];
            if( $remainder )
            {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words( $remainder );
            }
            break;
    }

    if( null !== $fraction && is_numeric( $fraction ) )
    {
        $string .= $decimal;
        $words = array( );
        foreach( str_split((string) $fraction) as $number )
        {
            $words[] = $dictionary[$number];
        }
        $string .= implode( ' ', $words );
    }

    return $string;
}
?>