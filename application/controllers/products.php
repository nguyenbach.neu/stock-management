<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller
{
    function __construct()
    {	
		
		parent::__construct();
	
    } 
	function index()
	{
		$data = array();
		$data["productsGold"] = $this->products_model->getAllByCateId(3);
		$data["productsSilver"] = $this->products_model->getAllByCateId(4);
		$this->_getlayoutbackend('admin/backend/index',$data,"backend");
	}
	function ajaxLoadFeed()
	{
		$data = array();
		$productsGold = $this->products_model->getAllByCateId(3);
		$productsSilver = $this->products_model->getAllByCateId(4);
		date_default_timezone_set('America/Los_Angeles');
		if ((file_exists('videos1.tmp')) && (time() - filemtime('videos1.tmp') < 60)){ //15*60
				$content = file_get_contents('videos1.tmp');
				echo $content;
		}else{
			ob_start();
			$html = "";
			$html .= '<div style="margin-left:-20px;margin-right:-20px;background-color:#000;color:#FFF;width:1920px;padding-top: 20px;">
						<div class="lp33">
							<h1 class="text-center head-feed">GOLD</h1>
							<span class="wp30 text-center pull-left">
								<strong>
									BID
								</strong><br />
								<span class="price">';
									$goldfeed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801gold.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
									
								$html .='</span>
							</span>
							<span class="wp30 text-center pull-left">
								<strong>
									ASK
								</strong></br>
								<span class="price">';
									$goldfeed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldask.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
								$html .='</span>
							</span>
							<span class="wp40 pull-left text-right">
								<strong>
									
								</strong><br />';
									$goldfeed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldchangedollar.php');
								$html .= '<span class="price price_' . (($goldfeed > 0) ? "up" : "down") . '">';
									$html .= ($goldfeed);
								$html .= '</span>
							</span>
							<div style="clear:both;"></div>
						</div>
						<div class="lp33">
							<h1 class="text-center head-feed">SILVER</h1>
							<span class="wp30 text-center pull-left">
								<strong>
									BID
								</strong><br />
								<span class="price">';
									$goldfeed =	file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silver.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
								$html .= '</span>
							</span>
							<span class="wp30 text-center pull-left">
								<strong>
									ASK
								</strong><br />
								<span class="price">';
									$goldfeed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverask.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
								$html .= '</span>
							</span>
							<span class="wp40 pull-left text-right">
								<strong>
									
								</strong><br />';
									$goldfeed =
									file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverchangedollar.php');
								$html .= '<span class="price price_' . (($goldfeed > 0) ? "up" : "down") . '">';
									$html .= ($goldfeed); 
								$html .= '</span>
							</span>
						</div>
						<div class="lp30">
							<h1 class="text-center head-feed">PLATINUM</h1>
							<span class="wp30 text-center pull-left">
								<strong>
									BID
								</strong><br />
								<span class="price">';$goldfeed =
									file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinum.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
								$html .= '</span>
							</span>
							<span class="wp30 text-center pull-left">
								<strong>
									ASK
								</strong><br />
								<span class="price">';
									$goldfeed =
									file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumask.php');
									$html .= '$'.number_format($goldfeed, 2, '.', ',');//round ($goldfeed , 2);
								$html .= '</span>
							</span>
							<span class="wp40 pull-left text-right">
								<strong>
									
								</strong><br />';
									$goldfeed =
									file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumchangedollar.php');
								$html .= '<span class="price price_' . (($goldfeed > 0) ? "up" : "down") . '">';
									$html .= ($goldfeed);
								$html .= '</span>				
							</span>
						</div>
						<div style="clear:both;"></div>

					</div>';
					$html .= '<div style="margin-left:-20px;margin-right:-20px;background-color:#FFF;min-height:600px;width:1920px;">
						<div class="span6 wp50">';
							if($productsGold) {
								$html .= "<table class='table-striped table-hover' width='100%'>";
								$gold_bid = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801gold.php');
								$gold_ask = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldask.php');
								foreach($productsGold as $k => $item) {
									$html .= "<tr>";
									$html .= "<td class='text-center td-img'>";
									$src = '../'.IMG_UPLOAD.$item['product_thumbnail'];
									$img = base_url() . SCRIPT . '/' . 'timthumb.php?src=' . $src . '&amp;h=auto&amp;w=100&amp;zc=1';  
									$html .= "<img src='" . $img . "' />";
										
									
									$html .= "</td>";
									$html .= "<td class='td-text'>";
									$html .= "<span>" . $item["product_name"] . "</span><br />";
									$gold_buy = "";
									$gold_sell = "";
									if($item["product_markup_type"] == FIXED){
										$gold_buy = $gold_bid + $item["product_markup_buy"];
										$gold_sell = $gold_ask + $item["product_markup_sell"];
									}
									else if($item["product_markup_type"] == PERCENTAGE) {
										$gold_buy = $gold_bid * (100 + $item["product_markup_buy"]) / 100;
										$gold_sell = $gold_ask *(100 + $item["product_markup_sell"]) /100;
									}
									$html .= "<p><strong>Buy $ " . number_format($gold_buy, 2, '.', ',') . "</strong></p>";
									$html .= "</td>";
									$html .= "<td class='td-sell td-text'>";
									$html .= "<br />";
									$html .= "<p><strong>Sell $ " . number_format($gold_sell, 2, '.', ',') . "</strong></p>";
									$html .= "</td>";
									
									$html .= "</tr>";
								}
								$html .= "</table>";
							}
						$html .= '</div>
						<div class="span6 wp50">';
							if($productsSilver) {
								$html .= "<table class='table-striped table-hover' width='100%'>";
								$silver_ask = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverask.php');
								$silver_bid = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silver.php');
								foreach($productsSilver as $k => $item) {
									$html .= "<tr>";
									$html .= "<td class='text-center td-img'>";
									$src = '../'.IMG_UPLOAD.$item['product_thumbnail'];
									$img = base_url() . SCRIPT . '/' . 'timthumb.php?src=' . $src . '&amp;h=auto&amp;w=100&amp;zc=1';  
									$html .= "<img src='" . $img . "' />";
										
									
									$html .= "</td>";
									$html .= "<td class='td-text'>";
									$html .= "<span>" . $item["product_name"] . "</span><br />";
									$silver_buy = "";
									$silver_sell = "";
									if($item["product_markup_type"] == FIXED){
										$silver_buy = $silver_bid + $item["product_markup_buy"];
										$silver_sell = $silver_ask + $item["product_markup_sell"];
									}
									else if($item["product_markup_type"] == PERCENTAGE) {
										$silver_buy = $silver_bid * (100 + $item["product_markup_buy"]) / 100;
										$silver_sell = $silver_ask *(100 + $item["product_markup_sell"]) /100;
									}
									$html .= "<p><strong>Buy $ " . number_format($silver_buy, 2, '.', ',') . "</strong></p>";
									$html .= "</td>";
									$html .= "<td class='td-sell td-text'>";
									$html .= "<br />";
									$html .= "<p><strong>Sell $ " . number_format($silver_sell, 2, '.', ',') . "</strong></p>";
									$html .= "</td>";
									
									$html .= "</tr>";
									echo $html;
								}
								$html .= "</table>";
							}
						$html .= '</div>
						<div style="clear:both;"></div>
					</div>';	
			echo $html;
			$content = $html;//ob_get_contents();
			ob_end_clean();
			file_put_contents('videos1.tmp', $content);
			echo $content; 
		}	
		//$this->_getlayoutbackend('admin/backend/ajaxLoadFeed',$data,"backend");
	}
}
?>