<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Histories extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
    }
	function stock($id)
	{

		$data = array();
		$histories = $this->stock_bill_model->getHistory($id);

		// print_r($histories);
		$data["stock"] = $this->stock_bill_model->getInfo($id);
		$data["products"] = $this->stock_bill_model->getInfoProducts($id);
		$data["histories"] = $histories;
		$this->_getlayoutbackend('admin/histories/stock',$data,"backend");
	}
	function stock_detail($id)
	{

		$data = array();
		$data["histories"] = $this->stock_bill_model->getProductHistory($id);
		$this->_getlayoutbackend('admin/histories/stock_detail',$data,"backend");
	}
	
}