<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stockreceipts extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
    }
	public function index()
	{

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/stockreceipts/index');
        $config["total_rows"] = $this->stock_bill_model->countStockReceipts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$stock_receipts = $this->stock_bill_model->getStockReceipts($page,$per_page);
		foreach($stock_receipts as $key => $stock_receipt)
        {
            $products = $this->stock_bill_model->getInfoProducts($stock_receipt["stock_bill_id"]);
            $total = 0;
            foreach ($products as $product)
            {
                $package_quantity = $product['stock_bill_product_quantity'] / $product['package_unit'];
                $total = $total + $package_quantity;
            }
            $stock_receipts[$key]['package_quantity'] = $total;
        }
        $data["stock_receipts"] = $stock_receipts;
		$data["sum"] = $this->stock_bill_model->sumStockReceipts();
		$this->_getlayoutbackend('admin/stockreceipts/index',$data);
	}
	function add($type = "salers")
	{

        $supplier_discount = 0;
		if($_POST) {
            for($i = 0; $i < $this->input->post("total_item"); $i++) {
                $_POST['package_quantity_' . $i] = str_replace(",", "", $_POST['package_quantity_' . $i]);
                $_POST['unit_quantity_' . $i] = str_replace(",", "", $_POST['unit_quantity_' . $i]);
                $product = $this->products_model->getInfo($this->input->post("product_id_" . $i));
                $_POST['product_quantity_' . $i] = $_POST['package_quantity_' . $i] * $product['package_unit'] + $_POST['unit_quantity_' . $i];
            }
		    if($_POST["stock_receipt_type"] == 1){
                $arr1 = $this->stock_bill_model->getInfoProducts($this->input->post("stock_bill_issue_id"));
                foreach($arr1 as $item){
                    $arrProdOld[] = $item["product_id"];
                }
            }
            for($i = 0; $i < $this->input->post("total_item"); $i++){
		        if(!empty($arrProdOld)){
                    if(!in_array($this->input->post("product_id_".$i), $arrProdOld)){
                        $error_quantity =  __("product was not exist in stock issue");
                        $this->form_validation->_error_array[] = $error_quantity;
                        $data["posts"] = $this->input->post();
                        $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                    }
                }
                if(empty($this->input->post("product_code_".$i))){
                    $error_quantity =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
                if (empty($this->input->post("product_name_".$i))){
                    $error_quantity =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
            }
			$this->form_validation->set_rules('stock_receipt_type', 'stock_receipt_type','trim|required|xss_clean');
			if($this->form_validation->run()) {
				if($this->input->post("stock_receipt_status") >= 0 ){//only process if javascript working and update the value for draft/save buttons
                    if($this->input->post("total_item") > 0){
                        $arr = array();
                        $arr["stock_bill_status"] = $this->input->post("stock_receipt_status");
                        $arr["stock_bill_type"] = $this->input->post("stock_receipt_type");
                        $arr["stock_bill_note"] = $this->input->post("stock_receipt_note");
                        if($this->input->post("supplier_id")){
                            $arr["stock_bill_object_id"] = $this->input->post("supplier_id");
                            $arr["stock_bill_account_id"] = $this->input->post("account_id");
                        }
                        if($this->input->post("stock_bill_issue_id")){
                            $arr["stock_bill_object_id"] = $this->input->post("stock_bill_issue_id");
                        }
                        $arr["stock_bill_date"] = $this->input->post("created_date");
                        $arr["stock_bill_created_date"] = date("Y-m-d H:i:s", now());
                        $arr["stock_bill_create_user_id"] = $this->session->userdata('user_id');
                        $arr["stock_bill_flow"] = IN;
                        $stock_receipt_id = $this->stock_bill_model->insert($arr);
                        $arrDetail = array();
                        $arrHistory = array();
                        for($i = 0; $i < $this->input->post("total_item"); $i++){
                            $_POST['package_quantity_'.$i]=str_replace(",","",$_POST['package_quantity_'.$i]);
                            $_POST['unit_quantity_'.$i]=str_replace(",","",$_POST['unit_quantity_'.$i]);
                            $item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                            $product_quantity = $_POST["package_quantity_".$i]*$item["package_unit"] + $_POST["unit_quantity_".$i];
                            $arrItem = array();
                            $arrItem["product_id"] = $this->input->post("product_id_".$i);
                            $arrItem["stock_bill_id"] = $stock_receipt_id;
                            $arrItem["stock_bill_product_code"] = $this->input->post("product_code_".$i);
                            $arrItem["stock_bill_product_name"] = $this->input->post("product_name_".$i);
                            if (!empty($_POST['discount_'.$i]) && empty($this->input->post('stock_bill_issue_id')))
                            {
                                $arrItem["stock_bill_product_price"] = (($this->input->post("stock_receipt_type") == STOCK_RECEIPT_TYPE_RETURN) ? $item['product_price'] : str_replace(",","",$this->input->post("product_cost_".$i)));
                                $arrItem["stock_bill_product_cost"] = 0;
                            } else {
                                $arrItem["stock_bill_product_price"] = (($this->input->post("stock_receipt_type") == STOCK_RECEIPT_TYPE_RETURN) ? $item['product_price'] : str_replace(",","",$this->input->post("product_cost_".$i)));
                                $arrItem["stock_bill_product_cost"] = $item['product_cost'];
                            }
                            $arrItem['stock_bill_product_discount'] = $item['product_discount'];
                            $arrItem["stock_bill_product_quantity"] = $product_quantity;

                            $arrDetail[] = $arrItem;
                            if($this->input->post("stock_receipt_status") == 1 ){
                                $product_quantity = $_POST["package_quantity_".$i]*$item["package_unit"] + $_POST["unit_quantity_".$i];
                                //array product_stock_history
                                $pr_st_his = array();
                                $pr_st_his["product_id"] = $item["product_id"];
                                $pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
                                $pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
                                $pr_st_his["new_product_stock_quantity"] = ($item["product_stock_quantity"] + $product_quantity);
                                $pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT;
                                $pr_st_his["linked_object_id"] = $stock_receipt_id;
                                $pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
                                $arrHistory[] = $pr_st_his;
                                $this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));

                            }
                        }


                        //insert batch stock_receipt_products
                        if(count($arrDetail) >0){
                            $this->stock_bill_model->insert_products($arrDetail);
                        }


                        //insert batch product_stock_history
                        if(count($arrHistory) >0){//history only available when stock_receipt_status is 1
                            $this->stock_bill_model->insert_histories($arrHistory);
                        }

                        //submit and process stock/cash
                        //only substract account when type purchase
                        if($this->input->post("stock_receipt_status") == 1 && $this->input->post("stock_receipt_type") == STOCK_RECEIPT_TYPE_PURCHASE) {
                            //substract amount from bank account (cash table)
                            $product_supplier_id = $this->input->post("supplier_id");
                            if($product_supplier_id){
                                $supplier_discounts = $this->stock_bill_model->getItemSupplier($product_supplier_id);
                                $supplier_discount = $supplier_discounts[0]['supplier_discount'];
                            } else {
                                $supplier_discount = 0;
                            }

                            $query = $this->db->query("SELECT sum(stock_bill_product_price*stock_bill_product_quantity) as totals FROM `stock_bill_products` WHERE `stock_bill_id` =".$stock_receipt_id);
                            if ($query->num_rows() > 0)
                            {
                                $totals = $query->row_array();
                                $totalArr = array();
                                $totalArr["cash_amount"] = floatval($totals['totals']) - (floatval($totals['totals']) * intval($supplier_discount)/100);
                                $totalArr["type"] = 'cost';
                                $totalArr["stock_bill_id"] = $stock_receipt_id;
                                $totalArr["cash_follow"] = -1;
                                $totalArr["saler_id"] = 0;
                                $totalArr["account_id"] = $this->input->post("account_id");
                                $totalArr["cash_create_date"] = date("Y-m-d H:i:s", now());
                                $totalArr["cash_create_user_id"] = $this->session->userdata('user_id');
                                $this->db->insert('cashs', $totalArr);
                                $this->db->insert_id();
                            }
                        }

                        success_message(__('insert_stock_receipt_successfully'));
                        redirect('admin/stockreceipts');
                    }else{
                        $error_quantity =  __("error_stock_receipt_no_product");
                        $this->form_validation->_error_array[] = $error_quantity;
                        $data["posts"] = $this->input->post();
                        $this->_getlayoutbackend('admin/stockreceipts/add',$data,"backend");
                    }
				} else {
                    error_message(__('the stock bill status is not set correctly - it must be either 0 or 1'));
                }
			}
		}
		$data["type"] = $type;
		$this->_getlayoutbackend('admin/stockreceipts/add',$data,"backend");
	}
	function edit($id)
	{
        $supplier_discount = 0;
		$product_supplier_id = $this->input->post("supplier_id");
		if($product_supplier_id){
			$supplier_discounts = $this->stock_bill_model->getItemSupplier($product_supplier_id);
			$supplier_discount = $supplier_discounts[0]['supplier_discount'];
		}
		$data = array();
		$data["stock_receipt"] = $this->stock_bill_model->getInfo($id);
		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$data["stock_receipt"]["stock_bill_status"]);
		if($_POST) {
            if($_POST["stock_receipt_type"] == 1){
                $arr1 = $this->stock_bill_model->getInfoProducts($this->input->post("stock_bill_issue_id"));
                foreach($arr1 as $item){
                    $arrProdOld[] = $item["product_id"];
                }
            }
            for($i = 0; $i < $this->input->post("total_item"); $i++){
                $_POST['package_quantity_'.$i]=str_replace(",","",$_POST['package_quantity_'.$i]);
                $_POST['unit_quantity_'.$i]=str_replace(",","",$_POST['unit_quantity_'.$i]);
                $product = $this->products_model->getInfo($this->input->post("product_id_".$i));
                $_POST['product_quantity_'.$i] = $_POST['package_quantity_'.$i]*$product['package_unit'] + $_POST['unit_quantity_'.$i];
                if(!empty($arrProdOld)){
                    if(!in_array($this->input->post("product_id_".$i), $arrProdOld)){
                        $error_quantity =  __("product was not exiting in stock issue");
                        $this->form_validation->_error_array[] = $error_quantity;
                        $data["posts"] = $this->input->post();
                        $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                    }
                }
                if(empty($this->input->post("product_code_".$i))){
                    $error_quantity =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
                if (empty($this->input->post("product_name_".$i))){
                    $error_quantity =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
            }
			if($this->input->post("stock_receipt_id") > 0 ){
				if($this->input->post("total_item") > 0){

					$stock_receipt = $this->stock_bill_model->getInfo($this->input->post("stock_receipt_id"));
					$stock_bill_products = $this->stock_bill_model->getInfoProducts($this->input->post("stock_receipt_id"));

					//check is_backup_history
					$flag = false;
					$is_change_quantity = false;
					if($this->input->post("curr_stock_receipt_status") == 1){
						if($this->input->post("total_item") != count($stock_bill_products)){
							$flag = $flag || true;
						}
						if($this->input->post("stock_bill_issue_id"))
						{
							if($this->input->post("stock_bill_issue_id") != $stock_receipt["stock_bill_object_id"]){
								$flag = $flag || true;
							}
						}
						if($this->input->post("supplier_id"))
						{
							if($this->input->post("supplier_id") != $stock_receipt["stock_bill_object_id"])
							{
								$flag = $flag || true;
							}
						}
						for($i = 0; $i < $this->input->post("total_item"); $i++){
							if($this->input->post("stock_bill_product_id_".$i)){
                                $_POST['package_quantity_'.$i]=str_replace(",","",$_POST['package_quantity_'.$i]);
                                $_POST['unit_quantity_'.$i]=str_replace(",","",$_POST['unit_quantity_'.$i]);
                                $product = $this->products_model->getInfo($this->input->post("product_id_".$i));
                                $product_quantity = $_POST['package_quantity_'.$i]*$product['package_unit'] + $_POST['unit_quantity_'.$i];
								$test_product = $this->stock_bill_model->getInfoProduct($this->input->post("stock_bill_product_id_".$i));
								if($test_product["stock_bill_product_quantity"] != $product_quantity){
									$flag = $flag || true;
									$is_change_quantity = true;
								}
							}
						}
					}
					if($flag){
						//insert history
						$array1 = array();
						$array1["stock_bill_id"] = $stock_receipt["stock_bill_id"];
						$array1["stock_bill_history_object_id"] = $stock_receipt["stock_bill_object_id"];
						$array1["stock_bill_history_create_user_id"] = $this->session->userdata('user_id');
						$array1["stock_bill_history_note"] = $stock_receipt["stock_bill_note"];
						$array1["stock_bill_history_status"] = $stock_receipt["stock_bill_status"];
						$array1["stock_bill_history_type"] = $stock_receipt["stock_bill_type"];
						$array1["stock_bill_history_created_date"] = date("Y-m-d H:i:s", now());;
						$array1["stock_bill_history_message"] = STOCK_ISSUE_HISTORY_TYPE_CHANGE_STOCK;
						$array1["stock_bill_history_flow"] = $stock_receipt["stock_bill_flow"];
						$value1 = $this->stock_bill_model->insert_history($array1);
						if(count($stock_bill_products) >0 ){
							foreach($stock_bill_products as $k => $item){
								$array2["stock_bill_history_id"] = $value1;
								$array2["stock_bill_product_history_name"] = $item["stock_bill_product_name"];
								$array2["stock_bill_product_history_code"] = $item["stock_bill_product_code"];
								$array2["stock_bill_product_history_price"] = $item["stock_bill_product_price"];
								$array2["stock_bill_product_history_cost"] = $item["stock_bill_product_cost"];
								$array2["stock_bill_product_history_discount"] = $item["stock_bill_product_discount"];
								$array2["stock_bill_product_history_quantity"] = $item["stock_bill_product_quantity"];
								$array2["product_id"] = $item["product_id"];
								$this->stock_bill_model->insert_product_history($array2);
							}
						}
					}
					$arr = array();
					$arr["stock_bill_id"] = $this->input->post("stock_receipt_id");
					if($this->input->post("supplier_id")){
						$arr["stock_bill_object_id"] = $this->input->post("supplier_id");
						$arr["stock_bill_account_id"] = $this->input->post("account_id");
					}
					if($this->input->post("stock_bill_issue_id")){
						$arr["stock_bill_object_id"] = $this->input->post("stock_bill_issue_id");
					}
					$arr["stock_bill_note"] = $this->input->post("stock_receipt_note");
					$arr["stock_bill_status"] = $this->input->post("stock_receipt_status");
                    $arr["stock_bill_date"] = $this->input->post("created_date");
					$arr["stock_bill_modified_date"] = date("Y-m-d H:i:s", now());

					$this->stock_bill_model->update($arr);
					$arrProdOld = array();
					$arr1 = $this->stock_bill_model->getInfoProducts($this->input->post("stock_receipt_id"),"stock_bill_product_id");
					foreach($arr1 as $item){
						$arrProdOld[] = $item["stock_bill_product_id"];
					}

					$arrProdNew = array();
					$arrDetail = array();
					$arrHistory = array();
					for($i = 0; $i < $this->input->post("total_item"); $i++){
						$item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                        $product_quantity = $_POST['package_quantity_'.$i]*$item['package_unit'] + $_POST['unit_quantity_'.$i];
						$item_product = array();
						$stock_bill_product_price = '';
						if($this->input->post('stock_bill_product_id_'.$i)){
							$item_product = $this->stock_bill_model->getInfoProduct($this->input->post('stock_bill_product_id_'.$i));
							$stock_bill_product_price = str_replace(",","",$this->input->post("product_cost_".$i));
						}
						else{
							if($stock_receipt['stock_bill_type'] == STOCK_RECEIPT_TYPE_RETURN){
								$stock_bill_product_price = $item['product_price'];
							}
							else{
								$stock_bill_product_price = str_replace(",","",$this->input->post("product_cost_".$i));
							}
						}
						$arrItem = array();
						$arrItem["product_id"] = $this->input->post("product_id_".$i);
						$arrItem["stock_bill_id"] = $this->input->post("stock_receipt_id");
						$arrItem["stock_bill_product_code"] = $this->input->post("product_code_".$i);
						$arrItem["stock_bill_product_name"] = $this->input->post("product_name_".$i);
                        $arrItem["stock_bill_product_discount"] = $item['product_discount'];
                        if (!empty($_POST['discount_'.$i]) && $stock_receipt['stock_bill_type'] == STOCK_RECEIPT_TYPE_PURCHASE)
                        {
                            $arrItem["stock_bill_product_cost"] = 0;
                            $arrItem["stock_bill_product_price"] = $stock_bill_product_price;

                        } else {
                            $arrItem["stock_bill_product_cost"] = $item['product_cost'];
                            $arrItem["stock_bill_product_price"] = $stock_bill_product_price;
                        }

						$arrItem["stock_bill_product_quantity"] = $product_quantity;
						$item = $this->products_model->getInfo($this->input->post("product_id_".$i));
						if($this->input->post("stock_receipt_status") == 1){//we have to adjust history and product quantity because now stock_receipt_status is 1

							if($this->input->post("curr_stock_receipt_status") == 1){//previous status is also 1 (processed already)
								$flag = false;
								$issueProdId = $this->input->post("stock_bill_product_id_".$i);
								if(in_array($issueProdId,$arrProdOld)){
									$itemIssueProd = $this->stock_bill_model->getInfoProduct($issueProdId);
									if($itemIssueProd["stock_bill_product_quantity"] != (float)str_replace(",","",$this->input->post("product_quantity_".$i))){
										$product_quantity = (float)str_replace(",","",$this->input->post("product_quantity_".$i)) - $itemIssueProd["stock_bill_product_quantity"];
										//array product_stock_history
										$pr_st_his = array();
										$pr_st_his["product_id"] = $item["product_id"];
										$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
										$pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
										$pr_st_his["new_product_stock_quantity"] = ($item["product_stock_quantity"] + $product_quantity);
										$pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT_CHANGE;
										$pr_st_his["linked_object_id"] = $this->input->post("stock_receipt_id");
										$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
										$arrHistory[] = $pr_st_his;
										//update quantity product
										$this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
									}
								}
							} else {//case change from Draft to Processed
								//array product_stock_history
								$pr_st_his = array();
								$pr_st_his["product_id"] = $item["product_id"];
								$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
								$pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
								$pr_st_his["new_product_stock_quantity"] = ($item["product_stock_quantity"] + $product_quantity);
								$pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT;
								$pr_st_his["linked_object_id"] = $this->input->post("stock_receipt_id");
								$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
								$arrHistory[] = $pr_st_his;
								//update quantity product
								$this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
							}

                        }


						if($this->input->post("stock_bill_product_id_".$i)){
							$arrProdNew[] = $this->input->post("stock_bill_product_id_".$i);
							$arrItem["stock_bill_product_id"] = $this->input->post("stock_bill_product_id_".$i);
							$this->stock_bill_model->update_product($arrItem);
						} else{
							$issueProdId = $this->stock_bill_model->insert_product($arrItem);
							if($this->input->post("curr_stock_receipt_status") == 1){
								$product_stock_quantity = $item["product_stock_quantity"] + $product_quantity;
								// update quantity product
								$this->products_model->update(array("product_id"=>$item["product_id"],"product_stock_quantity"=>$product_stock_quantity));
								//array product_stock_history
								$pr_st_his = array();
								$pr_st_his["product_id"] = $item["product_id"];
								$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
								$pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
								$pr_st_his["new_product_stock_quantity"] = $product_stock_quantity;
								$pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT_CHANGE;
								$pr_st_his["linked_object_id"] = $this->input->post("stock_receipt_id");
								$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
								$arrHistory[] = $pr_st_his;
							}
						}
						$arrDetail[] = $arrItem;
					}

                    //we proceed to delete deleted rows
					$result = array_diff($arrProdOld, $arrProdNew);
					if(count($result) > 0 && count($arrProdOld) > 0 ){
						foreach($result as $item){
							if($this->input->post("curr_stock_receipt_status") == 1){
								$stock_product = $this->stock_bill_model->getInfoProduct($item);
								$product = $this->products_model->getInfo($stock_product["product_id"]);
								$product_quantity = $product["product_stock_quantity"] - $stock_product["stock_bill_product_quantity"];

								// update quantity product
								$this->products_model->update(array("product_id"=>$product["product_id"],"product_stock_quantity"=>$product_quantity));
								// product_stock_history
								$pr_st_his = array();
								$pr_st_his["product_id"] = $product["product_id"];
								$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
								$pr_st_his["previous_product_stock_quantity"] = $product["product_stock_quantity"];
								$pr_st_his["new_product_stock_quantity"] = $product_quantity;
								$pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT_CHANGE;
								$pr_st_his["linked_object_id"] = $this->input->post("stock_receipt_id");
								$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
								$arrHistory[] = $pr_st_his;
								//delete stock product when processed
								$this->stock_bill_model->delete_product($item);
							}
							else{
								$this->stock_bill_model->delete_product($item);
							}
						}

					}

					//insert batch product_stock_history
					if(count($arrHistory) >0){
						$this->stock_bill_model->insert_histories($arrHistory);
					}

                    //we have to update cash because now stock_receipt_status is 1
                    // and this is a purchase bill
                    if($this->input->post("stock_receipt_status") == 1 && $stock_receipt["stock_bill_type"] == STOCK_RECEIPT_TYPE_PURCHASE){

                        // update or insert cash record
                        $cash_amount_exists = false;
                        $query_check = $this->db->query("SELECT count(*) as count FROM cashs WHERE stock_bill_id =".$id." AND type= 'cost'");
                        if ($query_check->num_rows() > 0)
                        {
                            $row_check = $query_check->row_array();
                            $cash_amount_exists = $row_check['count'];
                        }

                        $query = $this->db->query("SELECT sum(stock_bill_product_price*stock_bill_product_quantity) as totals FROM `stock_bill_products` WHERE `stock_bill_id` =".$id);
                        if ($query->num_rows() > 0)
                        {

                            $totals = $query->row_array();

                            $totalArr = array();
                            $totalArr["cash_amount"] = floatval($totals['totals']) - (floatval($totals['totals']) * intval($supplier_discount)/100);
                            $totalArr["type"] = 'cost';
                            $totalArr["stock_bill_id"] = $id;
                            $totalArr["cash_follow"] = -1;
                            $totalArr["account_id"] = $this->input->post("account_id");
                            $totalArr["cash_create_date"] = date("Y-m-d H:i:s", now());
                            $totalArr["cash_create_user_id"] = $this->session->userdata('user_id');

                            if($cash_amount_exists){
                                $this->db->where('stock_bill_id',$id);
                                $this->db->update('cashs', $totalArr);
                            }else{
                                $this->db->insert('cashs', $totalArr);
                                $this->db->insert_id();
                            }
                        }
                    }


					success_message(__('update_stock_receipt_successfully'));
					redirect('admin/stockreceipts');

				}
				else{
					$error_quantity =  __("error_stock_receipt_no_product");
					$this->form_validation->_error_array[] = $error_quantity;
					$data["posts"] = $this->input->post();
					$this->_getlayoutbackend('admin/stockreceipts/add',$data,"backend");
				}
			} else {
                error_message(__('the stock bill status is not set correctly - it must be either 0 or 1'));
            }
		}
        $products = $this->stock_bill_model->getInfoProducts($id);

        foreach ($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['stock_bill_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity'] = $product['stock_bill_product_quantity'] % $product['package_unit'];
        }
		$data["stock_receipt"]["products"] = $products;
		$this->_getlayoutbackend('admin/stockreceipts/edit',$data,"backend");
	}
	function delete($id)
	{
		$stock = $this->stock_bill_model->getInfo($id);
		$lock = false;
		if(isset($stock["stock_bill_status"])){
			if($stock["stock_bill_status"] == STOCK_CHANGE_STATUS_PROCESSED){
				$lock = true;
			}
		}
		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$lock);
		$stock = $this->stock_bill_model->getInfo($id);
		if($stock){
			if($stock["stock_bill_status"] == STOCK_CHANGE_STATUS_PROCESSED){
				$products = $this->stock_bill_model->getInfoProducts($id);
				if(count($products) >0){
					foreach($products as $k => $item){
						$product = $this->products_model->getInfo($item["product_id"]);
						$quantity = $product["product_stock_quantity"] - $item["stock_bill_product_quantity"];
						$this->products_model->update(array("product_id" => $product["product_id"],"product_stock_quantity" => $quantity));
						// product_stock_history
						$history["product_id"] = $item["product_id"];
						$history["modified_user_id"] = $this->session->userdata('user_id');
						$history["previous_product_stock_quantity"] = $product["product_stock_quantity"];
						$history["new_product_stock_quantity"] = $quantity;
						$history["reason"] = STOCK_CHANGE_REASON_RECEIPT;
						// $history["linked_object_id"] = $this->input->post("stock_issue_id");
						$history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
						$this->stock_bill_model->insert_stock_history($history);
					}
				}
			}
		}
		$this->stock_bill_model->delete($id);
		$this->stock_bill_model->delete_products($id);
		$products_his = $this->stock_bill_model->getHistory($id);
		if(count($products_his) >0){
			foreach($products_his as $item){
				$this->stock_bill_model->delete_products_history($item["stock_bill_history_id"]);
			}
		}
		$this->stock_bill_model->delete_history($id);
		success_message(__('delete_stock_receipt_successfully'));
		redirect('admin/stockreceipts');
	}
	function history($id)
	{

		$data = array();
		$data["histories"] = $this->stock_bill_model->getHistory($id);
		$this->_getlayoutbackend('admin/stockreceipts/history',$data,"backend");
	}
	function view($id = 0)
	{
        $supplier_discount = 0;

		if($_POST){
            check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),'add');
			$id = $this->input->post("stock_bill_id");
            $stock_bill = $this->stock_bill_model->getInfo($id);
            if ($stock_bill['stock_bill_status'] == STOCK_CHANGE_STATUS_PROCESSED) {
                error_message(__('Phieu nhap da duoc xu li'));
                redirect('admin/stockissues');
            }
			$products = $this->stock_bill_model->getInfoProducts($id);
			if(count($products) > 0 && $products){
				$arrHistory = array();
				$this->stock_bill_model->update(array("stock_bill_id"=>$id,
													"stock_bill_status"=>STOCK_CHANGE_STATUS_PROCESSED));
				foreach($products as $k => $item){
					$product = $this->products_model->getInfo($item["product_id"]);
					//array product_stock_history
					$pr_st_his = array();
					$pr_st_his["product_id"] = $item["product_id"];
					$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
					$pr_st_his["previous_product_stock_quantity"] = $product["product_stock_quantity"];
					$pr_st_his["new_product_stock_quantity"] = ($product["product_stock_quantity"] + $item["stock_bill_product_quantity"]);
					$pr_st_his["reason"] = STOCK_CHANGE_REASON_RECEIPT;
					$pr_st_his["linked_object_id"] = $id;
					$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
					$arrHistory[] = $pr_st_his;
					//update quantity product
					$this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
				}
				if(count($arrHistory) >0){
					$this->stock_bill_model->insert_histories($arrHistory);
				}


                //substract amount from bank account (cash table) in the case purchase receipt
                if ($stock_bill['stock_bill_type'] == STOCK_RECEIPT_TYPE_PURCHASE && $stock_bill['stock_bill_flow'] == 1) {
                    $query = $this->db->query("SELECT sum(stock_bill_product_price*stock_bill_product_quantity) as totals FROM `stock_bill_products` WHERE `stock_bill_id` =".$id);
                    if ($query->num_rows() > 0)
                    {
                        $totals = $query->row_array();
                        $totalArr = array();
                        $totalArr["cash_amount"] = floatval($totals['totals']) - (floatval($totals['totals']) * intval($supplier_discount)/100);
                        $totalArr["type"] = 'cost';
                        $totalArr["stock_bill_id"] = $id;
                        $totalArr["cash_follow"] = -1;
                        $totalArr["saler_id"] = 0;
                        $totalArr["account_id"] = $stock_bill['stock_bill_account_id'];
                        $totalArr["cash_create_date"] = date("Y-m-d H:i:s", now());
                        $totalArr["cash_create_user_id"] = $this->session->userdata('user_id');
                        $this->db->insert('cashs', $totalArr);
                        $this->db->insert_id();
                    }
                }


				success_message(__('update_stock_receipt_successfully'));
				redirect('admin/stockreceipts');
			}
		}
		$products = $this->stock_bill_model->getInfoProducts($id);

        foreach($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['stock_bill_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity']= $product['stock_bill_product_quantity'] % $product['package_unit'];
        }
		$data = array();
		$data["id"] = $id;
		$data["stock"] = $this->stock_bill_model->getInfo($id);
		$data["products"] = $products;
		$this->_getlayoutbackend('admin/stockreceipts/view',$data,"backend");
	}

}