<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cashs extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model('cashs_model');
    }
	public function index()
	{

		$data = array();

		$this->_getlayoutbackend('admin/cashs/index',$data);
	}
	
    function add($type) {
        switch ($type) {
            case 'receipt':
                $this->_add_receipt();
                break;
            case 'delivery_expenses':
                $this->_add_delivery_expenses();
                break;
            case 'discount_advance';
                $this->_add_discount_advance();
                break;
            case 'debit_advance';
                $this->_add_debit_advance();
                break;
            case 'debit_receipt';
                $this->_add_debit_receipt();
                break;
            case 'discount_receipt';
                $this->_add_discount_receipt();
                break;
            case 'commission_receipt';
                $this->_add_commission_receipt();
                break;
            case 'commission_discount';
                $this->_add_commission_discount();
                break;
            case 'expense';
                $this->_add_expense();
                break;
            case 'load';
                $this->_add_load();
                break;
            case 'salary';
                $this->_add_salary();
                break;
            case 'withdraw';
                $this->_add_withdraw();
                break;
        }
    }
	
    function edit($type, $id) {
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());
        switch ($type) {
            case 'receipt':
                $this->_edit_receipt($id);
                break;
            case 'delivery_expenses':
                $this->_edit_delivery_expenses($id);
                break;
            case 'discount_advance';
                $this->_edit_discount_advance($id);
                break;
            case 'debit_advance';
                $this->_edit_debit_advance($id);
                break;
            case 'debit_receipt';
                $this->_edit_debit_receipt($id);
            case 'discount_receipt';
                $this->_edit_discount_receipt($id);
                break;
            case 'commission_receipt';
                $this->_edit_commission_receipt($id);
                break;
            case 'commission_discount';
                $this->_edit_commission_discount($id);
                break;
            case 'expense';
                $this->_edit_expense($id);
                break;
            case 'load';
                $this->_edit_load($id);
                break;
            case 'salary';
                $this->_edit_salary($id);
                break;
            case 'withdraw';
                $this->_edit_withdraw($id);
                break;
        }
    }
    function delete($type, $id) {
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());
        switch ($type) {
            case 'receipt':
                $this->_delete_receipt($id);
                break;
            case 'delivery_expenses':
                $this->_delete_delivery_expenses($id);
                break;
            case 'debit_advance';
                $this->_delete_debit_advance($id);
                break;
            case 'debit_receipt';
                $this->_delete_debit_receipt($id);
                break;
            case 'discount_advance';
                $this->_delete_discount_advance($id);
                break;
            case 'discount_receipt';
                $this->_delete_discount_receipt($id);
                break;
            case 'commission_receipt';
                $this->_delete_commission_receipt($id);
                break;
            case 'commission_discount';
                $this->_delete_commission_discount($id);
                break;
            case 'expense';
                $this->_delete_expense($id);
                break;
            case 'load';
                $this->_delete_load($id);
                break;
            case 'salary';
                $this->_delete_salary($id);
                break;
            case 'withdraw';
                $this->_delete_withdraw($id);
                break;
        }
    }
    //receipts
    function receipts()
    {

        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/cashs/receipts');
        $config["total_rows"] = $this->cashs_model->countReceipts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $data["receipts"] = $this->cashs_model->getReceipts($page,$per_page);
        $data["sum"] = $this->cashs_model->sumReceipts();
        $this->_getlayoutbackend('admin/cashs/receipts',$data);
    }

    function _add_receipt()
    {

        if($_POST) {
            $this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
            if($this->form_validation->run()) {

                //validate so tien hop le theo PX
                //px = chi phi giao hang + xuat lai kho + tam ung giam gia ban hang + tien thu ve
                $delivery_expenses = $this->cashs_model->sumDeliveryExpensesByStockIssue($this->input->post("stock_bill_issue_id"));
                $receipt = $this->cashs_model->sumReceiptsByStockIssue($this->input->post("stock_bill_issue_id"));
                $discount = $this->cashs_model->sumDiscountsByStockIssue($this->input->post("stock_bill_issue_id"));
                $return = $this->stock_bill_model->sumStockReturnReceiptsByStockIssue($this->input->post("stock_bill_issue_id"));
                $total = $this->stock_bill_model->getTotal($this->input->post("stock_bill_issue_id"));

                if ($total['total'] < ($delivery_expenses['sum'] + $receipt['sum'] + $discount['sum'] + $return['sum'] + str_replace(",","",$this->input->post("cash_amount"))) ) {
                    error_message(__('Invalidate cash amount (total is too big)'));
                    redirect('admin/cashs/receipts');
                }

                //nhan tien - chi can update bang cash, lien let thong tin voi phieu xuat va tai khoan
                $arr = array();
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
                $arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
                $arr["cash_follow"] = 1;
                $arr["cash_create_user_id"] = $this->session->userdata('user_id');
                $arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
                $arr["account_id"] = $this->input->post("account_id");
                $arr["type"] = 'receipt';
                $this->cashs_model->insert($arr);

                //try to archive stock bill issue if total is matched
                $this->try_archive($this->input->post("stock_bill_issue_id"));

                success_message(__('insert_cashs_receipts_successfully'));
                redirect('admin/cashs/receipts');
            }

        }
        $data = array();
        $this->_getlayoutbackend('admin/cashs/addreceipt',$data);
    }
    
	function _edit_receipt($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'Sale user','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["type"] = 'receipt';
				$arr["cash_follow"] = 1;
				$this->cashs_model->update($arr);

                //try to archive stock bill issue if total is matched
                $this->try_archive($this->input->post("stock_bill_issue_id"));

				success_message(__('update_cashs_receipts_successfully')); 
				redirect('admin/cashs/receipts');
			}
			
		}
		$data = array();
		$data["receipt"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editreceipt',$data);
	}
	
	function _delete_receipt($id)
	{

		$this->cashs_model->delete($id);

        //try to archive stock bill issue if total is matched
        //$this->try_archive($this->input->post("stock_bill_issue_id"));

		success_message(__('delete_cashs_receipts_successfully')); 
		redirect('admin/cashs/receipts');
	}
	function paids()
	{

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/paids');
        $config["total_rows"] = $this->cashs_model->countAll(IN);
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["paids"] = $this->cashs_model->getAll(IN,$page,$per_page);
		$this->_getlayoutbackend('admin/cashs/paids',$data);
	}
	
	function _edit_paid($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('product_supplier_id', 'product_supplier_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["supplier_id"] = $this->input->post("supplier_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_paids_successfully')); 
				redirect('admin/cashs/paids');
			}
			
		}
		
		$data = array();
		$data["paid"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editpaid',$data);
	}
	function _delete_paid($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_paids_successfully')); 
		redirect('admin/cashs/paids');
	}

    function transfer()
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            $this->form_validation->set_rules('account_tranfer', 'account_tranfer','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
            if($this->form_validation->run()) {
                if($this->input->post("account_tranfer") != $this->input->post("account_id")){
                    $arr = array();
                    $arr["account_id"] = $this->input->post("account_tranfer");
                    $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                    $arr["cash_note"] = $this->input->post("cash_note");

                    $arr["cash_date"] = $this->input->post("created_date");
                    $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
                    $arr["cash_follow"] = 1;
                    $arr["type"] = 'tranfer';
                    $arr["cash_create_user_id"] = $this->session->userdata('user_id');
                    $arr["transfer_account"] = $this->input->post("account_id");
                    $this->cashs_model->insert($arr);

                    $arrSaveMoney = array();
                    $arrSaveMoney["account_id"] = $this->input->post("account_id");
                    $arrSaveMoney["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                    $arrSaveMoney["cash_note"] = $this->input->post("cash_note");
                    $arrSaveMoney["cash_date"] = $this->input->post("created_date");
                    $arrSaveMoney["cash_create_date"] = date("Y-m-d H:i:s", now());
                    $arrSaveMoney["cash_follow"] = -1;
                    $arrSaveMoney["type"] = 'tranfer';
                    $arrSaveMoney["cash_create_user_id"] = $this->session->userdata('user_id');
                    $this->cashs_model->insert($arrSaveMoney);

                    success_message(__('Da thuc hien chuyen tien thanh cong'));
                    redirect('admin/reports/accounts/');
                }else{
                    error_message(__('Tai khoan chuyen tien va nhan tien trung nhau'));
                    redirect('admin/cashs/transfer');
                }


            }

        }
        $data = array();
        $this->_getlayoutbackend('admin/cashs/transfer',$data);
    }
	function salary()
	{

		$filter = "type IS NOT NULL";
		if($_GET){
			if($this->input->get("saler") != 0){
				$filter .= " AND saler_id = ". $this->input->get("saler");
			}
			
			if($this->input->get("from") != "" && $this->input->get("to") != "" ){
				$filter .= " AND UNIX_TIMESTAMP(cash_create_date) > " . strtotime($this->input->get("from")) . " AND UNIX_TIMESTAMP(cash_create_date) < ". strtotime("+1day",strtotime($this->input->get("to")));
			}
		}
		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/salary');
        $config["total_rows"] = $this->cashs_model->countSalaries();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["salary"] = $this->cashs_model->getSalaries($page,$per_page);
        $data["sum"] = $this->cashs_model->sumSalaries();
		$this->_getlayoutbackend('admin/cashs/salary',$data);
	}
	function _add_salary()
	{

		if($_POST) {
			$this->form_validation->set_rules('sale_user_id', 'sale_user_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr = array();
				$arr["saler_id"] = $this->input->post("sale_user_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_follow"] = -1;
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = $this->input->post("type");
				$this->cashs_model->insert($arr);
				success_message(__('insert_cashs_salary_successfully')); 
				redirect('admin/cashs/salary');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/addsalary',$data);
	}
	function _edit_salary($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('sale_user_id', 'Sale user','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["saler_id"] = $this->input->post("sale_user_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = -1;
				$arr["type"] = $this->input->post("type");
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_salary_successfully')); 
				redirect('admin/cashs/salary');
			}
			
		}
		$data = array();
		$data["salary"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editsalary',$data);
	}
	function _delete_salary($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_salary_successfully')); 
		redirect('admin/cashs/salary');
	}
	
	
	function withdraw()
	{


		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/withdraw');
        $config["total_rows"] = $this->cashs_model->countWithdraws();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["withdraw"] = $this->cashs_model->getWithdraws($page,$per_page);
        $data["sum"] = $this->cashs_model->sumWithdraws();
		$this->_getlayoutbackend('admin/cashs/withdraw',$data);
	}
	function _add_withdraw()
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = -1;
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'withdraw';
				$this->cashs_model->insert($arr);
				success_message(__('insert_cashs_withdraw_successfully')); 
				redirect('admin/cashs/withdraw');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/addwithdraw',$data);
	}
	function _edit_withdraw($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'Account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["type"] = 'withdraw';
				$arr["cash_follow"] = -1;
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_withdraw_successfully')); 
				redirect('admin/cashs/withdraw');
			}
			
		}
		$data = array();
		$data["withdraw"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editwithdraw',$data);
	}
	function _delete_withdraw($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_withdraw_successfully')); 
		redirect('admin/cashs/withdraw');
	}
	
	//list nap tien
	function load()
	{


		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/load');
        $config["total_rows"] = $this->cashs_model->countLoads();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["load"] = $this->cashs_model->getLoads($page,$per_page);
        $data["sum"] = $this->cashs_model->sumLoads();
		$this->_getlayoutbackend('admin/cashs/load',$data);
	}
    
    
	function _add_load()
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = 1;
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'load';
				$this->cashs_model->insert($arr);
				success_message(__('insert_cashs_load_successfully')); 
				redirect('admin/cashs/load');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/addload',$data);
	}
	function _edit_load($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = 1;
				$arr["type"] = 'load';
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_load_successfully')); 
				redirect('admin/cashs/load');
			}
			
		}
		$data = array();
		$data["load"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editload',$data);
	}
	function _delete_load($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_load_successfully')); 
		redirect('admin/cashs/load');
	}
	
	//discount_receipt
	function discount_receipt()
	{


		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/discount_receipt');
        $config["total_rows"] = $this->cashs_model->countDiscountReceipts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["receipt"] = $this->cashs_model->getDiscountReceipts($page,$per_page);
        $data["sum"] = $this->cashs_model->sumDiscountReceipts();
		$this->_getlayoutbackend('admin/cashs/discount_receipt',$data);
	}
   
	
	//Them moi: nhan tien khuyen mai
	function _add_discount_receipt()
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            $this->form_validation->set_rules('sale_user_id', 'sale_user_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = 1;
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'discount';
                $arr["saler_id"] = $this->input->post("sale_user_id");
				$this->cashs_model->insert($arr);
				success_message(__('insert_cashs_discount_receipt_successfully')); 
				redirect('admin/cashs/discount_receipt');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/adddiscount_receipt',$data);
	}
	//Sua: nhan tien khuyen mai
	function _edit_discount_receipt($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["type"] = 'discount';
				$arr["cash_follow"] = 1;
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_discount_receipt_successfully')); 
				redirect('admin/cashs/discount_receipt');
			}
			
		}
		$data = array();
		$data["receipt"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editdiscount_receipt',$data);
	}
	function _delete_discount_receipt($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_receipt_successfully')); 
		redirect('admin/cashs/discount_receipt');
	}
    //debit_receipt
    function debit_receipt()
    {


        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/cashs/debit_receipt');
        $config["total_rows"] = $this->cashs_model->countDebitReceipts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $data["receipt"] = $this->cashs_model->getDebitReceipts($page,$per_page);
        $data["sum"] = $this->cashs_model->sumDebitReceipts();
        $this->_getlayoutbackend('admin/cashs/debit_receipt',$data);
    }


    //Them moi: nhan tien khuyen mai
    function _add_debit_receipt()
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            $this->form_validation->set_rules('sale_user_id', 'sale_user_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["account_id"] = $this->input->post("account_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_date"] = $this->input->post("created_date");
                $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
                $arr["cash_follow"] = 1;
                $arr["cash_create_user_id"] = $this->session->userdata('user_id');
                $arr["type"] = 'advance';
                $arr["saler_id"] = $this->input->post("sale_user_id");
                $this->cashs_model->insert($arr);
                success_message(__('insert_cashs_debit_receipt_successfully'));
                redirect('admin/cashs/debit_receipt');
            }

        }
        $data = array();
        $this->_getlayoutbackend('admin/cashs/adddebit_receipt',$data);
    }
    //Sua: nhan tien khuyen mai
    function _edit_debit_receipt($id)
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["cash_id"] = $this->input->post("cash_id");
                $arr["account_id"] = $this->input->post("account_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
                $arr["type"] = 'advance';
                $arr["cash_follow"] = 1;
                $this->cashs_model->update($arr);
                success_message(__('update_cashs_debit_receipt_successfully'));
                redirect('admin/cashs/debit_receipt');
            }

        }
        $data = array();
        $data["receipt"] = $this->cashs_model->getInfo($id);
        $this->_getlayoutbackend('admin/cashs/editdebit_receipt',$data);
    }
    function _delete_debit_receipt($id)
    {

        $this->cashs_model->delete($id);
        success_message(__('delete_cashs_receipt_successfully'));
        redirect('admin/cashs/debit_receipt');
    }
    
    function commission_receipt()
    {


        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/cashs/commission_receipt');
        $config["total_rows"] = $this->cashs_model->countCommissionReceipts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $data["receipt"] = $this->cashs_model->getCommissionReceipts($page,$per_page);
        $data["sum"] = $this->cashs_model->sumCommissionReceipts();
        $this->_getlayoutbackend('admin/cashs/commission_receipt',$data);
    }
    //Them moi: nhan tien khuyen mai
    function _add_commission_receipt()
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["account_id"] = $this->input->post("account_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_date"] = $this->input->post("created_date");
                $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
                $arr["cash_follow"] = 1;
                $arr["cash_create_user_id"] = $this->session->userdata('user_id');
                $arr["type"] = 'commission';
                $this->cashs_model->insert($arr);
                success_message(__('insert_cashs_discount_receipt_successfully'));
                redirect('admin/cashs/commission_receipt');
            }

        }
        $data = array();
        $this->_getlayoutbackend('admin/cashs/addcommission_receipt',$data);
    }
    //Sua: nhan tien khuyen mai
    function _edit_commission_receipt($id)
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["cash_id"] = $this->input->post("cash_id");
                $arr["account_id"] = $this->input->post("account_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
                $arr["type"] = 'commission';
                $arr["cash_follow"] = 1;
                $this->cashs_model->update($arr);
                success_message(__('update_cashs_discount_receipt_successfully'));
                redirect('admin/cashs/commission_receipt');
            }

        }
        $data = array();
        $data["receipt"] = $this->cashs_model->getInfo($id);
        $this->_getlayoutbackend('admin/cashs/editcommission_receipt',$data);
    }
    function _delete_commission_receipt($id)
    {

        $this->cashs_model->delete($id);
        success_message(__('delete_cashs_receipt_successfully'));
        redirect('admin/cashs/commission_receipt');
    }

    function commission_discount()
    {
        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/cashs/commission_discount');
        $config["total_rows"] = $this->cashs_model->countCommissionDiscounts();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $data["receipt"] = $this->cashs_model->getCommissionDiscounts($page,$per_page);
        $data["sum"] = $this->cashs_model->sumCommissionDiscounts();
        $this->_getlayoutbackend('admin/cashs/commission_discount',$data);
    }
    //Them moi: nhan tien khuyen mai
    function _add_commission_discount()
    {
        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["account_id"] = $this->input->post("account_id");
                $arr["supplier_id"] = $this->input->post("supplier_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_date"] = $this->input->post("created_date");
                $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
                $arr["cash_follow"] = 1;
                $arr["cash_create_user_id"] = $this->session->userdata('user_id');
                $arr["type"] = 'commission_discount';
                $this->cashs_model->insert($arr);
                success_message(__('insert_cashs_discount_receipt_successfully'));
                redirect('admin/cashs/commission_discount');
            }
        }
        $data = array();
        $this->_getlayoutbackend('admin/cashs/addcommission_discount',$data);
    }
    //Sua: nhan tien khuyen mai
    function _edit_commission_discount($id)
    {

        if($_POST) {
            $this->form_validation->set_rules('account_id', 'account','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
            if($this->form_validation->run()) {
                $arr = array();
                $arr["cash_id"] = $this->input->post("cash_id");
                $arr["supplier_id"] = $this->input->post("supplier_id");
                $arr["account_id"] = $this->input->post("account_id");
                $arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
                $arr["cash_note"] = $this->input->post("cash_note");
                $arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
                $arr["type"] = 'commission_dicount';
                $arr["cash_follow"] = 1;
                $this->cashs_model->update($arr);
                success_message(__('update_cashs_discount_receipt_successfully'));
                redirect('admin/cashs/commission_discount');
            }

        }
        $data = array();
        $data["receipt"] = $this->cashs_model->getInfo($id);
        $this->_getlayoutbackend('admin/cashs/editcommission_discount',$data);
    }
    function _delete_commission_discount($id)
    {

        $this->cashs_model->delete($id);
        success_message(__('delete_cashs_receipt_successfully'));
        redirect('admin/cashs/commission_discount');
    }
	
	function delivery_expenses()
	{
		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/delivery_expenses');
        $config["total_rows"] = $this->cashs_model->countDeliveryExpenses();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["delivery_expenses"] = $this->cashs_model->getDeliveryExpenses($page,$per_page);
        $data["sum"] = $this->cashs_model->sumDeliveryExpenses();
		$this->_getlayoutbackend('admin/cashs/delivery_expenses',$data);
	}
	function _add_delivery_expenses()
	{
        //need to rewrite - save to expense table instead of cash table

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
			
				//validate so tien hop le theo PX
				//px = chi phi giao hang + xuat lai kho + tam ung giam gia ban hang + tien thu ve
				$delivery_expenses = $this->cashs_model->sumDeliveryExpensesByStockIssue($this->input->post("stock_bill_issue_id"));
				$receipt = $this->cashs_model->sumReceiptsByStockIssue($this->input->post("stock_bill_issue_id"));
				$discount = $this->cashs_model->sumDiscountsByStockIssue($this->input->post("stock_bill_issue_id"));
				$return = $this->stock_bill_model->sumStockReturnReceiptsByStockIssue($this->input->post("stock_bill_issue_id"));
				
				$total = $this->stock_bill_model->getTotal($this->input->post("stock_bill_issue_id"));
				
				if ($total['total'] < ($delivery_expenses['sum'] + $receipt['sum'] + $discount['sum'] + $return['sum'] + str_replace(",","",$this->input->post("cash_amount"))) ) {
					error_message(__('Invalidate cash amount (total is too big)')); 
					redirect('admin/cashs/receipts');
				}
			
				$arr = array();
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
                $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_follow"] = 0;
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["type"] = 'expense';
				$this->cashs_model->insert($arr);
                //try to archive stock bill issue if total is matched
                $this->try_archive($this->input->post("stock_bill_issue_id"));

				success_message(__('insert_cashs_delivery_expenses_successfully')); 
				redirect('admin/cashs/delivery_expenses');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/adddelivery_expenses',$data);
	}
	function _edit_delivery_expenses($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'Sale user','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["type"] = 'expense';
				$arr["cash_follow"] = 0;
				$this->cashs_model->update($arr);

                //try to archive stock bill issue if total is matched
                $this->try_archive($this->input->post("stock_bill_issue_id"));
				success_message(__('update_cashs_delivery_expenses_successfully')); 
				redirect('admin/cashs/delivery_expenses');
			}
			
		}
		$data = array();
		$data["delivery_expenses"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editdelivery_expenses',$data);
	}
	function _delete_delivery_expenses($id)
	{

		$this->cashs_model->delete($id);
        //try to archive stock bill issue if total is matched
        $this->try_archive($this->input->post("stock_bill_issue_id"));

		success_message(__('delete_cashs_delivery_expenses_successfully')); 
		redirect('admin/cashs/delivery_expenses');
	}
	
	
	function expense()
	{


		
		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/expense');
        $config["total_rows"] = $this->cashs_model->countOperatingExpenses();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["expense"] = $this->cashs_model->getOperatingExpenses($page,$per_page);
        $data["sum"] = $this->cashs_model->sumOperatingExpenses();
		$this->_getlayoutbackend('admin/cashs/expense',$data);
	}
	
	//Them chi phi hoat dong
	function _add_expense()
	{

		if($_POST) {
			 $this->form_validation->set_rules('cash_amount', 'Sale user','trim|required|xss_clean');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'expense';
				$arr["cash_follow"] = -1;
				$this->cashs_model->insert($arr);
				success_message(__('insert_cashs_expense_successfully')); 
				redirect('admin/cashs/expense');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/addexpense',$data);
	}
	function _edit_expense($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('cash_amount', 'Sale user','trim|required|xss_clean');
            // $this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["account_id"] = $this->input->post("account_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date"] = date("Y-m-d H:i:s", now());
				$arr["type"] = 'expense';
				$arr["cash_follow"] = -1;
				$this->cashs_model->update($arr);
				success_message(__('update_cashs_receipts_successfully')); 
				redirect('admin/cashs/expense');
			}
			
		}
		
		$data = array();
		$data["expense"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editexpense',$data);
	}
	function _delete_expense($id)
	{

		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_expense_successfully')); 
		redirect('admin/cashs/expense');
	}
	
	
   
    //discount_advance
	function discount_advance()
	{

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/cashs/discount_advance');
        $config["total_rows"] = $this->cashs_model->countDiscountAdvances();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["advance"] = $this->cashs_model->getDiscountAdvances($page, $per_page);
        $data["sum"] = $this->cashs_model->sumDiscountAdvances();
		$this->_getlayoutbackend('admin/cashs/discount_advance',$data);
	}
	function _add_discount_advance()
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
			$this->form_validation->set_rules('sale_user_id', 'Sale user','trim|required|xss_clean');
			// $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["saler_id"] = $this->input->post("sale_user_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
                $arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'discount';
				$arr["cash_follow"] = 0;
				$this->cashs_model->insert($arr);
                 //update archive stock bill
                $this->try_archive($this->input->post("stock_bill_issue_id"));

				success_message(__('insert_cashs_discount_advance_successfully')); 
				redirect('admin/cashs/discount_advance');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/adddiscount_advance',$data);
	}
	function _edit_discount_advance($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
			$this->form_validation->set_rules('sale_user_id', 'Sale user','trim|required|xss_clean');
            //$this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["saler_id"] = $this->input->post("sale_user_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date  "] = date("Y-m-d H:i:s", now());
				$arr["type"] = 'discount';
				$arr["cash_follow"] = 0;
				
				$this->cashs_model->update($arr);
                 //update archive stock bill
                //$stock_bill_id = intval($this->input->post("stock_bill_issue_id"));
                //$delivery_expenses = $this->cashs_model->getDiscountForStockIssue($stock_bill_id);
                //$receipt = $this->cashs_model->getReceiptForStockIssue($stock_bill_id);;
                //$discount = $this->cashs_model->getDeliveryExpensesForStockIssue($stock_bill_id);;
                //$this->cashs_model->archive($stock_bill_id,floatval($delivery_expenses['0']['total']),floatval($receipt['0']['total']),floatval($discount['0']['total']));

				success_message(__('update_cashs_discount_advance_successfully')); 
				redirect('admin/cashs/discount_advance');
			}
			
		}
		$data = array();
		$data["advance"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editdiscount_advance',$data);
	}
	function _delete_discount_advance($id)
	{
		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_advance_successfully')); 
		redirect('admin/cashs/discount_advance');
	}
	//debit_advance
    function debit_advance() {
        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/cashs/debit_advance');
        $config["total_rows"] = $this->cashs_model->countDebitAdvances();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $data["advance"] = $this->cashs_model->getDebitAdvances($page, $per_page);
        $data["sum"] = $this->cashs_model->sumDebitAdvances();
        $this->_getlayoutbackend('admin/cashs/debit_advance',$data);

    }
	function _add_debit_advance()
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
			$this->form_validation->set_rules('sale_user_id', 'Sale user','trim|required|xss_clean');
			// $this->form_validation->set_rules('cash_amount', 'cash_amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["saler_id"] = $this->input->post("sale_user_id");
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_date"] = $this->input->post("created_date");
				$arr["cash_create_date"] = date("Y-m-d H:i:s", now());
				$arr["cash_create_user_id"] = $this->session->userdata('user_id');
				$arr["type"] = 'advance';
				$arr["cash_follow"] = 0;
				$this->cashs_model->insert($arr);
                 //update archive stock bill
                $this->try_archive($this->input->post("stock_bill_issue_id"));

				success_message(__('insert_cashs_discount_advance_successfully')); 
				redirect('admin/cashs/debit_advance');
			}
			
		}
		$data = array();
		$this->_getlayoutbackend('admin/cashs/adddebit_advance',$data);
	}
	function _edit_debit_advance($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('stock_bill_issue_id', 'stock_bill_issue_id','trim|required|xss_clean');
			$this->form_validation->set_rules('sale_user_id', 'Sale user','trim|required|xss_clean');
            //$this->form_validation->set_rules('cash_amount', 'Cash amount','trim|required|decimal');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["cash_id"] = $this->input->post("cash_id");
				$arr["stock_bill_id"] = $this->input->post("stock_bill_issue_id");
				$arr["saler_id"] = $this->input->post("sale_user_id");
				
				$arr["cash_amount"] = str_replace(",","",$this->input->post("cash_amount"));
				$arr["cash_note"] = $this->input->post("cash_note");
				$arr["cash_modify_date  "] = date("Y-m-d H:i:s", now());
				$arr["type"] = 'discount';
				$arr["cash_follow"] = 0;
				
				$this->cashs_model->update($arr);
				
				$this->try_archive($this->input->post("stock_bill_issue_id"));

				success_message(__('update_cashs_discount_advance_successfully')); 
				redirect('admin/cashs/debit_advance');
			}
			
		}
		$data = array();
		$data["advance"] = $this->cashs_model->getInfo($id);
		$this->_getlayoutbackend('admin/cashs/editdebit_advance',$data);
	}
	function _delete_debit_advance($id)
	{
		$this->cashs_model->delete($id);
		success_message(__('delete_cashs_advance_successfully')); 
		redirect('admin/cashs/debit_advance');
	}	

}