<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dailyreport extends  MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model("cashs_model");
    }
    //add report discount
    function index(){
        $stock_bill_id = array();
        $cash = array();
        $discount = array();
        $data = array();
        $config["base_url"] = site_url('admin/stockissues/index');
        $data["stock_issues"] = $this->stock_bill_model->getStockIssuesDaily();
        /* $cash = $this->cashs_model->sumReceiptsByStockIssue();*/
        foreach($data["stock_issues"] as $key => $value){
            $stock_bill_id[$key] = $value["stock_bill_id"];
        }
        foreach($stock_bill_id as $key => $value){
            $cash = $this->cashs_model->sumReceiptsByStockIssue($value)["sum"];
            if(empty($cash)){
                $cash =0;
            }
            $discount = $this->cashs_model->sumDiscountsByStockIssue($value)["sum"];
            if(empty($discount)){
                $discount =0;
            }
            $debit = $this->cashs_model->sumDebitsByStockIssue($value)["sum"];
            if(empty($debit)){
                $debit =0;
            }
            $stock_return = $this->stock_bill_model-> sumStockReturnReceiptsByStockIssue($value)["sum"];
            if(empty($stock_return)){
                $stock_return =0;
            }
            $delivery_expenses = $this->cashs_model->sumDeliveryExpensesByStockIssue($value)["sum"];
            if(empty($delivery_expenses)){
                $delivery_expenses =0;
            }
            $total_return = $cash + $discount + $debit + $stock_return + $delivery_expenses;
            $data["stock_issues"][$key]["total_return"] = $total_return;
        }
        $data["sum"] = $this->stock_bill_model->sumStockIssues();
        $this->_getlayoutbackend('admin/dailyreport/index',$data);
    }

}