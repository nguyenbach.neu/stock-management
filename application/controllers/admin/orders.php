<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once '/var/www/huongmai/application/libraries/mpdf/mpdf.php';

class Orders extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
        $this->load->model('orders_model');

    }

    //jaslfj
	public function index()
	{
        if ($_POST){
            $this->add_stock_issue();
        }

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/orders/index');
        $config["total_rows"] = $this->orders_model->countOrders();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["orders"] = $this->orders_model->getOrders($page,$per_page);

		$this->_getlayoutbackend('admin/orders/index',$data);
	}

	public function actions()
    {
        $data = $_POST;
        switch ($data['action']) {
            case 'general_issue':

            case 'stock_issue':
                break;
            case 'print_order':
                $this->printOrder($this->getOrderSelected($data));
        }

    }

    protected function getOrderSelected($data) {
        $result = array();
        for ($i=0; $i < count($data); $i++) {
            if (isset($data['order_selected_'.$i])) {
                $result[] = $data['order_selected_'.$i];
            }
        }
        return $result;
    }

    protected function printOrder($ids)
    {
        $mpdf = new mPDF('', 'A5');
        foreach($ids as $id) {
            $data = $this->prepareOrderData($id);
            $content = $this->load->view('admin/orders/bill',$data,true);
            $mpdf->WriteHTML($content);
            $mpdf->AddPage();
        }
        $mpdf->DeletePages($mpdf->page);
        return $mpdf->Output('/var/www/huongmai/test.pdf', 'I');
    }

    protected function prepareOrderData($id)
    {
        $products = $this->orders_model->getInfoProducts($id);
        foreach($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['order_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity']= $product['order_product_quantity'] % $product['package_unit'];
        }

        $data = array();
        $data["id"] = $id;
        $data["order"] = $this->orders_model->getInfo($id);
        $data["products"] = $products;
        return $data;
    }
	//jalkfjalkjf
	function add()
    {
        $data = array();
        if ($_POST) {
            $this->form_validation->set_rules('sale_user_id', 'sale_user_id', 'trim|required|xss_clean');
            // $this->form_validation->set_rules('product_quantity_0', 'product_quantity_0','trim|required|numeric');
            // $this->form_validation->set_rules('product_id_0', 'product_quantity_0','trim|required|numeric');
            if ($this->form_validation->run()) {
                if ($this->input->post("order_status") >= 0) {
                    if ($this->input->post("total_item") > 0) {

                        //$error_quantity = "";
                        for ($i = 0; $i < $this->input->post("total_item"); $i++) {
                            $item = $this->products_model->getInfo($this->input->post("product_id_" . $i));
                            $_POST["product_quantity_" . $i] = $item["package_unit"] * $this->input->post("package_quantity_" . $i) + $this->input->post("unit_quantity_" . $i);
                        }
                        if (!$this->checkQuantity($this->input->post())) {
                            $error_quantity[] = __("error_order_overload_quantity");
                            //$this->form_validation->_error_array[] = $error_quantity;
                            //$data["posts"] = $this->input->post();
                            //$this->_getlayoutbackend('admin/orders/add', $data, "backend");
                        }
                        $arr = array();
                        $arr["order_object_id"] = $this->input->post("sale_user_id");
                        $arr["order_note"] = $this->input->post("order_note");
                        $arr["order_status"] = $this->input->post("order_status");
                        $arr["order_date"] = $this->input->post("created_date");
                        $arr["order_created_date"] = date("Y-m-d H:i:s", now());
                        $arr["order_create_user_id"] = $this->session->userdata('user_id');

                        $arrDetail = array();

                        if (empty($error_quantity)) {
                            $order_id = $this->orders_model->insert($arr);
                            for ($i = 0; $i < $this->input->post("total_item"); $i++) {
                                $item = $this->products_model->getInfo($this->input->post("product_id_" . $i));

                                $product_quantity = $_POST["package_quantity_" . $i] * $item["package_unit"] + $_POST["unit_quantity_" . $i];
                                //array value stock_issue_products
                                $arrItem = array();
                                $arrItem["product_id"] = $this->input->post("product_id_" . $i);

                                $arrItem["order_id"] = $order_id;
                                $arrItem["order_product_code"] = $item["product_code"];
                                $arrItem["order_product_name"] = $item["product_name"];

                                $arrItem["order_product_price"] = $item['product_price'];
                                $arrItem["order_product_quantity"] = $product_quantity;
                                $arrDetail[] = $arrItem;
                            }
                            //insert batch stock_issue_products
                            if (count($arrDetail) > 0) {
                                $this->orders_model->insert_products($arrDetail);
                            }
                            success_message(__('insert_order_successfully'));
                            redirect('admin/orders');
                        } else {
                            foreach($error_quantity as $error)
                            {
                                //var_dump($error);
                                $this->form_validation->_error_array[] = $error;
                            }
                            $data["posts"] = $this->input->post();
                            $this->_getlayoutbackend('admin/orders/add', $data, "backend");
                        }
                    }
                }
            }
        }
        $this->_getlayoutbackend('admin/orders/add', $data, "backend");
    }

	function edit($id)
	{
		$data = array();
		$data["order"] = $this->orders_model->getInfo($id);

		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$data["order"]["order_status"]);
		if($_POST) {
            $error_quantity = array();
            for($i = 0; $i < $this->input->post("total_item"); $i++) {
                $_POST['package_quantity_' . $i] = str_replace(",", "", $_POST['package_quantity_' . $i]);
                $_POST['unit_quantity_' . $i] = str_replace(",", "", $_POST['unit_quantity_' . $i]);
                $product = $this->products_model->getInfo($this->input->post("product_id_" . $i));
                $_POST['product_quantity_' . $i] = $_POST['package_quantity_' . $i] * $product['package_unit'] + $_POST['unit_quantity_' . $i];
                if ($_POST['product_quantity_' . $i] > $product['product_stock_quantity']) {
                    $error_quantity[] = "Số  lượng sản phẩm ".$product['product_name']." không đủ";
                }
            }
            if ($this->input->post("order_id") > 0 && $this->input->post("total_item") > 0) {
                for ($i = 0; $i < $this->input->post("total_item"); $i++) {
                    $item = $this->products_model->getInfo($this->input->post("product_id_" . $i));
                    $_POST["product_quantity_" . $i] = $item["package_unit"] * $this->input->post("package_quantity_" . $i) + $this->input->post("unit_quantity_" . $i);
                }
                // var_dump($this->checkQuantity($this->input->post()));die;
                if (!$this->checkQuantity($this->input->post())) {
                    $error_quantity[] = __("error_order_overload_quantity");
                    //$this->form_validation->_error_array[] = $error_quantity;
                }
                $order = $this->orders_model->getInfo($this->input->post("order_id"));
                $order_products = $this->orders_model->getInfoProducts($this->input->post("order_id"));
                //check is_backup_history
                $flag = false;
                if ($this->input->post("curr_order_status") == 1) {
                    if ($this->input->post("total_item") != count($order_products)) {
                        $flag = $flag || true;
                    }
                    if ($this->input->post("sale_user_id") != $order["order_object_id"]) {
                        $flag = $flag || true;
                    }
                    for ($i = 0; $i < $this->input->post("total_item"); $i++) {
                        if ($this->input->post("order_product_id_" . $i)) {
                            $test_product = $this->orders_model->getInfoProduct($this->input->post("order_product_id_" . $i));
                            if ($test_product["order_product_quantity"] != (float)str_replace(",", "", $this->input->post("product_quantity_" . $i))) {
                                $flag = $flag || true;
                            }
                        }
                    }
                }
                if (empty($error_quantity)) {
                    $arr = array();
                    $arr["order_id"] = $this->input->post("order_id");
                    $arr["order_object_id"] = $this->input->post("sale_user_id");
                    $arr["order_note"] = $this->input->post("order_note");
                    $arr["order_status"] = $this->input->post("order_status");
                    $arr["order_date"] = $this->input->post("created_date");
                    $arr["order_modified_date"] = date("Y-m-d H:i:s", now());
                    $this->orders_model->update($arr);
                    $arrProdOld = array();
                    $arr1 = $this->orders_model->getInfoProducts($this->input->post("order_id"), "order_product_id");
                    foreach ($arr1 as $item) {
                        $arrProdOld[] = $item["order_product_id"];
                    }
                    if ($this->input->post("total_item") > 0) {
                        $arrProdNew = array();
                        for ($i = 0; $i < $this->input->post("total_item"); $i++) {
                            $item = $this->products_model->getInfo($this->input->post("product_id_" . $i));
                            $arrItem = array();
                            $arrItem["product_id"] = $this->input->post("product_id_" . $i);
                            $arrItem["order_id"] = $this->input->post("order_id");
                            $arrItem["order_product_code"] = $this->input->post("product_code_" . $i);
                            $arrItem["order_product_name"] = $this->input->post("product_name_" . $i);
                            $arrItem["order_product_price"] = $item["product_price"];
                            $arrItem["order_product_quantity"] = str_replace(",", "", $this->input->post("product_quantity_" . $i));


                            if ($this->input->post("order_product_id_" . $i)) {
                                $arrProdNew[] = $this->input->post("order_product_id_" . $i);
                                $arrItem["order_product_id"] = $this->input->post("order_product_id_" . $i);
                                $this->orders_model->update_product($arrItem);
                            } else {
                                $this->orders_model->insert_product($arrItem);
                            }
                        }
                        $result = array_diff($arrProdOld, $arrProdNew);
                        if (count($result) > 0 && count($arrProdOld) > 0) {
                            foreach ($result as $item) {
                                $this->orders_model->delete_product($item);
                            }
                        }


                    }
                    success_message(__('update_order_successfully'));
                    redirect('admin/orders');
                } else {
                    foreach($error_quantity as $error) {
                        $this->form_validation->_error_array[] = $error;
                    }
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/orders/add', $data, "backend");
                }
            }
        }

        $products = $this->orders_model->getInfoProducts($id);

        foreach($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['order_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity']= $product['order_product_quantity'] % $product['package_unit'];
        }
        $data["order"] = $this->orders_model->getInfo($id);
        $data["order"]["products"] = $products;
        $data["order"]["order_id"] = $id;
		$this->_getlayoutbackend('admin/orders/edit',$data,"backend");
	}
	function add_stock_issue(){
        $data = array();

        if($_POST) {
            $total_order = count($_POST) - 1;

            $this->form_validation->set_rules('sale_user_id', 'sale_user_id', 'trim|required|xss_clean');
            // $this->form_validation->set_rules('product_quantity_0', 'product_quantity_0','trim|required|numeric');
            // $this->form_validation->set_rules('product_id_0', 'product_quantity_0','trim|required|numeric');
            if ($this->form_validation->run()) {

                if ($this->input->post("stock_issue_status") >= 0) {
                    if ($total_order > 0) {
                        $result = array();
                        $var = 0;
                        foreach ($_POST as $value) {
                            $result[$var] = $value;
                            $var++;
                        }

                        $products = array();

                        for ($i = 1; $i <= $total_order; $i++) {
                            $order_products = $this->orders_model->getInfoProducts($result[$i]);
                            foreach ($order_products as $product) {
                                if (!empty($products[$product['product_id']])) {
                                    $products[$product['product_id']]['order_product_quantity'] += $product['order_product_quantity'];
                                } else {
                                    $products[$product['product_id']] = $product;
                                }
                            }
                        }
                        $error_quantity="";
                        $product_name = '';
                        foreach ($products as $product) {
                            $item = $this->products_model->getInfo($product["product_id"]);

                            $product_quantity = $product["order_product_quantity"];
                            if ($product_quantity > $item["product_stock_quantity"]) {
                                $error = 1;
                                $product_name .= ' '.$product["order_product_name"].',';
                            }
                        }
                        if(!empty($error)){
                            $error_quantity = __("error_stock_issue_overload_quantity ") .$product_name;
                            $this->form_validation->_error_array[] = $error_quantity;
                            $this->_getlayoutbackend('admin/orders/index', $data, "backend");
                        }
                        $arr = array();
                        $arr["stock_bill_object_id"] = $this->input->post("sale_user_id");
                        $arr["stock_bill_note"] = 0;
                        $arr["stock_bill_status"] = 0;
                        $arr["stock_bill_created_date"] = date("Y-m-d H:i:s", now());
                        $arr["stock_bill_create_user_id"] = $this->session->userdata('user_id');
                        $arr["stock_bill_flow"] = OUT;
                        $arrDetail = array();
                        $arrHistory = array();
                        if (empty($error_quantity)) {
                            $stock_issue_id = $this->stock_bill_model->insert($arr);
                            foreach ($products as $product) {
                                $item = $this->products_model->getInfo($product["product_id"]);
                                $product_quantity = $product["order_product_quantity"];
                                //array value stock_issue_products
                                $arrItem = array();
                                $arrItem["product_id"] = $product["product_id"];
                                $arrItem["stock_bill_product_code"] = $product["order_product_code"];
                                $arrItem["stock_bill_product_name"] = $product["order_product_name"];
                                $arrItem["stock_bill_product_price"] = $item['product_price'];
                                $arrItem["stock_bill_product_quantity"] = $product["order_product_quantity"];
                                $arrItem["stock_bill_product_discount"] = $item["product_discount"];
                                $arrItem["stock_bill_product_cost"] = $item["product_cost"];
                                $arrItem["stock_bill_id"] = $stock_issue_id;
                                $arrDetail[] = $arrItem;
                            }
                            //insert batch stock_issue_products
                            if (count($arrDetail) > 0) {
                                $this->stock_bill_model->insert_products($arrDetail);
                            }
                            //insert batch product_stock_history
                            if (count($arrHistory) > 0) {
                                // $this->stock_bill_model->insert_histories($arrHistory);
                            }
                            foreach($result as $id){
                                $array["order_id"] = $id;
                                $array["order_status"] = 1;
                                $this->orders_model->update($array);
                            }
                            success_message(__('insert_stock_issue_successfully'));
                            redirect('admin/stockissues');
                        }
                    }
                }
            }
        }
        $this->_getlayoutbackend('admin/orders/index',$data,"backend");
    }
	// view product in stock issue
	function view($id = 0)
	{
	    $products = $this->orders_model->getInfoProducts($id);
        foreach($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['order_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity']= $product['order_product_quantity'] % $product['package_unit'];
        }

		$data = array();
		$data["id"] = $id;
        $data["order"] = $this->orders_model->getInfo($id);
		$data["products"] = $products;
        $content = $this->load->view('admin/orders/view',$data,true);
        die($content);
		$content = $this->_getlayoutbackend('admin/orders/view',$data,"backend");
	}

	
	function delete($id)
	{
		$order = $this->orders_model->getInfo($id);
		$lock = false;
		if(isset($order["order_status"])){
			if($order["order_status"] == STOCK_CHANGE_STATUS_PROCESSED){
				$lock = true;
			}
		}
		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$lock);
		

		$this->orders_model->delete($id);
		//$this->orders_model->delete_products($id);

		success_message(__('delete_order_successfully'));
		redirect('admin/orders');
	}


    function checkQuantity($post = false,$type = "normal")
    {
        $flag = true;
        if($type == "normal"){
            $tmp = array();
            $unique = array();
            for($i= 0; $i < $this->input->post("total_item"); $i++){
                    $product_id = $this->input->post("product_id_".$i);
                    $product_quantity = (float)str_replace(",","",$this->input->post("product_quantity_".$i));
                    if (!in_array($product_id, $tmp)) {
                        $tmp[] = $product_id;
                        $unique[$product_id]["id"] = $product_id;
                        $unique[$product_id]["quantity"] = $product_quantity;
                    }
                    else{
                        $unique[$product_id]["quantity"] += $product_quantity;
                    }
            }

            if(count($unique) > 0){

                foreach($unique as $k => $item){
                    $product = $this->products_model->getInfo($item["id"]);
                    if($item["quantity"] > $product["product_stock_quantity"]){
                        $flag = false;
                    }
                }
            }
            else{
                $flag = true;
            }
        }
        else if($type == "process_view"){
            $stock = $this->orders_model->getInfo($post);
            $products = $this->orders_model->getInfoProducts($post);

            if(count($products) >0 && $products){
                $tmp = array();
                $unique = array();
                for($i=0; $i< count($products); $i++){
                    $product_id = $products[$i]["product_id"];
                    $product_quantity = $products[$i]["order_product_quantity"];
                    if (!in_array($product_id, $tmp)) {
                        $tmp[] = $product_id;
                        $unique[$product_id]["id"] = $product_id;
                        $unique[$product_id]["quantity"] = $product_quantity;
                    }
                    else{
                        $unique[$product_id]["quantity"] += $product_quantity;
                    }
                }
                if(count($unique) > 0){

                    foreach($unique as $k => $item){
                        $product = $this->products_model->getInfo($item["id"]);
                        if($item["quantity"] > $product["product_stock_quantity"]){
                            $flag = false;
                        }
                    }
                }
                else{
                    $flag = true;
                }
            }
            else {
                $flag = false;
            }
        }
        return $flag;
    }

    function printOrders()
    {
        $ids = $_POST;
        $order = $this->orders_model->getInfo();
        $mpdf = new mPDF('', 'A5');
    }

}
