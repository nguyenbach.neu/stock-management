<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dashboard extends  MY_Controller {
	function __construct()
    {
      parent::__construct();
	  $this->load->model('cashs_model');
    }
	public function index()
	{
		$data = array();
		$data["newIssues"] = $this->stock_bill_model->getLatestNewIssues();
		$data["newReceipts"] = $this->stock_bill_model->getLatestNewReceipts();
		$data["lastIssues"] = $this->stock_bill_model->getLastestModifiedIssues();
		$data["lastReceipts"] = $this->stock_bill_model->getLastestModifiedReceipts();
		$data["lastAdjustedStocks"] = $this->products_model->getLastestAdjustedStocks();
		$data["lastAdjustedPrices"] = $this->products_model->getLastestAdjustedPrices();
		$data["lastCashReceipts"] = $this->cashs_model->getLastestCashReceipts();
		$data["lastCashModified"] = $this->cashs_model->getLastestCashModified();
		$this->_getlayoutbackend('admin/dashboard/index',$data);
	}
	function logout()
	{
		$data = $this->session->userdata;
        $this->session->unset_userdata($data);
		 redirect('admin/dashboard/login');
	}
	function login()
	{
		if($_POST) {
			$this->form_validation->set_rules('username', 'username',
				'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'password',
                'trim|required|xss_clean');
				
			if($this->form_validation->run()) {
				$username = trim($this->input->post('username'));
                $password = sha1(trim($this->input->post('password')));
				
				if ($this->users_model->getLoginAdmin($username, $password)) {
                    $result = $this->users_model->getLoginAdmin($username, $password);
					$this->session->set_userdata($result);		
					$categories = $this->categories_model->getAll("cate_name","ASC");
					success_message('Login successfully!!!'); 
                    redirect('admin/categories/views/'.$categories[0]["cate_id"]);
                }
                $this->form_validation->_error_array[] = __('Email or Password is incorrect');

			}			
		}
		$this->_getlayoutbackend('admin/login',"","backend");
	}

}
?>