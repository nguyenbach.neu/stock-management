<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stockissues extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
    }
	public function index()
	{

        $stock_bill_id = array();
        $cash = array();
        $discount = array();
		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/stockissues/index');
        $config["total_rows"] = $this->stock_bill_model->countStockIssues();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["stock_issues"] = $this->stock_bill_model->getStockIssues($page,$per_page);
       /* $cash = $this->cashs_model->sumReceiptsByStockIssue();*/
        foreach($data["stock_issues"] as $key => $value){
            $stock_bill_id[$key] = $value["stock_bill_id"];
        }
        foreach($stock_bill_id as $key => $value){
            $cash = $this->cashs_model->sumReceiptsByStockIssue($value)["sum"];
            if(empty($cash)){
                $cash =0;
            }
            $discount = $this->cashs_model->sumDiscountsByStockIssue($value)["sum"];
            if(empty($discount)){
                $discount =0;
            }
            $debit = $this->cashs_model->sumDebitsByStockIssue($value)["sum"];
            if(empty($debit)){
                $debit =0;
            }
            $stock_return = $this->stock_bill_model-> sumStockReturnReceiptsByStockIssue($value)["sum"];
            if(empty($stock_return)){
                $stock_return =0;
            }
            $delivery_expenses = $this->cashs_model->sumDeliveryExpensesByStockIssue($value)["sum"];
            if(empty($delivery_expenses)){
                $delivery_expenses =0;
            }
            $total_return = $cash + $discount + $debit + $stock_return + $delivery_expenses;
            $data["stock_issues"][$key]["total_return"] = $total_return;
        }
		$data["sum"] = $this->stock_bill_model->sumStockIssues();
		$this->_getlayoutbackend('admin/stockissues/index',$data);
	}
	function add()
	{
		$data = array();
		if($_POST) {
			$error_quantity = array();
            for($i = 0; $i < $this->input->post("total_item"); $i++) {
                $_POST['package_quantity_' . $i] = str_replace(",", "", $_POST['package_quantity_' . $i]);
                $_POST['unit_quantity_' . $i] = str_replace(",", "", $_POST['unit_quantity_' . $i]);
                $product = $this->products_model->getInfo($this->input->post("product_id_" . $i));
				$_POST['product_quantity_' . $i] = $_POST['package_quantity_' . $i] * $product['package_unit'] + $_POST['unit_quantity_' . $i];
				if ($_POST['product_quantity_' . $i] > $product['product_stock_quantity']) {
                    $error_quantity[] = "Số  lượng sản phẩm ".$product['product_name']." không đủ";
                }
            }
            for($i = 0; $i < $this->input->post("total_item"); $i++){
                if(empty($this->input->post("product_code_".$i))){
                    $error_quantity[] =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
                if (empty($this->input->post("product_name_".$i))){
                    $error_quantity[] =  __("error_js_function");
                    $this->form_validation->_error_array[] = $error_quantity;
                    $data["posts"] = $this->input->post();
                    $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
            }

			$this->form_validation->set_rules('sale_user_id', 'sale_user_id','trim|required|xss_clean');
            // $this->form_validation->set_rules('product_quantity_0', 'product_quantity_0','trim|required|numeric');
            // $this->form_validation->set_rules('product_id_0', 'product_quantity_0','trim|required|numeric');
			if($this->form_validation->run()) {
				if($this->input->post("stock_issue_status") >= 0 ){
					if($this->input->post("total_item") > 0){

                        for ($i = 0; $i < $this->input->post("total_item"); $i++){
                            $item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                            $_POST["product_quantity_".$i] = $item["package_unit"]* $this->input->post("package_quantity_".$i) + $this->input->post("unit_quantity_".$i);
                        }

						if(!$this->checkQuantity($this->input->post())){
							if($this->input->post("stock_issue_status") == 1 ){
								$error_quantity[] =  __("error_stock_issue_overload_quantity");
								//$this->form_validation->_error_array[] = $error_quantity;
								//$data["posts"] = $this->input->post();
								//$this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
							}
						}
						$arr = array();
						$arr["stock_bill_object_id"] = $this->input->post("sale_user_id");
						$arr["stock_bill_note"] = $this->input->post("stock_issue_note");
						$arr["stock_bill_status"] = $this->input->post("stock_issue_status");
						$arr["stock_bill_date"] = $this->input->post("created_date");
                        $arr["stock_bill_created_date"] = date("Y-m-d H:i:s", now());
						$arr["stock_bill_create_user_id"] = $this->session->userdata('user_id');
						$arr["stock_bill_flow"] = OUT;
						$arrDetail = array();
						$arrHistory = array();		
						if(empty($error_quantity)){
							if($this->input->post("stock_issue_status") == 1 ){
								if(empty($error_quantity)){
									$stock_issue_id = $this->stock_bill_model->insert($arr);
									for($i = 0; $i < $this->input->post("total_item"); $i++){
										$item = $this->products_model->getInfo($this->input->post("product_id_".$i));

                                        $product_quantity = $_POST["package_quantity_".$i]*$item["package_unit"] + $_POST["unit_quantity_".$i];
										if($product_quantity > $item["product_stock_quantity"]){
											$error_quantity =  __("error_stock_issue_overload_quantity");
											$this->form_validation->_error_array[] = $error_quantity;
											break;
										}
										//array value stock_issue_products
										$arrItem = array();
										$arrItem["product_id"] = $this->input->post("product_id_".$i);
										$arrItem["stock_bill_product_code"] = $this->input->post("product_code_".$i);
										$arrItem["stock_bill_product_name"] = $this->input->post("product_name_".$i);
										$arrItem["stock_bill_product_price"] = $item['product_price'];
                                        $arrItem["stock_bill_product_cost"] = $item['product_cost'];
                                        $arrItem["stock_bill_product_discount"] = $item['product_discount'];
										$arrItem["stock_bill_product_quantity"] = $product_quantity;
										$arrItem["stock_bill_id"] = $stock_issue_id;
										$arrDetail[] = $arrItem;
										//array product_stock_history
										$pr_st_his = array();
										$pr_st_his["product_id"] = $item["product_id"];
										$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
										$pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
										$pr_st_his["new_product_stock_quantity"] = ($item["product_stock_quantity"] - $product_quantity);
										$pr_st_his["reason"] = STOCK_CHANGE_REASON_ISSUE;
										$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
										$pr_st_his["linked_object_id"] = $stock_issue_id;
										$this->stock_bill_model->insert_stock_history($pr_st_his);
										$this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
									}
									//insert batch stock_issue_products
									if(count($arrDetail) >0){
										$this->stock_bill_model->insert_products($arrDetail);
									}
									//insert batch product_stock_history
									if(count($arrHistory) >0){
										// $this->stock_bill_model->insert_histories($arrHistory);
									}
									success_message(__('insert_stock_issue_successfully')); 
									redirect('admin/stockissues');
								}
							} else {
								$stock_issue_id = $this->stock_bill_model->insert($arr);
								
								for($i = 0; $i < $this->input->post("total_item"); $i++){
									$item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                                    $product_quantity = $_POST["package_quantity_".$i]*$item["package_unit"] + $_POST["unit_quantity_".$i];
									$arrItem = array();
									$arrItem["product_id"] = $this->input->post("product_id_".$i);
									$arrItem["stock_bill_id"] = $stock_issue_id;
									$arrItem["stock_bill_product_code"] = $this->input->post("product_code_".$i);
									$arrItem["stock_bill_product_name"] = $this->input->post("product_name_".$i);
									$arrItem["stock_bill_product_price"] = $item['product_price'];
                                    $arrItem["stock_bill_product_cost"] = $item['product_cost'];
                                    $arrItem["stock_bill_product_discount"] = $item['product_discount'];
									$arrItem["stock_bill_product_quantity"] = $product_quantity;
									$arrDetail[] = $arrItem;
								}
								$this->stock_bill_model->insert_products($arrDetail);
								success_message(__('insert_stock_issue_successfully')); 
								redirect('admin/stockissues');
							}
						}else{
							foreach($error_quantity as $error) {
								$this->form_validation->_error_array[] = $error;
							}
							$data["posts"] = $this->input->post();
							$this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
						}
					}
				}
			}
		}
		$this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
	}
	function edit($id)
	{
		$data = array();
		$data["stock_issue"] = $this->stock_bill_model->getInfo($id);
		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$data["stock_issue"]["stock_bill_status"]);

		if($_POST) {
			$error_quantity = array();
            for($i = 0; $i < $this->input->post("total_item"); $i++) {
                $_POST['package_quantity_' . $i] = str_replace(",", "", $_POST['package_quantity_' . $i]);
                $_POST['unit_quantity_' . $i] = str_replace(",", "", $_POST['unit_quantity_' . $i]);
                $product = $this->products_model->getInfo($this->input->post("product_id_" . $i));
				$_POST['product_quantity_' . $i] = $_POST['package_quantity_' . $i] * $product['package_unit'] + $_POST['unit_quantity_' . $i];
				if ($_POST['product_quantity_' . $i] > $product['product_stock_quantity']) {
                    $error_quantity[] = "Số  lượng sản phẩm ".$product['product_name']." không đủ";
                }
            }
            for($i = 0; $i < $this->input->post("total_item"); $i++){
                if(empty($this->input->post("product_code_".$i))){
                    $error_quantity[] =  __("error_js_function");
                    // $this->form_validation->_error_array[] = $error_quantity;
                    // $data["posts"] = $this->input->post();
                    // $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
                if (empty($this->input->post("product_name_".$i))){
                    $error_quantity[] =  __("error_js_function");
                    // $this->form_validation->_error_array[] = $error_quantity;
                    // $data["posts"] = $this->input->post();
                    // $this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
                }
            }

			if($this->input->post("stock_issue_id") > 0 && $this->input->post("total_item") > 0){
				
				// var_dump($this->checkQuantity($this->input->post()));die;
                for ($i = 0; $i < $this->input->post("total_item"); $i++){
                    $item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                    $_POST["product_quantity_".$i] = $item["package_unit"]* $this->input->post("package_quantity_".$i) + $this->input->post("unit_quantity_".$i);
                }

				if(!$this->checkQuantity($this->input->post())){
					if($this->input->post("stock_issue_status") == 1 ){
						$error_quantity[] =  __("error_stock_issue_overload_quantity");
						//$this->form_validation->_error_array[] = $error_quantity;	
					}
				}
				$stock_issue = $this->stock_bill_model->getInfo($this->input->post("stock_issue_id"));
				$stock_bill_products = $this->stock_bill_model->getInfoProducts($this->input->post("stock_issue_id"));

				//check is_backup_history
				$flag = false;
				if($this->input->post("curr_stock_issue_status") == 1){
					if($this->input->post("total_item") != count($stock_bill_products)){
						$flag = $flag || true;
					}
					if($this->input->post("sale_user_id") != $stock_issue["stock_bill_object_id"]){
						$flag = $flag || true;
					}
					for($i = 0; $i < $this->input->post("total_item"); $i++){
						if($this->input->post("stock_issue_product_id_".$i)){
                            $item = $this->products_model->getInfo($this->input->post("product_id_".$i));
                            $product_quantity = $_POST['package_quantity_'.$i]*$item['package_unit'] + $_POST['unit_quantity_'.$i];
							$test_product = $this->stock_bill_model->getInfoProduct($this->input->post("stock_issue_product_id_".$i));
							if($test_product["stock_bill_product_quantity"] != $product_quantity){
								$flag = $flag || true;
							}
						}
					}
				}
				if(empty($error_quantity)){
					//add history
					if($flag){
						$array1 = array();
						$array1["stock_bill_id"] = $stock_issue["stock_bill_id"];
						$array1["stock_bill_history_object_id"] = $stock_issue["stock_bill_object_id"];
						$array1["stock_bill_history_create_user_id"] = $this->session->userdata('user_id');
						$array1["stock_bill_history_note"] = $stock_issue["stock_bill_note"];
						$array1["stock_bill_history_status"] = $stock_issue["stock_bill_status"];
						$array1["stock_bill_history_type"] = $stock_issue["stock_bill_type"];
						$array1["stock_bill_history_created_date"] = date("Y-m-d H:i:s", now());;
						$array1["stock_bill_history_message"] = STOCK_ISSUE_HISTORY_TYPE_CHANGE_STOCK;
						$array1["stock_bill_history_flow"] = $stock_issue["stock_bill_flow"];
						$value1 = $this->stock_bill_model->insert_history($array1);
						if(count($stock_bill_products) >0 ){
							foreach($stock_bill_products as $k => $item){
								$array2["stock_bill_history_id"] = $value1;
								$array2["stock_bill_product_history_name"] = $item["stock_bill_product_name"];
								$array2["stock_bill_product_history_code"] = $item["stock_bill_product_code"];
								$array2["stock_bill_product_history_price"] = $item["stock_bill_product_price"];
                                $array2["stock_bill_product_history_cost"] = $item["stock_bill_product_cost"];
                                $array2["stock_bill_product_history_discount"] = $item["stock_bill_product_discount"];
								$array2["stock_bill_product_history_quantity"] = $item["stock_bill_product_quantity"];
								$array2["product_id"] = $item["product_id"];
								// print_r($array2);
								$this->stock_bill_model->insert_product_history($array2);
							}
						}
					}
				
					$arr = array();
					$arr["stock_bill_id"] = $this->input->post("stock_issue_id");
					$arr["stock_bill_object_id"] = $this->input->post("sale_user_id");
					$arr["stock_bill_note"] = $this->input->post("stock_issue_note");
					$arr["stock_bill_status"] = $this->input->post("stock_issue_status");
                    $arr["stock_bill_date"] = $this->input->post("created_date");
					$arr["stock_bill_modified_date"] = date("Y-m-d H:i:s", now());
					if($this->input->post("curr_stock_issue_status") == 1){
						if($stock_issue["stock_bill_object_id"] != $this->input->post("sale_user_id")){
							
						}
					}
					$this->stock_bill_model->update($arr);
					$arrProdOld = array();
					$arr1 = $this->stock_bill_model->getInfoProducts($this->input->post("stock_issue_id"),"stock_bill_product_id");
					foreach($arr1 as $item){
						$arrProdOld[] = $item["stock_bill_product_id"];
					}
					if($this->input->post("total_item") > 0){
						$arrProdNew = array();
						for($i = 0; $i < $this->input->post("total_item"); $i++){
							$item = $this->products_model->getInfo($this->input->post("product_id_".$i));
							$item_product = array();
							if($this->input->post('stock_issue_product_id_'.$i)){
								$item_product = $this->stock_bill_model->getInfoProduct($this->input->post('stock_issue_product_id_'.$i));
							}
							$product_quantity = $_POST['package_quantity_'.$i]*$item['package_unit'] + $_POST['unit_quantity_'.$i];
							$arrItem = array();
							$arrItem["product_id"] = $this->input->post("product_id_".$i);
							$arrItem["stock_bill_id"] = $this->input->post("stock_issue_id");
							$arrItem["stock_bill_product_code"] = $this->input->post("product_code_".$i);
							$arrItem["stock_bill_product_name"] = $this->input->post("product_name_".$i);
							$arrItem["stock_bill_product_price"] = ((isset($item_product['stock_bill_product_price'])) ? $item_product['stock_bill_product_price'] : $item['product_price']);
                            $arrItem["stock_bill_product_cost"] = ((isset($item_product['stock_bill_product_cost'])) ? $item_product['stock_bill_product_cost'] : $item['product_cost']);
                            $arrItem["stock_bill_product_discount"] = ((isset($item_product['stock_bill_product_discount'])) ? $item_product['stock_bill_product_discount'] : $item['product_discount']);
							$arrItem["stock_bill_product_quantity"] = $product_quantity;
							if($this->input->post("stock_issue_status") == 1){
								
								if($this->input->post("curr_stock_issue_status") == 1){
									$issueProdId = $this->input->post("stock_issue_product_id_".$i);
									if(in_array($issueProdId,$arrProdOld)){
										$itemIssueProd = $this->stock_bill_model->getInfoProduct($issueProdId);
										if($itemIssueProd["stock_bill_product_quantity"] != $product_quantity){
											
											$product_stock_quantity = $item["product_stock_quantity"] + $itemIssueProd["stock_bill_product_quantity"] - $product_quantity;
											
											// update quantity product
											$this->products_model->update(array("product_id"=>$item["product_id"],"product_stock_quantity"=>$product_stock_quantity));
											//insert to table stock_issue_history
											$product_history["stock_bill_history_id"] = $itemIssueProd["stock_bill_id"];
											$product_history["stock_bill_product_history_name"] = $itemIssueProd["stock_bill_product_name"];
											$product_history["stock_bill_product_history_code"] = $itemIssueProd["stock_bill_product_code"];
											$product_history["stock_bill_product_history_price"] = $itemIssueProd["stock_bill_product_price"];
                                            $product_history["stock_bill_product_history_cost"] = $itemIssueProd["stock_bill_product_cost"];
                                            $product_history["stock_bill_product_history_discount"] = $itemIssueProd["stock_bill_product_discount"];
											$product_history["stock_bill_product_history_quantity"] = $itemIssueProd["stock_bill_product_quantity"];
											$product_history["product_id"] = $itemIssueProd["product_id"];
											$product_history_id = $this->stock_bill_model->insert_product_history($product_history);
											
											
											// product_stock_history
											$product_stock_history["product_id"] = $item["product_id"];
											$product_stock_history["modified_user_id"] = $this->session->userdata('user_id');
											$product_stock_history["previous_product_stock_quantity"] = $item["product_stock_quantity"];
											$product_stock_history["new_product_stock_quantity"] = $product_stock_quantity;
											$product_stock_history["reason"] = STOCK_CHANGE_REASON_ISSUE_CHANGE;
											$product_stock_history["linked_object_id"] = $this->input->post("stock_issue_id");
											$product_stock_history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
											$this->stock_bill_model->insert_stock_history($product_stock_history);
										}
									}
								}
								else{

									$pr_st_his["product_id"] = $item["product_id"];
									$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
									$pr_st_his["previous_product_stock_quantity"] = $item["product_stock_quantity"];
									$pr_st_his["new_product_stock_quantity"] = ($item["product_stock_quantity"] - $product_quantity);
									$pr_st_his["reason"] = STOCK_CHANGE_REASON_ISSUE;
									$pr_st_his["linked_object_id"] = $this->input->post("stock_issue_id");
									$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
									$this->products_model->update(array("product_id"=>$item["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
									$this->stock_bill_model->insert_stock_history($pr_st_his);
								}
							}
							
							if($this->input->post("stock_issue_product_id_".$i)){
								$arrProdNew[] = $this->input->post("stock_issue_product_id_".$i);
								$arrItem["stock_bill_product_id"] = $this->input->post("stock_issue_product_id_".$i);
								$this->stock_bill_model->update_product($arrItem);
							}
							else{
								$issueProdId = $this->stock_bill_model->insert_product($arrItem);
								if($this->input->post("curr_stock_issue_status") == 1){
									$product_stock_quantity = $item["product_stock_quantity"] - $product_quantity;
									// product_stock_history
									$product_stock_history["product_id"] = $item["product_id"];
									$product_stock_history["modified_user_id"] = $this->session->userdata('user_id');
									$product_stock_history["previous_product_stock_quantity"] = $item["product_stock_quantity"];
									$product_stock_history["new_product_stock_quantity"] = $product_stock_quantity;
									$product_stock_history["reason"] = STOCK_CHANGE_REASON_ISSUE_CHANGE;
									$product_stock_history["linked_object_id"] = $this->input->post("stock_issue_id");
									$product_stock_history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
									$this->stock_bill_model->insert_stock_history($product_stock_history);
									// update quantity product
									$this->products_model->update(array("product_id"=>$item["product_id"],"product_stock_quantity"=>$product_stock_quantity));
									
								}
							}
						}
						
						$result = array();
						$result = array_diff($arrProdOld, $arrProdNew);
						if(count($result) > 0 && count($arrProdOld) > 0 ){
							foreach($result as $item){
								if($this->input->post("curr_stock_issue_status") == 1){
									$stock_product = $this->stock_bill_model->getInfoProduct($item);
									$product = $this->products_model->getInfo($stock_product["product_id"]);

									$product_quantity = $product["product_stock_quantity"] + $stock_product["stock_bill_product_quantity"];

									// update quantity product
									$this->products_model->update(array("product_id"=>$product["product_id"],"product_stock_quantity"=>$product_quantity));
									// product_stock_history
									$product_stock_history["product_id"] = $product["product_id"];
									$product_stock_history["modified_user_id"] = $this->session->userdata('user_id');
									$product_stock_history["previous_product_stock_quantity"] = $product["product_stock_quantity"];
									$product_stock_history["new_product_stock_quantity"] = $product_quantity;
									$product_stock_history["reason"] = STOCK_CHANGE_REASON_ISSUE_CHANGE;
									$product_stock_history["linked_object_id"] = $this->input->post("stock_issue_id");
									$product_stock_history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
									$this->stock_bill_model->insert_stock_history($product_stock_history);
									//delete stock product when processed
									$this->stock_bill_model->delete_product($item);
								}
								else{
									$this->stock_bill_model->delete_product($item);
								}
							}
						}
						
					}
					success_message(__('update_stock_issue_successfully')); 
					redirect('admin/stockissues');
				}else{
					foreach($error_quantity as $error) {
						$this->form_validation->_error_array[] = $error;
					}
					$data["posts"] = $this->input->post();
					$this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
				}
			}
			else{
				foreach($error_quantity as $error) {
					$this->form_validation->_error_array[] = $error;
				}
				$data["posts"] = $this->input->post();
				$this->_getlayoutbackend('admin/stockissues/add',$data,"backend");
			}
		}
		$products = $this->stock_bill_model->getInfoProducts($id);

        foreach ($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['stock_bill_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity'] = $product['stock_bill_product_quantity'] % $product['package_unit'];
        }

		$data["stock_issue"]["products"] = $products;
		$this->_getlayoutbackend('admin/stockissues/edit',$data,"backend");
	}
	// view product in stock issue
	function view($id = 0)
	{

		if($_POST){
			$id = $this->input->post("stock_bill_id");
            $stock_bill = $this->stock_bill_model->getInfo($id);
			if ($stock_bill['stock_bill_status'] == STOCK_CHANGE_STATUS_PROCESSED) {
                error_message(__('Phieu xuat da duoc xu li'));
                redirect('admin/stockissues');
            }
            if($this->checkQuantity($id,"process_view")){
				$products = $this->stock_bill_model->getInfoProducts($id);
				if(count($products) > 0 && $products){
					$arrHistory = array();
					$this->stock_bill_model->update(array("stock_bill_id"=>$id,
														"stock_bill_status"=>STOCK_CHANGE_STATUS_PROCESSED,
														"stock_bill_created_date" => date("Y-m-d H:i:s", now())));
					foreach($products as $k => $item){
						$product = $this->products_model->getInfo($item["product_id"]);
						//array product_stock_history
						$pr_st_his = array();
						$pr_st_his["product_id"] = $item["product_id"];
						$pr_st_his["modified_user_id"] = $this->session->userdata('user_id');
						$pr_st_his["previous_product_stock_quantity"] = $product["product_stock_quantity"];
						$pr_st_his["new_product_stock_quantity"] = ($product["product_stock_quantity"] - $item["stock_bill_product_quantity"]);
						$pr_st_his["reason"] = STOCK_CHANGE_REASON_ISSUE;
						$pr_st_his["linked_object_id"] = $id;
						$pr_st_his["product_stock_history_date"] = date("Y-m-d H:i:s", now());
						$arrHistory[] = $pr_st_his;
						//update quantity product
						$this->products_model->update(array("product_id"=>$pr_st_his["product_id"],"product_stock_quantity"=>$pr_st_his["new_product_stock_quantity"]));
					}
					if(count($arrHistory) >0){
						$this->stock_bill_model->insert_histories($arrHistory);
					}
					success_message(__('update_stock_issue_successfully')); 
					redirect('admin/stockissues');
				}
			}
			else{
				$error_quantity =  __("Khong du hang trong kho de xuat");
				$this->form_validation->_error_array[] = $error_quantity;	
			}
		}
        $products = $this->stock_bill_model->getInfoProducts($id);

        foreach($products as $key => $product){
            $products[$key]['package_quantity'] = floor($product['stock_bill_product_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity']= $product['stock_bill_product_quantity'] % $product['package_unit'];
        }


        $data = array();
		$data["id"] = $id;
        $data["stock_issue"] = $this->stock_bill_model->getInfo($id);
		$data["products"] = $products;
		$data["stock_return_receipts"] = $this->stock_bill_model->getStockReturnReceiptsByStockIssue($id);
		$data["cash_receipts"] = $this->cashs_model->getReceiptsByStockIssue($id);
		$data["discounts"] = $this->cashs_model->getDiscountAdvancesByStockIssue($id);
		$data["debits"] = $this->cashs_model->getDebitAdvancesByStockIssue($id);
		$this->_getlayoutbackend('admin/stockissues/view',$data,"backend");
	}
	function history($id)
	{

		$data = array();
		$histories = $this->stock_bill_model->getHistory($id);
		if(count($histories >0)){
			foreach($histories as $k => $item){
				$histories[$k]["childs"] = $this->stock_bill_model->getProductHistory($item["stock_bill_history_id"]);
			}
		}
		// print_r($histories);
		$data["histories"] = $histories;
		$this->_getlayoutbackend('admin/stockissues/history',$data,"backend");
	}
	function archive($id) {
			
		$arr['stock_bill_archive'] = 1;
		
		$this->db->where('stock_bill_id', $id);
        $this->db->update('stock_bills', $arr);

		success_message(__('update_stock_issue_successfully')); 
		redirect('admin/stockissues');		
	}
	function unarchive($id) {
			
		$arr['stock_bill_archive'] = 0;
		
		$this->db->where('stock_bill_id', $id);
        $this->db->update('stock_bills', $arr);

		success_message(__('update_stock_issue_successfully')); 
		redirect('admin/stockissues');		
	}
	
	function archive_all() {
		$arr['stock_bill_archive'] = 1;
		echo(strtotime(date("Y-n-j 23:59:59", strtotime("last day of previous month")))).'xx';
		$this->db->where("UNIX_TIMESTAMP(stock_bill_created_date) < ". strtotime(date("Y-n-j 23:59:59", strtotime("last day of previous month"))));
        $this->db->update('stock_bills', $arr);

		success_message(__('Archived all old stock issues successfully')); 
		redirect('admin/stockissues');		
		
	}
	
	function delete($id)
	{
		$stock = $this->stock_bill_model->getInfo($id);
		$lock = false;
		if(isset($stock["stock_bill_status"])){
			if($stock["stock_bill_status"] == STOCK_CHANGE_STATUS_PROCESSED){
				$lock = true;
			}
		}
		check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method(),$lock);
		
		if($stock){
			if($stock["stock_bill_status"] == STOCK_CHANGE_STATUS_PROCESSED){
				$products = $this->stock_bill_model->getInfoProducts($id);
				if(count($products) >0){
					foreach($products as $k => $item){
						$product = $this->products_model->getInfo($item["product_id"]);
						$quantity = $product["product_stock_quantity"] + $item["stock_bill_product_quantity"];
						$this->products_model->update(array("product_id" => $product["product_id"],"product_stock_quantity" => $quantity));
						// product_stock_history
						$history["product_id"] = $item["product_id"];
						$history["modified_user_id"] = $this->session->userdata('user_id');
						$history["previous_product_stock_quantity"] = $product["product_stock_quantity"];
						$history["new_product_stock_quantity"] = $quantity;
						$history["reason"] = STOCK_CHANGE_REASON_ISSUE;
						// $history["linked_object_id"] = $this->input->post("stock_issue_id");
						$history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
						$this->stock_bill_model->insert_stock_history($history);
					}
				}
			}
		}
		$this->stock_bill_model->delete($id);
		$this->stock_bill_model->delete_products($id);
		$products_his = $this->stock_bill_model->getHistory($id);
		if(count($products_his) >0){
			foreach($products_his as $item){
				$this->stock_bill_model->delete_products_history($item["stock_bill_history_id"]);
			}
		}
		$this->stock_bill_model->delete_history($id);
		success_message(__('delete_stock_issue_successfully')); 
		redirect('admin/stockissues');
	}
	function checkQuantity($post = false,$type = "normal")
	{
		$flag = true;
		if($type == "normal"){
			if($this->input->post("stock_issue_status") == 1){
				$tmp = array();
				$unique = array();
				for($i= 0; $i < $this->input->post("total_item"); $i++){
					if($this->input->post("curr_stock_issue_status") == 1){
						$product_id = $this->input->post("product_id_".$i);
						if($this->input->post("stock_issue_product_id_".$i)){// exited product
							
							$stock_product = $this->stock_bill_model->getInfoProduct($this->input->post("stock_issue_product_id_".$i));
							if($stock_product["stock_bill_product_quantity"] != (float)str_replace(",","",$this->input->post("product_quantity_".$i))){
								
								$product_quantity = (float)str_replace(",","",$this->input->post("product_quantity_".$i)) - $stock_product["stock_bill_product_quantity"];

								if (!in_array($product_id, $tmp)) {
									$tmp[] = $product_id;
									$unique[$product_id]["id"] = $product_id;
									$unique[$product_id]["quantity"] = $product_quantity;
								}
								else{
									$unique[$product_id]["quantity"] += $product_quantity;
								}
							}

						}
						else{// new prdouct
							$product_quantity = (float)str_replace(",","",$this->input->post("product_quantity_".$i));
							if (!in_array($product_id, $tmp)) {

								$tmp[] = $product_id;
								$unique[$product_id]["id"] = $product_id;
								$unique[$product_id]["quantity"] = $product_quantity;
							}
							else{
								$unique[$product_id]["quantity"] += $product_quantity;
							}
						}
					}
					else{
						$product_id = $this->input->post("product_id_".$i);
						$product_quantity = (float)str_replace(",","",$this->input->post("product_quantity_".$i));
						if (!in_array($product_id, $tmp)) {
							$tmp[] = $product_id;
							$unique[$product_id]["id"] = $product_id;
							$unique[$product_id]["quantity"] = $product_quantity;
						}
						else{
							$unique[$product_id]["quantity"] += $product_quantity;
						}
					}
				}

				if(count($unique) > 0){
					
					foreach($unique as $k => $item){
						$product = $this->products_model->getInfo($item["id"]);
						if($item["quantity"] > $product["product_stock_quantity"]){
							$flag = false;
						}
					}
				}
				else{
					$flag = true;
				}
			}
			else{
				$flag = true;
			}
		}
		else if($type == "process_view"){
			$stock = $this->stock_bill_model->getInfo($post);
			$products = $this->stock_bill_model->getInfoProducts($post);
			
			if(count($products) >0 && $products){
				$tmp = array();
				$unique = array();
				for($i=0; $i< count($products); $i++){
					$product_id = $products[$i]["product_id"];
					$product_quantity = $products[$i]["stock_bill_product_quantity"];
					if (!in_array($product_id, $tmp)) {
						$tmp[] = $product_id;
						$unique[$product_id]["id"] = $product_id;
						$unique[$product_id]["quantity"] = $product_quantity;
					}
					else{
						$unique[$product_id]["quantity"] += $product_quantity;
					}
				}
				if(count($unique) > 0){
					
					foreach($unique as $k => $item){
						$product = $this->products_model->getInfo($item["id"]);
						if($item["quantity"] > $product["product_stock_quantity"]){
							$flag = false;
						}
					}
				}
				else{
					$flag = true;
				}
			}
			else {
				$flag = false;
			}
		}
		return $flag;
	}
}