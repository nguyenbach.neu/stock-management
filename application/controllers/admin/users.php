<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends  MY_Controller {
    function __construct()
    {
		parent::__construct();
    }
	public function index()
	{

		$data = array();
		$data["users"] = $this->users_model->getAll();
		$this->_getlayoutbackend('admin/users/index',$data);
	}
	function logout()
	{
		$data = $this->session->userdata;
        $this->session->unset_userdata($data);
		redirect('admin/users/login');
	}
	function login()
	{
		//
		if($_POST) {
			$this->form_validation->set_rules('username', 'username',
				'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'password',
                'trim|required|xss_clean');
				
			if($this->form_validation->run()) {
				$username = trim($this->input->post('username'));

                $password = sha1(trim($this->input->post('password')));
				$result = $this->users_model->getLoginAdmin($username, $password);

                if ($result){
                    $this->session->set_userdata($result);
                    success_message(__('login_successfully'));
                    redirect('admin/dashboard/');
                }
                $this->form_validation->_error_array[] = __('Email or Password is incorrect');

			}			
		}
		$this->_getlayoutbackend('admin/login',"","backend");
	}
	function password(){
        if ($_POST){
            $arr["user_id"] = $this->session->userdata['user_id'];
            $arr["user_password"] = sha1(trim($this->input->post("user_password")));
            if($this->users_model->update($arr)){
                success_message(__("user_edited"));
                redirect("admin/dashboard");
            }
        }
        $this->_getlayoutbackend('admin/users/password',"","backend");
    }
	function add()
	{

		if($_POST) {
			$this->form_validation->set_rules('user_name', 'user_name','trim|required|xss_clean');
            $this->form_validation->set_rules('user_email', 'user_email','trim|required|valid_email');
			$this->form_validation->set_rules('user_password', __('user_password'), 'trim|required|valid_password|matches[user_repassword]');
            $this->form_validation->set_rules('user_role', 'user_role','trim|required|xss_clean');
			if($this->form_validation->run()) {
				if($this->users_model->checkEmail(trim($this->input->post('user_email')))){
					$this->form_validation->_error_array[] = __('email_exist');
				}
				else{
					$arr = array();
					$arr["user_name"] = trim($this->input->post("user_name"));
					$arr["user_email"] = trim($this->input->post("user_email"));
					$arr["user_password"] = sha1(trim($this->input->post("user_password")));
					$arr["user_role"] = trim($this->input->post("user_role"));
					$arr["user_disabled"] = USER_ENABLE;
                    if(!empty($this->input->post('access'))){
                        $arr["access"] = $this->input->post('access');
                    }
					if($this->users_model->insert($arr)){
						success_message(__("USER_CREATED"));
						redirect("admin/users");
					}
				}
			}
		}
		$this->_getlayoutbackend('admin/users/add',"","backend");
	}
	function edit($id)
	{
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());
		if($_POST) {
			$this->form_validation->set_rules('user_name', 'user_name','trim|required|xss_clean');
			$this->form_validation->set_rules('user_password', __('user_password'), 'trim|valid_password|matches[user_repassword]');
            $this->form_validation->set_rules('user_role', 'user_role','trim|required|xss_clean');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["user_id"] = trim($this->input->post("user_id"));
				$arr["user_name"] = trim($this->input->post("user_name"));
				$arr["user_password"] = sha1(trim($this->input->post("user_password")));
				$arr["user_role"] = trim($this->input->post("user_role"));
				$arr["user_disabled"] = USER_ENABLE;
				if(!empty($arr['access'])){
				    $arr['access'] = $this->input->post("access");
                } else {
				    $arr['access'] = 0;
                }
				if($this->users_model->update($arr)){
					success_message(__("user_edited"));
					redirect("admin/users");
				}
			}
		}
		$data = array();
		$data["user"] = $this->users_model->getByID($id);
		$this->_getlayoutbackend('admin/users/edit',$data,"backend");
	}
	function delete($id)
	{
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());

        $error_message = array();
        //check discount amount
        $discount_advance = $this->cashs_model->sumDiscountAdvancesGroupBySaler($id);
        $discount_receipt = $this->cashs_model->sumDiscountReceiptsGroupBySaler($id);
        if ($discount_advance != $discount_receipt) {
            $error_message[] = __("<a href='reports/discounts'>Tiền khuyến mại chưa thanh toán xong</a>");
        }
        //check debit amount
        $debit_advance = $this->cashs_model->sumDebitAdvancesGroupBySaler($id);
        $debit_receipt = $this->cashs_model->sumDebitReceiptsGroupBySaler($id);
        if ($debit_advance != $debit_receipt) {
            $error_message[] = __("<a href='reports/debts'>Tiền nợ khách hàng chưa thanh toán xong</a>");
        }

        if(empty($error_message)){
            $this->users_model->delete($id);
            success_message(__("user_deleted"));
            redirect("admin/users");
        } else {
            $msg = "";
            foreach ($error_message as $message) {
                $msg .= $message.'</br>';
            }
            error_message($msg);
            redirect("admin/users");
        }
	}
	function access($id)
    {
        $arr["user_id"] = $id;
        $arr["access"] = 1;
        $this->users_model->update($arr);
        redirect("admin/users");
    }
    function lock($id)
    {
        $arr["user_id"] = $id;
        $arr["access"] = 0;
        $this->users_model->update($arr);
        redirect("admin/users");
    }
}
?>
