<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProductGroups extends  MY_Controller
{
    protected $columns = [
        "id",
        "group_name",
        "sort_order"
    ];
    function __construct()
    {
        parent::__construct();
        $this->load->model('product_groups_model');
    }

    public function index() {
        $data = array();
        $per_page = $this->config->item('number_per_page');
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config["base_url"] = site_url('admin/product_groups/index');
        $config["total_rows"] = $this->product_groups_model->countAll();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $data['paginator'] = $this->pagination->create_links();
        $productGroups = $this->product_groups_model->getAll($page,$per_page);
        $data['product_groups'] = $productGroups;
        $data['sum'] = $this->product_groups_model->countAll();
        $this->_getlayoutbackend('admin/productGroups/index',$data,"backend");
    }

    public function add() {
        if ($_POST) {
            foreach ($this->columns as $column) {
                if ($value =  $this->input->post($column)) {
                    $arr[$column] = trim($value);
                }
            }
            $groupId = $this->product_groups_model->insert($arr);

            if (!empty($groupId)) {
                success_message(__("Product group was created"));
                redirect("admin/product_groups");
            } else {
                success_message(__("Create product group failed"));
                redirect("admin/product_groups");
            }
        }
        $data = array();
        $this->_getlayoutbackend('admin/productGroups/add',$data,"backend");
    }

    public function edit($id) {
        if ($_POST) {

            foreach ($this->columns as $column) {
                if ($value =  $this->input->post($column)) {
                    $arr[$column] = trim($value);
                }
            }
            $groupId = $this->product_groups_model->update($arr);

            if (!empty($groupId)) {
                success_message(__("Product group was updated"));
                redirect("admin/product_groups");
            } else {
                error_message(__("Update product group failed"));
                $data = $this->input->post();
                $this->_getlayoutbackend('admin/productGroups/edit',$data,"backend");
            }
        }
        $data = array();
        $data['group'] = $this->product_groups_model->getInfo($id);
        $this->_getlayoutbackend('admin/productGroups/edit',$data,"backend");
    }
}