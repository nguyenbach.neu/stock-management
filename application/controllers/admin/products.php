<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends  MY_Controller 
{
	function __construct()
    {
      parent::__construct();
    }
	public function index() 
	{

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$config["base_url"] = site_url('admin/products/index');
        $config["total_rows"] = $this->products_model->countAll();
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$products = $this->products_model->getAll($page,$per_page);
		foreach ($products as $key => $product)
        {
            $products[$key]['package_quantity'] = floor($product['product_stock_quantity'] / $product['package_unit']);
            $products[$key]['unit_quantity'] = $product['product_stock_quantity'] % $product['package_unit'];
            $products[$key]['product_price'] = $product['product_price']*$product['package_unit'];
            $products[$key]['product_cost'] = $product['product_cost']*$product['package_unit'];
        }
        $data['products'] = $products;
        $data["sum"] = $this->products_model->sumAmount();
		$this->_getlayoutbackend('admin/products/index',$data,"backend");
	}
	function add()
	{

		if($_POST) {
			$this->form_validation->set_rules('product_name', __('Product Name'),'trim|required|xss_clean');
            $this->form_validation->set_rules('product_code', __('Product Code'),'trim|required');
			// $this->form_validation->set_rules('product_cost', __('product_cost'), 'trim|required');
            // $this->form_validation->set_rules('product_price', __('product_price'),'trim|required|xss_clean');
            // $this->form_validation->set_rules('product_quantity', __('product_quantity'),'trim|required|xss_clean');
            $this->form_validation->set_rules('supplier_id', __('product_supplier_id'),'trim|required|xss_clean');
			if($this->form_validation->run()) {
				if($this->products_model->checkCode(trim($this->input->post('product_code')))){
					$this->form_validation->_error_array[] = __('Product code exist!');
					$data["posts"] = $this->input->post();
					$this->_getlayoutbackend('admin/products/add',$data,"backend");
				}
				else{
				    $package_unit = trim($this->input->post("package_unit"));
                    //convert product cost
                    $product_cost = str_replace(",","",$this->input->post("product_cost"));
                    $product_cost = round(($product_cost/$package_unit),2);

                    //convert product price
                    $product_price = str_replace(",","",$this->input->post("product_price"));
                    $product_price = round(($product_price/$package_unit),2);

                    //convert product discount
                    $prduct_discount = str_replace(",","",$this->input->post("prduct_discount"));
                    $prduct_discount = round(($prduct_discount/$package_unit),2);

					$arr = array();
					$arr["product_name"] = trim($this->input->post("product_name"));
					$arr["product_code"] = trim($this->input->post("product_code"));
					$arr["product_cost"] = $product_cost;
					$arr["product_price"] = $product_price;
                    $arr["product_discount"] = $prduct_discount;
					$arr["product_stock_quantity"] = str_replace(",","",$this->input->post("product_quantity"));
                    $arr["package_unit"] = $package_unit;
					$arr["product_supplier_id"] = trim($this->input->post("supplier_id"));
					$arr["product_created_date"] = date("Y-m-d H:i:s", now());
					$arr["product_updated_date"] = date("Y-m-d H:i:s", now());
					$product_id = $this->products_model->insert($arr);
					$arr_product_history = array();
					$arr_product_history["product_id"] = $product_id;
					$arr_product_history["modified_user_id"] = $this->session->userdata('user_id');
					$arr_product_history["previous_product_stock_quantity"] = 0;
					$arr_product_history["new_product_stock_quantity"] = str_replace(",","",$this->input->post("product_quantity"));
					$arr_product_history["reason"] = INITIAL_STOCK;
					$arr_product_history["product_stock_history_date"] = date("Y-m-d H:i:s", now());
					$this->stock_bill_model->insert_st_pro_his($arr_product_history);

					success_message(__("PRODUCT_CREATED"));
					redirect("admin/products");
					
				}
			}
		}
		$data = array();
		$this->_getlayoutbackend('admin/products/add',$data,"backend");
	}
	function edit($id)
	{
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());
		if($_POST) {
			$this->form_validation->set_rules('product_name', __('Product Name'),'trim|required|xss_clean');
            $this->form_validation->set_rules('supplier_id', __('product_supplier_id'),'trim|required|xss_clean');
			if($this->form_validation->run()) {
				$arr = array();
				$arr["product_name"] = trim($this->input->post("product_name"));
				$arr["product_id"] = trim($this->input->post("product_id"));
				$arr["product_updated_date"] = date("Y-m-d H:i:s", now());
				if($this->products_model->update($arr)){
					success_message(__("product_edited"));
					redirect("admin/products");
				}
			}
		}
		$data = array();
		$data["product"] = $this->products_model->getInfo($id);
		$this->_getlayoutbackend('admin/products/edit',$data,"backend");
	}
	function delete($id)
	{
        check_permission($this->session->userdata('user_role'),$this->router->fetch_class(),$this->router->fetch_method());
        $product = $this->products_model->getInfo($id);
        if($product["product_stock_quantity"] != 0){
            error_message(__("The quantity of product is more than zezo"));
            redirect("admin/products");
        } else {
            $data["product_id"] = $id;
            $data["deleted"] = 1;
            $this->products_model->update($data);
            success_message(__("product_delete"));
            redirect("admin/products");
        }
	}
	function changeprice($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('product_cost', __('product_cost'),'trim|required|xss_clean');
            // $this->form_validation->set_rules('product_price', __('product_price'),'trim|required|xss_clean');
			if($this->form_validation->run()) {
			    $product = $this->products_model->getInfo($this->input->post("product_id"));
                //convert product cost
			    $product_cost = str_replace(",","",$this->input->post("product_cost"));
                $product_cost = round(($product_cost/$product['package_unit']),2);

                //convert product price
                $product_price = str_replace(",","",$this->input->post("product_price"));
                $product_price = round(($product_price/$product['package_unit']),2);

                //convert product discount
                $prduct_discount = str_replace(",","",$this->input->post("prduct_discount"));
                $prduct_discount = round(($prduct_discount/$product['package_unit']),2);

                if($product["product_cost"] != $product_cost || $product["product_price"] != $product_price
                    || $product["product_discount"] != $prduct_discount){
					$arr = array();
					$arr["product_cost"] = $product_cost;
					$arr["product_price"] = $product_price;
                    $arr["product_discount"] = $prduct_discount;
					$arr["product_id"] = trim($this->input->post("product_id"));
					$arr["product_updated_date"] = date("Y-m-d H:i:s", now());
					$this->products_model->update($arr);
					$arrHis = array();
					$arrHis["product_id"] = $product["product_id"];
					$arrHis["previous_product_cost"] = $product["product_cost"];
					$arrHis["previous_product_price"] = $product["product_price"];
                    $arrHis["previous_product_discount"] = $product["product_discount"];
					$arrHis["new_product_cost"] = $product_cost;
					$arrHis["new_product_price"] = $product_price;
					$arrHis["new_product_discount"] = $prduct_discount;
					$arrHis["modified_user_id"] = $this->session->userdata('user_id');
					$arrHis["product_price_history_date"] = date("Y-m-d H:i:s", now());
					$this->products_model->insert_history($arrHis);
				}
				success_message(__("product_change_price_successfully"));
				redirect("admin/products");
			}	
		}
		$data = array();
		$data["product"] = $this->products_model->getInfo($id);
		$data["product"]['product_price'] = $data["product"]['product_price'] * $data["product"]["package_unit"];
        $data["product"]['product_cost'] = $data["product"]['product_cost'] * $data["product"]["package_unit"];
		$this->_getlayoutbackend('admin/products/changeprice',$data,"backend");
	}
	function changequantity($id)
	{

		if($_POST) {
			$this->form_validation->set_rules('product_id', __('Product ID'),'trim|required|xss_clean');
			if($this->form_validation->run()) {
				$product = $this->products_model->getInfo($this->input->post("product_id"));
				if($product["product_cost"] != str_replace(",","",$this->input->post("product_quantity"))){
					$arr = array();
					$arr["product_stock_quantity"] = str_replace(",","",$this->input->post("product_quantity"));
					$arr["product_id"] = trim($this->input->post("product_id"));
					$arr["product_updated_date"] = date("Y-m-d H:i:s", now());
					$this->products_model->update($arr);
					$arrHis = array();
					$arrHis["product_id"] = $product["product_id"];
					$arrHis["previous_product_stock_quantity"] = $product["product_stock_quantity"];
					$arrHis["new_product_stock_quantity"] = str_replace(",","",$this->input->post("product_quantity"));
					$arrHis["modified_user_id"] = $this->session->userdata('user_id');
					$arrHis["reason"] = STOCK_CHANGE_REASON_ADJUSTMENT;
					$arrHis["product_stock_history_date"] = date("Y-m-d H:i:s", now());
					$this->stock_bill_model->insert_st_pro_his($arrHis);
				}
				success_message(__("product_change_quantity_successfully"));
				redirect("admin/products");
			}	
		}
		$data = array();
		$data["product"] = $this->products_model->getInfo($id);
        $data["product"]['product_price'] = $data["product"]['product_price'] * $data["product"]["package_unit"];
        $data["product"]['product_cost'] = $data["product"]['product_cost'] * $data["product"]["package_unit"];
		$this->_getlayoutbackend('admin/products/changequantity',$data,"backend");
	}
	function history($id)
	{

		$data = array();
		$per_page = $this->config->item('number_per_page');
		$page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
		$config["base_url"] = site_url('admin/products/history/'.$id);
        $config["total_rows"] = $this->products_model->count_quantity($id);
        $config["per_page"] = $per_page;
        // $config["uri_segment"] = 5;
        $config["page_query_string"] = TRUE;
		$this->pagination->initialize($config);
		$data['paginator'] = $this->pagination->create_links();
		$data["product"] = $this->products_model->getInfo($id);
		$data["his_prices"] = $this->products_model->his_price($id);
		$his_quantity = $this->products_model->his_quantity($id,$page,$per_page);
		foreach ($his_quantity as $key => $product)
        {
            $his_quantity[$key]['new_product_quantity'] = (number_format(floor($product['new_product_stock_quantity'] / $data['product']['package_unit']))).'t ('.$product['new_product_stock_quantity'] % $data['product']['package_unit'].'h)';
            $his_quantity[$key]['previous_product_quantity'] = (number_format(floor($product['previous_product_stock_quantity'] / $data['product']['package_unit']))).'t ('.$product['previous_product_stock_quantity'] % $data['product']['package_unit'].'h)';
        }
        $data['his_quantity'] = $his_quantity;
		$this->_getlayoutbackend('admin/products/history',$data,"backend");
	}

	public function changegroup($id) {
	    if ($_POST) {
	        $productId = $this->input->post("product_id");
	        if (!empty($productId)) {
                $arr = array();
                $arr['product_group'] = $this->input->post("product_group");
                $arr["product_id"] = $productId;
                $arr["product_updated_date"] = date("Y-m-d H:i:s", now());
                $this->products_model->update($arr);
                success_message(__("Update product group successful"));
                redirect("admin/products");
            }
        }
        $data = array();
        $data["product"] = $this->products_model->getInfo($id);
        $this->_getlayoutbackend('admin/products/changeproductgroup',$data,"backend");
    }

	function ajaxInfoProduct()
	{
		$id = $this->input->post("product_id");
		$product = $this->products_model->getInfo($id);
		echo json_encode($product);die;
	}
}