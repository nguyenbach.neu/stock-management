<?php
class PermissionHook {
    function postCheck() {
        $CI =& get_instance();
        if (($CI->router->fetch_class() == 'users' && $CI->router->fetch_method() == 'login')
            || ($CI->router->fetch_class() == 'users' && $CI->router->fetch_method() == 'logout')
            || ($CI->router->fetch_class() == 'dashboard' && $CI->router->fetch_method() == 'index')) {
            return true;
        }
        check_permission($CI->session->userdata('user_role'),$CI->router->fetch_class(),$CI->router->fetch_method(), false);
    }
}