ALTER TABLE `cashs` DROP `type`;
ALTER TABLE `cashs` ADD `account_id` INT NOT NULL , ADD `type` ENUM('expense','salary', 'cost', 'discount', 'advance', 'receipt', 'load', 'withdraw','tranfer') NOT NULL ;
UPDATE `cashs` SET `type`='receipt' WHERE 1;
UPDATE `cashs` SET `cash_follow`='1' WHERE 1;
ALTER TABLE `stock_bills` ADD `stock_bill_archive` BIT(1) NOT NULL DEFAULT b'0' ;
ALTER TABLE `cashs` ADD `saler_id` INT(11) NOT NULL ;
ALTER TABLE `stock_bills` ADD `stock_bill_supplier_discount` INT(11) NOT NULL ;
ALTER TABLE `stock_bills` ADD `stock_bill_account_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `cashs` CHANGE `cash_object_id` `stock_bill_id` INT(11) NULL DEFAULT '0';
ALTER TABLE `cashs` ADD `supplier_id` INT NOT NULL DEFAULT '0' AFTER `saler_id`;
ALTER TABLE `cashs` CHANGE `saler_id` `saler_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `cashs` CHANGE `account_id` `account_id` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `supplies` ADD `supplier_discount` INT(3) NOT NULL DEFAULT '0' ;
UPDATE `cashs` SET `stock_bill_id`=0  WHERE 1

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(256) NOT NULL,
  `account_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `expenses`;
CREATE TABLE IF NOT EXISTS `expenses` (
  `expense_id` int(11) NOT NULL,
  `expense_name` enum('salary','cost','discount','other') NOT NULL,
  `expense_amount` decimal(20,2) NOT NULL,
  `expense_date` datetime NOT NULL,
  `expense_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `stock_bills` CHANGE `stock_bill_supplier_discount` `stock_bill_supplier_discount` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `accounts`
  DROP PRIMARY KEY,
   ADD PRIMARY KEY(
     `account_id`);
ALTER TABLE `accounts` CHANGE `account_id` `account_id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `accounts` ADD `deleted` BOOLEAN NOT NULL DEFAULT FALSE ;
ALTER TABLE `cashs` ADD `deleted` BOOLEAN NOT NULL DEFAULT FALSE ;
ALTER TABLE `supplies` ADD `deleted` BOOLEAN NOT NULL DEFAULT FALSE ;
ALTER TABLE `users` ADD `deleted` BOOLEAN NOT NULL DEFAULT FALSE ;

UPDATE `stock_bills` SET `stock_bill_type`=2 WHERE `stock_bill_type`=0 and `stock_bill_flow`=1;

ALTER TABLE `accounts` ADD `account_cash` BOOLEAN NOT NULL DEFAULT FALSE AFTER `account_note`;

ALTER TABLE `cashs` CHANGE `type` `type` ENUM('expense','salary','cost','discount','advance','receipt','load','withdraw','tranfer','commission') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

- Phieu thu tien
+ Phieu xuat		object_id
+ So tien			cash_amount
+ Tai khoan			account_id
+ Note				cash_note
cash_flow = 1
object_name = stock_bills
type = receipt

- Chi phi giao hang
+ Phieu xuat
+ So tien
+ Note
cash_flow = 0
object_name = stock_bills
type = expense

- Chi phi hoat dong
+ So tien
+ Tai khoan
+ Note
cash_flow = -1
object_name = null
type = expense

- Tien khuyen mai
+ Phieu xuat
+ So tien
+ Nhan vien ban hang
+ Note
cash_flow = 0
object_name = stock_bills
type = discount


Nhap lai kho
+ Phieu xuat
+ Note

Phieu nhap
+ Nha cung cap
+ Chiet khau
+ Note

- Cash record (type advance)

Theo doi tien khuyen mai
- Tien khuyen mai tren phieu xuat
- 

Expense
- Products cost (cost)
- Tien giam gia (discount)
- Luong (salary)
- Cac chi phi khac (an trua, xang,....) (expense)

Loi nhuan tam tinh
- Tien quang cao (cash)
- Doanh thu (tren phieu xuat) - expense - cost (dua tren phieu xuat) - salary

Cong no nhan vien
- Tien phai thu tren phieu xuat
- Tien da nhan theo phieu xuat
- Chi phi giao hang theo phieu xuat

Bang luong nhan vien
- Luong va tien tam ung luong

Tra luong
- Cash record (type salary)

Nhan tien khuyen mai
- Cash record (type advance)

Tam ung tien luong
- Cash record (type advance)



Nhap hang
- Cash record (type cost)

Chi budget giam gia
- Cash record (discount)

Thu tien giao hang
- Cash record (type receipt)