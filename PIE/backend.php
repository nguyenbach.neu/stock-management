<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Apple iOS and Android stuff (do not remove) -->
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />

<!--login-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/core/login.css" media="screen" />
<!-- Login Script -->

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/fonts/ptsans/stylesheet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/fluid.css" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/mws.style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/icons/16x16.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/icons/24x24.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/icons/32x32.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url()?>skin/backend/css/jquery.fancybox.css" type="text/css"/>
<!-- Demo and Plugin Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/demo.css" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/colorpicker/colorpicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/imgareaselect/css/imgareaselect-default.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/fullcalendar/fullcalendar.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/fullcalendar/fullcalendar.print.css" media="print" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/chosen/chosen.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/prettyphoto/css/prettyPhoto.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/tipsy/tipsy.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/sourcerer/Sourcerer-1.2.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/jgrowl/jquery.jgrowl.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/plugins/spinner/ui.spinner.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/jui/css/jquery.ui.all.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/jui/css/jquery.ui.dialog.css" media="screen" />
<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>skin/backend/css/mws.theme.css" media="screen" />

<!-- JavaScript Plugins -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/jquery.fileinput.js"></script>


<!-- jQuery-UI Dependent Scripts -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/jui/js/jquery-ui-1.8.20.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/jui/js/jquery.ui.timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/jui/js/jquery.ui.touch-punch.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/spinner/ui.spinner.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/jui/js/jquery-ui-effects.min.js"></script>


<!-- Plugin Scripts -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/imgareaselect/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/duallistbox/jquery.dualListBox-1.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/jgrowl/jquery.jgrowl-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/prettyphoto/js/jquery.prettyPhoto.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/validate/jquery.validate-min.js"></script>

<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/flot/excanvas.min.js"></script>
<![endif]-->

<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/flot/jquery.flot.stack.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/colorpicker/colorpicker-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/tipsy/jquery.tipsy-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/plugins/sourcerer/Sourcerer-1.2-min.js"></script>

<!-- Core Script -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/core/mws.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/core/mws.wizard.js"></script>

<!-- Themer Script (Remove if not needed) -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/core/themer.js"></script>

<!-- Demo Scripts (remove if not needed) -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/demo/demo.js"></script>

<!-- Login Script -->
<script type="text/javascript" src="<?php echo base_url();?>skin/backend/js/core/login.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/tiny_mce/tiny_mce.js"></script>
<title>Trusties</title>
<script type="text/javascript">
	function displayConfirm(txt) {
		return confirm(txt);
	}
	$(document).ready(function() {
		$('.click-profile').fancybox();
		$('.click-change-pass').fancybox();
	})
</script>
</head>

<body>

<?php if($this->router->fetch_method() !='login'):?>
    <?php echo $template['partials']['header']; ?>
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        <?php echo $template['partials']['sidebar']; ?>
       <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
			<?php if($this->session->flashdata('success_message')) {
			echo '<div class="msg">';
				echo '<div>'.$this->session->flashdata('success_message').'</div>';
			echo '</div>';
		}?>          
		<?php if($this->session->flashdata('error_message')) {
			echo '<div class="err">';
			echo '<div>'.$this->session->flashdata('error_message').'</div>';
			echo '</div>';
		}?>	
			<?php echo $template['body']; ?>
			<?php echo $template['partials']['footer']; ?>
		</div>
     </div>
<?php else:?>
	<?php echo $template['body']; ?>
<?php endif;?>
<?php echo $template['partials']['profile']; ?>
<?php echo $template['partials']['change_password']; ?>
</body>
</html>